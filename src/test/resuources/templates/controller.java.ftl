package ${package.Controller};

import org.springframework.web.bind.annotation.*;
import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@Api(tags = {"${table.comment!}"})
@RestController
<#else>
@Slf4j
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??>:${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>public class ${table.controllerName} extends ${superControllerClass}{
<#else>public class ${table.controllerName} {
</#if>

    @Resource
    private ${table.serviceName} ${(table.serviceName?substring(1))?uncap_first};


    @ApiOperation(value = "新增${table.comment!}")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute ${entity} ${entity?uncap_first}){
        Integer count = ${(table.serviceName?substring(1))?uncap_first}.add(${entity?uncap_first});
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除${table.comment!}")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = ${(table.serviceName?substring(1))?uncap_first}.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新${table.comment!}")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute ${entity} ${entity?uncap_first}){
        Integer count = ${(table.serviceName?substring(1))?uncap_first}.updateData(${entity?uncap_first});
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询${table.comment!}分页数据")
    @GetMapping()
    public Result<IPage<${entity}>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute ${entity} ${entity?uncap_first}){
            IPage<${entity}> page = ${(table.serviceName?substring(1))?uncap_first}.findListByPage(pageVo, ${entity?uncap_first});
         return new ResultUtil<IPage<${entity}>>().setData(page);
    }

    @ApiOperation(value = "id查询${table.comment!}")
    @GetMapping("{id}")
    public Result<${entity}> findById(@PathVariable Long id){
        ${entity} ${entity?uncap_first}=  ${(table.serviceName?substring(1))?uncap_first}.findById(id);
        return new ResultUtil<${entity}>().setData(${entity?uncap_first});
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<${entity}>> findById(@ModelAttribute ${entity} ${entity?uncap_first}){
        List<${entity}> ${entity?uncap_first}List =  ${(table.serviceName?substring(1))?uncap_first}.findListAll(${entity?uncap_first});
        return new ResultUtil<List<${entity}>>().setData(${entity?uncap_first}List);
    }

}
</#if>