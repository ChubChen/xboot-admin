package ${package.Mapper};

import ${package.Mapper}.${table.mapperName};
import ${package.Entity}.${entity};
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    * ${table.comment!} 服务类
    * </p>
*
* @author ${author}
* @since ${date}
*/
public interface ${table.mapperName} extends BaseMapper<${entity}> {

    /**
     * 根据条件分页查询
     * @param page
     * @param ${entity?uncap_first}
     * @return IPage<${entity}>
    */
    IPage<${entity}> selectListByPage(IPage<${entity}> page, @Param("${entity?uncap_first}") ${entity} ${entity?uncap_first});

    /**
     * 根据条件查询全部
     * @param ${entity?uncap_first}
     * @return List<${entity}>
    */
    List<${entity}> selectAll(@Param("${entity?uncap_first}") ${entity} ${entity?uncap_first});

}
