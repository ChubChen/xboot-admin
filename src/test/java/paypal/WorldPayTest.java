package paypal;

import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.util.JAXBUtil;
import com.chubchen.admin.modules.easesea.worldpay.*;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.client.SystemDefaultHttpClient;
import org.apache.http.ssl.SSLContexts;
import org.junit.Test;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.Source;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import javax.xml.validation.ValidatorHandler;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


public class WorldPayTest {



    public static void main(String[] args) throws Exception {
//        WorldpayRestClient restClientient = new WorldpayRestClient("123123");
        try{
//            System.out.println(restClient.getVersion());
//            restClient.getOrderService().refund( "123123", 10);
//            CredentialsProvider credsProvider = new BasicCredentialsProvider();
//            Sale sale = new Sale();
//            sale.setReportGroup("Planets");
//            sale.setOrderId("12344");
//            sale.setAmount(1000L);
//            sale.setOrderSource(OrderSourceType.ECOMMERCE);
//            CardType card = new CardType();
//            card.setType(MethodOfPaymentTypeEnum.VI);
//            card.setNumber("4100000000000002");
//            card.setExpDate("1210");
//            sale.setCard(card);
//
//            // Peform the transaction on the Vantiv eCommerce Platform
//            SaleResponse response = new CnpOnline().sale(sale);
//            CnpOnlineRequest request = new CnpOnlineRequest();
//
//            Authentication authentication = new Authentication();
//            authentication.setUser("chenpeng");
//            authentication.setPassword("123");
//            request.setAuthentication(authentication);
            PaymentService paymentService = new PaymentService();
            paymentService.setMerchantCode("123");
            paymentService.setVersion("1.1");
            Modify modify= new Modify();
            WorldPayOrder order = new WorldPayOrder();
            order.setOrderCode("123123123");
            modify.setOrder(order);
            paymentService.setOp(modify);
            JAXBContext context = JAXBContext.newInstance(paymentService.getClass());
            Marshaller marshaller = context.createMarshaller();
            marshaller.setProperty( "com.sun.xml.bind.xmlHeaders", "<!DOCTYPE paymentService PUBLIC \"-//WorldPay//DTD WorldPay PaymentService v1//EN\"\n" +
                    "\"http://dtd.worldpay.com/paymentService_v1.dtd\">");
            StringWriter sw = new StringWriter();
            marshaller.marshal(paymentService,sw);
            String xmlRequest = sw.toString();
            System.out.println(xmlRequest);
//

            JAXBContext jc = JAXBContext.newInstance(paymentService.getClass());
            Unmarshaller u = jc.createUnmarshaller();
            System.out.println(u.unmarshal(IOUtils.toInputStream("<paymentService version=\"1.4\" merchantCode=\"STARLINKUBUSD\"><reply><error code=\"2\"><![CDATA[Premature end of file.]]></error><ok>\n" +
                    "      <captureReceived orderCode=\"ExampleOrder1\"> <!--The orderCode you supplied in the modification -->\n" +
                    "        <amount value=\"10965\" currencyCode=\"EUR\" exponent=\"2\" debitCreditIndicator=\"credit\"/>\n" +
                    "      </captureReceived>\n" +
                    "    </ok></reply></paymentService>")));

//            JAXBContext j2c = JAXBContext.newInstance(paymentService.getClass());
//            Unmarshaller u2 = j2c.createUnmarshaller();
//            SAXParserFactory spf = SAXParserFactory.newInstance();
//            spf.setValidating(false);
//            XMLReader xmlReader = spf.newSAXParser().getXMLReader();
//            InputSource inputSource = new InputSource(IOUtils.toInputStream(
//                    "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
//                            "<!DOCTYPE paymentService PUBLIC \"-//WorldPay//DTD WorldPay PaymentService v1//EN\"\n" +
//                            "                                \"http://dtd.worldpay.com/paymentService_v1.dtd\">\n" +
//                            "<paymentService version=\"1.4\" merchantCode=\"STARLINKUBUSD\"><reply><error code=\"2\"><![CDATA[Document root element \"paymentService\", must match DOCTYPE root \"null\".]]></error></reply></paymentService>"));
//            SAXSource source = new SAXSource(xmlReader, inputSource);
//            System.out.println(System.currentTimeMillis());
//            System.out.println(u2.unmarshal(source));
//            System.out.println(System.currentTimeMillis());



            String str = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<!DOCTYPE paymentService PUBLIC \"-//WorldPay//DTD WorldPay PaymentService v1//EN\"\n" +
                    "                                \"http://dtd.worldpay.com/paymentService_v1.dtd\">\n" +
                    "<paymentService version=\"1.4\" merchantCode=\"STARLINKUBUSD\"><reply><error code=\"2\"><![CDATA[Document root element \"paymentService\", must match DOCTYPE root \"null\".]]></error></reply></paymentService>";
            if(str.indexOf("<paymentService") > 0){
                str = "<paymentService" + str.split("<paymentService")[1];
            }
            System.out.println(str);
            JAXBContext j3c = JAXBContext.newInstance(paymentService.getClass());
            Unmarshaller u4 = j3c.createUnmarshaller();

            u4.unmarshal(IOUtils.toInputStream(str));
        }catch (Exception e){
            System.err.println(e);
//            System.out.println("Error code: " + e.getApiError().getCustomCode());
//            System.out.println("Error description: " + e.getApiError().getDescription());
//            System.out.println("Error message: " + e.getApiError().getMessage());
        }
    }


    private CloseableHttpClient getSelfSignedClient() throws Exception {
        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                SSLContexts.custom().loadTrustMaterial(null, new TrustSelfSignedStrategy()).build(),
                NoopHostnameVerifier.INSTANCE);
        RequestConfig requestConfig = RequestConfig.custom()
                .setSocketTimeout(10000)
                .setConnectTimeout(10000)
                .setConnectionRequestTimeout(10000)
                .build();
        return HttpClients.custom().setDefaultRequestConfig(requestConfig).setSSLSocketFactory(sslsf).build();
    }

    @Test
    public void testWordPayQuery() throws Exception{
        HttpClient httpClient = new HttpClient();
        httpClient.setCloseableHttpClient(getSelfSignedClient());
        String auth = httpClient.getAuthHeader("STARLINKCCUSD", "live2018");
        String url = "https://secure.worldpay.com/jsp/merchant/xml/paymentService.jsp";
        PaymentService paymentService = buildRequestOrder();
        HttpClient.HttpResponse httpResponse= httpClient.doAuthHeaderAndXML(url, paymentService, auth);
        System.out.println(httpResponse.getBody());
    }

    private PaymentService buildRequestOrder(){
        OrderInquiry orderInquiry = new OrderInquiry();
        orderInquiry.setOrderCode("c13381877071962.1");

        Inquiry inquiry = new Inquiry();
        inquiry.setOrderInquiry(orderInquiry);

        PaymentService paymentService = new PaymentService();
        paymentService.setVersion("1.4");
        paymentService.setInquiry(inquiry);
        paymentService.setMerchantCode("STARLINKCCUSD");
        return paymentService;
    }

    @Test
    public void testAdyen() throws Exception{
        final  String adyenUrl = "https://pal-live.adyen.com/pal/servlet/Payment/v64/refund";
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("originalReference", "1735954369112015");
        jsonObject.put("merchantAccount", "Paullentini");
        JSONObject amount = new JSONObject();
        amount.put("value", new BigDecimal(34.5).multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_DOWN).toString());
        amount.put("currency", "CAD");
        jsonObject.put("modificationAmount", amount);
        HttpClient httpClient = new HttpClient();
        httpClient.setCloseableHttpClient(getSelfSignedClient());
        Map<String, String> map = new HashMap<>();
        map.put("X-API-Key","AQEqhmfxLI7PbxNAw0m/n3Q5qf3Ve59MH59DVm6Z4izx1/flJDThszlu/+HSEMFdWw2+5HzctViMSCJMYAc=-t4whJxVp5v8ltTb8ybMEFE/eBuStL5KE0V0NL6BMkBo=-P;U6F7tS%_8+)9Mn");
        HttpClient.HttpResponse response = httpClient.doPost(adyenUrl, jsonObject, map);
        System.out.println(response.getBody());
        System.out.println(response.getCode());
    }
}
