package paypal;

import com.chubchen.admin.XbootApplication;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.modules.paypal.Tracker;
import com.chubchen.admin.modules.paypal.Trackers;
import com.paypal.api.payments.RefundRequest;
import com.paypal.base.rest.*;
import com.paypal.core.PayPalHttpClient;
import com.paypal.orders.OrdersCreateRequest;
import com.paypal.payments.CapturesRefundRequest;
import com.paypal.payments.RefundsGetRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = XbootApplication.class)
@Slf4j
public class PaypalServerTest {

    @Autowired
    private HttpClient client;

    public static final String clientID = "ATDVQkKF4tRW9gaLacjO0I5A7FOZcRDrwMCQyt48yFepv9EzUs44sE_XRrr1ws_G7i_gS5SHZEBd1mkv";
    public static final String clientSecret = "ECZaN4er3uhH3vOna9ICK0HSb60qIRoXufekwNCleU78fWSsADfxrN5LzIKPK-bkKdyh2moB5A4L3M7j";
    public static void main(String[] args) throws Exception {
        APIContext context = new APIContext(clientID, clientSecret, "sandbox");
        String pattern = "v1/shipping/trackers-batch";
        String resourcePath = RESTUtil.formatURIPath(pattern, new HashMap<>());
        List<Tracker> trackers = new ArrayList<>();
        Tracker tracker = new Tracker();
        tracker.setTransactionId("222123");
        tracker.setTrackingNumber("LDYGL1010686538YQ");
        tracker.setStatus(Tracker.TrackerStatusEnum.SHIPPED);
        tracker.setCarrierNameOther("测试");
        trackers.add(tracker);
        Trackers trackers1 = new Trackers();
        trackers1.setTrackers(trackers);
        String payLoad = JSONFormatter.toJSON(trackers1);
        System.out.println(payLoad);
        Trackers res = PayPalResource.configureAndExecute(context, HttpMethod.POST, resourcePath, payLoad, Trackers.class);
        System.out.println(res.getTrackerIdentifiers());
    }

    public static void test(){
//        PayPalHttpClient client = new PayPalHttpClient(environment);
        RefundRequest refundRequest = new RefundRequest();
        OrdersCreateRequest request = new OrdersCreateRequest();
        CapturesRefundRequest refundRequest1 = new CapturesRefundRequest("");

    }


    @Test
    public void tes111t(){
        String authHeader= client.getAuthHeader("","");
//        client.doAuthHeaderAndXML()
    }

}
