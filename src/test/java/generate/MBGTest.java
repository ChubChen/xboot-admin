package generate;


import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableField;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.ArrayList;
import java.util.List;

/**
 * 自动生成代码
 * @author chubchen
 */
public class MBGTest {

    public static void main(String[] args) throws Exception{

        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        String projectPath = System.getProperty("user.dir");

        // 全局配置
        GlobalConfig config = new GlobalConfig();
        config.setOutputDir(projectPath+ "/src/main/java");
        config.setAuthor("chubchen");
        config.setOpen(false);
        config.setBaseColumnList(true);
        config.setBaseResultMap(true);
        config.setActiveRecord(true);
        config.setFileOverride(true);
        mpg.setGlobalConfig(config);

        // 数据源配置

        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:3306/easesea?useUnicode=true&useSSL=false&characterEncoding=utf8");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("intercart");
        dsc.setPassword("0okmnhy!QWE");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName("mybatis");
        pc.setParent("com.chubchen.admin");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        // 如果模板引擎是 freemarker
        String templatePath = "/templates/mapper.xml.ftl";
        // 如果模板引擎是 velocity
        // String templatePath = "/templates/mapper.xml.vm";

        // 自定义输出配置
        List<FileOutConfig> focList = new ArrayList<>();
        // 自定义配置会被优先输出
        focList.add(new FileOutConfig(templatePath) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名 ， 如果你 Entity 设置了前后缀、此处注意 xml 的名称会跟着发生变化！！
                return projectPath + "/src/main/resources/mapper/" + pc.getModuleName()
                        + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
            }
        });
        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);
//
//        TemplateConfig templateConfig = new TemplateConfig();
//
//        templateConfig.setXml(null);
//        mpg.setTemplate(templateConfig);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setEntityTableFieldAnnotationEnable(true);
        strategy.setTablePrefix("t_");
//        strategy.setTablePrefix("");
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setSuperEntityClass("com.chubchen.admin.base.XbootBaseEntity");
//        strategy.setSuperEntityClass("com.chubchen.admin.base.XbootBaseEntity");
        //使用lombok
        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);
        // 公共父类
        strategy.setSuperControllerClass("com.chubchen.admin.base.XbootBaseController");
//        strategy.setSuperControllerClass("com.chubchen.admin.base.XbootBaseController");
        // 写于父类中的公共字段
        strategy.setSuperEntityColumns("id");
//        strategy.setSuperEntityColumns("id");
        //生成的表名称 逗号分隔
        strategy.setInclude("");
        strategy.setInclude("shopify_checkouts");
        //同上排除表，两者存在一个即可
        //strategy.setExclude("");
        strategy.setControllerMappingHyphenStyle(true);
//      strategy.setTablePrefix(pc.getModuleName() + "_");
        mpg.setStrategy(strategy);

        mpg.setTemplateEngine(new FreemarkerTemplateEngine());

        mpg.execute();
    }
}

