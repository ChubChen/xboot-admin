package mapper;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;

import com.chubchen.admin.XbootApplication;

import com.chubchen.admin.modules.base.entity.Department;
import com.chubchen.admin.modules.base.service.DepartmentService;
import com.chubchen.admin.modules.easesea.service.IPlatformProductsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = XbootApplication.class)
public class MapperTest {



    @Autowired
    private DepartmentService departmentService;


    @Autowired
    private IPlatformProductsService platformProductsService;

    @Test
    public void testDepartment(){
        List<Department> list = departmentService.list(new QueryWrapper<>());
        System.out.println(list.get(0).getId());
        System.out.println(list.get(0).getCreateBy());
        System.out.println(list.get(0).getCreateTime());
        System.out.println(list.get(0).getUpdateTime());
    }

    public static void main(String[] args) {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("test","2020-09-01T00:01:33");
        LocalDateTime test = jsonObject.getObject("test",LocalDateTime.class);
        System.out.println(test);
    }

}
