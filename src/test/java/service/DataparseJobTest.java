package service;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.XbootApplication;
import com.chubchen.admin.modules.easesea.constants.AdAccountStatusEnum;
import com.chubchen.admin.modules.easesea.constants.DataParseConstants;
import com.chubchen.admin.modules.easesea.constants.MongodbCollection;
import com.chubchen.admin.modules.easesea.constants.PlatformConstants;
import com.chubchen.admin.modules.easesea.dataparse.DataParaseDisparcher;
import com.chubchen.admin.modules.easesea.dataparse.ShopifyDataParseHandle;
import com.chubchen.admin.modules.easesea.entity.AdAccount;
import com.chubchen.admin.modules.easesea.service.IAdAccountService;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = XbootApplication.class)
@Slf4j
public class DataparseJobTest {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    @Qualifier("chooseMongoTemplate")
    private MongoTemplate mongoTemplate21;

    @Autowired
    private DataParaseDisparcher dataParaseDisparcher;

    @Autowired
    private ShopifyDataParseHandle shopifyDataParseHandle;

    @Autowired
    private IAdAccountService adAccountService;

    @Test
    public void testJob() throws Exception{
        log.info("定时任务job 执行清洗mongodb数据到数据库中");
        //step1 扫描mongodb 数据库需要解析的数据

//        Query query = new Query(Criteria.where("status").is(DataParseConstants.PARSE_STATUS_FINISH)
//                .and("platform").is("shopify").and("type").is("orders").and("createDate").gt(DateTime.now().minusHours(1).toDate())).limit(10);
//        List<JSONObject> list = mongoTemplate.find(query, JSONObject.class, MongodbCollection.DATA_PARSE_QUEUE);
        List<JSONObject> list = new ArrayList<>();
        JSONObject test = new JSONObject();
        test.put("platform","shopify");
        test.put("type","orders");
        test.put("startDate", DateTime.now().minusDays(1).toDate());
        test.put("endDate",new Date());
        list.add(test);
        if(list != null && list.size() > 0){
            //遍历 并处理结果
            list.stream().forEach(jsonObject -> dataParaseDisparcher.dispatch(jsonObject.getString("platform"), jsonObject));
        }
    }

    @Test
    public void testOne() throws Exception{
        JSONObject jsonObject = mongoTemplate.findOne(new Query().limit(1), JSONObject.class, "orders");
        JSONObject jsonObject1 = mongoTemplate21.findOne(new Query().limit(1), JSONObject.class, "image_collector");
        System.out.println(shopifyDataParseHandle.buildOrder(jsonObject));
        System.out.println(jsonObject1);
    }

    @Test
    public void testTikTok() throws Exception{
        AdAccount adAccount = new AdAccount();
        adAccount.setAccountId("6845987051553161222");
        adAccount.setAdPlatform(PlatformConstants.AD_TIKTOK);
        adAccountService.syncPlatformData(adAccount);
    }

    @Test
    public void getImages() throws Exception{

        Query query = new Query(Criteria.where("image_id").in("catalog/product/2020-03-10/94201664-E771-4CD4-8161-1D887C54D890-1583807081697.png"));
        List<JSONObject> list = mongoTemplate21.find(query, JSONObject.class, "image_collector");
        list.forEach(item->{
            System.out.println(item.getJSONArray("shops"));
        });
    }

    @Test
    public void testQueryRegexMongo() throws Exception{
        List<String> eventCodeRegex = new ArrayList<>(2);
        eventCodeRegex.add("/T11\\d*/");
        eventCodeRegex.add("/T00\\d*/");
        Query queryCount = new Query(Criteria.where("transaction_info.transaction_event_code").regex("(T11\\d*)|(T00\\d*)"));
        long count = mongoTemplate.count(queryCount, "paypal_transaction");
        System.out.println(count);
    }
}
