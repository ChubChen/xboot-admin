package tongtu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.codec.binary.Base64;
import org.junit.Test;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestClass {

    public static void main(String[] args) {
        Integer i = new Integer(2);
        Integer b = new Integer(1);
        System.out.println(i.compareTo(b));
        String str = "12312-123";
        System.out.println(Arrays.toString(str.split("/|-")));
        System.out.println(i.compareTo(b));
        System.out.println(new SimpleDateFormat("yyyy-MM-dd HH:mm:ssZ").format(new Date()));
        Pattern pattern =Pattern.compile("(GO[A-Z]{2}[0-9]+[A-Z])([(0-9)\\-]+)");
        Matcher m = pattern.matcher("GOAC00000V03");
        System.out.println(m.matches());
        System.out.println(m.groupCount());
        System.out.println(m.group());
        System.out.println(m.group(1));

        List<String> pOrderIdList = Arrays.asList("1,2,3,4,5".split(","));

//        List<String> queryList = pOrderIdList.subList(0,10);
//        System.out.println(queryList);
        Pattern InvoiceIdExp = Pattern.compile("c\\d*\\.\\d");
        Matcher m123 = InvoiceIdExp.matcher("c14366462836885..2");
        System.out.println(m123.matches());
        System.out.println("c14366462836885.2".substring(1,"c14366462836885.2".length()-2));
        String auth = "STARLINKUBUSD" + ":" + "sdlk_2018_";
        System.out.println(new String(Base64.encodeBase64(auth.getBytes(Charset.forName("UTF-8")))));
    }

    @Test
    public void testJson(){
        String s  = "{\n" +
                "   \"live\":\"false\",\n" +
                "   \"notificationItems\":[\n" +
                "      {\n" +
                "         \"NotificationRequestItem\":{\n" +
                "            \"eventCode\":\"AUTHORISATION\",\n" +
                "            \"success\":\"true\",\n" +
                "            \"eventDate\":\"2019-06-28T18:03:50+01:00\",\n" +
                "            \"merchantAccountCode\":\"YOUR_MERCHANT_ACCOUNT\",\n" +
                "            \"pspReference\": \"7914073381342284\",\n" +
                "            \"merchantReference\": \"YOUR_REFERENCE\",\n" +
                "            \"amount\": {\n" +
                "                \"value\":1130,\n" +
                "                \"currency\":\"EUR\"\n" +
                "            }\n" +
                "         }\n" +
                "      }\n" +
                "   ]\n" +
                "}";
        JSONObject jsonObject = JSON.parseObject(s);
        System.out.println(jsonObject.getJSONArray("notificationItems").getJSONObject(0).getJSONObject("NotificationRequestItem").getString("eventCode"));
    }
}
