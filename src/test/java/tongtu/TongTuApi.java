package tongtu;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;
import com.chubchen.admin.XbootApplication;
import com.chubchen.admin.common.utils.DateTimeUtil;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.modules.tongtu.module.*;
import com.chubchen.admin.modules.tongtu.utils.TongTuSign;
import org.apache.http.impl.client.HttpClientBuilder;
import org.joda.time.DateTime;
;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringRunner.class)
//@SpringBootTest(classes = XbootApplication.class)
public class TongTuApi {

    private static final String AccountCode = "深圳前海帕拓逊网络技术有限公司";

    private static final String merchantId="b7b6bf558e5a5dd37c58172ec3c0cc4f";

    private static final String app_token = "08e655045e8021bd52993fc53edfbb98";

    private static final String secretAccessKey = "f1d1091ffc5140fd9a4ff7ab3fc2d3cb51035a0f76e24e65bfb17debaa6deca6";

    private static final String API_KEY = "3ad721dfbb024f8a89124bbf2e13d97d";

    //@Autowired
    private MongoTemplate mongoTemplate;

    public RestTemplate getTemplate(){
        SimpleClientHttpRequestFactory simpleClientHttpRequestFactory = new SimpleClientHttpRequestFactory();
        simpleClientHttpRequestFactory.setConnectTimeout(15000);
        simpleClientHttpRequestFactory.setReadTimeout(5000);
        return new RestTemplate(simpleClientHttpRequestFactory);
    }

    @Test
    public void getApp_token() throws Exception{

        RestTemplate restTemplate = getTemplate();
        OrdersQueryRequest ordersQueryRequest = new OrdersQueryRequest();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        TongTuSign s = new TongTuSign();
        String sign = s.getSign(app_token,timeStamp, secretAccessKey);
        Map<String, String> map = new HashMap<>();
        map.put("app_token",app_token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);

        ordersQueryRequest.setAccountCode(AccountCode);
        ordersQueryRequest.setMerchantId(merchantId);
        ordersQueryRequest.setPageNo(1);
        ordersQueryRequest.setPageSize(100);
        ordersQueryRequest.setOrderStatus("waitPacking");
        System.out.println(timeStamp);
        System.out.println(app_token);
        System.out.println(sign);
        ResponseEntity<String> responseEntity = restTemplate.postForEntity("https://open.tongtool.com/api-service/openapi/tongtool/ordersQuery?app_token={app_token}&timestamp={timestamp}&sign={sign}", ordersQueryRequest,
                String.class, map);
        System.out.println(responseEntity.getStatusCode());
        System.out.println(responseEntity.getBody());
    }

    @Test
    public void getHttpOrders() throws Exception{

        RestTemplate restTemplate = getTemplate();
        OrdersQueryRequest ordersQueryRequest = new OrdersQueryRequest();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        TongTuSign s = new TongTuSign();
        String sign = s.getSign(app_token,timeStamp, secretAccessKey);
        Map<String, String> map = new HashMap<>();
        map.put("app_token",app_token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);

        ordersQueryRequest.setAccountCode("shopifyls");
        ordersQueryRequest.setMerchantId(merchantId);
        ordersQueryRequest.setPageNo(1);
        ordersQueryRequest.setPageSize(100);
        //ordersQueryRequest.setOrderId("shopifyls-9041");
//        ordersQueryRequest.setOrderStatus("waitPacking");
        ordersQueryRequest.setUpdatedDateFrom(new DateTime(2020, 1, 6, 7,4).toString("yyyy-MM-dd HH:mm:ss"));
        ordersQueryRequest.setUpdatedDateTo(new DateTime(2020, 1, 7, 7,8).toString("yyyy-MM-dd HH:mm:ss"));

        HttpClient h = new HttpClient();
        HttpClient.HttpResponse response = h.doPostMain("https://open.tongtool.com/api-service/openapi/tongtool/ordersQuery", ordersQueryRequest, map);
        System.out.println(response.getCode());
        System.out.println(response.getBody());
//        TongTuBaseResponse<GoogsQueryResponse> responseObject = JSONObject.parseObject(response.getBody(),  new TypeReference<TongTuBaseResponse<GoogsQueryResponse>>(){});
//        if(responseObject != null && responseObject.getDatas() != null &&
//                responseObject.getDatas().getArray() != null && !responseObject.getDatas().getArray().isEmpty()){
//            responseObject.getDatas().getArray().forEach(goods ->{
//                mongoTemplate.save(goods);
//            });
//        }

    }

    @Test
    public void getProduct() throws Exception{

        ProductRequest product = new ProductRequest();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        TongTuSign s = new TongTuSign();
        String sign = s.getSign(app_token,timeStamp, secretAccessKey);
        Map<String, String> map = new HashMap<>();
        map.put("app_token",app_token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);

        product.setMerchantId(merchantId);
        ArrayList<String> list = new ArrayList<>();
        list.add("GOPA4321S");
        product.setSkuList(list);
        product.setPageNum(1);
        product.setPageSize(10);

        HttpClient h = new HttpClient();
        HttpClient.HttpResponse response = h.doPostMain("https://open.tongtool.com/api-service//openapi/product/query", product, map);
        System.out.println(response.getCode());
        System.out.println(response.getBody());
    }

    @Test
    public void getGoodsQuery() throws Exception{

        GoodsQueryRequest goods = new GoodsQueryRequest();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        TongTuSign s = new TongTuSign();
        String sign = s.getSign(app_token,timeStamp, secretAccessKey);
        Map<String, String> map = new HashMap<>();
        map.put("app_token",app_token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);

        goods.setMerchantId(merchantId);
        ArrayList<String> list = new ArrayList<>();
        list.add("GOPA4321S");
        goods.setSkus(list);
        goods.setProductType("0");
        goods.setPageNo(1);
        goods.setPageSize(10);

        HttpClient h = new HttpClient();
        HttpClient.HttpResponse response = h.doPostMain("https://open.tongtool.com/api-service//openapi/tongtool/goodsQuery", goods, map);
        System.out.println(response.getCode());
        System.out.println(response.getBody());
    }

    @Test
    public void getPackages() throws Exception{

        PackageRequest packages = new PackageRequest();
        String timeStamp = String.valueOf(System.currentTimeMillis());
        TongTuSign s = new TongTuSign();
        String sign = s.getSign(app_token,timeStamp, secretAccessKey);
        Map<String, String> map = new HashMap<>();
        map.put("app_token",app_token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);

        packages.setMerchantId(merchantId);
        packages.setPackageStatus("waitPrint");
        packages.setAssignTimeFrom("2019-12-26 00:00:00");
        packages.setAssignTimeTo("2019-12-26 23:59:59");
//        packages.setDespatchTimeFrom("2019-12-22 00:00:00");
//        packages.setDespatchTimeTo("2019-12-22 23:59:59");
        packages.setPageNo(1);
        packages.setPageSize(10);

        HttpClient h = new HttpClient();
        HttpClient.HttpResponse response = h.doPostMain("https://open.tongtool.com/api-service/openapi/tongtool/packagesQuery",packages , map);
        System.out.println(response.getCode());
        System.out.println(response.getBody());
    }


    @Test
    public void getToken() throws Exception{

        HttpClient h = new HttpClient();
        h.setCloseableHttpClient(HttpClientBuilder.create().build());
        Map<String, Object> map = new HashMap<>();
        map.put("accessKey",API_KEY);
        map.put("secretAccessKey",secretAccessKey);
        String string = h.doGet("https://open.tongtool.com/open-platform-service/devApp/appToken", map);
        System.out.println(JSON.parseObject(string).getString("datas"));
        System.out.println(string);

    }

    @Test
    public void getMerchantId() throws Exception{

        HttpClient h = new HttpClient();
        h.setCloseableHttpClient(HttpClientBuilder.create().build());
        TongTuSign s = new TongTuSign();
        Map baseMap = s.getBaseMap(app_token, secretAccessKey);
        String string = h.doGet("https://open.tongtool.com/open-platform-service/partnerOpenInfo/getAppBuyerList", baseMap);
        System.out.println(JSON.parseObject(string).getString("datas"));
        System.out.println(string);

    }

}
