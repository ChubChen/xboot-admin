package alibaba;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.imagesearch.model.v20190325.SearchImageRequest;
import com.aliyuncs.imagesearch.model.v20190325.SearchImageResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import org.apache.commons.codec.binary.Base64;

import java.io.*;

public class ImageSearchTest {

    private static final String region = "cn-shanghai";
    private static final String ACCESS_KEY_ID = "LTAI4G2riQEV2JkSMtChahUo";
    private static final String ACCESS_KEY_SECRET = "wNg3Q851N55oon52x1s2O6Bz3z4wxG";

    public static void main(String[] args) {
        DefaultProfile.addEndpoint( region, "ImageSearch", "imagesearch."+region+".aliyuncs.com");
        IClientProfile profile = DefaultProfile.getProfile(region, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        IAcsClient client = new DefaultAcsClient(profile);
        searchImageRequest(client);
    }

    public static void searchImageRequest(IAcsClient client ){
        SearchImageRequest request = new SearchImageRequest();
        request.setInstanceName("imagesearch20200526jay");
        request.setType("SearchByPic");
        // 图片内容，Base64编码。最多支持 2MB大小图片以及5s的传输等待时间。当前仅支持jpg和png格式图片；
        // 对于商品、商标、通用图片搜索，图片长和宽的像素必须都大于等于200，并且小于等于1024；
        // 对于布料搜索，图片长和宽的像素必须都大于等于448，并且小于等于1024；
        // 图像中不能带有旋转信息。
        byte[] bytes2 = getBytes("/Users/chubchen/Desktop/demo.png");
        Base64 base64 = new Base64();
        String encodePicContent = base64.encodeToString(bytes2);
        // 1. Type=SearchByPic时，必填
        // 2. Type=SearchByName时，无需填写。
        request.setPicContent(encodePicContent);
        // 选填，商品类目。
        // 1. 对于商品搜索：若设置类目，则以设置的为准；若不设置类目，将由系统进行类目预测，预测的类目结果可在Response中获取 。
        // 2. 对于布料、商标、通用搜索：不论是否设置类目，系统会将类目设置为88888888。
        //request.setCategoryId(1);

        // 选填，是否需要进行主体识别，默认为true。
        // 1.为true时，由系统进行主体识别，以识别的主体进行搜索，主体识别结果可在Response中获取。
        // 2. 为false时，则不进行主体识别，以整张图进行搜索。
        // 3.对于布料图片搜索，此参数会被忽略，系统会以整张图进行搜索。
        request.setCrop(false);

        // 选填，图片的主体区域，格式为 x1,x2,y1,y2, 其中 x1,y1 是左上角的点，x2，y2是右下角的点。
        // 若用户设置了Region，则不论Crop参数为何值，都将以用户输入Region进行搜索。
        // 3.对于布料图片搜索，此参数会被忽略，系统会以整张图进行搜索。
        request.setRegion("280,486,232,351");

        // 选填，返回结果的数目。取值范围：1-100。默认值：10。
        request.setNum(100);
        // 选填，返回结果的起始位置。取值范围：0-499。默认值：0。
        request.setStart(1);

        try {
            SearchImageResponse response = client.getAcsResponse(request);
            System.out.println(response.getCode());
            System.out.println(response.getAuctions());
        } catch (ClientException e) {
            e.printStackTrace();
        }
    }
    private static byte[] getBytes(String filePath) {
        byte[] buffer = null;
        try {
            File file = new File(filePath);
            FileInputStream fis = new FileInputStream(file);
            // picture max size is 2MB
            ByteArrayOutputStream bos = new ByteArrayOutputStream(2000 * 1024);
            byte[] b = new byte[1000];
            int n;
            while ((n = fis.read(b)) != -1) {
                bos.write(b, 0, n);
            }
            fis.close();
            bos.close();
            buffer = bos.toByteArray();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return buffer;
    }
}
