package alibaba;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.logistics.param.AlibabaLogisticsOpenPlatformLogisticsOrder;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewParam;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewResult;
import com.alibaba.ocean.rawsdk.ApiExecutor;
import com.alibaba.ocean.rawsdk.common.SDKResult;
import com.alibaba.trade.param.AlibabaOpenplatformTradeModelTradeInfo;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListParam;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListResult;
import com.chubchen.admin.XbootApplication;
import com.chubchen.admin.common.utils.DateTimeUtil;
import com.chubchen.admin.modules.alibaba.config.AlibabaConfig;
import com.chubchen.admin.modules.alibaba.constants.CollectionNames;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaQueryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.joda.time.DateTime;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = XbootApplication.class)
@Slf4j
public class AlibabaPayQuery {

    @Autowired
    private AlibabaQueryService alibabaQueryService;

    @Autowired
    private AlibabaConfig alibabaConfig;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Test
    public void test1688Request() throws Exception{
        //设置appkey和密钥(seckey)
        ApiExecutor apiExecutor = new ApiExecutor(alibabaConfig.getAppKey(),alibabaConfig.getSecKey());

        //构造API入参和出参
        //API出入参类命名规则：API名称每个单词首字母大写，并去掉分隔符（“.”），末尾加上Param（或Result），其中Param为入参、Result为出参
        //以创建订单API为例，API名称：alibaba.trade.fastCreateOrder
        AlibabaTradeGetLogisticsInfosBuyerViewParam param = new AlibabaTradeGetLogisticsInfosBuyerViewParam();
        param.setOrderId(900419105757305743L);
        param.setWebSite("1688");

        //调用API并获取返回结果
        SDKResult<AlibabaTradeGetLogisticsInfosBuyerViewResult> result = apiExecutor.execute(param,alibabaConfig.getAccessToken());

        //对返回结果进行操作
        System.out.println(result.getResult().getErrorCode());
        if(result.getResult() != null &&  result.getResult().getResult() != null){
            for (AlibabaLogisticsOpenPlatformLogisticsOrder alibabaLogisticsOpenPlatformLogisticsOrder : result.getResult().getResult()) {
                //物流订单号。 运单号
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getLogisticsBillNo());
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getLogisticsCompanyId());
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getLogisticsCompanyName());
                //物流信息ID 1688ID
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getLogisticsId());
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getStatus());
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getGmtSystemSend());
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getOrderEntryIds());
                System.out.println(JSON.toJSONString(alibabaLogisticsOpenPlatformLogisticsOrder.getSender()));
                System.out.println(alibabaLogisticsOpenPlatformLogisticsOrder.getRemarks());
            }
        }
    }


    @Test
    public void TestOrderList(){
        ApiExecutor apiExecutor = new ApiExecutor(alibabaConfig.getAppKey(),alibabaConfig.getSecKey());
        AlibabaTradeGetBuyerOrderListParam param = new AlibabaTradeGetBuyerOrderListParam();
        param.setModifyStartTime(DateTimeUtil.minusDays(new DateTime(), 1).toDate());
        param.setModifyEndTime(new Date());
        param.setOrderStatus("waitbuyerreceive");
        //默认返回20条。 totalRecord 是总条数
        SDKResult<AlibabaTradeGetBuyerOrderListResult> result = apiExecutor.execute(param,alibabaConfig.getAccessToken());
        System.out.println(result.getErrorCode());
        System.out.println(result.getResult().getErrorCode());
        if(result.getResult() != null && result.getResult().getResult()!= null){
            System.out.println(result.getResult().getTotalRecord());
            System.out.println(JSON.toJSONString(result.getResult().getResult()));
        }
    }

    @Test
    public void testJob(){
        Date startDate;
            //step 1 查询
        AlibabaOpenplatformTradeModelTradeInfo tradeInfo = alibabaQueryService.getLastUpdateTimeOrders();
        if(tradeInfo != null && tradeInfo.getBaseInfo() != null && tradeInfo.getBaseInfo().getModifyTime() != null){
            startDate = tradeInfo.getBaseInfo().getModifyTime();
        }else{
            startDate = DateUtils.addDays(new Date(), -1);

        }
        if(startDate != null){
            int i = 1;
            while (true){
                //step 2 根据更新时间和更新结束时间同步订单数据到mongoddb
                AlibabaTradeGetBuyerOrderListResult result = alibabaQueryService.queryAlibbaOrdersByModifyTime(startDate, new Date(), i);
                if(result != null && StringUtils.isEmpty(result.getErrorCode())){

                    //step 3 批量保存数据
                    log.info("查询1688订单正常返回，当前page{},开始保存订单数据", i);
                    alibabaQueryService.batchSaveAlibabaOrders(result.getResult());
                    log.info("page{},保存订单数据成功", i);

                    //step4 是否还有下一页
                    long total = result.getTotalRecord();
                    if((20*i) >= total){
                        log.info("没有下一页，查询结束");
                        break;
                    }
                    if(result.getResult() == null || result.getResult().length <= 0){
                        log.info("当前页没有数据，查询结束");
                        break;
                    }
                }
                i++;
            }
        }else{
            log.error("定时任务同步1688订单因为开始时间没有参数是：{}");
        }
    }

    @Test
    public void testRemove(){
        ArrayList<String> list = new ArrayList<>(1);
        list.add("900344035010305743");
        mongoTemplate.remove(Query.query(Criteria.where("baseInfo.idOfStr").in(list)),
                AlibabaOpenplatformTradeModelTradeInfo.class, CollectionNames.ORDER_COLLECTON_NAME);
    }

    @Test
    public void testQuery(){
        Query query = Query.query(Criteria.where("id").is(2199225663573L)).limit(1);
        System.out.println(mongoTemplate.findOne(query, JSONObject.class,"orders"));
        System.out.println(mongoTemplate.findOne(query, String.class,"orders"));
    }
}
