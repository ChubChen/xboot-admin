package tiktok;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.modules.easesea.entity.AdReport;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Demo {
    private static final String ACCESS_TOKEN = "e161fa3f2de738b63af3aa19e733135b71d95e6b";
    private static final String PATH = "/open_api/2/reports/advertiser/get/";
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * Build request URL
     *
     * @param path Request path
     * @return Request URL
     */
    private static String buildUrl(String path) throws URISyntaxException {
        URI uri = new URI("https", "ads.tiktok.com", path, "", "");
        return uri.toString();
    }


    /**
     * Send GET request
     *
     * @param jsonStr:Args in JSON format
     * @return Response in JSON format
     */
    private static String get(String jsonStr) throws IOException, URISyntaxException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        URIBuilder ub = new URIBuilder(buildUrl(PATH));
        Map<String, Object> map = mapper.readValue(jsonStr, Map.class);
        map.forEach((k, v) -> {
            try {
                ub.addParameter(k, v instanceof String ? (String) v : mapper.writeValueAsString(v));
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        });
        URL url = ub.build().toURL();

        Request request = new Request.Builder()
                .url(url)
                .method("GET", null)
                .addHeader("Access-Token", ACCESS_TOKEN)
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        String end_date = "2020-07-16";
        List<String> fields_list = new ArrayList<>();
        fields_list.add("ctr");
        fields_list.add("ecpm");
        fields_list.add("show_cnt");
        fields_list.add("stat_cost");
        fields_list.add("show_uv");
        fields_list.add("conversion_cost");
        fields_list.add("conversion_rate");
        fields_list.add("convert_cnt");
        fields_list.add("click_cnt");
        fields_list.add("click_cost");
        fields_list.add("dy_comment");
        fields_list.add("dy_home_visited");
        fields_list.add("dy_like");
        fields_list.add("dy_share");
        fields_list.add("skip");
        String fields = mapper.writeValueAsString(fields_list);
        System.out.println(fields);
        Long page_size = 200L;
        String start_date = "2020-07-16";
        Long advertiser_id = 6845987051553161222L;
//        List<String> group_by_list = "STAT_GROUP_BY_FIELD_STAT_TIME";
//        String group_by = mapper.writeValueAsString(group_by_list);
        String time_granularity = "STAT_TIME_GRANULARITY_HOURLY";
        Long page = 1L;

        // Args in JSON format
        String myArgs = String.format("{\"end_date\": \"%s\", \"fields\": %s, \"page_size\": \"%s\", \"start_date\": \"%s\", \"advertiser_id\": \"%s\", \"time_granularity\": \"%s\", \"page\": \"%s\"}", end_date, fields, page_size, start_date, advertiser_id, time_granularity, page);
        String s = get(myArgs);
        JSONObject jsonObject = JSON.parseObject(s);
        System.out.println(jsonObject.getJSONObject("data").getJSONArray("list").toJavaList(AdReport.class));
    }
}