import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.*;
import org.apache.http.client.utils.URIBuilder;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class AuthCode {
    private static final String ACCESS_TOKEN = "xxx";
    private static final String PATH = "/open_api/oauth2/access_token_v2/";
    private static final ObjectMapper mapper = new ObjectMapper();

    /**
     * Build request URL
     *
     * @param path Request path
     * @return Request URL
     */
    private static String buildUrl(String path) throws URISyntaxException {
        URI uri = new URI("https", "ads.tiktok.com", path, "", "");
        return uri.toString();
    }


    /**
     * Send POST request
     *
     * @param jsonStr Args in JSON format
     * @return Response in JSON format
     */
    private static String post(String jsonStr) throws IOException, URISyntaxException {
        OkHttpClient client = new OkHttpClient().newBuilder().build();
        String url = buildUrl(PATH);

        RequestBody body = RequestBody.create(MediaType.parse("application/json"), jsonStr);
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .addHeader("Content-Type", "application/json")
                .build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }

    public static void main(String[] args) throws IOException, URISyntaxException {
        String secret = "c8e95265fc679ca5ce1589bfaab348cd8a0ed4df";
        Long app_id = 6849418686554439685L;
        String auth_code = "ca5ef4b586f766ffff2ed97684044fac61490ef1";

        // Args in JSON format
        String myArgs = String.format("{\"secret\": \"%s\", \"app_id\": \"%s\", \"auth_code\": \"%s\"}",secret, app_id, auth_code);
        System.out.println(post(myArgs));
    }
}