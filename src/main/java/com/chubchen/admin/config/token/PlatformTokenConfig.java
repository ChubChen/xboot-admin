package com.chubchen.admin.config.token;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author chubchen
 */
@Data
@Configuration
public class PlatformTokenConfig {

    @Value("${platformToken.tiktok}")
    private String tiktok;

}
