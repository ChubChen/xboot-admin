package com.chubchen.admin.config.mongodb;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoClientDbFactory;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

/**
 * @author chubchen
 */
@Data
@Configuration
public class ChooseMongodb  {

    @Value("${spring.data.mongodb.choose.connectString}")
    private String connectString;


    @Value("${spring.data.mongodb.uri}")
    private String uri;

    public @Bean(name = "chooseMongoTemplate")
    MongoTemplate getMongoTemplate(MongoMappingContext context) throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(new SimpleMongoClientDbFactory(connectString));

        return mongoTemplate;
    }

    @Primary
    @Bean
    MongoTemplate getMongoTemplate2(MongoMappingContext context) throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(new SimpleMongoClientDbFactory(uri));

        return mongoTemplate;
    }
}