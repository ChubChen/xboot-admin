package com.chubchen.admin.common.constant;

public interface Bu8ShopifyConstant {

    /**
     * shopify 站点状态 -- 启用
     */
    Integer SHOPITFY_ACCOUNT_STATUS_ENABLE = 1;

    /**
     * shopify 站点状态 -- 废弃
     */
    Integer SHOPITFY_ACCOUNT_STATUS_DISABLE = 2;

    /**
     * shopify 站点状态 -- 新建
     */

    Integer SHOPITFY_ACCOUNT_STATUS_INIT = 0;



}
