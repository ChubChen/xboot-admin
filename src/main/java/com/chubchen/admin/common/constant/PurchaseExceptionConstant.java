package com.chubchen.admin.common.constant;

/**
 * @author chubchen
 */
public interface PurchaseExceptionConstant {

    /**
     * 任务开启状态
     */
    Integer TASK_OPEN = 0;

    /**
     * 任务完成状态
     */
    Integer TASK_COLSE = 1;


    /**
     * 流程 未处理状态
     */
    Integer FLOW_INIT = 0;

    /**
     * 流程 已处理状态
     */
    Integer FLOW_FINISH = 1;



}
