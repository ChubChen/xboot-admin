package com.chubchen.admin.common.constant;

/**
 * @author chubchen
 */
public interface PurchaseConstant {


    /**
     * 采购订单记录状态 1-已采购
     */
    Integer PURCHASE_ORDER_STATUS_INIT = 1;

    /**
     * 采购订单记录状态 2-已发货
     */
    Integer PURCHASE_ORDER_STATUS_SEND = 2;

    /**
     * 采购订单记录状态 3-已到货
     */
    Integer PURCHASE_ORDER_STATUS_FINISH = 3;

    /**
     * 采购订单记录状态 4-异常
     */
    Integer PURCHASE_ORDER_STATUS_ERROR = 4;


    /**
     * 采购订单记录状态 0 未采购
     */
    Integer TONGTOOL_PURCHASE_STATUS_INIT = 0;

    /**
     * 采购订单记录状态 1 部分采购
     */
    Integer TONGTOOL_PURCHASE_STATUS_SECTION = 1;

    /**
     * 采购订单记录状态 2 全部采购完成
     */
    Integer TONGTOOL_PURCHASE_STATUS_FINISH = 2;

    /**
     * 采购订单记录状态 3 采购异常
     */
    Integer TONGTOOL_PURCHASE_STATUS_ERROR = 3;

    /**
     * 采购订单记录状态 4 部分采购不等待剩余
     */
    Integer TONGTOOL_PURCHASE_STATUS_ERROR_FINISH = 4;

    String PURCHASE_DEP_ID = "251092240925986816";

}
