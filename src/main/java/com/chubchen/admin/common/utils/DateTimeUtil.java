package com.chubchen.admin.common.utils;

import org.joda.time.DateTime;
import org.joda.time.Months;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 时间工具类
 *
 * @author chubchen
 * @date 2018/11/8 上午11:40
 **/
@SuppressWarnings({"unused", "WeakerAccess"})
public class DateTimeUtil {

    public static final String FORMAT = "yyyy-MM-dd";

    public static final String FULL_FORMAT = "yyyy-MM-dd HH:mm:ss";

    /**
     * 获取今天的0点到23点59分59秒
     *
     * @return map
     */
    public static Map<String, Date> getNowDayRange(DateTime dateTime) {

        Date startTime = dateTime
                .withHourOfDay(0)
                .withMinuteOfHour(0)
                .withSecondOfMinute(0)
                .withMillisOfSecond(0)
                .toDate();

        Date endTime = dateTime
                .withHourOfDay(23)
                .withMinuteOfHour(59)
                .withSecondOfMinute(59)
                .withMillisOfSecond(999)
                .toDate();

        return new HashMap<String, Date>(2) {
            {
                put("startTime", startTime);
                put("endTime", endTime);
            }
        };

    }

    public static Date get0DefaultTime(){
        return new DateTime(1970,01,01,0,0,0,0).toDate();
    }

    /**
     * 将传入的日期减去相应的天数
     *
     * @param dateTime 日期
     * @param i        要减掉的天数
     * @return 目标日期
     */
    public static DateTime minusDays(DateTime dateTime, int i) {
        return dateTime.minusDays(i);
    }

    /**
     * 将传入的日期减去相应的天数
     *
     * @param date 日期
     * @param i    要减掉的天数
     * @return 目标日期
     */
    public static Date minusDays(Date date, int i) {
        return new DateTime(date).minusDays(i).toDate();
    }


    /**
     * 将传入的日期加上相应的天数
     *
     * @param dateTime 日期
     * @param i        要加上的天数
     * @return 目标日期
     */
    public static DateTime plusDays(DateTime dateTime, int i) {
        return dateTime.plusDays(i);
    }

    /**
     * 将传入的日期加上相应的天数
     *
     * @param date 日期
     * @param i    要加上的天数
     * @return 目标日期
     */
    public static Date plusDays(Date date, int i) {
        return new DateTime(date).plusDays(i).toDate();
    }

    /**
     * 将日期转换成字符串
     *
     * @param target 目标日期
     * @return 字符串
     */
    public static String format(Date target) {
        DateTime dateTime = new DateTime(target);
        return dateTime.toString(FORMAT);
    }

    /**
     * 将日期转换成字符串
     *
     * @param target 目标日期
     * @return 字符串
     */
    public static String format(Date target, String format) {
        DateTime dateTime = new DateTime(target);
        return dateTime.toString(format);
    }

    /**
     * 将日期转换成字符串
     *
     * @param target 目标日期
     * @return 字符串
     */
    public static String format(DateTime target) {
        return target.toString(FORMAT);
    }

    /**
     * 判断传入的日期是否在今天之前
     *
     * @param date 要判断的日期
     * @return 是否
     */
    public static boolean isBeforeNowDay(Date date) {
        return isBeforeTargetDate(date, now());
    }

    /**
     * 判断传入的源日期是否在目标日期之前
     *
     * @param sourceDate 源日期
     * @param targetDate 目标日期
     * @return 是否
     */
    public static boolean isBeforeTargetDate(Date sourceDate, Date targetDate) {
        DateTime sourceTime = new DateTime(sourceDate).withTime(0, 0, 0, 0);
        DateTime targetTime = new DateTime(targetDate).withTime(0, 0, 0, 0);
        return sourceTime.isBefore(targetTime);
    }

    /**
     * 判断传入的源日期是否在目标日期之前
     *
     * @param sourceDate 源日期
     * @param targetDate 目标日期
     * @return 是否
     */
    public static long betweenDate(Date sourceDate, Date targetDate) {
        DateTime sourceTime = new DateTime(sourceDate);
        DateTime targetTime = new DateTime(targetDate);
        return sourceTime.getMillis() - targetTime.getMillis();
    }
    /**
     * 判断传入的源日期是否大于等于目标日期
     *
     * @param sourceDate 源日期
     * @param targetDate 目标日期
     * @return 是否
     */
    public static boolean isMoreTargetTime(Date sourceDate, Date targetDate) {
        DateTime sourceTime = new DateTime(sourceDate).withTime(0, 0, 0, 0);
        DateTime targetTime = new DateTime(targetDate).withTime(0, 0, 0, 0);
        return !sourceTime.isBefore(targetTime);
    }

    /**
     * 判断传入的日期是否为今天
     *
     * @param targetDate 目标日期
     * @return 是否
     */
    public static boolean isNowDay(Date targetDate) {
        return isSameDay(targetDate, now());
    }

    /**
     * 判断传入的两个日期是否相同
     *
     * @param sourceDate 源日期
     * @param targetDate 目标日期
     * @return 是否
     */
    public static boolean isSameDay(Date sourceDate, Date targetDate) {
        DateTime sourceTime = new DateTime(sourceDate).withTime(0, 0, 0, 0);
        DateTime targetTime = new DateTime(targetDate).withTime(0, 0, 0, 0);
        return sourceTime.compareTo(targetTime) == 0;
    }

    /**
     * 获取当前Date
     *
     * @return 当前Date
     */
    public static Date now() {
        return DateTime.now().toDate();
    }

    /**
     * 获取两个日期之间的月份差
     *
     * @return 月份数
     */
    public static int getMonthsDiff(DateTime source, DateTime target) {
        return Math.abs(Months.monthsBetween(source, target).getMonths());
    }

    public static void main(String[] args) {
        System.out.println(DateTimeUtil.get0DefaultTime());
    }

}
