package com.chubchen.admin.common.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializeConfig;
import com.chubchen.admin.modules.easesea.util.JAXBUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.nio.charset.Charset;
import java.util.Map;

@Component
@Slf4j
public class HttpClient {

    /** 默认字符集 */
    public static final String DEFAULT_CHARSET = "UTF-8";

    @Autowired
    private CloseableHttpClient closeableHttpClient;

    @Autowired
    private RequestConfig config;


    /**
     * 用于JSON格式的API调用
     * @param url url地址
     * @param requestParameter  请求参数
     * @param clazz 接口返回值的类型
     * @return
     * @throws Exception
     */
    public <T> T doGet(String url, Map<String, Object> requestParameter, Class<T> clazz) throws Exception {
        String responseJson = this.doGet(url, requestParameter);
        T response = JSON.parseObject(responseJson, clazz);
        return response;
    }

    public String doGet(String url, Map<String, Object> requestParameter) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(url);

        if (requestParameter != null) {
            for (Map.Entry<String, Object> entry : requestParameter.entrySet()) {
                uriBuilder.setParameter(entry.getKey(), entry.getValue().toString());
            }
        }

        return this.doGet(uriBuilder.build().toString());
    }

    public String getAuthHeader(String username, String password){
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
        String authHeader = "Basic " + new String(encodedAuth);
        return authHeader;
    }

    public String doGet(String url) throws Exception {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setConfig(config);
        httpGet.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
        CloseableHttpResponse response = this.closeableHttpClient.execute(httpGet);

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("api request exception, http reponse code:" + statusCode);
        }

        return EntityUtils.toString(response.getEntity(), DEFAULT_CHARSET);
    }

    public JSONObject doGetJson(String url, Map<String, Object> requestParameter, Map<String, String> headerMap) throws Exception {
        URIBuilder uriBuilder = new URIBuilder(url);

        if (requestParameter != null) {
            for (Map.Entry<String, Object> entry : requestParameter.entrySet()) {
                uriBuilder.setParameter(entry.getKey(), entry.getValue().toString());
            }
        }
        HttpGet httpGet = new HttpGet(uriBuilder.build().toString());
        httpGet.setConfig(config);
        httpGet.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
        if(headerMap != null) {
            headerMap.forEach((key, value)-> httpGet.addHeader(key, value));
        }
        CloseableHttpResponse response = this.closeableHttpClient.execute(httpGet);

        int statusCode = response.getStatusLine().getStatusCode();
        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("api request exception, http reponse code:" + statusCode);
        }

        return JSON.parseObject(EntityUtils.toString(response.getEntity(), DEFAULT_CHARSET));
    }


    public <T> T doPost(String url, Map<String, Object> requestParameter, Class<T> clazz) throws Exception {
        HttpResponse httpResponse = this.doPost(url, requestParameter);
        int statusCode = httpResponse.getCode();
        if (statusCode != HttpStatus.SC_OK) {
            throw new Exception("api request exception, http reponse code:" + statusCode);
        }

        T response = JSONObject.parseObject(httpResponse.getBody(), clazz);
        return response;
    }


    /**
     *  JSON 类型请求
     * @param url
     * @param request
     * @return
     * @throws Exception
     */
    public HttpResponse doPost(String url,Object request) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(config);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());

        if (request != null) {
            String requestBody = JSONObject.toJSONString(request);
            StringEntity postEntity = new StringEntity(requestBody, "UTF-8");
            httpPost.setEntity(postEntity);
        }

        CloseableHttpResponse response = this.closeableHttpClient.execute(httpPost);
        // 对请求的响应进行简单的包装成自定义的类型
        return new HttpResponse(response.getStatusLine().getStatusCode(), EntityUtils.toString(
                response.getEntity(), DEFAULT_CHARSET));
    }

    /**
     *  JSON 类型请求
     * @param url
     * @param request
     * @return
     * @throws Exception
     */
    public HttpResponse doPost(String url,JSONObject request, Map<String, String> headersMap) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(config);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());
        if(headersMap != null){
            for(Map.Entry<String,String> item: headersMap.entrySet()){
                httpPost.addHeader(item.getKey(), item.getValue());
            }
        }

        if (request != null) {
            StringEntity postEntity = new StringEntity(request.toJSONString(), "UTF-8");
            httpPost.setEntity(postEntity);
        }

        CloseableHttpResponse response = this.closeableHttpClient.execute(httpPost);
        // 对请求的响应进行简单的包装成自定义的类型
        return new HttpResponse(response.getStatusLine().getStatusCode(), EntityUtils.toString(
                response.getEntity(), DEFAULT_CHARSET));
    }

    public HttpResponse doPost(String url) throws Exception {
        return this.doPost(url, null);
    }


    public HttpResponse doAuthHeaderAndXML(String url,Object request, String auth) throws Exception {
        HttpPost httpPost = new HttpPost(url);
        httpPost.setConfig(config);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_XML.getMimeType());
        httpPost.addHeader("Authorization", auth);

        if (request != null) {
            String xmlRequest = JAXBUtil.getXml(request);
            log.info("发起请求:{}", xmlRequest);
            httpPost.setEntity(new StringEntity(xmlRequest, "UTF-8"));
        }

        CloseableHttpResponse response = this.closeableHttpClient.execute(httpPost);
        // 对请求的响应进行简单的包装成自定义的类型
        return new HttpResponse(response.getStatusLine().getStatusCode(), EntityUtils.toString(
                response.getEntity(), DEFAULT_CHARSET));
    }

    /**
     *  JSON 类型请求
     * @param url
     * @param request
     * @return
     * @throws Exception
     */
    public HttpResponse doPost(String url,Object request, Map<String,String> urlParam) throws Exception {
        URIBuilder builder = new URIBuilder(url);
        if(urlParam != null && !urlParam.isEmpty()){
            for (String key: urlParam.keySet()) {
                builder.setParameter(key, urlParam.get(key));
            }
        }
        HttpPost httpPost = new HttpPost(builder.build());
        httpPost.setConfig(config);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());

        if (request != null) {
            String requestBody = JSONObject.toJSONString(request);
            StringEntity postEntity = new StringEntity(requestBody, "UTF-8");
            httpPost.setEntity(postEntity);
        }

        CloseableHttpResponse response = this.closeableHttpClient.execute(httpPost);
        // 对请求的响应进行简单的包装成自定义的类型
        return new HttpResponse(response.getStatusLine().getStatusCode(), EntityUtils.toString(
                response.getEntity(), DEFAULT_CHARSET));
    }

    /**
     *  JSON 类型请求
     * @param url
     * @param request
     * @return
     * @throws Exception
     */
    public HttpResponse doPostMain(String url,Object request, Map<String,String> urlParam) throws Exception {
        URIBuilder builder = new URIBuilder(url);
        if(urlParam != null && !urlParam.isEmpty()){
            for (String key: urlParam.keySet()) {
                builder.setParameter(key, urlParam.get(key));
            }
        }
        log.info(builder.getQueryParams().toString());
        HttpPost httpPost = new HttpPost(builder.build());
        httpPost.setConfig(config);
        httpPost.addHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.getMimeType());

        if (request != null) {
            String requestBody = JSONObject.toJSONString(request);
            StringEntity postEntity = new StringEntity(requestBody, "UTF-8");
            System.out.println(requestBody);
            httpPost.setEntity(postEntity);
        }

        CloseableHttpResponse response = HttpClientBuilder.create().build().execute(httpPost);
        // 对请求的响应进行简单的包装成自定义的类型
        return new HttpResponse(response.getStatusLine().getStatusCode(), EntityUtils.toString(
                response.getEntity(), DEFAULT_CHARSET));
    }


    /**
     * 封住请求的响应码和响应的内容
     */
    public class HttpResponse {
        /** http status */
        private Integer code;
        /** http response content */
        private String body;

        public HttpResponse() { }

        public HttpResponse(Integer code, String body) {
            this.code = code;
            this.body = body;
        }

        public Integer getCode() {
            return code;
        }

        public void setCode(Integer code) {
            this.code = code;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }

    public CloseableHttpClient getCloseableHttpClient() {
        return closeableHttpClient;
    }

    public void setCloseableHttpClient(CloseableHttpClient closeableHttpClient) {
        this.closeableHttpClient = closeableHttpClient;
    }

    public RequestConfig getConfig() {
        return config;
    }

    public void setConfig(RequestConfig config) {
        this.config = config;
    }
}