package com.chubchen.admin.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chubchen
 */
@Component
@Slf4j
public class SendEmailUtil {

    @Autowired
    private HttpClient httpClient;

    public void sendErrorEmail(String subject, String message){
        Map<String, String> map = new HashMap<>(2);
        map.put("subject", subject);
        map.put("text", message);
        try {
            httpClient.doPost("http://198.11.176.1:1000/api/mail/sendReport", map);
        } catch (Exception e) {
           log.error("发送报警邮件失败", e);
        }
    }
}
