package com.chubchen.admin.common.utils;

import com.chubchen.admin.common.constant.CommonConstant;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.base.entity.Permission;
import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.entity.User;
import com.chubchen.admin.modules.base.service.UserService;
import com.chubchen.admin.modules.base.service.mybatis.IUserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

/**
 * @author chubchen
 */
@Component
@Slf4j
public class SecurityUtil {

    @Autowired
    private UserService userService;

    @Autowired
    private IUserRoleService iUserRoleService;

    /**
     * 获取当前登录用户
     * @return
     */
    public User getCurrUser(){

        UserDetails user = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userService.findByUsername(user.getUsername());
    }

    /**
     * 获取当前用户数据权限 null代表具有所有权限
     */
    public List<String> getDeparmentIds(){

        User u = getCurrUser();
        // 用户角色
        List<Role> userRoleList = iUserRoleService.findByUserId(u.getId());
        // 判断有无全部数据的角色
        Boolean flagAll = false;
        Boolean flagSelf = false;
        for(Role r : userRoleList){
            if(r.getDataType()==null||r.getDataType().intValue() == CommonConstant.DATA_TYPE_ALL.intValue()){
                flagAll = true;
                break;
            }else if (CommonConstant.DATA_TYPE_SELF.intValue() == r.getDataType().intValue()){
                flagSelf = true;
            }
        }
        if(flagAll){
            return null;
        }
        List<String> customList ;
        if(flagSelf){
            //递归查询所有子节点组
            customList = iUserRoleService.findDepIdsByUserId(u.getId(), u.getDepartmentId());
        }else{
            customList = iUserRoleService.findDepIdsByUserId(u.getId(), null);
        }
        // 查找自定义
        return customList;
    }


    /**
     * 获取当前用户数据权限 null代表具有所有权限
     */
    public List<String> getStoreCodes(){
        User u = getCurrUser();
        List<String> list = this.getDeparmentIds();
        return iUserRoleService.findStoreCodes(u.getId(), list);
    }

    /**
     *  新的权限接口 获取用户拥有权限的域名id
     */
    public List<Integer> getUserDomainId(){
        User u = getCurrUser();
        List<String> list = this.getDeparmentIds();
        return iUserRoleService.findDomainIdByDeparmentId(u.getId(), list);
    }

    /**
     * 通过用户名获取用户拥有权限
     * @param username
     */
    public List<GrantedAuthority> getCurrUserPerms(String username){

        List<GrantedAuthority> authorities = new ArrayList<>();
        for(Permission p : userService.findByUsername(username).getPermissions()){
            authorities.add(new SimpleGrantedAuthority(p.getTitle()));
        }
        return authorities;
    }

    /**
     * 通过数据权限初始化searchVo参数
     */
    public void setSearchVoPermissionCode(String code, SearchVo searchVo){
        if(StringUtils.isBlank(code)){
            searchVo.setPermissionCodes(this.getStoreCodes());
        }
    }

    /**
     * 通过数据权限初始化searchVo参数
     */
    public void setSearchVoPermissionCode(List<String> codeList, SearchVo searchVo){
        if(codeList == null || codeList.isEmpty()){
            searchVo.setPermissionCodes(this.getStoreCodes());
        }
    }

    /**
     * 通过数据权限初始化searchVo参数
     */
    public void setSearchVoPermissionDomain(Integer domainId, SearchVo searchVo){
        if(searchVo.isFilterPermission()){
            if(domainId == null || domainId <= 0){
                searchVo.setPermissionDomain(this.getUserDomainId());
            }
        }
    }

    /**
     * 通过数据权限初始化searchVo参数
     */
    public void setSearchVoPermissionDomain(List<Integer> domainIdList, SearchVo searchVo){
        if(searchVo.isFilterPermission()) {
            if (domainIdList == null || domainIdList.isEmpty()) {
                searchVo.setPermissionDomain(this.getUserDomainId());
            }
        }
    }
}
