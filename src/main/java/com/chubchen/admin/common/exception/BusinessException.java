package com.chubchen.admin.common.exception;

/**
 * 业务异常类
 * @author chubchen
 */
public class BusinessException extends RuntimeException {

    private String msg;

    public BusinessException(String msg){
        super(msg);
        this.msg = msg;
    }
}
