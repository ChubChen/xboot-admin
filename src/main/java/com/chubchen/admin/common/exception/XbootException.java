package com.chubchen.admin.common.exception;

import lombok.Data;

/**
 * @author chubchen
 */
@Data
public class XbootException extends RuntimeException {

    private String msg;

    public XbootException(String msg){
        super(msg);
        this.msg = msg;
    }
}
