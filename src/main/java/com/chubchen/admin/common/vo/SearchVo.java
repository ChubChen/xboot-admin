package com.chubchen.admin.common.vo;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author chubchen
 */
@Data
public class SearchVo implements Serializable {

    private String startDate;

    private String endDate;

    private String searchData;

    private List<String> permissionCodes;

    private List<Integer> permissionDomain;

    private boolean filterPermission = true;

    /**
     * 过滤时间的字段
     */
    private String fields;
}
