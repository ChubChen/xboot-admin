package com.chubchen.admin.common.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author chubchen
 */
@Data
@AllArgsConstructor
public class IpInfo {

    String url;

    String p;
}
