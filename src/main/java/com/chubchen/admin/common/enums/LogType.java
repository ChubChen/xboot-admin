package com.chubchen.admin.common.enums;

/**
 * @author chubchen
 */

public enum LogType {

    /**
     * 默认0操作
     */
    OPERATION,

    /**
     * 1登录
     */
    LOGIN
}
