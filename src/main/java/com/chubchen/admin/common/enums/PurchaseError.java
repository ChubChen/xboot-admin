package com.chubchen.admin.common.enums;

import lombok.Getter;

import java.io.Serializable;


/**
 * @author chubchen
 */
@Getter
public enum PurchaseError implements Serializable {

    /**
     * 缺货
     */
    OUT_OF_STOCK(1, "缺货"),
    PRODUCT_DISCONTINUED (2, "产品下架"),
    URL_ERROR(3, "连接错误"),
    MISMATCH(4, "SKU与连接不符"),
    OTHER(5, "断码"),
    BREAK_CODE(6, "其他");

    private Integer code;

    private String value;

    PurchaseError(int code, String value){
        this.code = code;
        this.value = value;
    }
}
