package com.chubchen.admin.quartz.jobs;

import com.alibaba.fastjson.JSON;
import com.alibaba.trade.param.AlibabaOpenplatformTradeModelTradeInfo;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListResult;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaQueryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;


/**
 * @author chubchen
 * 定时任务同步1688订单到数据库中。并更新状态查询物流跟踪号
 */
@Slf4j
public class Query1688OrderJob implements Job {

    @Autowired
    private AlibabaQueryService alibabaQueryService;

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    private static final int pageSize= 20;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try{
            //step1 获取开始时间
            Date startDate = parseParameter();
            if(startDate != null){
                int i = 1;
                Date endDate = new Date();
                AlibabaTradeGetBuyerOrderListResult result = alibabaQueryService.queryAlibbaOrdersByModifyTime(startDate, endDate, i);
                //step 2 根据更新时间和更新结束时间同步订单数据到mongoddb
                if(result != null && StringUtils.isEmpty(result.getErrorCode())){
                    long total = result.getTotalRecord();
                    //step 3 批量保存数据
                    log.info("查询1688订单正常返回，当前page{},开始保存订单数据", i);
                    alibabaQueryService.batchSaveAlibabaOrders(result.getResult());
                    log.info("page{},保存订单数据成功", i);

                    while (((pageSize * i) <= total) && total > 0){
                        boolean flag = this.paginateQuery(startDate, endDate, i);
                        if(!flag){
                            break;
                        }
                        i++;
                    }
                }
            }else{
                log.error("定时任务同步1688订单因为开始时间没有参数是：{}", this.parameter);
            }
        }catch (Exception e){
            log.error("翻页同步数据发生异常", e);
        }
    }

    private boolean paginateQuery(Date startDate , Date endDate, int page){
        AlibabaTradeGetBuyerOrderListResult resultPage = alibabaQueryService.queryAlibbaOrdersByModifyTime(startDate, endDate, page);
        if(resultPage != null && StringUtils.isEmpty(resultPage.getErrorCode())){
            //step 3 批量保存数据
            log.info("查询1688订单正常返回，当前page{},开始保存订单数据", page);
            alibabaQueryService.batchSaveAlibabaOrders(resultPage.getResult());
            log.info("page{},保存订单数据成功", page);
            if(resultPage.getResult() == null || resultPage.getResult().length <= 0){
                log.info("当前页没有数据，查询结束");
                return false;
            }
        }
        return true;
    }

    private Date parseParameter(){
        log.info("开始执行1688订单同步定时任务，参数：{}", this.parameter);
        Date startDate;
        if(StringUtils.isNotBlank(this.parameter)){
            startDate = JSON.parseObject(parameter).getDate("startDate");
        }else{
            //step 1 查询
            AlibabaOpenplatformTradeModelTradeInfo tradeInfo = alibabaQueryService.getLastUpdateTimeOrders();
            if(tradeInfo != null && tradeInfo.getBaseInfo() != null && tradeInfo.getBaseInfo().getModifyTime() != null){
                startDate = tradeInfo.getBaseInfo().getModifyTime();
            }else{
                startDate = DateUtils.addDays(new Date(), -1);

            }
        }
        return startDate;
    }
}
