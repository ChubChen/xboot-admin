package com.chubchen.admin.quartz.jobs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.modules.easesea.constants.DataParseConstants;
import com.chubchen.admin.modules.easesea.constants.MongodbCollection;
import com.chubchen.admin.modules.easesea.dataparse.DataParaseDisparcher;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;


/**
 * @author chubchen
 * 数据解析job
 */
@Slf4j
public class DataParseJob implements Job {

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private DataParaseDisparcher dataParaseDisparcher;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("定时任务job 执行清洗mongodb数据到数据库中");
        //step1 扫描mongodb 数据库需要解析的数据

        Query query = new Query(Criteria.where("status").is(DataParseConstants.PARSE_STATUS_INIT).and("createDate").gte(DateTime.now().minusDays(1).toDate()));
        List<JSONObject> list = mongoTemplate.find(query, JSONObject.class, MongodbCollection.DATA_PARSE_QUEUE);
        if(list != null && list.size() > 0){
            //遍历 并处理结果
            list.stream().forEach(jsonObject -> dataParaseDisparcher.dispatch(jsonObject.getString("platform"), jsonObject));
        }else{
            log.info("没有要解析的数据");
        }
    }

    private JSONObject parseParameter(){
        log.info("开始执行带参数的job任务，参数：{}", this.parameter);
        return JSON.parseObject(this.parameter);
    }
}
