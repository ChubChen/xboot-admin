package com.chubchen.admin.quartz.jobs;

import cn.hutool.core.date.DateUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestService;
import com.chubchen.admin.modules.easesea.util.JAXBUtil;
import com.chubchen.admin.modules.easesea.worldpay.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
public class WorldPayRefundOrderStatusJob implements Job {

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Autowired
    private ISlPaypalRefundRequestDetailService paypalRefundRequestDetailService;

    @Autowired
    private ISlPaypalRefundRequestService paypalRefundRequestService;

    @Autowired
    private ISlPaypalAccountService accountService;

    @Autowired
    private HttpClient httpClient;


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("定时任务查询world订单状态结果");
        List<SlPaypalRefundRequestDetail> list = paypalRefundRequestDetailService.list(new QueryWrapper<SlPaypalRefundRequestDetail>()
                .eq("status",SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.Accepted.getCode())
                .eq("platform", SlPaypalRefundRequestDetail.PayPlatform.WORLDPAY.getCode())
                .gt("created_time", DateUtils.addDays(new Date(), -3)).orderByAsc("created_time"));
        if(list != null && list.size() > 0 ){
            List<SlPaypalAccount> paypalAccountList = accountService.
                    list(new QueryWrapper<SlPaypalAccount>().eq("is_del", 0)
                            .eq("is_sandbox", 0));
            Map<String,SlPaypalAccount> accountMap = paypalAccountList.stream().
                    collect(Collectors.toMap(SlPaypalAccount::getAccount, item->item));
            Set<String> batchId = new HashSet<String>();
            list.forEach(item->{
                //查询订单状态。并更新结果
                SlPaypalAccount account = accountMap.get(item.getAccount());
                if(account != null){
                    try{
                        batchId.add(item.getBatchId());
                        WorldPayRequetInner(item, account);
                    }catch (Exception e){
                        log.error("查询退款状态异常", item.getCaptureId());
                    }
                }
            });
            batchId.forEach(item->{
                paypalRefundRequestService.updateRequestStatus(item);
            });
        }

    }

    private void WorldPayRequetInner(SlPaypalRefundRequestDetail item, SlPaypalAccount account) throws Exception{
        log.info("开始查询worldpay 订单状态， worldPay订单号：{}", item.getCaptureId());
        String auth = httpClient.getAuthHeader(account.getClientId(), account.getSecret());
        String url = "https://secure.worldpay.com/jsp/merchant/xml/paymentService.jsp";
        PaymentService paymentService = buildRequestOrder(item, account);
        HttpClient.HttpResponse httpResponse= httpClient.doAuthHeaderAndXML(url, paymentService, auth);
        if(httpResponse != null && httpResponse.getCode() == 200){
            log.info(httpResponse.getBody());
            try{
                PaymentService result = JAXBUtil.parseXMl(httpResponse.getBody(), PaymentService.class);
                if(result.getReply() != null && result.getReply().getOrderStatus() != null){
                    WorldPayError payError = result.getReply().getOrderStatus().getError();
                    if(payError != null){
                        item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                        item.setMessage("请求接口发生错误:"+payError.getValue());
                        item.setRefundTime(new Date());
                        paypalRefundRequestDetailService.updateById(item);
                    }else{
                        //检验退款是否成功
                        Payment payment = result.getReply().getOrderStatus().getPayment();
                        String reference = payment.getReference();
                        String lastEvent = payment.getLastEvent();
                        if(WorldPayStatusConstans.REFUNDED.equals(lastEvent)|| WorldPayStatusConstans.REFUNDED_BY_MERCHANT.equals(lastEvent)){
                            item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.COMPLETED.getCode());
                            item.setRefundTime(new Date());
                            paypalRefundRequestDetailService.updateById(item);
                        }
                        if(new Date().after(DateUtils.addMinutes(item.getRefundTime(), 30))){
                            if(WorldPayStatusConstans.REFUSED.equals(lastEvent)
                                    || WorldPayStatusConstans.CANCELLED.equals(lastEvent)
                                    || WorldPayStatusConstans.REFUND_FAILED.equals(lastEvent)
                                    || WorldPayStatusConstans.SETTLED_BY_MERCHANT.equals(lastEvent)
                                    || WorldPayStatusConstans.CHARGED_BACK.equals(lastEvent)
                                    || WorldPayStatusConstans.SETTLED.equals(lastEvent)){
                                item.setStatus(lastEvent);
                                item.setRefundTime(new Date());
                                paypalRefundRequestDetailService.updateById(item);
                            }
                        }
                    }
                }
            }catch (Exception e){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("请求接口发生错误:"+e.getMessage());
                item.setRefundTime(new Date());
                paypalRefundRequestDetailService.updateById(item);
                log.error("返回的xml解释错误:"+httpResponse.getBody());
            }
        }
    }


    private PaymentService buildRequestOrder(SlPaypalRefundRequestDetail item, SlPaypalAccount account){
        OrderInquiry orderInquiry = new OrderInquiry();
        orderInquiry.setOrderCode(item.getCaptureId());

        Inquiry inquiry = new Inquiry();
        inquiry.setOrderInquiry(orderInquiry);

        PaymentService paymentService = new PaymentService();
        paymentService.setVersion("1.4");
        paymentService.setInquiry(inquiry);
        paymentService.setMerchantCode(account.getClientId());
        return paymentService;
    }
}
