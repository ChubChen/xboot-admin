package com.chubchen.admin.quartz.jobs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.utils.SendEmailUtil;
import com.chubchen.admin.modules.easesea.constants.OrdersFinancialStatusEnum;
import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import com.chubchen.admin.modules.easesea.entity.Orders;
import com.chubchen.admin.modules.easesea.service.IAccountBackendService;
import com.chubchen.admin.modules.easesea.service.IOrdersService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author chubchen
 * OMS 订单是否缺失对照核对
 */
@Slf4j
public class OmsOrderCheckJob implements Job {

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Autowired
    private IOrdersService ordersService;

    @Autowired
    private IAccountBackendService backendService;

    @Autowired
    private SendEmailUtil sendEmailUtil;

    @Autowired
    private RestTemplate restTemplate;

    private static int PAGE_SIZE = 1000;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("定时任务job 每日检查订单是否有缺失并报警");
        String startTime = DateTime.now().minusDays(1).toString("yyyy-MM-dd 00:00:00");
        String endTime = DateTime.now().minusDays(1).toString("yyyy-MM-dd 23:59:59");
        log.info("开始查询需要核对的订单, 开始时间:{}, 结束时间：{}", startTime, endTime);
        //step1 根据下单时间查询订单。
        List<Orders> list = ordersService.list(new QueryWrapper<Orders>().between("created_at", startTime, endTime)
                .eq("financial_status", OrdersFinancialStatusEnum.paid.getCode()));
        List<AccountBackend> backendList = backendService.list();
        if(list != null && list.size() >0){
            List<String> pOrderIdList = list.stream().map(Orders::getPOrderId).collect(Collectors.toList());
            Map<String, Orders> mapOrders = list.stream().collect(Collectors.toMap(Orders::getPOrderId, item->item));
            Map<Integer, Integer> backendMap = backendList.stream().collect(
                    Collectors.toMap(AccountBackend::getBackendId, AccountBackend::getOmsId));
            int start = 0;
            int end;
            do{
                end =  start + PAGE_SIZE > pOrderIdList.size() ? pOrderIdList.size() : start + PAGE_SIZE;
                log.info("开始分页核对订单信息, start:{}, end:{}", start, end);
                StringBuilder stringBuilder = new StringBuilder();
                try{
                    List<String> queryList = pOrderIdList.subList(start,end);
                    Map<String, Object> map = new HashMap<>(1);
                    map.put("externalOrderIdList", StringUtils.join(queryList,","));
                    JSONObject object = restTemplate.getForObject("http://oms-core-server/web/orders/getOrderByExternalOrderIdList?externalOrderIdList={externalOrderIdList}",
                            JSONObject.class, map);

                    if(object.getBoolean("success")){
                        JSONArray jsonArray = object.getJSONArray("result");
                        if(jsonArray != null && jsonArray.size() > 0){
                            for (int i=0; i < jsonArray.size() ; i++) {
                                JSONObject omsOrders = jsonArray.getJSONObject(i);
                                String externalOrderId = omsOrders.getString("externalOrderId");
                                int channelId = omsOrders.getIntValue("channelId");
                                BigDecimal totalAmount = omsOrders.getBigDecimal("totalAmount");
                                Orders orders = mapOrders.get(externalOrderId);
                                if(orders == null){
                                    log.error("缺失订单,OMS 平台订单号：{}", externalOrderId);
                                    stringBuilder.append("缺失订单,OMS 平台订单号：" + externalOrderId);
                                    stringBuilder.append("\n");
                                    continue;
                                }
                                if(orders.getTotalPrice().compareTo(totalAmount) != 0){
                                    log.error("订单号: {} 金额不匹配;", orders.getId() );
                                    stringBuilder.append("订单号：" + orders.getId() + ", 金额不匹配;");
                                    stringBuilder.append("\n");
                                }
                                if(backendMap.get(orders.getBackendId()).intValue() != channelId){
                                    log.error("backendId :{} , omsId不匹配,请尽快修改;", orders.getBackendId() );
                                    stringBuilder.append("backendId:" + orders.getBackendId() + ", omsId不匹配,请尽快修改;");
                                    stringBuilder.append("\n");
                                }
                                queryList.remove(externalOrderId);
                            }
                        }
                        if(queryList.size() > 0){
                            stringBuilder.append("Oms系统缺失的平台订单号是:" + queryList.toString());
                        }
                        if(stringBuilder.length() > 0){
                            log.info("发送邮件 : {}", stringBuilder.toString());
                            //发送报警邮件
                            sendEmailUtil.sendErrorEmail("OMS系统订单对照异常", stringBuilder.toString());
                        }
                    }else{
                        log.error("调用远程服务接口失败");
                    }
                }catch (Exception e){
                    log.error("翻页对账失败", e);
                }
            } while(end < pOrderIdList.size());
        }else{
            log.info("并没有需要核对的订单信息");
        }
    }

    private JSONObject parseParameter(){
        log.info("开始执行带参数的job任务，参数：{}", this.parameter);
        return JSON.parseObject(this.parameter);
    }
}
