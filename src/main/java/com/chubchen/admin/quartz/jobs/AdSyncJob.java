package com.chubchen.admin.quartz.jobs;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.utils.DateTimeUtil;
import com.chubchen.admin.modules.easesea.constants.AdAccountStatusEnum;
import com.chubchen.admin.modules.easesea.entity.AdAccount;
import com.chubchen.admin.modules.easesea.service.IAdAccountService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

/**
 * 广告同步数据JOB
 *
 * @author chubchen
 */
@Slf4j
public class AdSyncJob implements Job {

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Autowired
    private IAdAccountService adAccountService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        log.info("定时任务同步广告账户数据");

        JSONObject jsonObject = parseParameter();
        Date startDate = jsonObject.getDate("startDate");
        Date endDate = jsonObject.getDate("endDate");
        if (startDate != null && endDate != null && endDate.after(startDate)) {
            while (startDate.before(endDate)) {
                log.info("补充历史数据到库中startDate :{}", startDate);
                Date tempEndDate = DateTimeUtil.plusDays(startDate, 1);
                DateTime date = new DateTime(startDate.getTime());
                //step1 扫描mongodb 数据库需要解析的数据
                List<AdAccount> accountList = adAccountService.list(new QueryWrapper<AdAccount>()
                        .eq("account_status", AdAccountStatusEnum.OPEN.getCode()));
                accountList.forEach(adAccount -> {
                    try {
                        adAccountService.queryTiKTokDataAndSave(adAccount, date);
                    } catch (Exception e) {
                        log.error("补充历史数据到库中失败", e);
                    }
                });
                startDate = tempEndDate;
            }
        } else {
            //step1 扫描mongodb 数据库需要解析的数据
            List<AdAccount> accountList = adAccountService.list(new QueryWrapper<AdAccount>()
                    .eq("account_status", AdAccountStatusEnum.OPEN.getCode()));
            accountList.forEach(adAccount -> adAccountService.syncPlatformData(adAccount));
        }
    }

    private JSONObject parseParameter() {
        log.info("开始执行带参数的同步广告账户数据job任务，参数：{}", this.parameter);
        if(StringUtils.isNotBlank(this.parameter)){
            return JSON.parseObject(this.parameter);
        }else{
            return new JSONObject();
        }
    }
}
