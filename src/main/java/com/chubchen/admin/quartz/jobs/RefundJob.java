package com.chubchen.admin.quartz.jobs;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.utils.DateTimeUtil;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;

@Slf4j
public class RefundJob implements Job {

    /**
     * 若参数变量名修改 QuartzJobController中也需对应修改
     */
    private String parameter;

    public void setParameter(String parameter) {
        this.parameter = parameter;
    }

    @Autowired
    private ISlPaypalRefundRequestService paypalRefundRequestService;

    @Autowired
    private ISlPaypalRefundRequestDetailService paypalRefundRequestDetailService;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        SlPaypalRefundRequest runningRequest = paypalRefundRequestService.getOne(
                new QueryWrapper<SlPaypalRefundRequest>().eq("status",
                        SlPaypalRefundRequest.SlPaypalRefundStatusEnum.RUNNING.getCode()).last("LIMIT 1")
        );
        if(runningRequest == null || StringUtils.isEmpty(runningRequest.getBatchId()) ){
            //没有处理中的状态可以执行下一个任务了。
            //查询未处理的任务开始进行处理
            SlPaypalRefundRequest slPaypalRefundRequest = paypalRefundRequestService.getOne(
                    new QueryWrapper<SlPaypalRefundRequest>().eq("status",
                            SlPaypalRefundRequest.SlPaypalRefundStatusEnum.INIT.getCode()).last("LIMIT 1")
            );
            if(slPaypalRefundRequest != null){
                paypalRefundRequestService.handelRequest(slPaypalRefundRequest);
            }
        }else{
            log.info("paypal退款存在处理中的数据，请排队等候");
            //检查处理中 可能因为重启系统造成部分为发送请求的情况需要进行重试
            if(DateTimeUtil.betweenDate(new Date(), runningRequest.getCreatedTime()) > 60 * 60 * 1000){
                //查询是否存在 INIT的状态数据，如果有则重试
                int count = paypalRefundRequestDetailService.count(
                        new QueryWrapper<SlPaypalRefundRequestDetail>()
                                .eq("batch_id", runningRequest.getBatchId())
                                .eq("status", SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.INIT.getCode()));
                if(count >0){
                    paypalRefundRequestService.handelRequestRetry(runningRequest);
                }
            }
        }
        //检查worldpay 或者adyen 是否完成
        List<SlPaypalRefundRequest> list = paypalRefundRequestService.list(new QueryWrapper<SlPaypalRefundRequest>().eq("status",
                SlPaypalRefundRequest.SlPaypalRefundStatusEnum.PENDING.getCode()));
        if(list != null && list.size()>0){
            list.forEach(item-> paypalRefundRequestService.updateRequestStatus(item.getBatchId()));
        }
    }
}
