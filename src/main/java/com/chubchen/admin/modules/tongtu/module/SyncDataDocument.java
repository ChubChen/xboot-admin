package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

/**
 *@author chubchen
 */
@Getter
@Setter
@Document
public class SyncDataDocument extends AbstractDocument{

    private String accountCode;

    private Integer type;

    private Date startTime;

    private Date endTime;

}
