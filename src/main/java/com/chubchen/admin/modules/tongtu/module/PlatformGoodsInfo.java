package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author chubchen
 */
@Getter
@Setter
public class PlatformGoodsInfo implements Serializable{

    private String product_id;

    private Integer quantity;

    private String webTransactionId;

    private String webstoreCustomLabel;

    private String webstoreItemId;

    private String webstoreSku;
}
