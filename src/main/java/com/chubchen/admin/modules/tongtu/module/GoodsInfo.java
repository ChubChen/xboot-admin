package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 商品信息
 * @author chubchen
 */
@Getter
@Setter
public class GoodsInfo implements Serializable {

    private List<PlatformGoodsInfo> platformGoodsInfoList;

    private List<TongTuGoodsInfo> tongToolGoodsInfoList;
}
