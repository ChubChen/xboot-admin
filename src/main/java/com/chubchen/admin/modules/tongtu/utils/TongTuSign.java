package com.chubchen.admin.modules.tongtu.utils;


import com.chubchen.admin.config.tongtu.TongTuConfig;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * @author chubchen
 */
@Component
public class TongTuSign {

    @Autowired
    private TongTuConfig tongTuConfig;

    public String getSign(String token,String timeStamp, String accessKey) throws Exception{
        StringBuffer sb = new StringBuffer();
        sb.append("app_token");
        sb.append(token);
        sb.append("timestamp");
        sb.append(timeStamp);
        sb.append(accessKey);
        return DigestUtils.md5Hex(sb.toString().getBytes("UTF-8"));
    }

    public Map<String, String> getBaseMap() throws Exception {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String sign = getSign(tongTuConfig.getAppToken(),timeStamp, tongTuConfig.getSecretAccessKey());
        Map<String, String> map = new HashMap<>(3);
        map.put("app_token", tongTuConfig.getAppToken());
        map.put("timestamp",timeStamp);
        map.put("sign",sign);
        return map;
    }

    public Map<String, String> getBaseMap(String token, String accessKey) throws Exception {
        String timeStamp = String.valueOf(System.currentTimeMillis());
        String sign = getSign(token,timeStamp, accessKey);
        Map<String, String> map = new HashMap<>(3);
        map.put("app_token", token);
        map.put("timestamp",timeStamp);
        map.put("sign",sign);
        return map;
    }

    public static void main(String[] args) throws Exception{
        TongTuSign  s = new TongTuSign();
        System.out.println(s.getSign("36780dd67049006cf6d86dff0bbeccf0", "1544166843695", "096ab7aa62af4b308098c4ada5fb24435382508794c849cdb6f67517793c9b9d"));
    }
}
