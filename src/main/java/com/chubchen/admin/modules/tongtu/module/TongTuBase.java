package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author chubchen
 */
@Getter
@Setter
public class TongTuBase implements Serializable {

    private String app_token;

    private String timestamp;

    private String sign;

}
