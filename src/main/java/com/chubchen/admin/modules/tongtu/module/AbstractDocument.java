package com.chubchen.admin.modules.tongtu.module;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.data.annotation.Id;

import java.io.Serializable;
import java.math.BigInteger;

/**
 * @author chubchen
 */
public class AbstractDocument implements Serializable {

    @Id
    private BigInteger id;

    /**
     * Returns the identifier of the document.
     *
     * @return the id
     */
    public BigInteger getId() {
        return id;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        AbstractDocument that = (AbstractDocument) o;

        return new EqualsBuilder()
                .append(id, that.id)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
