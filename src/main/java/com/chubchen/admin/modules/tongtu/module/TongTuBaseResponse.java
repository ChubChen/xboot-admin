package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * 通途返回response 对象
 * @author chubchen
 */
@Getter
@Setter
public class TongTuBaseResponse<T> implements Serializable {

    /**
     * code
     */
    private Integer code;

    /**
     * message
     */
    private String message;

    /**
     * 其他信息
     */
    private String others;

    /**
     * 数据
     */
    private TongTuData<T> datas;
}
