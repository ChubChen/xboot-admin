package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Document
public class GoogsQueryResponse extends AbstractDocument{

    private BigDecimal actualTotalPrice;

    private String assignstockCompleteTime;
    /**
     *买家id
     */
    private String buyerAccountId;
    /**
     *买家城市
     */
    private String buyerCity;
    /**
     *买家国家
     */
    private String buyerCountry;
    /**
     *买家邮箱
     */
    private String buyerEmail;
    /**
     *买家手机
     */
    private String buyerMobile;
    /**
     *买家名称
     */
    private String buyerName;
    /**
     *买家电话
     */
    private String buyerPhone;
    /**
     *买家省份
     */
    private String buyerState;
    /**
     *上传物流的carrier
     */
    private String carrier;
    /**
     *物流商类型 ( 0:通途API对接、 1:通途Excel文件导出、 2:通途离线生成跟踪号 3:无对接、 4:自定义Excel对接)
     */
    private String carrierType;
    /**
     *物流网络地址
     */
    private String carrierUrl;
    /**
     *
     订单发货完成时间
     */
    private String despatchCompleteTime;
    /**
     *邮寄方式名称
     */
    private String dispathTypeName;
    /**
     *订单备注
     */
    private String ebayNotes;
    /**
     *站点
     */
    private String ebaySiteEnName;
    /**
     *买家所付保费
     */
    private String insuranceIncome;
    /**
     *买家所付保费币种
     */
    private String insuranceIncomeCurrency;
    /**
     *是否作废(0,''，null 未作废，1 手工作废 2 订单任务下载永久作废 3 拆分单主单作废 4 拆分单子单作废)
     */
    private String isInvalid;
    /**
     *是否需要人工审核 (1需要人工审核,0或null不需要)
     */
    private String isSuspended;
    /**
     *承运人简称
     */
    private String merchantCarrierShortname;
    /**
     *订单总金额(商品金额+运费+保费)
     */
    private String orderAmount;
    /**
     *订单金额币种
     */
    private String orderAmountCurrency;
    /**
     *通途订单号
     */
    private String orderIdCode;
    /**
     * waitPacking/等待配货 ,waitPrinting/等待打印 ,waitingDespatching/等待发货 ,despatched/已发货
     */
    private String orderStatus;
    /**
     * 订单付款完成时间
     */
    private String paidTime;
    /**
     * 通途中平台代码
     */
    private String platformCode;
    /**
     * 平台手续费
     */
    private String platformFee;
    /**
     *
     买家邮编
     */
    private String postalCode;
    /**
     * 订单打印完成时间
     */
    private String printCompleteTime;
    /**
     * 金额小计币种
     */
    private String productsTotalCurrency;
    /**
     * 金额小计(只商品金额)
     */
    private String productsTotalPrice;
    /**
     * 收货地址
     */
    private String receiveAddress;
    /**
     * 退款时间
     */
    private String refundedTime;
    /**
     *
     卖家账号
     */
    private String saleAccount;
    /**
     * 订单生成时间
     */
    private String saleTime;
    /**
     * 平台订单号
     */
    private String salesRecordNumber;
    /**
     * 买家所支付的运费
     */
    private String shippingFeeIncome;
    /**
     *买家所付运费币种
     */
    private String shippingFeeIncomeCurrency;
    /**
     * 发货截止时间
     */
    private String shippingLimiteDate;
    /**
     * 税费币种
     */
    private String taxCurrency;
    /**
     * 税费
     */
    private String taxIncome;
    /**
     * 订单付款完成时间
     */
    private String warehouseIdKey;
    /**
     * 仓库名称
     */
    private String warehouseName;
    /**
     * 平台交易号
     */
    private String webstoreOrderId;
    /**
     * 平台站点id
     */
    private String webstore_item_site;
    /**
     * 物流信息
     */
    private List<TrackingInfo> packageInfoList;

    /**
     * 订单明细
     */
    private List<OrderDetail> orderDetails;

    /**
     * goodsInfo
     */
    private GoodsInfo goodsInfo;

}