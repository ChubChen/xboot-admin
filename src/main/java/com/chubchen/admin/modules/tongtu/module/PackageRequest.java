package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chubchen
 */
@Getter
@Setter
public class PackageRequest {

    /**
     * 配货开始时间
     */
    private String assignTimeFrom;
    /**
     * 配货结束时间
     */
    private String assignTimeTo;
    /**
     * 发货开始时间
     */
    private String despatchTimeFrom;
    /**
     * 发货结束时间
     */
    private String orderId;
    private String despatchTimeTo;
    private String merchantId;
    /**
     *  包裹状态： waitPrint 等待打印 waitDeliver 等待发货 delivered 已发货 cancel 作废
     */
    private String packageStatus;

    private Integer pageNo;

    private Integer pageSize;

    private String shippingMethodName;


}
