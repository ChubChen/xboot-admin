package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

/**
 * @author chubchen
 */
@Getter
@Setter
public class OrdersQueryRequest extends TongTuBase{

    /**
     * 账号
     */
    private String accountCode;
    /**
     * 买家邮箱  否
     */
    private String buyerEmail;
    /**
     * 商户ID  是
     */
    private String merchantId;
    /**
     *订单号 否
     */
    private String orderId;
    /**
     * 订单状态 waitPacking/等待配货 ,waitPrinting/等待打印,waitingDespatching/等待发货 ,despatched/已发货
     */
    private String orderStatus;
    /**
     * 页数
     */
    private Integer pageNo;
    /**
     * 数量
     */
    private Integer pageSize;
    /**
     * 付款起始时间
     */
    private String payDateFrom;
    /**
     * 付款结束时间
     */
    private String payDateTo;
    /**
     * 通途中平台代码
     */
    private String platformCode;
    /**
     * 退款起始时间
     */
    private String refundedDateFrom;
    /**
     * 退款结束时间
     */
    private String refundedDateTo;
    /**
     * 销售起始时间
     */
    private String saleDateFrom;
    /**
     * 销售结束时间
     */
    private String saleDateTo;
    /**
     * 是否需要查询1年表和归档表数据（根据时间参数或者全量查询订单的时候使用该参数，”0”查询活跃表，”1”为查询1年表，”2”为查询归档表，默认为”0”）
     */
    private String storeFlag = "0";
    /**
     * 更新开始时间
     */
    private String updatedDateFrom;
    /**
     * 更新结束时间
     */
    private String updatedDateTo;




}
