package com.chubchen.admin.modules.tongtu.module;


import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chubchen
 */
@Getter
@Setter
public class OrderDetail implements Serializable{

    private Integer goodsMatchedQuantity;

    private String goodsMatchedSku;

    private Integer quantity;

    private BigDecimal transaction_price;

    private String webstore_custom_label;

    private String webstore_item_id;

    private String webstore_sku;
}
