package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author chubchen
 */
@Getter
@Setter
public class TongTuData<T> {

    /**
     * page
     */
    private Integer pageNo;

    /**
     * pageSize
     */
    private Integer pageSize;

    /**
     * 数据对象
     */
    private List<T> array;

}
