package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chubchen
 */
@Getter
@Setter
public class TongTuGoodsInfo implements Serializable{

    private BigDecimal goodsAverageCost;

    private BigDecimal goodsCurrentCost;

    private String goodsImageGroupId;
    private BigDecimal goodsPackagingCost;
    private BigDecimal goodsPackagingWeight;
    private String goodsSku;
    private String goodsTitle;
    private BigDecimal goodsWeight;
    private BigDecimal packageHeight;
    private BigDecimal packageLength;
    private BigDecimal packageWidth;
    private BigDecimal packagingCost;
    private BigDecimal packagingWeight;
    private BigDecimal productAverageCost;
    private BigDecimal productCurrentCost;
    private BigDecimal productHeight;
    private BigDecimal productLength;
    private String productName;
    private BigDecimal productWeight;
    private BigDecimal quantity;
    private String wotId;
    private BigDecimal productWidth;


}
