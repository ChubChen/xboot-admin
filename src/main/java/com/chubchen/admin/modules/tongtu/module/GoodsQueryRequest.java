package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author chubchen
 */
@Getter
@Setter
public class GoodsQueryRequest {

    private String categoryName;
    private String merchantId;
    private Integer pageNo;
    private Integer pageSize;
    private String productStatus;
    private String productType;
    private List<String> skuAliases;
    private List<String> skus;
    private String supplierName;
    private String updatedDateBegin;
    private String updatedDateEnd;
}
