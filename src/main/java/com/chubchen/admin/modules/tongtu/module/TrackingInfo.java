package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author chubchen
 */
@Getter
@Setter
public class TrackingInfo implements Serializable{

    /**
     * trackingNumber
     */
    private String trackingNumber;

    /**
     * 物流跟踪号获取状态(00:未就绪 01:就绪 02:处理中 03:处理成功 04:处理失败)
     */
    private String trackingNumberStatus;

    /**
     * 物流跟踪号获取时间
     */
    private String trackingNumberTime;
}
