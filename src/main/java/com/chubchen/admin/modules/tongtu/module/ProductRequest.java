package com.chubchen.admin.modules.tongtu.module;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author chubchen
 */
@Getter
@Setter
public class ProductRequest extends TongTuBase {

    private String merchantId;
    private String productCategoryId;
    private List<String> productGoodsIdList;
    private String queryType;
    private List<String> skuList;
    private String updatedEndTime;
    private String updatedStartTime;
    private Integer pageNum;
    private Integer pageSize;

}
