package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-12
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ShippingAddress对象", description="")
public class ShippingAddress extends Model<ShippingAddress> {

    @Tolerate
    public ShippingAddress(){

    }

    private static final long serialVersionUID = 1L;

    private String id;

    private String orderId;

    @ApiModelProperty(value = "地址1")
    private String address1;

    @ApiModelProperty(value = "地址2")
    private String address2;

    @ApiModelProperty(value = "城市")
    private String city;

    @ApiModelProperty(value = "国家")
    private String country;

    @ApiModelProperty(value = "first_name")
    private String firstName;

    @ApiModelProperty(value = "last_name")
    private String lastName;

    @ApiModelProperty(value = "电话号")
    private String phone;

    private String zip;

    @ApiModelProperty(value = "姓名")
    private String name;

    @ApiModelProperty(value = "国家code")
    private String countryCode;

    @ApiModelProperty(value = "省份code")
    private String provinceCode;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
