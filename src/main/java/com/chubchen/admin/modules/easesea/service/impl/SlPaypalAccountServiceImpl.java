package com.chubchen.admin.modules.easesea.service.impl;

import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalAccountMapper;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-30
 */
@Service
public class SlPaypalAccountServiceImpl extends ServiceImpl<SlPaypalAccountMapper, SlPaypalAccount> implements ISlPaypalAccountService {

    @Override
    public  IPage<SlPaypalAccount> findListByPage(PageVo pageVo, SlPaypalAccount slPaypalAccount){
        IPage<SlPaypalAccount> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, slPaypalAccount);
    }

    @Override
    public List<SlPaypalAccount> findListAll(SlPaypalAccount slPaypalAccount){
        return baseMapper.selectAll(slPaypalAccount);
    }

    @Override
    public int add(SlPaypalAccount slPaypalAccount){
        return baseMapper.insert(slPaypalAccount);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(SlPaypalAccount slPaypalAccount){
        return baseMapper.updateById(slPaypalAccount);
    }

    @Override
    public SlPaypalAccount findById(Long id){
        return  baseMapper.selectById(id);
    }
}
