package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

/**
 * 订单支付状态
 * @author chubchen
 */
@Getter
public enum OrdersFinancialStatusEnum {

    waiting("waiting","待支付"),
    paying("paying","支付中"),
    partially_paid("partially_paid","部分已支付"),
    paid("paid","已完成支付"),
    partially_refunded("partially_refunded","部分退款"),
    refunded("refunded","已退款"),
    voided("voided","无效的");

    private String code;

    private String desc;

    OrdersFinancialStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
