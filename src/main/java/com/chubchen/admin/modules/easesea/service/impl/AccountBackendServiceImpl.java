package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.modules.easesea.constants.BackendStatusEnum;
import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import com.chubchen.admin.modules.easesea.dao.mapper.AccountBackendMapper;
import com.chubchen.admin.modules.easesea.service.IAccountBackendService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-05
 */
@Service
public class AccountBackendServiceImpl extends ServiceImpl<AccountBackendMapper, AccountBackend> implements IAccountBackendService {

    @Override
    public  IPage<AccountBackend> findListByPage(PageVo pageVo, AccountBackend accountBackend){
        IPage<AccountBackend> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountBackend);
    }

    @Override
    public List<AccountBackend> findListAll(AccountBackend accountBackend){
        return baseMapper.selectAll(accountBackend);
    }

    @Override
    public int add(AccountBackend accountBackend){
        return baseMapper.insert(accountBackend);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountBackend accountBackend){
        return baseMapper.updateById(accountBackend);
    }

    @Override
    public AccountBackend findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public Integer updateStatusById(Integer status, Integer[] ids) {
        return baseMapper.updateStatusById(status, ids);
    }


}
