package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.utils.SnowFlakeUtil;
import com.chubchen.admin.modules.easesea.constants.DataParseConstants;
import com.chubchen.admin.modules.easesea.constants.DataParseType;
import com.chubchen.admin.modules.easesea.constants.MongodbCollection;
import com.chubchen.admin.modules.easesea.constants.PlatformConstants;
import com.chubchen.admin.modules.easesea.entity.*;
import com.chubchen.admin.modules.easesea.service.*;
import io.swagger.annotations.Scope;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@Slf4j
@Component
public abstract class AbstractDataParseHandle implements DataParseHandle {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private IOrdersService ordersService;

    @Autowired
    private IOrderItemsService orderItemsService;

    @Autowired
    private IOrderFulfillmentsService orderFulfillmentsService;

    @Autowired
    private IOrderFulfillmentItemsService orderFulfillmentItemsService;

    @Autowired
    private IShippingAddressService shippingAddressService;

    @Autowired
    private IPlatformProductsService platformProductsService;

    @Autowired
    private IPlatformProductVariantsService platformProductVariantsService;

    @Autowired
    private IAbandonedOrdersService abandonedOrdersService;

    @Autowired
    private IAbandonedOrderItemsService abandonedOrderItemsService;

    static final Pattern spuPattern = Pattern.compile("(GO[A-Z]{2}[0-9]+[A-Z])([(0-9)\\-]+)");

    static int pageSize = 500;

    @Override
    public void handle(JSONObject dataParse) {
        try{
            // step1 校验数据
            boolean flag = this.validateParams(dataParse);
            if(!flag){
                log.info("解析校验失败{}", dataParse.toJSONString());
                return;
            }
            // step2 更新mongo的状态到 处理中
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_ING);

            String type = dataParse.getString("type");
            if(DataParseType.toSet().contains(type)){
                // step3 处理数据
                this.parseData(dataParse);
            }else{
                log.info("暂时不支持该type:{},数据的解析跳过", type);
            }

            //step4 更新mongo状态到 处理成功
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_FINISH);
        }catch (Exception e){
            log.error("解析数据转换失败", e);
            //step5 更新mongo状态到 处理失败
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_ERROR);
        }
    }

    
    boolean validateParams(JSONObject dataParse){
        String type = dataParse.getString("type");
        Date startDate = dataParse.getDate("startDate");
        Date endData = dataParse.getDate("endDate");
        if(StringUtils.isNotBlank(type)
                && startDate != null
                && endData != null
                && startDate.before(endData)){
            return true;
        }else{
            return false;
        }
    }

    String getMongoCollectionName(JSONObject dataParse){
        String type = dataParse.getString("type");
        String platform = dataParse.getString("platform");
        if(PlatformConstants.SHOPIFY.equals(platform)){
            return type;
        }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
            return PlatformConstants.SHOPLAZZA+"_"+type;
        }else{
           log.error("暂时不支持{}平台数据解析", platform);
           return null;
        }
    }

    /**
     * 订单数据方法入口
     */
    void parseData(JSONObject dataParse) {
        //step 1 查询要解析的订单数据
        Date startDate = dataParse.getDate("startDate");
        Date endDate = dataParse.getDate("endDate");
        String collectionName = this.getMongoCollectionName(dataParse);
        log.info("开始解析数据，开始时间：{}， 结束时间：{}, 集合名称：{}", startDate, endDate, collectionName);
        Query queryCount = new Query(Criteria.where("lastModified").gte(startDate).lte(endDate));
        if(StringUtils.isNotBlank(collectionName)){
            long count = mongoTemplate.count(queryCount, collectionName);
            log.info("开始解析数据，总数量:{}", count);
            long start = 0;
            while (start < count) {
                log.info("分页解析数据当前开始数量:{}，总共数量:{}, 当前集合：{}", start, count, collectionName);
                Query query = new Query(Criteria.where("lastModified").gte(startDate).lte(endDate)).skip(start).limit(pageSize);
                List<JSONObject> dataList = mongoTemplate.find(query, JSONObject.class, collectionName);
                if(dataList != null && dataList.size() > 0){
                    this.parseDataByType(dataParse, dataList);
                }
                start = start + pageSize;
            }
        }
    }

    /**
     *  根据type判断数据进入那个方法解析
     * @param dataList
     */
    void parseDataByType(JSONObject dataParse, List<JSONObject> dataList){
        String type = dataParse.getString("type");
        String platform = dataParse.getString("platform");
        if(DataParseType.Orders.getCode().equals(type)){
            this.parseOrders(dataList, platform);
        }else if(DataParseType.Products.getCode().equals(type)){
            this.parseProduct(dataList, platform);
        }else if(DataParseType.Checkouts.getCode().equals(type)){
            this.parseCheckout(dataList, platform);
        }

    }

    String parseSkuToSpu(String sku){
        if(StringUtils.isNotBlank(sku)){
            Matcher m = spuPattern.matcher(sku);
            if(sku.startsWith("GO") && m.matches()){
                return m.group(1);
            }else if((sku.startsWith("U") || sku.startsWith("YZH")) && sku.length() >= 9){
                return sku.substring(0,9);
            }else{
               return "";
            }
        }else{
            return "";
        }
    }

    /**
     * 产品解析方法
     * @param dataList
     */
    public abstract void parseOrders(List<JSONObject> dataList, String platform);


    /**
     * 弃购解析方法
     * @param dataList
     */
    public abstract void parseCheckout(List<JSONObject> dataList, String platform);

    /**
     * 产品解析方法
     * @param dataList
     */
    public abstract void parseProduct(List<JSONObject> dataList, String platform);

    /**
     * 构建订单到list中
     * @param jsonOrder
     * @param list
     * @param itemList
     * @param orderFulfillments
     * @param orderFulfillmentItemssList
     * @param shippingAddresses
     */
    void thisBuildOrder(JSONObject jsonOrder, List<Orders> list, List<OrderItems> itemList,
                        List<OrderFulfillments> orderFulfillments, List<OrderFulfillmentItems> orderFulfillmentItemssList,
                        List<ShippingAddress> shippingAddresses,String platform ){
        Orders orders = this.buildOrder(jsonOrder);
        list.add(orders);
        itemList.addAll(this.buildOrderItems(jsonOrder, orders, platform));
        List<OrderFulfillments> orderFulfillmentsList = this.buildFulfillments(jsonOrder, orders, platform);
        if(!orderFulfillmentsList.isEmpty()){
            orderFulfillments.addAll(orderFulfillmentsList);
            orderFulfillmentsList.forEach(item->orderFulfillmentItemssList.addAll(item.getOrderFulfillmentItemsList()));
        }
        ShippingAddress shippingAddress = this.buildShippingAddress(jsonOrder, orders);
        if(shippingAddress != null){
            shippingAddresses.add(shippingAddress);
        }
    }
    /**
     * 构建订单对象
     * @param jsonOrder
     * @return
     */
    public abstract Orders buildOrder(JSONObject jsonOrder);

    /**
     * 根据平台对象构建本地订单明细信息
     * @param jsonOrder
     * @param orders
     * @return
     */
    public List<OrderItems> buildOrderItems(JSONObject jsonOrder, Orders orders, String platform){
        JSONArray jsonArray = jsonOrder.getJSONArray("line_items");
        if(jsonArray != null && jsonArray.size() >0){
            List<OrderItems> list = new ArrayList<>(jsonArray.size());
            for (int i = 0; i < jsonArray.size() ; i++) {
                JSONObject itemJson = jsonArray.getJSONObject(i);
                if(itemJson != null){
                    OrderItems orderItems = OrderItems.builder().id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                            .pOrderId(orders.getPOrderId())
                            .domainId(orders.getDomainId())
                            .backendId(orders.getBackendId())
                            .orderId(orders.getId())
                            .pVariantTitle(itemJson.getString("variant_title"))
                            .sku(itemJson.getString("sku"))
                            .quantity(itemJson.getInteger("quantity"))
                            .price(itemJson.getBigDecimal("price"))
                            .build();
                    if(PlatformConstants.SHOPIFY.equals(platform)){
                        orderItems.setPLineItemId(String.valueOf(itemJson.getLong("id")));
                        orderItems.setPProductTitle(itemJson.getString("title"));
                        orderItems.setPProductId(String.valueOf(itemJson.getLong("product_id")));
                        orderItems.setPVariantId(String.valueOf(itemJson.getLong("variant_id")));
                        orderItems.setCreatedAt(itemJson.getDate("created_at"));
                        orderItems.setPlacedAt(orders.getPlacedAt());
                    }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
                        orderItems.setPLineItemId(itemJson.getString("id"));
                        orderItems.setPProductTitle(itemJson.getString("product_title"));
                        orderItems.setPProductId(itemJson.getString("product_id"));
                        orderItems.setPVariantId(itemJson.getString("variant_id"));
                        orderItems.setCreatedAt(orders.getCreatedAt());
                        orderItems.setPlacedAt(orders.getPlacedAt());
                    }
                    list.add(orderItems);
                }
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    /**
     * 根据订单信息构建运单明细
     * @param jsonOrder
     * @param orders
     * @return
     */
    public abstract List<OrderFulfillments> buildFulfillments(JSONObject jsonOrder, Orders orders, String platform);

    /**
     * 根据订单构建运单明细
     * @param fulfillment
     * @param orderFulfillments
     * @return
     */
    public List<OrderFulfillmentItems> buildFulfillmentItems(JSONObject fulfillment, OrderFulfillments orderFulfillments, String platform){
        JSONArray lineItems = fulfillment.getJSONArray("line_items");
        if(lineItems != null && lineItems.size()>0){
            List<OrderFulfillmentItems> list = new ArrayList<>();
            for (int i = 0; i < lineItems.size() ; i++) {
                JSONObject itemJson = lineItems.getJSONObject(i);
                OrderFulfillmentItems fillmentItems = OrderFulfillmentItems.builder()
                        .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                        .fulfillmentId(orderFulfillments.getId())
                        .orderId(orderFulfillments.getOrderId())
                        .pFulfillmentId(orderFulfillments.getPFulfillmentId())
                        .sku(itemJson.getString("sku"))
                        .quantity(itemJson.getInteger("quantity"))
                        .createdAt(itemJson.getDate("created_at"))
                        .updatedAt(itemJson.getDate("updated_at"))
                        .build();
                if(PlatformConstants.SHOPIFY.equals(platform)){
                    fillmentItems.setPFulfillmentId(String.valueOf(itemJson.getLong("id")));
                    fillmentItems.setPProductId(String.valueOf(itemJson.getLong("product_id")));
                    fillmentItems.setPVariantId(String.valueOf(itemJson.getLong("variant_id")));
                }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
                    fillmentItems.setPFulfillmentId(itemJson.getString("id"));
                    fillmentItems.setPProductId(itemJson.getString("product_id"));
                    fillmentItems.setPVariantId(itemJson.getString("variant_id"));
                }
                list.add(fillmentItems);
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    /**
     * 构建运单信息
     * @param jsonOrder
     * @param orders
     * @return
     */
    public ShippingAddress buildShippingAddress(JSONObject jsonOrder, Orders orders){
        JSONObject jsonShipping = jsonOrder.getJSONObject("shipping_address");
        if(jsonShipping != null){
            ShippingAddress shippingAddress = ShippingAddress.builder()
                    .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                    .orderId(orders.getId())
                    .address1(jsonShipping.getString("address1"))
                    .address2(jsonShipping.getString("address2"))
                    .city(jsonShipping.getString("city"))
                    .country(jsonShipping.getString("country"))
                    .countryCode(jsonShipping.getString("country_code"))
                    .firstName(jsonShipping.getString("first_name"))
                    .lastName(jsonShipping.getString("last_name"))
                    .phone(jsonShipping.getString("phone"))
                    .name(jsonShipping.getString("name"))
                    .provinceCode(jsonShipping.getString("province_code"))
                    .zip(jsonShipping.getString("zip"))
                    .build();
            return shippingAddress;
        }else{
            return null;
        }
    }

    /**
     * 构建产品信息
     * @param jsonProduct
     * @return
     */
    public PlatformProducts buildProduct(JSONObject jsonProduct, String platform){
        PlatformProducts products = PlatformProducts.builder()
                .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                .backendId(jsonProduct.getInteger("backend_id"))
                .handle(jsonProduct.getString("handle"))
                .title(jsonProduct.getString("title"))
                .imageSrc(jsonProduct.getJSONObject("image") != null ? jsonProduct.getJSONObject("image").getString("src"): "")
                .createdAt(jsonProduct.getDate("created_at"))
                .updatedAt(jsonProduct.getDate("updated_at"))
                .build();
        String tags = jsonProduct.getString("tags");
        if(StringUtils.isNotBlank(tags) && tags.length() > 512){
            tags = tags.substring(0,500);
        }
        products.setTags(tags);
        if(PlatformConstants.SHOPIFY.equals(platform)){
            products.setPProductId(String.valueOf(jsonProduct.getLong("id")));
        }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
            products.setPProductId(jsonProduct.getString("id"));
        }
        return products;
    }

    /**
     * 构建产品变体信息
     * @param jsonProduct
     * @param products
     * @return
     */
    public abstract List<PlatformProductVariants> buildProductVariants(JSONObject jsonProduct, PlatformProducts products);

    /**
     * 构建生成AbandonedOrders
     * @param jsonOrder
     * @return
     */
    public AbandonedOrders buildAbandonedOrder(JSONObject jsonOrder, String platform){
        AbandonedOrders abandonedOrders = AbandonedOrders.builder()
                .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                .backendId(jsonOrder.getInteger("backend_id"))
                .domainId(jsonOrder.getInteger("domain_id"))
                .createdAt(jsonOrder.getDate("created_at"))
                .abandonedUrl(jsonOrder.getString("abandoned_checkout_url"))
                .build();
        JSONObject customer = jsonOrder.getJSONObject("customer");
        if(customer != null){
            abandonedOrders.setEmail(customer.getString("email"));
            abandonedOrders.setFirstName(customer.getString("first_name"));
            abandonedOrders.setLastName(customer.getString("last_name"));
            abandonedOrders.setOrdersCount(customer.getInteger("orders_count"));
            abandonedOrders.setTotalSpent(customer.getBigDecimal("total_spent"));
            JSONObject default_address = customer.getJSONObject("default_address");
            abandonedOrders.setCountry( default_address!= null ? default_address.getString("country") : "");
        }
        if(PlatformConstants.SHOPIFY.equals(platform)){
            abandonedOrders.setGateway(jsonOrder.getString("gateway"));
            abandonedOrders.setSubtotalPrice(jsonOrder.getBigDecimal("subtotal_price"));
            abandonedOrders.setPOrderId(String.valueOf(jsonOrder.getLong("id")));
        }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
            JSONObject jsonObject = jsonOrder.getJSONObject("payment_line");
            abandonedOrders.setGateway(jsonObject != null ? jsonObject.getString("payment_channel") : "");
            abandonedOrders.setSubtotalPrice(jsonOrder.getBigDecimal("sub_total"));
            abandonedOrders.setPOrderId(jsonOrder.getString("id"));
        }
        return abandonedOrders;
    }

    /**
     * 构建生成AbandonedOrderItems
     * @param jsonOrder
     * @param order
     * @return
     */
    public  List<AbandonedOrderItems> buildAbandonedOrderItems(JSONObject jsonOrder, AbandonedOrders order, String platform){
        JSONArray jsonArray = jsonOrder.getJSONArray("line_items");
        if(jsonArray != null && jsonArray.size() >0){
            List<AbandonedOrderItems> list = new ArrayList<>(jsonArray.size());
            for (int i = 0; i < jsonArray.size() ; i++) {
                JSONObject itemJson = jsonArray.getJSONObject(i);
                if(itemJson != null){
                    AbandonedOrderItems orderItems = AbandonedOrderItems.builder()
                            .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                            .pOrderId(order.getPOrderId())
                            .domainId(order.getDomainId())
                            .backendId(order.getBackendId())
                            .orderId(order.getId())
                            .pProductTitle(itemJson.getString("product_title"))
                            .pVariantTitle(itemJson.getString("variant_title"))
                            .sku(itemJson.getString("sku"))
                            .quantity(itemJson.getInteger("quantity"))
                            .price(itemJson.getBigDecimal("price"))
                            .createdAt(order.getCreatedAt())
                            .build();
                    if(PlatformConstants.SHOPIFY.equals(platform)){
                        orderItems.setPLineItemId(SnowFlakeUtil.getFlowIdInstance().nextIdStr());
                        orderItems.setPProductId(String.valueOf(itemJson.getLong("product_id")));
                        orderItems.setPVariantId(String.valueOf(itemJson.getLong("variant_id")));
                    }else if(PlatformConstants.SHOPLAZZA.equals(platform)){
                        orderItems.setPLineItemId(itemJson.getString("id"));
                        orderItems.setPProductId(itemJson.getString("product_id"));
                        orderItems.setPVariantId(itemJson.getString("variant_id"));
                    }
                    list.add(orderItems);
                }
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    /**
     * 保存数据
     * @param list
     * @param itemList
     */
    @DS("easesea")
    @Transactional(rollbackFor = Exception.class)
    public void saveAbandonedOrder(List<AbandonedOrders> list, List<AbandonedOrderItems> itemList){
        //step1 根据业务id进行删除
        List<AbandonedOrders> exists = abandonedOrdersService.findByUniqueId(list);
        if(exists != null && exists.size()>0){
            List<String> orderIds = exists.stream().map(AbandonedOrders::getId).collect(Collectors.toList());
            abandonedOrdersService.removeByIds(orderIds);
            abandonedOrderItemsService.remove(new QueryWrapper<AbandonedOrderItems>().in("order_id", orderIds));
        }
        if(list != null && !list.isEmpty()){
            abandonedOrdersService.saveBatch(list);
            abandonedOrderItemsService.saveBatch(itemList);
        }
    }

    @DS("easesea")
    @Transactional(rollbackFor = Exception.class)
    public void saveOrders(List<Orders> ordersList,
                           List<OrderItems> orderItemsList,
                           List<OrderFulfillments> fulfillmentsList,
                           List<OrderFulfillmentItems> fulfillmentItemsList,
                           List<ShippingAddress> shippingAddressList) {
        //step1 根据业务id进行删除
        List<Orders> exists = ordersService.findByUniqueId(ordersList);
        if(exists != null && exists.size()>0){
            List<String> orderIds = exists.stream().map(Orders::getId).collect(Collectors.toList());
            ordersService.removeByIds(orderIds);
            orderItemsService.remove(new QueryWrapper<OrderItems>().in("order_id", orderIds));
            shippingAddressService.remove(new QueryWrapper<ShippingAddress>().in("order_id", orderIds));
            orderFulfillmentsService.remove(new QueryWrapper<OrderFulfillments>().in("order_id", orderIds));
            orderFulfillmentItemsService.remove(new QueryWrapper<OrderFulfillmentItems>().in("order_id", orderIds));
        }
        if(ordersList != null && !ordersList.isEmpty()){
            log.info("开始批量保存数据，订单数量是:{}", ordersList.size());
            ordersService.saveBatch(ordersList);

            if(shippingAddressList != null && !shippingAddressList.isEmpty()){
                shippingAddressService.saveBatch(shippingAddressList);
            }

            if(orderItemsList != null && !orderItemsList.isEmpty()){
                orderItemsService.saveBatch(orderItemsList);
            }
            if(fulfillmentsList != null && !fulfillmentsList.isEmpty()){
                orderFulfillmentsService.saveBatch(fulfillmentsList);
            }
            if(fulfillmentItemsList != null && !fulfillmentItemsList.isEmpty()){
                orderFulfillmentItemsService.saveBatch(fulfillmentItemsList);
            }
        }
    }

    @DS("easesea")
    @Transactional(rollbackFor = Exception.class)
    public void saveProducts(List<PlatformProducts> list, List<PlatformProductVariants> variantList) {
        List<PlatformProducts> exists = platformProductsService.findByUniqueId(list);
        if(exists != null && exists.size()>0){
            List<String> productIds = exists.stream().map(PlatformProducts::getId).collect(Collectors.toList());
            platformProductsService.removeByIds(productIds);
            platformProductVariantsService.remove(new QueryWrapper<PlatformProductVariants>().in("product_id", productIds));
        }
        if(list != null && !list.isEmpty()){
            platformProductsService.saveBatch(list);
        }
        if(variantList != null && !variantList.isEmpty()){
            //step 1 根据产品id删除全部变体进行插入
            platformProductVariantsService.saveBatch(variantList);
        }

    }

    void updateParseStatus(JSONObject dataParse, int status) {
        Update update = new Update();
        update.set("status", status);
        String uniqueId = dataParse.getString("uniqId");
        mongoTemplate.updateFirst(new Query(Criteria.where("uniqId").is(uniqueId)),
                update, MongodbCollection.DATA_PARSE_QUEUE);
    }
}
