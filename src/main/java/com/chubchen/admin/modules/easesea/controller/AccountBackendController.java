package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.easesea.constants.BackendStatusEnum;
import com.chubchen.admin.modules.easesea.constants.PlatformConstants;
import com.chubchen.admin.modules.easesea.entity.AccountDomain;
import com.chubchen.admin.modules.easesea.service.IEaseseaAccountDomainService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAccountBackendService;
import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-05
 */
@RestController
@RequestMapping("/xboot/easesea/account-backend")
public class AccountBackendController {

    @Resource
    private IAccountBackendService accountBackendService;

    @Resource
    private IEaseseaAccountDomainService accountDomainService;


    @ApiOperation(value = "新增")
    @SystemLog(description = "增加站点信息", type = LogType.OPERATION)
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AccountBackend accountBackend){
        Result result = validateParam(accountBackend);
        if(result.isSuccess()){
            AccountBackend exists = accountBackendService.getOne(new QueryWrapper<AccountBackend>().eq("platform", accountBackend.getPlatform())
                    .eq("platform_name", accountBackend.getPlatformName()));
            if(exists == null || exists.getBackendId() != null){
                accountBackend.setCreatedAt(new Date());
                Integer count = accountBackendService.add(accountBackend);
                if(count > 0){
                    return new ResultUtil<Integer>().setData(count);
                }else{
                    return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                }
            }else{
                return new ResultUtil<Integer>().setErrorMsg("该平台账号已经添加过，请不要重复添加");
            }
        }else{
            return result;
        }
    }


    @ApiOperation(value = "新增")
    @SystemLog(description = "增加站点信息", type = LogType.OPERATION)
    @PostMapping("/import")
    public Result<Integer> importAdd(@RequestBody List<AccountBackend> accountBackendList){
        int count = 0;
        for (AccountBackend item: accountBackendList) {
            Result result = validateParam(item);
            if(result.isSuccess()){
                AccountBackend exists = accountBackendService.getOne(new QueryWrapper<AccountBackend>().eq("platform", item.getPlatform())
                        .eq("platform_name", item.getPlatformName()));
                if(exists == null || exists.getBackendId() != null) {
                    item.setCreatedAt(new Date());
                    item.setUpdatedAt(new Date());
                    count += accountBackendService.add(item);
                }
            }
        }
        return new ResultUtil<Integer>().setSuccessMsg("成功导入"+ count+ "条数据");
    }

    private Result validateParam(AccountBackend accountBackend){
        if(StringUtils.isBlank(accountBackend.getAdminUrl())){
            return new ResultUtil<Integer>().setErrorMsg("adminUrl参数必填");
        }
        if(StringUtils.isBlank(accountBackend.getStoreName())){
            return new ResultUtil<Integer>().setErrorMsg("店铺名称参数必填");
        }
        if(StringUtils.isBlank(accountBackend.getPlatform())){
            return new ResultUtil<Integer>().setErrorMsg("platform参数必填");
        }
        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除")
    @SystemLog(description = "删除站点信息", type = LogType.OPERATION)
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountBackendService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @SystemLog(description = "更新站点信息", type = LogType.OPERATION)
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AccountBackend accountBackend){
        Result result = validateParam(accountBackend);
        if(result.isSuccess()) {
            accountBackend.setUpdatedAt(new Date());
            Integer count = accountBackendService.updateData(accountBackend);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AccountBackend>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountBackend accountBackend){
         IPage<AccountBackend> page = accountBackendService.findListByPage(pageVo, accountBackend);
         return new ResultUtil<IPage<AccountBackend>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AccountBackend> findById(@PathVariable Long id){
        AccountBackend accountBackend=  accountBackendService.findById(id);
        return new ResultUtil<AccountBackend>().setData(accountBackend);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountBackend>> findById(@ModelAttribute AccountBackend accountBackend){
        List<AccountBackend> accountBackendList =  accountBackendService.findListAll(accountBackend);
        return new ResultUtil<List<AccountBackend>>().setData(accountBackendList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getExceptionType(){
        Map<Integer, String> map = Arrays.stream(BackendStatusEnum.values()).collect(
                Collectors.toMap(BackendStatusEnum::getCode, BackendStatusEnum::getDesc));
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "启用或者关闭状态", type = LogType.OPERATION)
    @ApiOperation(value = "开启或者关闭单个站点")
    public Result<Integer> enable(@RequestParam Integer[] ids, @RequestParam Integer status){
        if(!BackendStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Integer>().setErrorMsg("不存在的状态定义");
        }
        if(BackendStatusEnum.DISABLE.getCode() == status){
            List<AccountBackend> list = accountBackendService.listByIds(Arrays.asList(ids));
            list.forEach((item)->{
                if(PlatformConstants.SHOPIFY.equals(item.getPlatform())){
                    accountDomainService.update(new UpdateWrapper<AccountDomain>().set("shopify_diabled", true).eq("backend_id", item.getBackendId()));
                }
            });
        }
        Integer count = accountBackendService.updateStatusById(status, ids);
        return new ResultUtil<Integer>().setData(count);
    }

}
