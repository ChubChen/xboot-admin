package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillments;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IOrderFulfillmentsService extends IService<OrderFulfillments> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param orderFulfillments 过滤条件
     * @return IPage<OrderFulfillments>
     */
    IPage<OrderFulfillments> findListByPage(PageVo pageVo, OrderFulfillments orderFulfillments);

   /**
    * 查询分页数据
    *
    * @return  List<OrderFulfillments>
    */
    List<OrderFulfillments> findListAll(OrderFulfillments orderFulfillments);

    /**
     * 添加
     *
     * @param orderFulfillments 
     * @return int
     */
    int add(OrderFulfillments orderFulfillments);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param orderFulfillments 
     * @return int
     */
    int updateData(OrderFulfillments orderFulfillments);

    /**
     * id查询数据
     *
     * @param id id
     * @return OrderFulfillments
     */
    OrderFulfillments findById(Long id);
}
