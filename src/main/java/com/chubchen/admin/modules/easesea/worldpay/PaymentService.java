package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.*;

/**
 *  @author chubchen
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class PaymentService {

    @XmlAttribute
    private String merchantCode;

    @XmlAttribute
    private String version;

    @XmlElement(name = "modify")
    private Modify op;

    @XmlElement(name = "reply")
    private Reply reply;

    @XmlElement
    private Inquiry inquiry;

}
