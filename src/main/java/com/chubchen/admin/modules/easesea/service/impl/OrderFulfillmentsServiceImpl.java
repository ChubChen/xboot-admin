package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillments;
import com.chubchen.admin.modules.easesea.dao.mapper.OrderFulfillmentsMapper;
import com.chubchen.admin.modules.easesea.service.IOrderFulfillmentsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class OrderFulfillmentsServiceImpl extends ServiceImpl<OrderFulfillmentsMapper, OrderFulfillments> implements IOrderFulfillmentsService {

    @Override
    public  IPage<OrderFulfillments> findListByPage(PageVo pageVo, OrderFulfillments orderFulfillments){
        IPage<OrderFulfillments> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, orderFulfillments);
    }

    @Override
    public List<OrderFulfillments> findListAll(OrderFulfillments orderFulfillments){
        return baseMapper.selectAll(orderFulfillments);
    }

    @Override
    public int add(OrderFulfillments orderFulfillments){
        return baseMapper.insert(orderFulfillments);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(OrderFulfillments orderFulfillments){
        return baseMapper.updateById(orderFulfillments);
    }

    @Override
    public OrderFulfillments findById(Long id){
        return  baseMapper.selectById(id);
    }
}
