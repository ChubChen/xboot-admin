package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.PaypalTransaction;
import com.chubchen.admin.modules.easesea.dao.mapper.PaypalTransactionMapper;
import com.chubchen.admin.modules.easesea.service.IPaypalTransactionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-21
 */
@Service
@DS("easesea")
public class PaypalTransactionServiceImpl extends ServiceImpl<PaypalTransactionMapper, PaypalTransaction> implements IPaypalTransactionService {

    @Override
    public  IPage<PaypalTransaction> findListByPage(PageVo pageVo, PaypalTransaction paypalTransaction){
        IPage<PaypalTransaction> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, paypalTransaction);
    }

    @Override
    public List<PaypalTransaction> findListAll(PaypalTransaction paypalTransaction){
        return baseMapper.selectAll(paypalTransaction);
    }

    @Override
    public int add(PaypalTransaction paypalTransaction){
        return baseMapper.insert(paypalTransaction);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(PaypalTransaction paypalTransaction){
        return baseMapper.updateById(paypalTransaction);
    }

    @Override
    public PaypalTransaction findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void saveOrUpdateBatchUnqiId(List<PaypalTransaction> list) {
        //step1 根据业务id进行删除
        if(list != null && list.size() > 0){
            List<PaypalTransaction> exists = baseMapper.findByUniqueId(list);
            if(exists != null && exists.size()>0){
                List<String> orderIds = exists.stream().map(PaypalTransaction::getId).collect(Collectors.toList());
                baseMapper.deleteBatchIds(orderIds);
            }
            baseMapper.insertBatch(list);
        }
    }
}
