package com.chubchen.admin.modules.easesea.constants;

/**
 * @author chubchen
 *
 */
public interface PlatformConstants {
    /**
     * SHOPIFY
     */
    String SHOPIFY = "shopify";

    /**
     * 店匠
     */
    String SHOPLAZZA = "shoplazza";

    /**
     * PayPal
     */
    String PAYPAL = "paypal";



    /**
     * TIKTOK 广告平台
     */
    String AD_TIKTOK = "tiktok";

    /**
     * Google 广告平台
     */

    String Ad_GOOGLE = "google";



}
