package com.chubchen.admin.modules.easesea.constants;

/**
 * @author chubchen
 * mongodb 集合名称
 */
public interface MongodbCollection {

    /**
     * 数据解析表
     */
    String DATA_PARSE_QUEUE= "java_data_parse_queue";

    /**
     * shopify 订单
     */
    String SHOPIFY_ORDERS = "orders";

    /**
     * shopify产品
     */
    String SHOPIFY_PRODUCTS = "products";

    /**
     * 店匠订单
     */
    String SHOPLAZZA_ORDERS = "shoplazza_orders";

    /**
     * 店匠产品
     */
    String SHOPLAZZA_PRODUCTS = "shoplazza_products";



}
