package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.PaypalTransaction;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-21
 */
public interface IPaypalTransactionService extends IService<PaypalTransaction> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param paypalTransaction 过滤条件
     * @return IPage<PaypalTransaction>
     */
    IPage<PaypalTransaction> findListByPage(PageVo pageVo, PaypalTransaction paypalTransaction);

   /**
    * 查询分页数据
    *
    * @return  List<PaypalTransaction>
    */
    List<PaypalTransaction> findListAll(PaypalTransaction paypalTransaction);

    /**
     * 添加
     *
     * @param paypalTransaction 
     * @return int
     */
    int add(PaypalTransaction paypalTransaction);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param paypalTransaction 
     * @return int
     */
    int updateData(PaypalTransaction paypalTransaction);

    /**
     * id查询数据
     *
     * @param id id
     * @return PaypalTransaction
     */
    PaypalTransaction findById(Long id);

    /**
     * 根据唯一索引查询保存或更新
     * @param paypalTransactions
     */
    void saveOrUpdateBatchUnqiId(List<PaypalTransaction> paypalTransactions);
}
