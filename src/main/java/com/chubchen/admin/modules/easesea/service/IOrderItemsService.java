package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderItems;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IOrderItemsService extends IService<OrderItems> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param orderItems 过滤条件
     * @return IPage<OrderItems>
     */
    IPage<OrderItems> findListByPage(PageVo pageVo, OrderItems orderItems);

   /**
    * 查询分页数据
    *
    * @return  List<OrderItems>
    */
    List<OrderItems> findListAll(OrderItems orderItems);

    /**
     * 添加
     *
     * @param orderItems 
     * @return int
     */
    int add(OrderItems orderItems);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param orderItems 
     * @return int
     */
    int updateData(OrderItems orderItems);

    /**
     * id查询数据
     *
     * @param id id
     * @return OrderItems
     */
    OrderItems findById(Long id);
}
