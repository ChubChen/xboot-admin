package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IShippingAddressService;
import com.chubchen.admin.modules.easesea.entity.ShippingAddress;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-12
 */
@Api(tags = {""})
@RestController
@RequestMapping("/shipping-address")
public class ShippingAddressController {

    @Resource
    private IShippingAddressService shippingAddressService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute ShippingAddress shippingAddress){
        Integer count = shippingAddressService.add(shippingAddress);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = shippingAddressService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute ShippingAddress shippingAddress){
        Integer count = shippingAddressService.updateData(shippingAddress);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<ShippingAddress>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute ShippingAddress shippingAddress){
            IPage<ShippingAddress> page = shippingAddressService.findListByPage(pageVo, shippingAddress);
         return new ResultUtil<IPage<ShippingAddress>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<ShippingAddress> findById(@PathVariable Long id){
        ShippingAddress shippingAddress=  shippingAddressService.findById(id);
        return new ResultUtil<ShippingAddress>().setData(shippingAddress);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<ShippingAddress>> findById(@ModelAttribute ShippingAddress shippingAddress){
        List<ShippingAddress> shippingAddressList =  shippingAddressService.findListAll(shippingAddress);
        return new ResultUtil<List<ShippingAddress>>().setData(shippingAddressList);
    }

}
