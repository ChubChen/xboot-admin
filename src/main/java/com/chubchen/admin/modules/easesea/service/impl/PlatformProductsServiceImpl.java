package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProducts;
import com.chubchen.admin.modules.easesea.dao.mapper.PlatformProductsMapper;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import com.chubchen.admin.modules.easesea.entity.ShopifyProducts;
import com.chubchen.admin.modules.easesea.service.IPlatformProductsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Slf4j
@Service
@DS("easesea")
public class PlatformProductsServiceImpl extends ServiceImpl<PlatformProductsMapper, PlatformProducts> implements IPlatformProductsService {

    @Override
    public  IPage<PlatformProducts> findListByPage(PageVo pageVo,
                                                   PlatformProducts platformProducts,
                                                   SearchVo searchVo){
        IPage<PlatformProducts> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, platformProducts, searchVo);
    }

    @Override
    public List<PlatformProducts> findListAll(PlatformProducts platformProducts){
        return baseMapper.selectAll(platformProducts);
    }

    @Override
    public int add(PlatformProducts platformProducts){
        return baseMapper.insert(platformProducts);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(PlatformProducts platformProducts){
        return baseMapper.updateById(platformProducts);
    }

    @Override
    public PlatformProducts findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public List<PlatformProducts> findByUniqueId(List<PlatformProducts> list) {
        if(list == null || list.isEmpty()){
            return new ArrayList<>();
        }
        return baseMapper.findByUniqueId(list);
    }

    @Override
    public IPage<PlatformProducts> findSPUProductStatistic(PlatformProducts products, PageVo pageVo, SearchVo searchVo) {
        IPage<PlatformProducts> page = PageUtil.initMpPage(pageVo);
        page = baseMapper.findSPUProductStatistic(page, products, searchVo);
        return page;
    }

    @Override
    public List<PlatformProducts> findSPUProductStatisticAll(PlatformProducts products, SearchVo searchVo) {
        List<PlatformProducts> list = baseMapper.findSPUProductStatistic(products, searchVo);
        return list;
    }

    @Override
    public IPage<PlatformProducts> getSPUNoSaleProduct(PlatformProducts products, PageVo pageVo, SearchVo searchVo) {
        Page<ShopifyProducts> page = PageUtil.initMpPage(pageVo);
        return baseMapper.getSPUNoSaleProduct(page, products, searchVo);
    }

    @Override
    public List<ShopifyOrderStatistic> getProductStaticByTime(String spu, String productId, SearchVo searchVo) {
        return baseMapper.getProductStaticByTime(spu, productId, searchVo);
    }

    @Override
    public List<String> getShoplazzaIdByUrl(List<String> handle, Integer backendId) {
        return baseMapper.getShoplazzaIdByUrl(handle, backendId);
    }

    @Override
    public IPage<PlatformProducts> getNoSaleProduct(PlatformProducts platformProducts, PageVo pageVo, SearchVo searchVo) {
        IPage<PlatformProducts> page = PageUtil.initMpPage(pageVo);
        return baseMapper.getNoSaleProduct(page, platformProducts, searchVo);
    }
}
