package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillmentItems;
import com.chubchen.admin.modules.easesea.dao.mapper.OrderFulfillmentItemsMapper;
import com.chubchen.admin.modules.easesea.service.IOrderFulfillmentItemsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class OrderFulfillmentItemsServiceImpl extends ServiceImpl<OrderFulfillmentItemsMapper, OrderFulfillmentItems> implements IOrderFulfillmentItemsService {

    @Override
    public  IPage<OrderFulfillmentItems> findListByPage(PageVo pageVo, OrderFulfillmentItems orderFulfillmentItems){
        IPage<OrderFulfillmentItems> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, orderFulfillmentItems);
    }

    @Override
    public List<OrderFulfillmentItems> findListAll(OrderFulfillmentItems orderFulfillmentItems){
        return baseMapper.selectAll(orderFulfillmentItems);
    }

    @Override
    public int add(OrderFulfillmentItems orderFulfillmentItems){
        return baseMapper.insert(orderFulfillmentItems);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(OrderFulfillmentItems orderFulfillmentItems){
        return baseMapper.updateById(orderFulfillmentItems);
    }

    @Override
    public OrderFulfillmentItems findById(Long id){
        return  baseMapper.selectById(id);
    }
}
