package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PlatformProductVariants对象", description="")
public class PlatformProductVariants extends Model<PlatformProductVariants> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public PlatformProductVariants(){

    }

    private String id;

    private Integer backendId;

    @ApiModelProperty(value = "platform_product 表Id")
    private String productId;

    @ApiModelProperty(value = "变体id")
    private String pVariantId;

    @ApiModelProperty(value = "产品id")
    private String pProductId;

    @ApiModelProperty(value = "spu")
    private String spu;

    @ApiModelProperty(value = "变体title")
    private String title;

    @ApiModelProperty(value = "sku")
    private String sku;

    private String imageSrc;

    private BigDecimal price;

    private BigDecimal comparedAtPrice;

    @ApiModelProperty(value = "重量g")
    private Integer grams;

    private Date createdAt;

    private Date updatedAt;

    @TableField(exist = false)
    private Integer quantity;


    @Override
    protected Serializable pkVal() {
        return null;
    }

}
