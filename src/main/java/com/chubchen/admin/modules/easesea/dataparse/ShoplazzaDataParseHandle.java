package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.common.utils.SnowFlakeUtil;
import com.chubchen.admin.common.utils.SpringContextUtil;
import com.chubchen.admin.modules.easesea.constants.DataParseConstants;
import com.chubchen.admin.modules.easesea.constants.OrdersFinancialStatusEnum;
import com.chubchen.admin.modules.easesea.constants.OrdersFulfillmentStatusEnum;
import com.chubchen.admin.modules.easesea.constants.OrdersStatusEnum;
import com.chubchen.admin.modules.easesea.entity.*;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * 店匠平台的数据解析类
 * @author chubchen
 */
@Component
public class ShoplazzaDataParseHandle extends AbstractDataParseHandle {

    @Override
    public void parseCheckout(List<JSONObject> dataList,String platform) {

    }

    @Override
    public void parseOrders(List<JSONObject> dataList, String platform) {
        List<Orders> list = new ArrayList<>(dataList.size());
        List<OrderItems> itemList = new ArrayList<>(dataList.size());
        List<OrderFulfillments> orderFulfillments = new ArrayList<>(dataList.size());
        List<OrderFulfillmentItems> orderFulfillmentItemssList = new ArrayList<>(dataList.size());
        List<ShippingAddress> shippingAddresses = new ArrayList<>(dataList.size());
        List<AbandonedOrders> abandonedOrdersList = new ArrayList<>();
        List<AbandonedOrderItems> abandonedOrderItemsList = new ArrayList<>();
        dataList.forEach(jsonOrder->{
            String status = jsonOrder.getString("status");
            if(DataParseConstants.SHOPLAZZA_STATUS_OPENED.equals(status)){
                AbandonedOrders abandonedOrders = this.buildAbandonedOrder(jsonOrder, platform);
                abandonedOrdersList.add(abandonedOrders);
                abandonedOrderItemsList.addAll(this.buildAbandonedOrderItems(jsonOrder, abandonedOrders, platform));
            }else{
                this.thisBuildOrder(jsonOrder, list, itemList, orderFulfillments, orderFulfillmentItemssList, shippingAddresses, platform);
            }
        });
        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveOrders(list, itemList,
                orderFulfillments, orderFulfillmentItemssList, shippingAddresses);

        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveAbandonedOrder(abandonedOrdersList,
                abandonedOrderItemsList);
    }



    @Override
    public Orders buildOrder(JSONObject jsonOrder) {
        Orders orders =  Orders.builder()
                .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                .pOrderId(jsonOrder.getString("id"))
                .domainId(jsonOrder.getInteger("domain_id"))
                .backendId(jsonOrder.getInteger("backend_id"))
                .createdAt(jsonOrder.getDate("created_at"))
                .canceledAt(jsonOrder.getDate("cancelled_at"))
                .updatedAt(jsonOrder.getDate("updated_at"))
                .gateway(jsonOrder.getString("gateway"))
                .pAbandonedId(jsonOrder.getString("id"))
                .placedAt(jsonOrder.getDate("placed_at"))
                .number(jsonOrder.getString("number"))
                .subtotalPrice(jsonOrder.getBigDecimal("sub_total"))
                .totalPrice(jsonOrder.getBigDecimal("total_price"))
                .totalDiscount(jsonOrder.getBigDecimal("total_discount"))
                .totalTax(jsonOrder.getBigDecimal("total_tax"))
                .currency(jsonOrder.getString("currency"))
                .totalShipping(jsonOrder.getBigDecimal("total_shipping"))
                .build();
        JSONObject paymentLine = jsonOrder.getJSONObject("payment_line");
        if(paymentLine != null){
            orders.setGateway(paymentLine.getString("payment_channel"));
        }
        String financialStatus = jsonOrder.getString("financial_status");
        switch (financialStatus){
            case "waiting" : orders.setFinancialStatus(OrdersFinancialStatusEnum.waiting.getCode()); break;
            case "paying" : orders.setFinancialStatus(OrdersFinancialStatusEnum.paying.getCode()); break;
            case "paid" : orders.setFinancialStatus(OrdersFinancialStatusEnum.paid.getCode()); break;
            case "partially_paid" : orders.setFinancialStatus(OrdersFinancialStatusEnum.partially_paid.getCode()); break;
            case "refunded" : {
                orders.setFinancialStatus(OrdersFinancialStatusEnum.refunded.getCode());
                orders.setRefund(orders.getTotalPrice());
                orders.setRefundAt(jsonOrder.getDate("updated_at"));
            } break;
            case "failed" : orders.setFinancialStatus(OrdersFinancialStatusEnum.voided.getCode()); break;
            case "cancelled" : orders.setFinancialStatus(OrdersFinancialStatusEnum.voided.getCode()); break;
            default : orders.setFinancialStatus(""); break;
        }
        String fulfillmentStatus = jsonOrder.getString("fulfillment_status");
        if(fulfillmentStatus == null){
            fulfillmentStatus = "null";
        }
        switch (fulfillmentStatus){
            case "initialled" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.unShipped.getCode()); break;
            case "waiting" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.unShipped.getCode()); break;
            case "partially_shipped" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.partial.getCode()); break;
            case "partially_finished" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.partial.getCode()); break;
            case "shipped" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.shipped.getCode()); break;
            case "fulfilled" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.shipped.getCode()); break;
            case "cancelled" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.cancelled.getCode()); break;
            case "returning" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.returned.getCode()); break;
            case "returned" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.returned.getCode()); break;
            default : orders.getFulfillmentStatus(); break;
        }
        String status = jsonOrder.getString("status");
        if(DataParseConstants.SHOPLAZZA_STATUS_CANCEL.equals(status)){
            orders.setStatus(OrdersStatusEnum.canceled.getCode());
        }else{
            orders.setStatus(OrdersStatusEnum.normal.getCode());
        }
        JSONObject customer = jsonOrder.getJSONObject("customer");
        if(customer != null){
            orders.setOrdersCount(customer.getInteger("orders_count"));
            JSONObject defaultAddress = customer.getJSONObject("default_address");
            orders.setCountry( defaultAddress!= null ? defaultAddress.getString("country") : "");
        }
        return orders;
    }

    @Override
    public List<OrderFulfillments> buildFulfillments(JSONObject jsonOrder, Orders orders, String platform) {
        JSONArray jsonArray = jsonOrder.getJSONArray("fulfillments");
        if(jsonArray != null && jsonArray.size() > 0){
            orders.setFulfilledAt(orders.getCreatedAt());
            List<OrderFulfillments> list = new ArrayList<>(jsonArray.size());
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject fulfillment = jsonArray.getJSONObject(i);
                OrderFulfillments orderFulfillments = OrderFulfillments.builder().id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                        .orderId(orders.getId())
                        .pOrderId(orders.getPOrderId())
                        .backendId(orders.getBackendId())
                        .domainId(orders.getDomainId())
                        .pFulfillmentId(fulfillment.getString("id"))
                        .trackingNumber(fulfillment.getString("tracking_number"))
                        .trackingCompany(fulfillment.getString("tracking_company"))
                        .trackingUrl(fulfillment.getString("tracking_company_code"))
                        .updatedAt(fulfillment.getDate("updated_at"))
                        .createdAt(fulfillment.getDate("created_at"))
                        .build();
                Date createdAt = fulfillment.getDate("created_at");
                if(orders.getFulfilledAt().before(createdAt)){
                    orders.setFulfilledAt(createdAt);
                }
                orderFulfillments.setOrderFulfillmentItemsList(this.buildFulfillmentItems(fulfillment, orderFulfillments, platform));
                list.add(orderFulfillments);
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public void parseProduct(List<JSONObject> dataList,String platform) {
        List<PlatformProducts> list = new ArrayList<>(dataList.size());
        List<PlatformProductVariants> variantList = new ArrayList<>();
        dataList.forEach( jsonOrder->{
            PlatformProducts products = this.buildProduct(jsonOrder, platform);
            list.add(products);
            variantList.addAll(this.buildProductVariants(jsonOrder, products));
        });
        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveProducts(list, variantList);
    }


    @Override
    public List<PlatformProductVariants> buildProductVariants(JSONObject jsonProduct, PlatformProducts products) {
        JSONArray jsonArray = jsonProduct.getJSONArray("variants");
        if(jsonArray != null && jsonArray.size()>0) {
            Map<String, String> map = new HashMap<>();
            JSONArray imageArray = jsonProduct.getJSONArray("images");
            if(imageArray != null && imageArray.size() > 0){
                for (int i = 0; i < imageArray.size(); i++) {
                    JSONObject image = imageArray.getJSONObject(i);
                    map.put(image.getString("id"), image.getString("src"));
                }
            }
            List<PlatformProductVariants> list = new ArrayList<>();
            Set<String> spuSet = new HashSet<>(1);
            String spu = "";
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonVariants = jsonArray.getJSONObject(i);
                PlatformProductVariants variants = PlatformProductVariants.builder()
                        .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                        .productId(products.getId())
                        .backendId(products.getBackendId())
                        .pProductId(jsonVariants.getString("product_id"))
                        .pVariantId(jsonVariants.getString("id"))
                        .title(jsonVariants.getString("title"))
                        .sku(jsonVariants.getString("sku"))
                        .price(jsonVariants.getBigDecimal("price"))
                        .comparedAtPrice(jsonVariants.getBigDecimal("compared_at_price"))
                        .grams(jsonVariants.getInteger("grams"))
                        .createdAt(jsonVariants.getDate("created_at"))
                        .updatedAt(jsonVariants.getDate("updated_at"))
                        .build();
                spu = this.parseSkuToSpu(jsonVariants.getString("sku"));
                variants.setImageSrc(map.get(jsonVariants.getString("image_id")));
                variants.setSpu(spu);
                spuSet.add(spu);
                list.add(variants);
            }
            if(spuSet.size() == 1){
                products.setSpu(spu);
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

}
