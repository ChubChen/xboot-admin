package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrders;
import com.chubchen.admin.modules.easesea.dao.mapper.AbandonedOrdersMapper;
import com.chubchen.admin.modules.easesea.service.IAbandonedOrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.easesea.vo.AbandonedStatic;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class AbandonedOrdersServiceImpl extends ServiceImpl<AbandonedOrdersMapper, AbandonedOrders> implements IAbandonedOrdersService {

    @Override
    public  IPage<AbandonedOrders> findListByPage(PageVo pageVo, AbandonedOrders abandonedOrders, SearchVo searchVo){
        IPage<AbandonedOrders> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, abandonedOrders, searchVo);
    }

    @Override
    public List<AbandonedOrders> findListAll(AbandonedOrders abandonedOrders){
        return baseMapper.selectAll(abandonedOrders);
    }

    @Override
    public int add(AbandonedOrders abandonedOrders){
        return baseMapper.insert(abandonedOrders);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AbandonedOrders abandonedOrders){
        return baseMapper.updateById(abandonedOrders);
    }

    @Override
    public AbandonedOrders findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public List<AbandonedOrders> findByUniqueId(List<AbandonedOrders> list) {
        if(list == null || list.isEmpty()){
            return new ArrayList<>();
        }
        return baseMapper.findByUniqueId(list);
    }

    @Override
    public IPage<AbandonedStatic> getStatic(Integer domainId, PageVo pageVo, SearchVo searchVo) {
        IPage<AbandonedOrders> page = PageUtil.initMpPage(pageVo);
        return baseMapper.getStatic(page, domainId, searchVo);
    }
}
