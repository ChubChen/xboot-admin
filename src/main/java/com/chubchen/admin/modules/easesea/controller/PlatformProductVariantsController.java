package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IPlatformProductVariantsService;
import com.chubchen.admin.modules.easesea.entity.PlatformProductVariants;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/platform-product-variants")
public class PlatformProductVariantsController {

    @Resource
    private IPlatformProductVariantsService platformProductVariantsService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute PlatformProductVariants platformProductVariants){
        Integer count = platformProductVariantsService.add(platformProductVariants);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = platformProductVariantsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute PlatformProductVariants platformProductVariants){
        Integer count = platformProductVariantsService.updateData(platformProductVariants);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<PlatformProductVariants>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute PlatformProductVariants platformProductVariants){
            IPage<PlatformProductVariants> page = platformProductVariantsService.findListByPage(pageVo, platformProductVariants);
         return new ResultUtil<IPage<PlatformProductVariants>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<PlatformProductVariants> findById(@PathVariable Long id){
        PlatformProductVariants platformProductVariants=  platformProductVariantsService.findById(id);
        return new ResultUtil<PlatformProductVariants>().setData(platformProductVariants);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<PlatformProductVariants>> findById(@ModelAttribute PlatformProductVariants platformProductVariants){
        List<PlatformProductVariants> platformProductVariantsList =  platformProductVariantsService.findListAll(platformProductVariants);
        return new ResultUtil<List<PlatformProductVariants>>().setData(platformProductVariantsList);
    }

}
