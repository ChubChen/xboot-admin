package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-07-30
 */
@Api(tags = {""})
@RestController
@RequestMapping("/sl-paypal-account")
public class SlPaypalAccountController {

    @Resource
    private ISlPaypalAccountService slPaypalAccountService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute SlPaypalAccount slPaypalAccount){
        Integer count = slPaypalAccountService.add(slPaypalAccount);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = slPaypalAccountService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute SlPaypalAccount slPaypalAccount){
        Integer count = slPaypalAccountService.updateData(slPaypalAccount);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<SlPaypalAccount>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute SlPaypalAccount slPaypalAccount){
            IPage<SlPaypalAccount> page = slPaypalAccountService.findListByPage(pageVo, slPaypalAccount);
         return new ResultUtil<IPage<SlPaypalAccount>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<SlPaypalAccount> findById(@PathVariable Long id){
        SlPaypalAccount slPaypalAccount=  slPaypalAccountService.findById(id);
        return new ResultUtil<SlPaypalAccount>().setData(slPaypalAccount);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<SlPaypalAccount>> findById(@ModelAttribute SlPaypalAccount slPaypalAccount){
        List<SlPaypalAccount> slPaypalAccountList =  slPaypalAccountService.findListAll(slPaypalAccount);
        return new ResultUtil<List<SlPaypalAccount>>().setData(slPaypalAccountList);
    }

}
