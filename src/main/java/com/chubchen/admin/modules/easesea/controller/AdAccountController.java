package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.easesea.constants.AdAccountStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAdAccountService;
import com.chubchen.admin.modules.easesea.entity.AdAccount;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/ad-account")
public class AdAccountController {

    @Resource
    private IAdAccountService adAccountService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AdAccount adAccount){
        Result result = validateParam(adAccount);
        if (result.isSuccess()) {
            AdAccount exists = adAccountService.getOne(new QueryWrapper<AdAccount>()
                    .eq("account_name", adAccount.getAccountName())
                    .or().eq("account_id", adAccount.getAccountId()));
            if(exists == null || exists.getAccountId() != null) {
                adAccount.setCreatedAt(new Date());
                Integer count = adAccountService.add(adAccount);
                if(count > 0){
                    return new ResultUtil<Integer>().setData(count);
                }else{
                    return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                }
            }else{
                return new ResultUtil<Integer>().setErrorMsg("该账号已经存在,不能重复添加");
            }
        }else{
            return result;
        }
    }

    private Result validateParam(AdAccount AdAccount){
        if(StringUtils.isBlank(AdAccount.getAccountId())){
            return new ResultUtil<Integer>().setErrorMsg("账户ID不能为空");
        }
        if(StringUtils.isBlank(AdAccount.getAccountName())){
            return new ResultUtil<Integer>().setErrorMsg("账户名称不能为空");
        }

        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = adAccountService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AdAccount adAccount){
        Result result = validateParam(adAccount);
        if(result.isSuccess()){
            Integer count = adAccountService.updateData(adAccount);
            if(count > 0){
                return new ResultUtil<Integer>().setData(count);
            }else{
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AdAccount>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AdAccount adAccount){
            IPage<AdAccount> page = adAccountService.findListByPage(pageVo, adAccount);
         return new ResultUtil<IPage<AdAccount>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AdAccount> findById(@PathVariable Long id){
        AdAccount adAccount=  adAccountService.findById(id);
        return new ResultUtil<AdAccount>().setData(adAccount);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AdAccount>> findById(@ModelAttribute AdAccount adAccount){
        List<AdAccount> adAccountList =  adAccountService.findListAll(adAccount);
        return new ResultUtil<List<AdAccount>>().setData(adAccountList);
    }
    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AdAccountStatusEnum.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "启用或者关闭状态", type = LogType.OPERATION)
    @ApiOperation(value = "开启或者关闭单个站点")
    public Result<Boolean> enable(@RequestParam List<Long> ids, @RequestParam Integer status){
        if(!AdAccountStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = adAccountService.update(new UpdateWrapper<AdAccount>()
                .set("account_status", status)
                .set("updated_at", new Date())
                .in("account_id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
