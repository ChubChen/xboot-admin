package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IOrderFulfillmentsService;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillments;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/order-fulfillments")
public class OrderFulfillmentsController {

    @Resource
    private IOrderFulfillmentsService orderFulfillmentsService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute OrderFulfillments orderFulfillments){
        Integer count = orderFulfillmentsService.add(orderFulfillments);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = orderFulfillmentsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute OrderFulfillments orderFulfillments){
        Integer count = orderFulfillmentsService.updateData(orderFulfillments);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<OrderFulfillments>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute OrderFulfillments orderFulfillments){
            IPage<OrderFulfillments> page = orderFulfillmentsService.findListByPage(pageVo, orderFulfillments);
         return new ResultUtil<IPage<OrderFulfillments>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<OrderFulfillments> findById(@PathVariable Long id){
        OrderFulfillments orderFulfillments=  orderFulfillmentsService.findById(id);
        return new ResultUtil<OrderFulfillments>().setData(orderFulfillments);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<OrderFulfillments>> findById(@ModelAttribute OrderFulfillments orderFulfillments){
        List<OrderFulfillments> orderFulfillmentsList =  orderFulfillmentsService.findListAll(orderFulfillments);
        return new ResultUtil<List<OrderFulfillments>>().setData(orderFulfillmentsList);
    }

}
