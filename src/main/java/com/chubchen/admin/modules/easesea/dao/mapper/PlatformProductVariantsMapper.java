package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProductVariants;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface PlatformProductVariantsMapper extends BaseMapper<PlatformProductVariants> {

    /**
     * 根据条件分页查询
     * @param page
     * @param platformProductVariants
     * @return IPage<PlatformProductVariants>
    */
    IPage<PlatformProductVariants> selectListByPage(IPage<PlatformProductVariants> page, @Param("platformProductVariants") PlatformProductVariants platformProductVariants);

    /**
     * 根据条件查询全部
     * @param platformProductVariants
     * @return List<PlatformProductVariants>
    */
    List<PlatformProductVariants> selectAll(@Param("platformProductVariants") PlatformProductVariants platformProductVariants);

    /**
     * 根据spu或者productId查询变体销量
     * @param spu
     * @param productId
     * @param searchVo
     * @return
     */
    List<PlatformProductVariants> getSaleVariantsByProductId(@Param("spu") String spu,
                                                             @Param("productId") String productId,
                                                             @Param("searchVo") SearchVo searchVo);
}
