package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
public interface ISlPaypalRefundRequestDetailService extends IService<SlPaypalRefundRequestDetail> {

    /**
     * 添加
     *
     * @param slPaypalRefundRequestDetail 
     * @return int
     */
    int add(SlPaypalRefundRequestDetail slPaypalRefundRequestDetail);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param slPaypalRefundRequestDetail 
     * @return int
     */
    int updateData(SlPaypalRefundRequestDetail slPaypalRefundRequestDetail);

    /**
     * id查询数据
     *
     * @param id id
     * @return SlPaypalRefundRequestDetail
     */
    SlPaypalRefundRequestDetail findById(Long id);

    /**
     * 处理请求PayPal
     *
     * @param item
     * @param account
     * @return
     * @throws Exception
     */
    int handleRequestPaypal(SlPaypalRefundRequestDetail item, SlPaypalAccount account) throws Exception;

}
