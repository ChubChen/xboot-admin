package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.common.utils.SnowFlakeUtil;
import com.chubchen.admin.modules.easesea.constants.*;
import com.chubchen.admin.modules.easesea.entity.PaypalTransaction;
import com.chubchen.admin.modules.easesea.service.IPaypalTransactionService;
import com.google.api.client.json.Json;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author chenpeng
 *
 * Paypal 数据解析入库
 */
@Component
@Slf4j
public class PayPalDataParseHandle implements DataParseHandle{

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private IPaypalTransactionService paypalTransactionService;

    private static final int pageSize = 500;

    /**
     * shopify 交易号
     */
    static final Pattern InvoiceIdExp = Pattern.compile("c\\d*\\.\\d");

    private static final String eventCodeReg = "(T11\\d*)|(T00\\d*)";

    @Override
    public void handle(JSONObject dataParse) {
        try{
            // step1 校验数据
            boolean flag = this.validateParams(dataParse);
            if(!flag){
                log.info("解析校验失败{}", dataParse.toJSONString());
                return;
            }
            // step2 更新mongo的状态到 处理中
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_ING);

            String type = dataParse.getString("type");
            if(PaypalDataParseType.toSet().contains(type)){
                // step3 处理数据
                this.parseData(dataParse);
            }else{
                log.info("暂时不支持该type:{},数据的解析跳过", type);
            }

            //step4 更新mongo状态到 处理成功
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_FINISH);
        }catch (Exception e){
            log.error("解析数据转换失败", e);
            //step5 更新mongo状态到 处理失败
            this.updateParseStatus(dataParse, DataParseConstants.PARSE_STATUS_ERROR);
        }
    }

    private void parseData(JSONObject dataParse) {
        //step 1 查询要解析的订单数据
        Date startDate = dataParse.getDate("startDate");
        Date endDate = dataParse.getDate("endDate");
        String collectionName = PlatformConstants.PAYPAL + "_" + dataParse.getString("type");
        log.info("开始解析Paypal数据，开始时间：{}， 结束时间：{}, 集合名称：{}", startDate, endDate, collectionName);
        Query queryCount = new Query(Criteria.where("lastModified").gte(startDate).lte(endDate)
                .and("transaction_info.transaction_event_code").regex(eventCodeReg));
        if(StringUtils.isNotBlank(collectionName)){
            long count = mongoTemplate.count(queryCount, collectionName);
            log.info("开始Paypal解析数据，总数量:{}", count);
            long start = 0;
            while (start < count) {
                log.info("分页解析Paypal数据当前开始数量:{}，总共数量:{}, 当前集合：{}", start, count, collectionName);
                Query query = queryCount.skip(start).limit(pageSize);
                List<JSONObject> dataList = mongoTemplate.find(query, JSONObject.class, collectionName);
                if(dataList != null && dataList.size() > 0){
                    this.parseDataByType(dataParse, dataList);
                }
                start = start + pageSize;
            }
        }
    }

    private void parseDataByType(JSONObject dataParse, List<JSONObject> dataList) {
        String type = dataParse.getString("type");
        if(PaypalDataParseType.TRANSACTION.getCode().equals(type)){
            List<PaypalTransaction> paypalTransactions = this.buildPayPalTransaction(dataList);
            paypalTransactionService.saveOrUpdateBatchUnqiId(paypalTransactions);
        }
    }

    private List<PaypalTransaction> buildPayPalTransaction(List<JSONObject> dataList) {
        List<PaypalTransaction> list = new ArrayList<>(dataList.size());
        dataList.forEach(item->{
            PaypalTransaction paypalTransaction = new PaypalTransaction();
            paypalTransaction.setId(SnowFlakeUtil.getFlowIdInstance().nextIdStr());
            JSONObject payerInfo = item.getJSONObject("payer_info");
            if(payerInfo != null ){
                paypalTransaction.setPayerEmail(payerInfo.getString("email_address"));
                paypalTransaction.setCountryCode(payerInfo.getString("country_code"));


                JSONObject payerName = payerInfo.getJSONObject("payer_name");
                if(payerName != null){
                    paypalTransaction.setPayerFullName(payerName.getString("alternate_full_name"));
                }

            }
            JSONObject transactionInfo = item.getJSONObject("transaction_info");
            if(transactionInfo != null){
                paypalTransaction.setTransactionEventCode(transactionInfo.getString("transaction_event_code"));
                //交易金额
                JSONObject amountInfo = transactionInfo.getJSONObject("transaction_amount");
                if(amountInfo != null){
                    paypalTransaction.setTransactionAmount(amountInfo.getBigDecimal("value"));
                    paypalTransaction.setCurrencyCode(amountInfo.getString("currency_code"));
                }
                paypalTransaction.setTransactionStatus(transactionInfo.getString("transaction_status"));
                paypalTransaction.setTransactionInitiationDate(transactionInfo.getDate("transaction_initiation_date"));
                //交易号
                String invoice_id = transactionInfo.getString("invoice_id");
                Matcher m = InvoiceIdExp.matcher(invoice_id);
                if(m.matches()){
                    paypalTransaction.setInvoiceId(invoice_id.substring(1, invoice_id.length() - 2));
                }else{
                    paypalTransaction.setInvoiceId(invoice_id);
                }
                //可用余额
                JSONObject availableBalance = transactionInfo.getJSONObject("available_balance");
                if(availableBalance != null){
                    paypalTransaction.setAvailableBlance(availableBalance.getBigDecimal("value"));
                }
            }
            paypalTransaction.setPaypalAccountId(item.getInteger("paypal_account_id"));
            paypalTransaction.setTransactionId(item.getString("transaction_id"));
            list.add(paypalTransaction);
        });
        return list;
    }


    boolean validateParams(JSONObject dataParse){
        String type = dataParse.getString("type");
        Date startDate = dataParse.getDate("startDate");
        Date endData = dataParse.getDate("endDate");
        if(StringUtils.isNotBlank(type)
                && startDate != null
                && endData != null
                && startDate.before(endData)){
            return true;
        }else{
            return false;
        }
    }

    void updateParseStatus(JSONObject dataParse, int status) {
        Update update = new Update();
        update.set("status", status);
        String uniqueId = dataParse.getString("uniqId");
        mongoTemplate.updateFirst(new Query(Criteria.where("uniqId").is(uniqueId)),
                update, MongodbCollection.DATA_PARSE_QUEUE);
    }

}
