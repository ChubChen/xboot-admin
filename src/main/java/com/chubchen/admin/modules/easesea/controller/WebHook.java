package com.chubchen.admin.modules.easesea.controller;


import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.modules.easesea.constants.AdyenEventCode;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @author chubchen
 *  1688推送接口订阅类
 */
@RestController
@RequestMapping("/api/webhook")
@Slf4j
public class WebHook {

    @Resource
    private ISlPaypalRefundRequestDetailService refundRequestDetailService;

    @ResponseBody
    @RequestMapping("/adyen")
    public String subscription1688Gateway(@RequestBody JSONObject message){
        log.info("收到adyen推送消息，消息:{}", message);
        try{
            boolean live = message.getBooleanValue("live");
            if(live){
                JSONArray items = message.getJSONArray("notificationItems");
                if(items != null && items.size() > 0){
                    for (int i =0; i < items.size(); i ++){
                        JSONObject noItems = items.getJSONObject(i);
                        JSONObject eventItem = noItems.getJSONObject("NotificationRequestItem");
                        String eventCode = eventItem.getString("eventCode");
                        if(AdyenEventCode.getMap().containsKey(eventCode)){
                            if(AdyenEventCode.REFUND.getCode().equals(eventCode)){
                                refundAdyen(eventItem);
                            }else if(AdyenEventCode.CANCEL_OR_REFUND.getCode().equals(eventCode)){
                                cancelOrRefund(eventItem);
                            }else if(AdyenEventCode.REFUND_FAILED.getCode().equals(eventCode) ||
                                    AdyenEventCode.REFUNDED_REVERSED.getCode().equals(eventCode)){
                                refundFail(eventItem);
                            }else{
                                continue;
                            }
                        }else{
                            continue;
                        }
                    }
                }
            }
            return "[accepted]";
        }catch (Exception e){
            log.error("处理推销消息失败", e);
            return "[error]";
        }
    }

    private void refundAdyen(JSONObject eventItem){
        boolean success = eventItem.getBooleanValue("success");
        String originalReference = eventItem.getString("originalReference");
        if(success){
            log.info("captuerId:{} 退款成功开始更新数据库", originalReference);
            refundRequestDetailService.update(new UpdateWrapper<SlPaypalRefundRequestDetail>()
                    .set("status",SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.COMPLETED.getCode())
                    .eq("capture_id", originalReference)
                    .eq("platform", SlPaypalRefundRequestDetail.PayPlatform.ADYEN.getCode()));
        }else{
            String reason = eventItem.getString("reason");
            failUpdate(originalReference, reason);
        }
    }


    private void cancelOrRefund(JSONObject eventItem){
        boolean success = eventItem.getBooleanValue("success");
        String originalReference = eventItem.getString("originalReference");
        if(success){
            log.info("captuerId:{} 退款成功开始更新数据库", originalReference);
            refundRequestDetailService.update(new UpdateWrapper<SlPaypalRefundRequestDetail>()
                    .set("status",SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.CANCELLED.getCode())
                    .eq("capture_id", originalReference)
                    .eq("platform", SlPaypalRefundRequestDetail.PayPlatform.ADYEN.getCode()));
        }
    }

    private void failUpdate(String originalReference, String reason){
        log.info("captuerId:{} 退款失败开始更新数据库", originalReference);
        refundRequestDetailService.update(new UpdateWrapper<SlPaypalRefundRequestDetail>()
                .set("status",SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode())
                .set("message", reason)
                .eq("capture_id", originalReference)
                .eq("platform", SlPaypalRefundRequestDetail.PayPlatform.ADYEN.getCode()));
    }

    private void refundFail(JSONObject eventItem){
        boolean success = eventItem.getBooleanValue("success");
        String originalReference = eventItem.getString("originalReference");
        if(success){
            failUpdate(originalReference, "退款失败");
        }
    }
}
