package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author chubchen
 */
@Data
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class WorldPayAmount {

    @XmlAttribute
    private String value;
    @XmlAttribute
    private String currencyCode;
    @XmlAttribute
    private String exponent="2";
    @XmlAttribute
    private String debitCreditIndicator="credit";
}
