package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalRefundRequestMapper;
import com.chubchen.admin.modules.easesea.entity.AccountPaypal;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-29
*/
@DS("easesea")
public interface SlPaypalRefundRequestMapper extends BaseMapper<SlPaypalRefundRequest> {


    /**
     * 分页查询
     * @param page
     * @param searchVo
     * @param slPaypalRefundRequest
     * @return
     */
    IPage<SlPaypalRefundRequest> findListByPage(IPage<AccountPaypal> page,
                                                @Param("search") SearchVo searchVo,
                                                @Param("request") SlPaypalRefundRequest slPaypalRefundRequest);
}
