package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

/**
 * @author chubchen
 *
 */
@Getter
public enum OrdersStatusEnum {

    normal("normal","正常状态"),
    canceled("canceled","取消");

    private String code;

    private String desc;

    OrdersStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
