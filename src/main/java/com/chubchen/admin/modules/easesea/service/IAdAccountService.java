package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.AdAccount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import org.joda.time.DateTime;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
public interface IAdAccountService extends IService<AdAccount> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param adAccount 过滤条件
     * @return IPage<AdAccount>
     */
    IPage<AdAccount> findListByPage(PageVo pageVo, AdAccount adAccount);

   /**
    * 查询分页数据
    *
    * @return  List<AdAccount>
    */
    List<AdAccount> findListAll(AdAccount adAccount);

    /**
     * 添加
     *
     * @param adAccount 
     * @return int
     */
    int add(AdAccount adAccount);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param adAccount 
     * @return int
     */
    int updateData(AdAccount adAccount);

    /**
     * id查询数据
     *
     * @param id id
     * @return AdAccount
     */
    AdAccount findById(Long id);

    /**
     * 从广告平台同步数据并存入数据库中
     * @param adAccount
     */
    void syncPlatformData(AdAccount adAccount);

    /**
     * 查询tiktok的数据并保存
     * @param adAccount
     * @param start
     * @throws Exception
     */
    public void queryTiKTokDataAndSave(AdAccount adAccount, DateTime start) throws Exception;
}
