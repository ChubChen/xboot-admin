package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AdAccount对象", description="")
public class AdAccount extends Model<AdAccount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "account_id")
    private String accountId;

    private String accountName;

    @ApiModelProperty(value = "0-新建，1使用中")
    private Integer accountStatus;

    private Integer domainId;

    private String bmId;

    private String bmName;

    private String company;

    private String agency;

    private String owner;

    private String note;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    private String timeZone;

    private String adPlatform;


    @Override
    protected Serializable pkVal() {
        return this.accountId;
    }

}
