package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.*;

/**
 * @author   陈鹏
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
public class WorldPayOrder {

    @XmlAttribute
    private String orderCode;

    @XmlElement(name = "refund")
    private WorldRefund worldRefund;
}
