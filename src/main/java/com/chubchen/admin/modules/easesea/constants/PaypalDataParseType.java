package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author chubchen
 * PayPal 解析数据类型
 */
@Getter
public enum PaypalDataParseType {

    /**
     * 交易
     */
    TRANSACTION("transaction");

    /**
     * code
     */
    private String code;

    PaypalDataParseType(String code){
        this.code = code;
    }

    public static Set<String> toSet(){
        return Arrays.stream(PaypalDataParseType.values()).map(PaypalDataParseType::getCode).collect(Collectors.toSet());
    }
}
