package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.AdReport;
import com.chubchen.admin.modules.easesea.dao.mapper.AdReportMapper;
import com.chubchen.admin.modules.easesea.service.IAdReportService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
@Service
@DS("easesea")
public class AdReportServiceImpl extends ServiceImpl<AdReportMapper, AdReport> implements IAdReportService {

    @Override
    public int add(AdReport adReport){
        return baseMapper.insert(adReport);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AdReport adReport){
        return baseMapper.updateById(adReport);
    }

    @Override
    public AdReport findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchSaveUpdate(List<AdReport> list) {
        if(list == null || list.size() <=0){
            return;
        }
        list.forEach(item->{
            AdReport dbReport = baseMapper.selectByUniqueIndexForUpdate(item);
            if(dbReport != null && dbReport.getId() > 0){
                item.setId(dbReport.getId());
                baseMapper.updateById(item);
            }else{
                baseMapper.insert(item);
            }
        });
    }
}
