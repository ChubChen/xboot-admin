package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderFulfillments对象", description="")
public class OrderFulfillments extends Model<OrderFulfillments> {

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty(value = "平台发货id")
    private String pFulfillmentId;

    @ApiModelProperty(value = "域名id")
    private Integer domainId;

    @ApiModelProperty(value = "后台账户id")
    private Integer backendId;

    @ApiModelProperty(value = "订单表主键id")
    private String orderId;

    @ApiModelProperty(value = "平台订单id")
    private String pOrderId;

    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    private Date updatedAt;

    @ApiModelProperty(value = "物流公司")
    private String trackingCompany;

    @ApiModelProperty(value = "物流跟踪号")
    private String trackingNumber;

    @ApiModelProperty(value = "物流跟踪url")
    private String trackingUrl;

    @TableField(exist = false)
    private List<OrderFulfillmentItems> orderFulfillmentItemsList;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
