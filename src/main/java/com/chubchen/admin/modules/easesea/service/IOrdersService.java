package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IOrdersService extends IService<Orders> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param orders 过滤条件
     * @param searchVo 过滤条件
     * @return IPage<Orders>
     */
    IPage<Orders> findListByPage(PageVo pageVo, Orders orders, SearchVo searchVo);

   /**
    * 查询分页数据
    * @param orders 过滤条件
    * @param searchVo 过滤条件
    * @return  List<Orders>
    */
    List<Orders> findListAll(Orders orders,SearchVo searchVo);

    /**
     * 添加
     *
     * @param orders 
     * @return int
     */
    int add(Orders orders);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param orders 
     * @return int
     */
    int updateData(Orders orders);

    /**
     * id查询数据
     *
     * @param id id
     * @return Orders
     */
    Orders findById(Long id);

    /**
     * 批量保存
     * @return
     */
    int batchSaveOnDuplicateKey(List<Orders> orders);

    /**
     * 根据业务ID 唯一索引删除数据然后插入
     * @param ordersList
     * @return
     */
    List<Orders> findByUniqueId(List<Orders> ordersList);

   /**
    * 查询各项汇总信息
    * @param timeZone
    * @param flag
    * @param domainIdList
    * @param pageVo
    * @param searchVo
    * @return
    */
   List<ShopifyOrderStatistic> getSaleOrderStatistic(String timeZone, Boolean flag, List<Integer> domainIdList, PageVo pageVo, SearchVo searchVo);
}
