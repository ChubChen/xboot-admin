package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.AdReport;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
public interface IAdReportService extends IService<AdReport> {

    /**
     * 添加
     *
     * @param adReport 
     * @return int
     */
    int add(AdReport adReport);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param adReport 
     * @return int
     */
    int updateData(AdReport adReport);

    /**
     * id查询数据
     *
     * @param id id
     * @return AdReport
     */
    AdReport findById(Long id);

    /**
     * 批量保持和更新数据
     * @param list
     */
    void batchSaveUpdate(List<AdReport> list);
}
