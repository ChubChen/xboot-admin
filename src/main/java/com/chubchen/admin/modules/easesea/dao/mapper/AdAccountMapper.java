package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.AdAccountMapper;
import com.chubchen.admin.modules.easesea.entity.AdAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-16
*/
@DS("easesea")
public interface AdAccountMapper extends BaseMapper<AdAccount> {

    /**
     * 根据条件分页查询
     * @param page
     * @param adAccount
     * @return IPage<AdAccount>
    */
    IPage<AdAccount> selectListByPage(IPage<AdAccount> page, @Param("adAccount") AdAccount adAccount);

    /**
     * 根据条件查询全部
     * @param adAccount
     * @return List<AdAccount>
    */
    List<AdAccount> selectAll(@Param("adAccount") AdAccount adAccount);

}
