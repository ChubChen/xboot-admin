package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountDomain对象", description="")
public class AccountDomain extends Model<AccountDomain> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "domain_id", type = IdType.AUTO)
    private Integer domainId;

    @ApiModelProperty(value = "域名地址")
    private String domainUrl;

    @ApiModelProperty(value = "域名状态： - 0 备用 - -1 不可用")
    private Integer domainStatus;

    @ApiModelProperty(value = "在 shopify 是否被封禁")
    private Boolean shopifyDisabled;

    @ApiModelProperty(value = "对应的后端店铺 id")
    private Integer backendId;

    @ApiModelProperty(value = "创建日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdAt = new Date();

    @ApiModelProperty(value = "过期日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expiredAt;

    @ApiModelProperty(value = "网站 logo 地址")
    private String logoUrl;

    @ApiModelProperty(value = "Google 账号")
    private String googleAccount;

    @ApiModelProperty(value = "Google 密码")
    private String googlePassword;

    @ApiModelProperty(value = "Instagram 密码")
    private String insPassword;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt = new Date();


    @Override
    protected Serializable pkVal() {
        return this.domainId;
    }

}
