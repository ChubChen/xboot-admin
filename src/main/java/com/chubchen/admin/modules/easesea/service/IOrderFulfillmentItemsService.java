package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillmentItems;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IOrderFulfillmentItemsService extends IService<OrderFulfillmentItems> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param orderFulfillmentItems 过滤条件
     * @return IPage<OrderFulfillmentItems>
     */
    IPage<OrderFulfillmentItems> findListByPage(PageVo pageVo, OrderFulfillmentItems orderFulfillmentItems);

   /**
    * 查询分页数据
    *
    * @return  List<OrderFulfillmentItems>
    */
    List<OrderFulfillmentItems> findListAll(OrderFulfillmentItems orderFulfillmentItems);

    /**
     * 添加
     *
     * @param orderFulfillmentItems 
     * @return int
     */
    int add(OrderFulfillmentItems orderFulfillmentItems);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param orderFulfillmentItems 
     * @return int
     */
    int updateData(OrderFulfillmentItems orderFulfillmentItems);

    /**
     * id查询数据
     *
     * @param id id
     * @return OrderFulfillmentItems
     */
    OrderFulfillmentItems findById(Long id);
}
