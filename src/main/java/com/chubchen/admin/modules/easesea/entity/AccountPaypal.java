package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountPaypal对象", description="")
public class AccountPaypal extends Model<AccountPaypal> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "paypal_id", type = IdType.AUTO)
    private Integer paypalId;

    @ApiModelProperty(value = "0-备用,1-启用中,2-禁用")
    private Integer status;

    @ApiModelProperty(value = "账户名称")
    private String name;

    @ApiModelProperty(value = "所使用的邮箱")
    private String email;

    @ApiModelProperty(value = "登录密码")
    private String password;

    @ApiModelProperty(value = "注册时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date registerAt;

    @ApiModelProperty(value = "API接口ClientId")
    private String paypalClientId;

    @ApiModelProperty(value = "PaylPal接口秘钥")
    private String paypalSecret;

    @ApiModelProperty(value = "电话")
    private String telphone;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "注册日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    @ApiModelProperty(value = "备注信息")
    private String note;


    @Override
    protected Serializable pkVal() {
        return this.paypalId;
    }

}
