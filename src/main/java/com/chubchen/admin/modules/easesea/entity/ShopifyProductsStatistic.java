package com.chubchen.admin.modules.easesea.entity;


import lombok.Data;

import java.math.BigDecimal;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-02-15
 */
@Data
public class ShopifyProductsStatistic {


    private String cDate;

    private Integer quantity;

    private BigDecimal price;

    private Integer domainId;

}
