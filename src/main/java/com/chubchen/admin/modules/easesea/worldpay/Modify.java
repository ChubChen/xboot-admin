package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author chubchen
 */
@Data
@XmlRootElement(name = "modify")
@XmlAccessorType(XmlAccessType.FIELD)
public class Modify {

    @XmlElement(name = "orderModification")
    private WorldPayOrder order;
}
