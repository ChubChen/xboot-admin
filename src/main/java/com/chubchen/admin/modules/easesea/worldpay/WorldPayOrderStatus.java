package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.*;

@Data
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class WorldPayOrderStatus {

    @XmlElement
    private WorldPayError error;

    @XmlElement
    private Payment payment;
}
