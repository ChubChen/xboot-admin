package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SlPaypalRefundRequest对象", description="")
public class SlPaypalRefundRequest extends Model<SlPaypalRefundRequest> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "batch_id")
    private String batchId;

    private Integer count;

    private Date createdTime;

    private Date finishedTime;

    private Integer status;

    @Getter
    public enum SlPaypalRefundStatusEnum {
        CANCEL(-1,"取消"),
        INIT(0,"排队等待处理中"),
        RUNNING(1,"处理中"),
        PENDING(3,"全部请求完成等待结果返回"),
        FINISH(2,"已完成");

        private int code;

        private String message;

        SlPaypalRefundStatusEnum(int code,String messsage){
            this.code = code;
            this.message = messsage;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(SlPaypalRefundStatusEnum.values())
                    .collect(Collectors.toMap(SlPaypalRefundStatusEnum::getCode, SlPaypalRefundStatusEnum::getMessage));
        }
    }

    @Override
    protected Serializable pkVal() {
        return this.batchId;
    }

}
