package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProducts;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IPlatformProductsService extends IService<PlatformProducts> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param platformProducts 过滤条件
     * @return IPage<PlatformProducts>
     */
    IPage<PlatformProducts> findListByPage(PageVo pageVo, PlatformProducts platformProducts, SearchVo searchVo);

   /**
    * 查询分页数据
    *
    * @return  List<PlatformProducts>
    */
    List<PlatformProducts> findListAll(PlatformProducts platformProducts);

    /**
     * 添加
     *
     * @param platformProducts 
     * @return int
     */
    int add(PlatformProducts platformProducts);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param platformProducts 
     * @return int
     */
    int updateData(PlatformProducts platformProducts);

    /**
     * id查询数据
     *
     * @param id id
     * @return PlatformProducts
     */
    PlatformProducts findById(Long id);

    /**
     * 根据业务Id查询
     * @param list
     * @return
     */
    List<PlatformProducts> findByUniqueId(List<PlatformProducts> list);

    /**
     * 统计SPU维度的产品
     * @param products
     * @param pageVo
     * @param searchVo
     * @return
     */
    IPage<PlatformProducts> findSPUProductStatistic(PlatformProducts products, PageVo pageVo, SearchVo searchVo);

    /**
     * 导出全部SPU维度的产品数据
     * @param products
     * @param searchVo
     * @return
     */
    List<PlatformProducts> findSPUProductStatisticAll(PlatformProducts products, SearchVo searchVo);

   /**
    * 获取无销量的产品
    * @param products
    * @param pageVo
    * @param searchVo
    * @return
    */
   IPage<PlatformProducts> getSPUNoSaleProduct(PlatformProducts products, PageVo pageVo, SearchVo searchVo);

    /**
     * 根据时间统计
     * @param spu
     * @param productId
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getProductStaticByTime(String spu, String productId, SearchVo searchVo);

    /**
     * 店匠根据hanle查询url
     * @param handle
     * @param backendId
     * @return
     */
    List<String> getShoplazzaIdByUrl(List<String> handle, Integer backendId);

    /**
     * 非SPU站点无销量产品统计
     * @param platformProducts
     * @param pageVo
     * @param searchVo
     * @return
     */
    IPage<PlatformProducts> getNoSaleProduct(PlatformProducts platformProducts, PageVo pageVo, SearchVo searchVo);
}
