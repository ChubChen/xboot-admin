package com.chubchen.admin.modules.easesea.worldpay;


import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author chubchen
 */
@Data
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class Reply {

    @XmlElement
    private WorldPayError error;

    @XmlElement
    private WorldPayOk  ok;

    @XmlElement
    private WorldPayOrderStatus orderStatus;
}
