package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AccountPaypal;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalRefundRequestMapper;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
@Service
@DS("easesea")
public class SlPaypalRefundRequestServiceImpl extends ServiceImpl<SlPaypalRefundRequestMapper, SlPaypalRefundRequest> implements ISlPaypalRefundRequestService {

    @Autowired
    private ISlPaypalRefundRequestDetailService paypalRefundRequestDetailService;
    @Autowired
    private ISlPaypalRefundRequestService paypalRefundRequestService;
    @Autowired
    private ISlPaypalAccountService accountService;

    @Override
    public IPage<SlPaypalRefundRequest> findListByPage(SearchVo searchVo,
                                                       PageVo pageVo,
                                                       SlPaypalRefundRequest slPaypalRefundRequest) {
        IPage<AccountPaypal> page = PageUtil.initMpPage(pageVo);
        return baseMapper.findListByPage(page, searchVo, slPaypalRefundRequest);
    }

    @Override
    public int add(SlPaypalRefundRequest slPaypalRefundRequest){
        return baseMapper.insert(slPaypalRefundRequest);
    }

    @Override
    public int delete(String id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(SlPaypalRefundRequest slPaypalRefundRequest){
        return baseMapper.updateById(slPaypalRefundRequest);
    }

    @Override
    public SlPaypalRefundRequest findById(String id){
        return  baseMapper.selectById(id);
    }

    @Override
    public void handelRequest(SlPaypalRefundRequest request){
        SlPaypalRefundRequest dbRequest = baseMapper.selectById(request.getBatchId());
        if(dbRequest != null && SlPaypalRefundRequest.SlPaypalRefundStatusEnum.INIT.getCode() == dbRequest.getStatus().intValue()){
            //开始处理任务
            dbRequest.setStatus(SlPaypalRefundRequest.SlPaypalRefundStatusEnum.RUNNING.getCode());
            baseMapper.updateById(dbRequest);
            handelRequestInner(dbRequest);
        }
    }

    @Override
    public void handelRequestRetry(SlPaypalRefundRequest request){
        SlPaypalRefundRequest dbRequest = baseMapper.selectById(request.getBatchId());
        if(dbRequest != null && SlPaypalRefundRequest.SlPaypalRefundStatusEnum.RUNNING.getCode() == dbRequest.getStatus().intValue()){
            handelRequestInner(dbRequest);
        }
    }

    private void handelRequestInner(SlPaypalRefundRequest dbRequest){
        List<SlPaypalRefundRequestDetail> resultList = paypalRefundRequestDetailService.list(new QueryWrapper<SlPaypalRefundRequestDetail>()
                .eq("batch_id", dbRequest.getBatchId())
                .eq("status", SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.INIT.getCode()));
        List<SlPaypalAccount> paypalAccountList = accountService.
                list(new QueryWrapper<SlPaypalAccount>().eq("is_del", 0)
                        .eq("is_sandbox", 0));
        Map<String,SlPaypalAccount> accountMap = paypalAccountList.stream().
                collect(Collectors.toMap(SlPaypalAccount::getAccount, item->item));

        resultList.forEach(item-> {
            try{
                int statusCode = paypalRefundRequestDetailService.handleRequestPaypal(item, accountMap.get(item.getAccount()));
                if(statusCode == 200 || statusCode == 201){
                    Thread.sleep(1000 );
                }
                if(statusCode == 429 ){
                    Thread.sleep( 5*60 * 1000);
                }
            }catch (Exception e){
                log.error("处理异常", e);
            }
        });
        updateRequestStatus(dbRequest.getBatchId());
    }

    @Override
    public void updateRequestStatus(String batchId) {
        SlPaypalRefundRequest dbRequest = paypalRefundRequestService.findById(batchId);
        if(dbRequest != null){
            //判断处理完成后更行处理完成
            int count = paypalRefundRequestDetailService.count(new QueryWrapper<SlPaypalRefundRequestDetail>()
                    .eq("batch_id", dbRequest.getBatchId())
                    .in("status", SlPaypalRefundRequestDetail.NO_END_LIST));
            if(count <= 0 ){
                int countAccepted = paypalRefundRequestDetailService.count(new QueryWrapper<SlPaypalRefundRequestDetail>()
                        .eq("batch_id", dbRequest.getBatchId())
                        .eq("status", SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.Accepted.getCode())
                        .ne("platform", SlPaypalRefundRequestDetail.PayPlatform.PAYPAL.getCode()));
                dbRequest.setFinishedTime(new Date());
                if(countAccepted > 0){
                    dbRequest.setStatus(SlPaypalRefundRequest.SlPaypalRefundStatusEnum.PENDING.getCode());
                }else{
                    dbRequest.setStatus(SlPaypalRefundRequest.SlPaypalRefundStatusEnum.FINISH.getCode());
                }
                baseMapper.updateById(dbRequest);
            }
        }
    }
}
