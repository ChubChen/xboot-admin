package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProducts;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import com.chubchen.admin.modules.easesea.entity.ShopifyProducts;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface PlatformProductsMapper extends BaseMapper<PlatformProducts> {

    /**
     * 根据条件分页查询
     * @param page
     * @param platformProducts
     * @return IPage<PlatformProducts>
    */
    IPage<PlatformProducts> selectListByPage(IPage<PlatformProducts> page,
                                             @Param("platformProducts") PlatformProducts platformProducts,
                                             @Param("search")SearchVo searchVo);

    /**
     * 根据条件查询全部
     * @param platformProducts
     * @return List<PlatformProducts>
    */
    List<PlatformProducts> selectAll(@Param("platformProducts") PlatformProducts platformProducts);

    /**
     * 根据业务Id查询数据
     * @param list
     * @return
     */
    List<PlatformProducts> findByUniqueId(List<PlatformProducts> list);

    /**
     * 根据统计产品相关
     * @param timeZone
     * @param flag
     * @param domainIdList
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getSaleProductStatistic(@Param("timeZone") String timeZone,
                                                        @Param("flag") Boolean flag,
                                                        @Param("domainIdList") List<Integer> domainIdList,
                                                        @Param("searchVo") SearchVo searchVo);
    /**
     * 查询SPU统计
     * @param page
     * @param platformProducts
     * @param searchVo
     * @return
     */

    IPage<PlatformProducts> findSPUProductStatistic(IPage page,
                                                   @Param("product") PlatformProducts platformProducts,
                                                   @Param("search")SearchVo searchVo);

    /**
     * 查询SPU统计全部
     * @param platformProducts
     * @param searchVo
     * @return
     */

    List<PlatformProducts> findSPUProductStatistic(@Param("product") PlatformProducts platformProducts,
                                                  @Param("search")SearchVo searchVo);

    /**
     * 统计无销量SPU
     * @param page
     * @param products
     * @param searchVo
     * @return
     */
    IPage<PlatformProducts> getSPUNoSaleProduct(Page<ShopifyProducts> page,
                                                @Param("product") PlatformProducts products,
                                                @Param("search") SearchVo searchVo);

    /**
     * spu根据日期汇总
     * @param spu
     * @param productId
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getProductStaticByTime(@Param("spu") String spu,
                                                         @Param("productId") String productId,
                                                         @Param("search") SearchVo searchVo);

    /**
     * 根据后台id以及handle查询产品url
     * @param handle
     * @param backendId
     * @return
     */
    List<String> getShoplazzaIdByUrl(@Param("handles")List<String> handle,
                                     @Param("backendId")Integer backendId);

    /**
     * 无销量产品统计
     * @param page
     * @param platformProducts
     * @param searchVo
     * @return
     */
    IPage<PlatformProducts> getNoSaleProduct(IPage<PlatformProducts> page,
                                             @Param("product") PlatformProducts platformProducts,
                                             @Param("search")SearchVo searchVo);
}
