package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SlPaypalRefundRequestDetail对象", description="")
public class SlPaypalRefundRequestDetail extends Model<SlPaypalRefundRequestDetail> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id")
    private String id;

    @ApiModelProperty(value = "批次号")
    private String batchId;

    private String captureId;

    @ApiModelProperty(value = "订单id")
    private String orderId;

    @ApiModelProperty(value = "账号")
    private String account;

    @ApiModelProperty(value = "站点")
    private String site;

    @ApiModelProperty(value = "金额")
    private BigDecimal amount;

    @ApiModelProperty(value = "币种")
    private String currencyCode;

    @ApiModelProperty(value = "退款成功的退款ID")
    private String refundId;

    @ApiModelProperty(value = "状态")
    private String status;

    @ApiModelProperty(value = "错误描述")
    private String message;

    private String platform;

    private Date createdTime;

    private Date refundTime;

    @Getter
    public  enum SlPaypalRefundDetailStatusEnum {
        INIT("INIT","排队等待处理中"),
        RUNNING("RUNNING","处理中"),
        ERROR("ERROR","处理失败"),
        COMPLETED("COMPLETED","处理成功"),
        PENDING("PENDING","结果待定"),
        Accepted("ACCEPTED", "已受理"),
        CANCEL("CANCEL","取消"),
        CANCELLED("CANCELLED","已取消的或者已退款"),
        REFUSED("REFUSED","被拒绝"),
        SETTLED_BY_MERCHANT("SETTLED_BY_MERCHANT","收单机构将确认实际结算"),
        CHARGED_BACK("CHARGED_BACK","CHARGED_BACK"),
        REFUND_FAILED("REFUND_FAILED","退款失败");

        private String code;

        private String message;

        SlPaypalRefundDetailStatusEnum(String code,String messsage){
            this.code = code;
            this.message = messsage;
        }

        public static Map<String,String> getMap(){
           return Arrays.stream(SlPaypalRefundDetailStatusEnum.values()).collect(Collectors.toMap(
                   SlPaypalRefundDetailStatusEnum::getCode, SlPaypalRefundDetailStatusEnum::getMessage
           ));
        }
    }

    @Getter
    public  enum PayPlatform {
        PAYPAL("PAYPAL","paypal"),
        WORLDPAY("WORLDPAY","WorldPay"),
        ADYEN("ADYEN","adyen");

        private String code;

        private String message;

        PayPlatform(String code ,String message){
            this.code = code;
            this.message = message;
        }

        public static Map<String,String> getMap(){
            return Arrays.stream(PayPlatform.values()).collect(Collectors.toMap(
                    PayPlatform::getCode, PayPlatform::getMessage
            ));
        }
    }

    public static List<String> NO_END_LIST =  new ArrayList<>();
    static {
        NO_END_LIST.add(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.INIT.getCode());
        NO_END_LIST.add(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.RUNNING.getCode());
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
