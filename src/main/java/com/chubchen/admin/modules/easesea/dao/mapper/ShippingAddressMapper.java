package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.ShippingAddressMapper;
import com.chubchen.admin.modules.easesea.entity.ShippingAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-12
*/
@DS("easesea")
public interface ShippingAddressMapper extends BaseMapper<ShippingAddress> {

    /**
     * 根据条件分页查询
     * @param page
     * @param shippingAddress
     * @return IPage<ShippingAddress>
    */
    IPage<ShippingAddress> selectListByPage(IPage<ShippingAddress> page, @Param("shippingAddress") ShippingAddress shippingAddress);

    /**
     * 根据条件查询全部
     * @param shippingAddress
     * @return List<ShippingAddress>
    */
    List<ShippingAddress> selectAll(@Param("shippingAddress") ShippingAddress shippingAddress);

}
