package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@Getter
public enum AdAccountStatusEnum {


    DISABLE(-1, "被封"),
    NEW(0, "新建"),
    OPEN(1, "使用中");

    private int code;

    private String desc;

    AdAccountStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> getMap(){
        return Arrays.stream(AdAccountStatusEnum.values()).
                collect(Collectors.toMap(AdAccountStatusEnum::getCode, AdAccountStatusEnum::getDesc));
    }
}
