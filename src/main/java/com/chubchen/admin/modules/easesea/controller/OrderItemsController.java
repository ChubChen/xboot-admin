package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IOrderItemsService;
import com.chubchen.admin.modules.easesea.entity.OrderItems;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/order-items")
public class OrderItemsController {

    @Resource
    private IOrderItemsService orderItemsService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute OrderItems orderItems){
        Integer count = orderItemsService.add(orderItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = orderItemsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute OrderItems orderItems){
        Integer count = orderItemsService.updateData(orderItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<OrderItems>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute OrderItems orderItems){
            IPage<OrderItems> page = orderItemsService.findListByPage(pageVo, orderItems);
         return new ResultUtil<IPage<OrderItems>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<OrderItems> findById(@PathVariable Long id){
        OrderItems orderItems=  orderItemsService.findById(id);
        return new ResultUtil<OrderItems>().setData(orderItems);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<OrderItems>> findById(@ModelAttribute OrderItems orderItems){
        List<OrderItems> orderItemsList =  orderItemsService.findListAll(orderItems);
        return new ResultUtil<List<OrderItems>>().setData(orderItemsList);
    }

}
