package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.modules.easesea.constants.PlatformConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 分发器
 */
@Component
@Slf4j
public class DataParaseDisparcher {

    private Map<String, DataParseHandle> handlers = new ConcurrentHashMap<>();

    @Autowired
    private ShopifyDataParseHandle shopifyDataParseHandle;

    @Autowired
    private ShoplazzaDataParseHandle shoplazzaDataParseHandle;

    @Autowired
    private PayPalDataParseHandle payPalDataParseHandle;

    @PostConstruct
    public void init() {
        this.handlers.put(PlatformConstants.SHOPIFY, shopifyDataParseHandle);
        this.handlers.put(PlatformConstants.SHOPLAZZA, shoplazzaDataParseHandle);
        this.handlers.put(PlatformConstants.PAYPAL, payPalDataParseHandle);
    }


    public void dispatch(String platform, JSONObject dataParse) {
        try{
            if(this.handlers.containsKey(platform)){
                this.handlers.get(platform).handle(dataParse);
            }else{
                log.error("暂时不能解析该平台的消息，消息dataParse:{}", dataParse);
            }
        }catch (Exception e){
            log.error("解析数据失败", e);
        }
    }
}
