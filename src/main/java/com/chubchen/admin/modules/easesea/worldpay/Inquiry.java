package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Data
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class Inquiry {

   private OrderInquiry orderInquiry;
}
