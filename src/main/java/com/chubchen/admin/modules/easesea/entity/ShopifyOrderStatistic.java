package com.chubchen.admin.modules.easesea.entity;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author chubchen
 * 订单统计信息
 */
@Data
@ToString
public class ShopifyOrderStatistic implements Serializable {


    private String cDate;

    @Deprecated
    private String storeUrl;

    private Integer domainId;

    private Integer abandonedOrders;

    private Integer sendEmailOrders;

    private Integer recoverOrders;

    private Integer totalOrders;

    private Integer validOrders;


    private Integer fulfilledOrders;


    private Integer fulfilled5Days;


    private Integer fulfilled10Days;


    private BigDecimal totalAmount;

    private BigDecimal refundAmount;

    private BigDecimal netTotalAmount;

    /**
     * 复购订单数
     */
    private BigDecimal returning;

    private Integer productsCount;

    private Integer  spuCount;

    private Integer quantitySum;

    private BigDecimal spend;

    /**
     * 金额
     */
    private BigDecimal purchase;


    private BigDecimal purchaseValue;

    /**
     * ROAS
     */
    private BigDecimal roas;

}
