package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

/**
 * @author chubchen
 */
@Getter
public enum OrdersFulfillmentStatusEnum {

    unShipped("unShipped","未发货"),
    partial("partial","部分发货"),
    shipped("shipped","已发货"),
    cancelled("cancelled","已取消"),
    returned("returned","已退货");

    private String code;

    private String desc;

    OrdersFulfillmentStatusEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
