package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrderItems;
import com.chubchen.admin.modules.easesea.dao.mapper.AbandonedOrderItemsMapper;
import com.chubchen.admin.modules.easesea.service.IAbandonedOrderItemsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class AbandonedOrderItemsServiceImpl extends ServiceImpl<AbandonedOrderItemsMapper, AbandonedOrderItems> implements IAbandonedOrderItemsService {

    @Override
    public  IPage<AbandonedOrderItems> findListByPage(PageVo pageVo, AbandonedOrderItems abandonedOrderItems){
        IPage<AbandonedOrderItems> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, abandonedOrderItems);
    }

    @Override
    public List<AbandonedOrderItems> findListAll(AbandonedOrderItems abandonedOrderItems){
        return baseMapper.selectAll(abandonedOrderItems);
    }

    @Override
    public int add(AbandonedOrderItems abandonedOrderItems){
        return baseMapper.insert(abandonedOrderItems);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AbandonedOrderItems abandonedOrderItems){
        return baseMapper.updateById(abandonedOrderItems);
    }

    @Override
    public AbandonedOrderItems findById(Long id){
        return  baseMapper.selectById(id);
    }
}
