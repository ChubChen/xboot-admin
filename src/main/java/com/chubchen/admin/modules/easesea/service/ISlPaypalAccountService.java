package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-30
 */
public interface ISlPaypalAccountService extends IService<SlPaypalAccount> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param slPaypalAccount 过滤条件
     * @return IPage<SlPaypalAccount>
     */
    IPage<SlPaypalAccount> findListByPage(PageVo pageVo, SlPaypalAccount slPaypalAccount);

   /**
    * 查询分页数据
    *
    * @return  List<SlPaypalAccount>
    */
    List<SlPaypalAccount> findListAll(SlPaypalAccount slPaypalAccount);

    /**
     * 添加
     *
     * @param slPaypalAccount 
     * @return int
     */
    int add(SlPaypalAccount slPaypalAccount);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param slPaypalAccount 
     * @return int
     */
    int updateData(SlPaypalAccount slPaypalAccount);

    /**
     * id查询数据
     *
     * @param id id
     * @return SlPaypalAccount
     */
    SlPaypalAccount findById(Long id);
}
