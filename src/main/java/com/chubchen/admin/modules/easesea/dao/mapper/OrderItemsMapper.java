package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.OrderItemsMapper;
import com.chubchen.admin.modules.easesea.entity.OrderItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface OrderItemsMapper extends BaseMapper<OrderItems> {

    /**
     * 根据条件分页查询
     * @param page
     * @param orderItems
     * @return IPage<OrderItems>
    */
    IPage<OrderItems> selectListByPage(IPage<OrderItems> page, @Param("orderItems") OrderItems orderItems);

    /**
     * 根据条件查询全部
     * @param orderItems
     * @return List<OrderItems>
    */
    List<OrderItems> selectAll(@Param("orderItems") OrderItems orderItems);

}
