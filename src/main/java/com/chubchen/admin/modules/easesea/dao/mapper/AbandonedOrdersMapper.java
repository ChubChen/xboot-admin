package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import com.chubchen.admin.modules.easesea.vo.AbandonedStatic;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface AbandonedOrdersMapper extends BaseMapper<AbandonedOrders> {

    /**
     * 根据条件分页查询
     * @param page
     * @param abandonedOrders
     * @return IPage<AbandonedOrders>
    */
    IPage<AbandonedOrders> selectListByPage(IPage<AbandonedOrders> page,
                                            @Param("abandonedOrders") AbandonedOrders abandonedOrders,
                                            @Param("search")SearchVo search);

    /**
     * 根据条件查询全部
     * @param abandonedOrders
     * @return List<AbandonedOrders>
    */
    List<AbandonedOrders> selectAll(@Param("abandonedOrders") AbandonedOrders abandonedOrders);

    /**
     * 根据业务id查询
     * @return
     */
    List<AbandonedOrders> findByUniqueId(@Param("list")List<AbandonedOrders> list);

    /**
     * 根据过滤条件查询数据
     * @param timeZone
     * @param flag
     * @param domainIdList
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getAbandonedOrderStatistic(@Param("timeZone") String timeZone,
                                                           @Param("flag") Boolean flag,
                                                           @Param("domainIdList") List<Integer> domainIdList,
                                                           @Param("searchVo") SearchVo searchVo);

    /**
     * 汇总每天的弃购信息
     * @param page
     * @param domainId
     * @param searchVo
     * @return
     */
    IPage<AbandonedStatic> getStatic(IPage<AbandonedOrders> page,
                                     @Param("domainId") Integer domainId,
                                     @Param("search") SearchVo searchVo);
}
