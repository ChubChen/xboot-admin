package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AbandonedOrders对象", description="")
public class AbandonedOrders extends Model<AbandonedOrders> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public AbandonedOrders(){

    }

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "后台id")
    private Integer backendId;

    private Integer domainId;

    @ApiModelProperty(value = "平台弃购id")
    private String pOrderId;

    @ApiModelProperty(value = "订单创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "支付网关")
    private String gateway;

    private String email;

    private String firstName;

    private String lastName;

    @ApiModelProperty(value = "客户历史下单数量")
    private Integer ordersCount;

    @ApiModelProperty(value = "客户消费总额")
    private BigDecimal totalSpent;

    private String currency;

    @ApiModelProperty(value = "国家")
    private String country;

    private BigDecimal subtotalPrice;

    @ApiModelProperty(value = "是否发送邮件")
    private Integer sentEmail;

    @ApiModelProperty(value = "发送时间")
    private Date sentTime;

    @ApiModelProperty(value = "弃购购买url")
    private String abandonedUrl;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
