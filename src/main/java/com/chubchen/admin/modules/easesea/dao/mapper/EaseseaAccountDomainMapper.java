package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AccountDomain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-08
*/
@DS("easesea")
public interface EaseseaAccountDomainMapper extends BaseMapper<AccountDomain> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountDomain
     * @return IPage<AccountDomain>
    */
    IPage<AccountDomain> selectListByPage(IPage<AccountDomain> page,
                                          @Param("accountDomain") AccountDomain accountDomain,
                                          @Param("search")SearchVo searchVo);

    /**
     * 根据条件查询全部
     * @param accountDomain
     * @return List<AccountDomain>
    */
    List<AccountDomain> selectAll(@Param("accountDomain") AccountDomain accountDomain,@Param("search")SearchVo searchVo);

}
