package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.AccountBackendMapper;
import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-05
*/
@DS("easesea")
public interface AccountBackendMapper extends BaseMapper<AccountBackend> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountBackend
     * @return IPage<AccountBackend>
    */
    IPage<AccountBackend> selectListByPage(IPage<AccountBackend> page, @Param("accountBackend") AccountBackend accountBackend);

    /**
     * 根据条件查询全部
     * @param accountBackend
     * @return List<AccountBackend>
    */
    List<AccountBackend> selectAll(@Param("accountBackend") AccountBackend accountBackend);

    /**
     * 批量更新状态
     * @param status
     * @param ids
     * @return
     */
    Integer updateStatusById(@Param("status") Integer status,@Param("ids") Integer[] ids);
}
