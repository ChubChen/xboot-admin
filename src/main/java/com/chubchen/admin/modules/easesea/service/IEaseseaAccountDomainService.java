package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AccountDomain;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
public interface IEaseseaAccountDomainService extends IService<AccountDomain> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param searchVo      页码
     * @param accountDomain 过滤条件
     * @return IPage<AccountDomain>
     */
    IPage<AccountDomain> findListByPage(PageVo pageVo, AccountDomain accountDomain,SearchVo searchVo);

    /**
     * 查询分页数据
     * @param accountDomain
     * @param searchVo
     * @return
     */
    List<AccountDomain> findListAll(AccountDomain accountDomain,SearchVo searchVo);

    /**
     * 添加
     *
     * @param accountDomain 
     * @return int
     */
    int add(AccountDomain accountDomain);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param accountDomain 
     * @return int
     */
    int updateData(AccountDomain accountDomain);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountDomain
     */
    AccountDomain findById(Long id);

    /**
     * 查询全部信息从缓存中
     * @return
     */
    List<AccountDomain> getAll();
}
