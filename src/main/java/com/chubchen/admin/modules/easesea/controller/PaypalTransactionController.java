package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IPaypalTransactionService;
import com.chubchen.admin.modules.easesea.entity.PaypalTransaction;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-07-21
 */
@Api(tags = {""})
@RestController
@RequestMapping("/paypal-transaction")
public class PaypalTransactionController {

    @Resource
    private IPaypalTransactionService paypalTransactionService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute PaypalTransaction paypalTransaction){
        Integer count = paypalTransactionService.add(paypalTransaction);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = paypalTransactionService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute PaypalTransaction paypalTransaction){
        Integer count = paypalTransactionService.updateData(paypalTransaction);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<PaypalTransaction>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute PaypalTransaction paypalTransaction){
            IPage<PaypalTransaction> page = paypalTransactionService.findListByPage(pageVo, paypalTransaction);
         return new ResultUtil<IPage<PaypalTransaction>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<PaypalTransaction> findById(@PathVariable Long id){
        PaypalTransaction paypalTransaction=  paypalTransactionService.findById(id);
        return new ResultUtil<PaypalTransaction>().setData(paypalTransaction);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<PaypalTransaction>> findById(@ModelAttribute PaypalTransaction paypalTransaction){
        List<PaypalTransaction> paypalTransactionList =  paypalTransactionService.findListAll(paypalTransaction);
        return new ResultUtil<List<PaypalTransaction>>().setData(paypalTransactionList);
    }

}
