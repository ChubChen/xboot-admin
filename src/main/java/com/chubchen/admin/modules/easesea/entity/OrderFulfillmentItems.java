package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="OrderFulfillmentItems对象", description="")
public class OrderFulfillmentItems extends Model<OrderFulfillmentItems> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public OrderFulfillmentItems(){

    }

    @ApiModelProperty(value = "主键id")
    private String id;

    private String fulfillmentId;

    @ApiModelProperty(value = "平台运单id")
    private String pFulfillmentId;

    @ApiModelProperty(value = "平台运单明细id")
    private String pFulfillmentItemId;

    @ApiModelProperty(value = "order表主键ID")
    private String orderId;

    private String sku;

    private Integer quantity;

    private String pProductId;

    private String pVariantId;

    private Date createdAt;

    private Date updatedAt;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
