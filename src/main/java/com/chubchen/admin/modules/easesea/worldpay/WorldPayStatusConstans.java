package com.chubchen.admin.modules.easesea.worldpay;

public interface WorldPayStatusConstans {

    String SENT_FOR_REFUND = "SENT_FOR_REFUND";

    String REFUNDED = "REFUNDED";

    String REFUNDED_BY_MERCHANT = "REFUNDED_BY_MERCHANT";

    String SETTLED_BY_MERCHANT = "SETTLED_BY_MERCHANT";

    /**
     * 已拦截被拒绝
     */
    String REFUSED = "REFUSED";

    /**
     * 已取消
     */

    String CANCELLED = "CANCELLED";

    /**
     * 退款失败
     */
    String REFUND_FAILED = "REFUND_FAILED";

    /**
     * CHARGED_BACK
     */
    String CHARGED_BACK = "CHARGED_BACK";

    /**
     * SETTLED
     */
    String SETTLED = "SETTLED";

}
