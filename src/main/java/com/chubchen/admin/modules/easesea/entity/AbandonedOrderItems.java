package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AbandonedOrderItems对象", description="")
public class AbandonedOrderItems extends Model<AbandonedOrderItems> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public AbandonedOrderItems(){

    }

    @ApiModelProperty(value = "主键id")
    private String id;

    @ApiModelProperty(value = "后台id")
    private Integer backendId;

    private Integer domainId;

    @ApiModelProperty(value = "orders表主键ID")
    private String orderId;

    @ApiModelProperty(value = "平台订单id")
    private String pOrderId;

    @ApiModelProperty(value = "平台line_item_id")
    private String pLineItemId;

    @ApiModelProperty(value = "订单创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "平台产品ID")
    private String pProductId;

    @ApiModelProperty(value = "渠道产品名称")
    private String pProductTitle;

    @ApiModelProperty(value = "变体id")
    private String pVariantId;

    @ApiModelProperty(value = "变体title")
    private String pVariantTitle;

    @ApiModelProperty(value = "sku")
    private String sku;

    @ApiModelProperty(value = "数量")
    private Integer quantity;

    @ApiModelProperty(value = "价格")
    private BigDecimal price;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
