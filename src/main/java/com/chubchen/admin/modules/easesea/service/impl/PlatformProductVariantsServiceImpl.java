package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProductVariants;
import com.chubchen.admin.modules.easesea.dao.mapper.PlatformProductVariantsMapper;
import com.chubchen.admin.modules.easesea.service.IPlatformProductVariantsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class PlatformProductVariantsServiceImpl extends ServiceImpl<PlatformProductVariantsMapper, PlatformProductVariants> implements IPlatformProductVariantsService {

    @Override
    public  IPage<PlatformProductVariants> findListByPage(PageVo pageVo, PlatformProductVariants platformProductVariants){
        IPage<PlatformProductVariants> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, platformProductVariants);
    }

    @Override
    public List<PlatformProductVariants> findListAll(PlatformProductVariants platformProductVariants){
        return baseMapper.selectAll(platformProductVariants);
    }

    @Override
    public int add(PlatformProductVariants platformProductVariants){
        return baseMapper.insert(platformProductVariants);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(PlatformProductVariants platformProductVariants){
        return baseMapper.updateById(platformProductVariants);
    }

    @Override
    public PlatformProductVariants findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public List<PlatformProductVariants> getSaleVariantsByProductId(String spu, String productId, SearchVo searchVo) {
        return baseMapper.getSaleVariantsByProductId(spu, productId, searchVo);
    }
}
