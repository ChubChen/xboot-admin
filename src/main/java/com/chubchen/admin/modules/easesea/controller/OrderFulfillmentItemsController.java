package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IOrderFulfillmentItemsService;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillmentItems;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/order-fulfillment-items")
public class OrderFulfillmentItemsController {

    @Resource
    private IOrderFulfillmentItemsService orderFulfillmentItemsService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute OrderFulfillmentItems orderFulfillmentItems){
        Integer count = orderFulfillmentItemsService.add(orderFulfillmentItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = orderFulfillmentItemsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute OrderFulfillmentItems orderFulfillmentItems){
        Integer count = orderFulfillmentItemsService.updateData(orderFulfillmentItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<OrderFulfillmentItems>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute OrderFulfillmentItems orderFulfillmentItems){
            IPage<OrderFulfillmentItems> page = orderFulfillmentItemsService.findListByPage(pageVo, orderFulfillmentItems);
         return new ResultUtil<IPage<OrderFulfillmentItems>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<OrderFulfillmentItems> findById(@PathVariable Long id){
        OrderFulfillmentItems orderFulfillmentItems=  orderFulfillmentItemsService.findById(id);
        return new ResultUtil<OrderFulfillmentItems>().setData(orderFulfillmentItems);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<OrderFulfillmentItems>> findById(@ModelAttribute OrderFulfillmentItems orderFulfillmentItems){
        List<OrderFulfillmentItems> orderFulfillmentItemsList =  orderFulfillmentItemsService.findListAll(orderFulfillmentItems);
        return new ResultUtil<List<OrderFulfillmentItems>>().setData(orderFulfillmentItemsList);
    }

}
