package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalRefundRequestDetailMapper;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-29
*/
@DS("easesea")
public interface SlPaypalRefundRequestDetailMapper extends BaseMapper<SlPaypalRefundRequestDetail> {

    /**
     *根据ID加锁
     * @param id
     * @return
     */
    SlPaypalRefundRequestDetail queryForUpdateById(@Param("id") String id);
}
