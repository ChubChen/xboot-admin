package com.chubchen.admin.modules.easesea.util;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author chubchen
 *  TikTokAPI 查询工具类
 */
@Slf4j
public class TikTokUtil {

    public static final String ADVERTISER_PATH = "/open_api/2/reports/advertiser/get/";

    /**
     * Build request URL
     *
     * @param path Request path
     * @return Request URL
     */
    public static String buildUrl(String path) throws URISyntaxException {
        URI uri = new URI("https", "ads.tiktok.com", path, "", "");
        return uri.toString();
    }

    public static String advertiserFields(){
        List<String> fields_list = new ArrayList<>();
        fields_list.add("ctr");
        fields_list.add("ecpm");
        fields_list.add("show_cnt");
        fields_list.add("stat_cost");
        fields_list.add("show_uv");
        fields_list.add("conversion_cost");
        fields_list.add("conversion_rate");
        fields_list.add("convert_cnt");
        fields_list.add("click_cnt");
        fields_list.add("click_cost");
        fields_list.add("dy_comment");
        fields_list.add("dy_home_visited");
        fields_list.add("dy_like");
        fields_list.add("dy_share");
        fields_list.add("skip");
        return JSON.toJSONString(fields_list);
    }

}
