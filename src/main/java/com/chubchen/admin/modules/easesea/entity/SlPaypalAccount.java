package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="SlPaypalAccount对象", description="")
public class SlPaypalAccount extends Model<SlPaypalAccount> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String account;

    private String clientId;

    private String secret;

    private Integer isDel;

    @ApiModelProperty(value = "是否是沙箱环境账户")
    private Integer isSandbox;

    private String accountType;

    @ApiModelProperty(value = "APP NAME")
    private String appName;

    private Integer eid;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
