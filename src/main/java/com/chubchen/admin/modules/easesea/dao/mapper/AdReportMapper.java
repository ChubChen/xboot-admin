package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.AdReportMapper;
import com.chubchen.admin.modules.easesea.entity.AdReport;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-16
*/
@DS("easesea")
public interface AdReportMapper extends BaseMapper<AdReport> {

    /**
     * 根据唯一索引查询并加锁
     * @param item
     * @return
     */
    AdReport selectByUniqueIndexForUpdate(@Param("report") AdReport item);
}
