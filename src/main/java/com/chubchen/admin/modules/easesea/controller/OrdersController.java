package com.chubchen.admin.modules.easesea.controller;

import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IOrdersService;
import com.chubchen.admin.modules.easesea.entity.Orders;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/orders")
public class OrdersController {

    @Resource
    private IOrdersService ordersService;

    @Autowired
    private SecurityUtil securityUtil;


    @RequestMapping(value="/findOrderStatisticByTime", method = {RequestMethod.GET, RequestMethod.POST})
    public Result<List<ShopifyOrderStatistic>> getSaleOrderStatistic(@RequestParam(value = "timeZone")String timeZone,
                                                                     @RequestParam(value = "domainIdList",required = false) List<Integer> domainIdList,
                                                                     @RequestParam(required = false, defaultValue = "false") Boolean flag,
                                                                     @ModelAttribute PageVo pageVo, @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(domainIdList, searchVo);
        List<ShopifyOrderStatistic> list = ordersService.getSaleOrderStatistic(timeZone,flag, domainIdList, pageVo, searchVo);
        return new ResultUtil<List<ShopifyOrderStatistic>>().setData(list);
    }


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute Orders orders){
        Integer count = ordersService.add(orders);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = ordersService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute Orders orders){
        Integer count = ordersService.updateData(orders);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<Orders>> findListByPage(@ModelAttribute PageVo pageVo,
                                                @ModelAttribute Orders orders,
                                                @ModelAttribute SearchVo search){
            IPage<Orders> page = ordersService.findListByPage(pageVo, orders, search);
         return new ResultUtil<IPage<Orders>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<Orders> findById(@PathVariable Long id){
        Orders orders=  ordersService.findById(id);
        return new ResultUtil<Orders>().setData(orders);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<Orders>> findById(@ModelAttribute Orders orders,@ModelAttribute SearchVo search){
        List<Orders> ordersList =  ordersService.findListAll(orders, search);
        return new ResultUtil<List<Orders>>().setData(ordersList);
    }

}
