package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AccountPaypal;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-24
 */
public interface IAccountPaypalService extends IService<AccountPaypal> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param searchVo      时间检索
     * @param accountPaypal 过滤条件
     * @return IPage<AccountPaypal>
     */
    IPage<AccountPaypal> findListByPage(PageVo pageVo, SearchVo searchVo, AccountPaypal accountPaypal);

   /**
    * 查询分页数据
    *
    * @return  List<AccountPaypal>
    */
    List<AccountPaypal> findListAll(AccountPaypal accountPaypal,SearchVo searchVo);

    /**
     * 添加
     *
     * @param accountPaypal 
     * @return int
     */
    int add(AccountPaypal accountPaypal);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param accountPaypal 
     * @return int
     */
    int updateData(AccountPaypal accountPaypal);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountPaypal
     */
    AccountPaypal findById(Long id);
}
