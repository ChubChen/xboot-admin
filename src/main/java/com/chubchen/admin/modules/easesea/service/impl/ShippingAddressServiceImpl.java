package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.ShippingAddress;
import com.chubchen.admin.modules.easesea.dao.mapper.ShippingAddressMapper;
import com.chubchen.admin.modules.easesea.service.IShippingAddressService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-12
 */
@Service
@DS("easesea")
public class ShippingAddressServiceImpl extends ServiceImpl<ShippingAddressMapper, ShippingAddress> implements IShippingAddressService {

    @Override
    public  IPage<ShippingAddress> findListByPage(PageVo pageVo, ShippingAddress shippingAddress){
        IPage<ShippingAddress> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, shippingAddress);
    }

    @Override
    public List<ShippingAddress> findListAll(ShippingAddress shippingAddress){
        return baseMapper.selectAll(shippingAddress);
    }

    @Override
    public int add(ShippingAddress shippingAddress){
        return baseMapper.insert(shippingAddress);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(ShippingAddress shippingAddress){
        return baseMapper.updateById(shippingAddress);
    }

    @Override
    public ShippingAddress findById(Long id){
        return  baseMapper.selectById(id);
    }
}
