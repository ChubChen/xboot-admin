package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProductVariants;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IPlatformProductVariantsService extends IService<PlatformProductVariants> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param platformProductVariants 过滤条件
     * @return IPage<PlatformProductVariants>
     */
    IPage<PlatformProductVariants> findListByPage(PageVo pageVo, PlatformProductVariants platformProductVariants);

   /**
    * 查询分页数据
    *
    * @return  List<PlatformProductVariants>
    */
    List<PlatformProductVariants> findListAll(PlatformProductVariants platformProductVariants);

    /**
     * 添加
     *
     * @param platformProductVariants 
     * @return int
     */
    int add(PlatformProductVariants platformProductVariants);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param platformProductVariants 
     * @return int
     */
    int updateData(PlatformProductVariants platformProductVariants);

    /**
     * id查询数据
     *
     * @param id id
     * @return PlatformProductVariants
     */
    PlatformProductVariants findById(Long id);

    /**
     * 根据id或者spu统计变体销量明细
     * @param spu
     * @param productId
     * @param searchVo
     * @return
     */
    List<PlatformProductVariants> getSaleVariantsByProductId(String spu, String productId, SearchVo searchVo);
}
