package com.chubchen.admin.modules.easesea.worldpay;

import lombok.Data;

import javax.xml.bind.annotation.*;


/**
 * @author chubchen
 */
@Data
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class WorldPayError {

    @XmlAttribute
    private String code;

    @XmlValue
    private String value;
}
