package com.chubchen.admin.modules.easesea.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalRefundRequestDetailMapper;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.easesea.util.JAXBUtil;
import com.chubchen.admin.modules.easesea.worldpay.*;
import com.paypal.core.PayPalEnvironment;
import com.paypal.core.PayPalHttpClient;
import com.paypal.http.HttpResponse;
import com.paypal.payments.CapturesRefundRequest;
import com.paypal.payments.Money;
import com.paypal.payments.Refund;
import com.paypal.payments.RefundRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
@Slf4j
@Service
@DS("easesea")
public class SlPaypalRefundRequestDetailServiceImpl extends ServiceImpl<SlPaypalRefundRequestDetailMapper, SlPaypalRefundRequestDetail> implements ISlPaypalRefundRequestDetailService {

    @Autowired
    private ISlPaypalAccountService paypalAccountService;

    @Autowired
    private HttpClient httpClient;

    final static String adyenUrl = "https://pal-live.adyen.com/pal/servlet/Payment/v64/refund";

    @Override
    public int add(SlPaypalRefundRequestDetail slPaypalRefundRequestDetail){
        return baseMapper.insert(slPaypalRefundRequestDetail);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(SlPaypalRefundRequestDetail slPaypalRefundRequestDetail){
        return baseMapper.updateById(slPaypalRefundRequestDetail);
    }

    @Override
    public SlPaypalRefundRequestDetail findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int handleRequestPaypal(SlPaypalRefundRequestDetail item, SlPaypalAccount account) throws Exception{
        //加锁
        SlPaypalRefundRequestDetail dbResult = baseMapper.queryForUpdateById(item.getId());
        if(dbResult != null &&
                SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.INIT.getCode().equals(dbResult.getStatus())){
            if(account ==null){
                dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                dbResult.setMessage("该邮箱没有申请clientId以及接口权限");
                dbResult.setRefundTime(new Date());
                baseMapper.updateById(dbResult);
                return 200;
            }
            //更新为处理中
            dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.RUNNING.getCode());
            dbResult.setRefundTime(new Date());
            baseMapper.updateById(dbResult);
            //发起请求
            if(SlPaypalRefundRequestDetail.PayPlatform.PAYPAL.getCode().equals(dbResult.getPlatform())){
                return PaypalRequestInner(dbResult, account);
            }else if(SlPaypalRefundRequestDetail.PayPlatform.WORLDPAY.getCode().equals(dbResult.getPlatform())){
                return WorldPayRequetInner(dbResult, account);
            }else if(SlPaypalRefundRequestDetail.PayPlatform.ADYEN.getCode().equals(dbResult.getPlatform())){
                return adyenReqeustInner(dbResult, account);
            }else{
                return 200;
            }
        }else{
            return 200;
        }
    }

    private int adyenReqeustInner(SlPaypalRefundRequestDetail dbResult, SlPaypalAccount account){
        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("originalReference", dbResult.getCaptureId());
            jsonObject.put("merchantAccount", account.getAccount());
            JSONObject amount = new JSONObject();
            amount.put("value", dbResult.getAmount().multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_DOWN).toString());
            amount.put("currency", dbResult.getCurrencyCode());
            jsonObject.put("modificationAmount", amount);
            Map<String, String> map = new HashMap<>();
            map.put("X-API-Key",account.getSecret());
            HttpClient.HttpResponse response = httpClient.doPost(adyenUrl, jsonObject, map);
            if(response.getCode() == 200){
                dbResult.setRefundTime(new Date());
                dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.Accepted.getCode());
                JSONObject result = JSON.parseObject(response.getBody());
                dbResult.setRefundId(result.getString("pspReference"));
                baseMapper.updateById(dbResult);
            }else{
                log.error("请求出错,错误code:{}", response.getCode());
                errorUpdate(dbResult, "请求出错,错误code:"+response.getCode());
            }
            return response.getCode();
        }catch (Exception e){
            log.error("交易号：{}, 请求处理失败。", dbResult.getCaptureId(),e);
            errorUpdate(dbResult, e.getMessage());
            return 200;
        }
    }

    private void errorUpdate(SlPaypalRefundRequestDetail dbResult, String message){
        dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
        dbResult.setRefundTime(new Date());
        if(StringUtils.isEmpty(message)){
            dbResult.setMessage("请求paypal接口异常");
        }else{
            if(message.length() > 512){
                dbResult.setMessage(message.substring(0,510));
            }else{
                dbResult.setMessage(message);
            }
        }
        baseMapper.updateById(dbResult);
    }

    private int PaypalRequestInner(SlPaypalRefundRequestDetail dbResult, SlPaypalAccount account){
        try{
            HttpResponse<Refund> response = requestRefund(dbResult, account);
            int code = response.statusCode();
            //根据结果处理
            if(code == 201 || code == 200){
                Refund refund = response.result();
                if(refund != null){
                    dbResult.setStatus(refund.status());
                    dbResult.setRefundTime(new Date());
                    dbResult.setRefundId(refund.id());
                    baseMapper.updateById(dbResult);
                }
            }
            return code;
        }catch (Exception e){
            log.info("交易号:{},请求处理结果是{}",dbResult.getCaptureId(), e.getMessage());
            errorUpdate(dbResult, e.getMessage());
            return 200;
        }
    }

    private int WorldPayRequetInner(SlPaypalRefundRequestDetail dbResult, SlPaypalAccount account) throws Exception{
        String auth = httpClient.getAuthHeader(account.getClientId(), account.getSecret());
        String url = "https://secure.worldpay.com/jsp/merchant/xml/paymentService.jsp";
        try {
            PaymentService paymentService = buildRefundXml(account.getClientId(), dbResult.getCaptureId(),
                    dbResult.getId(), dbResult.getAmount(), dbResult.getCurrencyCode());
            HttpClient.HttpResponse httpResponse = httpClient.doAuthHeaderAndXML(url, paymentService, auth);
            if (httpResponse != null && httpResponse.getCode() == 200) {
                log.info(httpResponse.getBody());
                PaymentService result = JAXBUtil.parseXMl(httpResponse.getBody(), PaymentService.class);
                if (result.getReply() != null) {
                    if (result.getReply().getOk() != null && result.getReply().getError() == null) {
                        //worldPay 已经受理
                        dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.Accepted.getCode());
                        dbResult.setRefundTime(new Date());
                        baseMapper.updateById(dbResult);
                    }
                    if (result.getReply().getError() != null) {
                        dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                        dbResult.setMessage(result.getReply().getError().getValue());
                        dbResult.setRefundTime(new Date());
                        baseMapper.updateById(dbResult);
                    }
                }
            }
            return httpResponse.getCode();
        }catch (Exception e){
            dbResult.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
            dbResult.setMessage("请求接口发生错误:"+dbResult.getCaptureId());
            dbResult.setRefundTime(new Date());
            baseMapper.updateById(dbResult);
            log.error("返回的xml解释错误:", e);
            return 200;
        }
    }

    private HttpResponse<Refund> requestRefund(SlPaypalRefundRequestDetail detail, SlPaypalAccount paypalAccount)throws Exception{
        PayPalEnvironment environment = new PayPalEnvironment.Live(paypalAccount.getClientId(), paypalAccount.getSecret());
        PayPalHttpClient client = new PayPalHttpClient(environment);
        CapturesRefundRequest refundRequest = new CapturesRefundRequest(detail.getCaptureId());
        RefundRequest refundBody = new RefundRequest();
        Money money = new Money();
        detail.setAmount(detail.getAmount().setScale(2, BigDecimal.ROUND_HALF_DOWN));
        money.value(detail.getAmount().toString());
        money.currencyCode(detail.getCurrencyCode());
        refundBody.amount(money);
        refundRequest = refundRequest.requestBody(refundBody);
        return client.execute(refundRequest);

    }


    /**
     * WorldPay
     * @param merchantCode
     * @param orderCode
     * @param reference
     * @param amount
     * @param currenyCode
     * @return
     * @throws Exception
     */
    private PaymentService buildRefundXml(String merchantCode, String orderCode, String reference,
                                  BigDecimal amount, String currenyCode) throws Exception{
        WorldPayAmount amountObj = new WorldPayAmount();
        amountObj.setValue(amount.multiply(new BigDecimal(100)).setScale(0, BigDecimal.ROUND_HALF_DOWN).toString());
        amountObj.setCurrencyCode(currenyCode);

        WorldRefund refund = new WorldRefund();
        refund.setReference(reference);
        refund.setAmount(amountObj);

        WorldPayOrder order = new WorldPayOrder();
        order.setOrderCode(orderCode);
        order.setWorldRefund(refund);

        Modify modify = new Modify();
        modify.setOrder(order);

        PaymentService paymentService = new PaymentService();
        paymentService.setVersion("1.4");
        paymentService.setMerchantCode(merchantCode);
        paymentService.setOp(modify);

        return paymentService;
    }


}
