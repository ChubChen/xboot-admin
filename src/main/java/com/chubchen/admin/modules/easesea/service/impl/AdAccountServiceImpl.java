package com.chubchen.admin.modules.easesea.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.chubchen.admin.common.utils.DateTimeUtil;
import com.chubchen.admin.common.utils.HttpClient;
import com.chubchen.admin.config.token.PlatformTokenConfig;
import com.chubchen.admin.modules.easesea.constants.PlatformConstants;
import com.chubchen.admin.modules.easesea.entity.AdAccount;
import com.chubchen.admin.modules.easesea.dao.mapper.AdAccountMapper;
import com.chubchen.admin.modules.easesea.entity.AdReport;
import com.chubchen.admin.modules.easesea.service.IAdAccountService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.easesea.service.IAdReportService;
import com.chubchen.admin.modules.easesea.util.TikTokUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
@Service
@DS("easesea")
public class AdAccountServiceImpl extends ServiceImpl<AdAccountMapper, AdAccount> implements IAdAccountService {

    @Autowired
    private HttpClient httpClient;

    @Autowired
    private PlatformTokenConfig platformTokenConfig;

    @Autowired
    private IAdReportService adReportService;

    private static final int DAY = 12;

    @Override
    public  IPage<AdAccount> findListByPage(PageVo pageVo, AdAccount adAccount){
        IPage<AdAccount> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, adAccount);
    }

    @Override
    public List<AdAccount> findListAll(AdAccount adAccount){
        return baseMapper.selectAll(adAccount);
    }

    @Override
    public int add(AdAccount adAccount){
        return baseMapper.insert(adAccount);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AdAccount adAccount){
        return baseMapper.updateById(adAccount);
    }

    @Override
    public AdAccount findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public void syncPlatformData(AdAccount adAccount) {
        try{
            if(PlatformConstants.AD_TIKTOK.equals(adAccount.getAdPlatform()) && StringUtils.isNotBlank(adAccount.getAccountId())){
                if(DateTime.now().getHourOfDay() < DAY){
                    for(int i = 1; i >= 0 ; i--){
                        queryTiKTokDataAndSave(adAccount, DateTime.now().minusDays(i));
                    }
                }else{
                    queryTiKTokDataAndSave(adAccount, DateTime.now());
                }
            }else{

            }
        }catch (Exception e){
            log.error("同步广告账户数据异常", e);
        }
    }

    /**
     * 按照时间维度每小时汇总报告
     * @param adAccount
     * @param start
     * @throws Exception
     */
    @Override
    public void queryTiKTokDataAndSave(AdAccount adAccount, DateTime start) throws Exception{
        String url = TikTokUtil.buildUrl(TikTokUtil.ADVERTISER_PATH);
        Map<String, String> headerMap = new HashMap<>(1);
        headerMap.put("Access-Token", platformTokenConfig.getTiktok());
        Map<String, Object> requestMap = new HashMap<>(6);
        requestMap.put("advertiser_id", adAccount.getAccountId());
        requestMap.put("fields", TikTokUtil.advertiserFields());
        requestMap.put("page_size", 100);
        requestMap.put("page", 1);
        requestMap.put("start_date", start.toString("yyyy-MM-dd"));
        requestMap.put("end_date", start.toString("yyyy-MM-dd"));
        requestMap.put("time_granularity", "STAT_TIME_GRANULARITY_HOURLY");
        JSONObject jsonObject = httpClient.doGetJson(url,requestMap,headerMap);
        if(jsonObject.getIntValue("code") == 0 ){
            JSONObject data = jsonObject.getJSONObject("data");
            JSONArray array = data.getJSONArray("list");
            List<AdReport> list = buildReport(array);
            adReportService.batchSaveUpdate(list);
        }else{
            throw new RuntimeException(jsonObject.getString("message"));
        }
    }

    private List<AdReport> buildReport(JSONArray array){
        if(array != null && array.size() > 0){
            List<AdReport> list = new ArrayList<>(array.size());
            for (int i = 0; i < array.size(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                AdReport adReport = array.getObject(i, AdReport.class);
                adReport.setAccountId(jsonObject.getString("advertiser_id"));
                adReport.setPlatform(PlatformConstants.AD_TIKTOK);
                adReport.setDate(DateTimeUtil.format(adReport.getStatDatetime(), DateTimeUtil.FORMAT));
                adReport.setHour(DateTimeUtil.format(adReport.getStatDatetime(), "HH"));
                list.add(adReport);
            }
            return list;
        }else{
            return null;
        }
    }
}
