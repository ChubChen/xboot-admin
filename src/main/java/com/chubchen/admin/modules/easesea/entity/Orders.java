package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;

import com.chubchen.admin.common.utils.DateTimeUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="Orders对象", description="")
public class Orders extends Model<Orders> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public Orders(){

    }

    @ApiModelProperty(value = "主键id")
    private String id = "";

    @ApiModelProperty(value = "后台id")
    private Integer backendId = 0;

    private Integer domainId = 0;

    @ApiModelProperty(value = "平台订单id")
    private String pOrderId = "";

    @ApiModelProperty(value = "平台checkout id")
    private String pAbandonedId = "";

    @ApiModelProperty(value = "订单创建时间")
    private Date createdAt = DateTimeUtil.get0DefaultTime();

    private Date closedAt = DateTimeUtil.get0DefaultTime();

    @ApiModelProperty(value = "取消订单时间")
    private Date canceledAt = DateTimeUtil.get0DefaultTime();

    @ApiModelProperty(value = "确认时间，数据统计时间")
    private Date placedAt = DateTimeUtil.get0DefaultTime();

    @ApiModelProperty(value = "订单编号")
    private String number = "";

    @ApiModelProperty(value = "支付渠道")
    private String gateway = "";

    @ApiModelProperty(value = "订单状态normal,cancelled")
    private String status = "normal";

    @ApiModelProperty(value = "支付状态 waiting,paying,partially_paid,paid,partially_refunded,refunded,voided")
    private String financialStatus = "waiting";

    @ApiModelProperty(value = "物流状态unShipped,partial,shipped,cancelled,returned")
    private String fulfillmentStatus = "unShipped";

    @ApiModelProperty(value = "发货时间")
    private Date fulfilledAt = DateTimeUtil.get0DefaultTime();

    @ApiModelProperty(value = "总价")
    private BigDecimal subtotalPrice = BigDecimal.ZERO;

    @ApiModelProperty(value = "实际总价")
    private BigDecimal totalPrice = BigDecimal.ZERO;

    private BigDecimal totalDiscount = BigDecimal.ZERO;

    private BigDecimal totalTax = BigDecimal.ZERO;

    private BigDecimal totalShipping = BigDecimal.ZERO;

    private BigDecimal refund = BigDecimal.ZERO;

    private Date refundAt = DateTimeUtil.get0DefaultTime();

    private String currency = "";

    private String country = "";

    @ApiModelProperty(value = "当前客户的第几个订单")
    private Integer ordersCount = 0;

    @ApiModelProperty(value = "平台客户ID")
    private String pCustomerId = "";

    @ApiModelProperty(value = "更新时间")
    private Date updatedAt = DateTimeUtil.get0DefaultTime();


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
