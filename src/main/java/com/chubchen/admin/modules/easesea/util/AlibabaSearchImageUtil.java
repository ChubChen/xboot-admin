package com.chubchen.admin.modules.easesea.util;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 *  图片搜索
 *  @author chubchen
 */
@Slf4j
@Component
@Data
public class AlibabaSearchImageUtil {

    @Value("${platformToken.alibabaSearchImage.region}")
    private String region = "cn-shanghai";

    @Value("${platformToken.alibabaSearchImage.accessKey}")
    private String ACCESS_KEY_ID;

    @Value("${platformToken.alibabaSearchImage.accesskeySecret}")
    private String ACCESS_KEY_SECRET;

    @Value("${platformToken.alibabaSearchImage.instantName}")
    private String instanceName;

    private IAcsClient client;

    @PostConstruct
    public void init(){
        DefaultProfile.addEndpoint( region, "ImageSearch", "imagesearch."+region+".aliyuncs.com");
        IClientProfile profile = DefaultProfile.getProfile(region, ACCESS_KEY_ID, ACCESS_KEY_SECRET);
        client = new DefaultAcsClient(profile);
    }


}
