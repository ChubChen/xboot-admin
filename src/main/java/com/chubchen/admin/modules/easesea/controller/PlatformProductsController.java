package com.chubchen.admin.modules.easesea.controller;

import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.PlatformProductVariants;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import com.chubchen.admin.modules.easesea.service.IPlatformProductVariantsService;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IPlatformProductsService;
import com.chubchen.admin.modules.easesea.entity.PlatformProducts;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/platform-products")
public class PlatformProductsController {

    @Resource
    private IPlatformProductsService platformProductsService;

    @Resource
    private IPlatformProductVariantsService variantsService;

    @Autowired
    private SecurityUtil securityUtil;

    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute PlatformProducts platformProducts){
        Integer count = platformProductsService.add(platformProducts);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = platformProductsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute PlatformProducts platformProducts){
        Integer count = platformProductsService.updateData(platformProducts);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<PlatformProducts>> findListByPage(@ModelAttribute PageVo pageVo,
                                                          @ModelAttribute PlatformProducts platformProducts,
                                                          @ModelAttribute SearchVo searchVo){
         securityUtil.setSearchVoPermissionDomain(0, searchVo);
         IPage<PlatformProducts> page = platformProductsService.findListByPage(pageVo, platformProducts, searchVo);
         return new ResultUtil<IPage<PlatformProducts>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<PlatformProducts> findById(@PathVariable Long id){
        PlatformProducts platformProducts=  platformProductsService.findById(id);
        return new ResultUtil<PlatformProducts>().setData(platformProducts);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<PlatformProducts>> findById(@ModelAttribute PlatformProducts platformProducts){
        List<PlatformProducts> platformProductsList =  platformProductsService.findListAll(platformProducts);
        return new ResultUtil<List<PlatformProducts>>().setData(platformProductsList);
    }

    @RequestMapping(value = "/getNoSaleProduct",method = RequestMethod.GET)
    public Result<IPage<PlatformProducts>> getNoSaleProduct(@ModelAttribute PlatformProducts platformProducts,
                                                           @ModelAttribute PageVo pageVo,
                                                           @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(0, searchVo);
        IPage<PlatformProducts> page = platformProductsService.getNoSaleProduct(platformProducts, pageVo, searchVo);

        return new ResultUtil<IPage<PlatformProducts>>().setData(page);
    }


    @RequestMapping(value = "/findSPUProductStatistic",method = RequestMethod.GET)
    public Result<IPage<PlatformProducts>> findSPUProductStatistic(@ModelAttribute PlatformProducts products,
                                                                  @ModelAttribute PageVo pageVo,
                                                                  @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(0, searchVo);
        IPage<PlatformProducts> page = platformProductsService.findSPUProductStatistic(products, pageVo, searchVo);

        return new ResultUtil<IPage<PlatformProducts>>().setData(page);
    }

    @RequestMapping(value = "/findSPUProductStatisticAll",method = RequestMethod.GET)
    public Result<List<PlatformProducts>> findSPUProductStatisticAll(@ModelAttribute PlatformProducts products,
                                                                    @ModelAttribute PageVo pageVo,
                                                                    @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(0, searchVo);
        List<PlatformProducts> page = platformProductsService.findSPUProductStatisticAll(products, searchVo);

        return new ResultUtil<List<PlatformProducts>>().setData(page);
    }

    @RequestMapping(value = "/getSPUNoSaleProduct",method = RequestMethod.GET)
    public Result<IPage<PlatformProducts>> getSPUNoSaleProduct(@ModelAttribute PlatformProducts products,
                                                              @ModelAttribute PageVo pageVo,
                                                              @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(0, searchVo);
        IPage<PlatformProducts> page = platformProductsService.getSPUNoSaleProduct(products, pageVo, searchVo);

        return new ResultUtil<IPage<PlatformProducts>>().setData(page);
    }

    /**
     * 查询产品变体的销量统计
     */
    @RequestMapping(value = "/getVariantSale",method = RequestMethod.GET)
    public Result<Object> getVariantsSaleByProductId(@ModelAttribute SearchVo searchVo,
                                                     @RequestParam(required = false) String productId,
                                                     @RequestParam(required = false) String spu){
        if(StringUtils.isNotBlank(spu) || StringUtils.isNotBlank(productId)){
            List<PlatformProductVariants> list = variantsService.getSaleVariantsByProductId(spu, productId, searchVo);
            if(list != null){
                PlatformProductVariants first = list.get(0);
                if(first.getTitle() != null){
                    if(first.getTitle().indexOf("/") > 0 || first.getTitle().indexOf("-") > 0){
                        List<VariantsObject> optionList = new ArrayList<>(list.size());
                        list.stream().forEach(item->{
                            VariantsObject option = new VariantsObject();
                            option.setOption1(item.getTitle().split("/|-")[0].trim());
                            option.setOption2(item.getTitle().split("/|-")[1].trim());
                            option.setQuantity(item.getQuantity());
                            optionList.add(option);
                        });
                        return new ResultUtil<Object>().setData(optionList.stream().collect(
                                Collectors.groupingBy(VariantsObject::getOption1,
                                        Collectors.groupingBy(VariantsObject::getOption2,
                                                Collectors.summingInt(VariantsObject::getQuantity)))));
                    }else{
                        return new ResultUtil<Object>().setData(list.stream().
                                collect(Collectors.groupingBy(PlatformProductVariants::getTitle,
                                        Collectors.summingInt(PlatformProductVariants::getQuantity))));
                    }
                }else{
                    return new ResultUtil<>().setData(list);
                }
            }else{
                return new ResultUtil<>().setData(list);
            }
        }else{
            return new ResultUtil<>().setErrorMsg("缺少必要参数");
        }
    }


    @RequestMapping(value = "/getProductStaticByTime",method = RequestMethod.GET)
    public Result<List<ShopifyOrderStatistic>> getProductStaticByTime(@RequestParam(required = false)  String spu,
                                                                      @RequestParam(required = false)  String productId,
                                                                      @ModelAttribute SearchVo searchVo){
        if(StringUtils.isNotBlank(spu) || StringUtils.isNotBlank(productId)) {
            securityUtil.setSearchVoPermissionDomain(0, searchVo);
            List<ShopifyOrderStatistic> shopifyProductsStatisticList = platformProductsService
                    .getProductStaticByTime(spu, productId, searchVo);
            return new ResultUtil<List<ShopifyOrderStatistic>>().setData(shopifyProductsStatisticList);
        }else{
            return new ResultUtil<List<ShopifyOrderStatistic>>().setErrorMsg("缺少必要参数");
        }
    }

    @RequestMapping(value = "getShoplazzaIdByUrl",  method= {RequestMethod.GET, RequestMethod.POST})
    @ApiOperation(value = "获取id")
    public Result<List<String>> getShoplazzaIdByUrl(@RequestParam(value = "handles") List<String> handle,
                                                    @RequestParam Integer backendId){
        if(!handle.isEmpty()){
            List<String> ids = platformProductsService.getShoplazzaIdByUrl(handle, backendId);
            return new ResultUtil<List<String>>().setData(ids);
        }else{
            return new ResultUtil<List<String>>().setErrorMsg("缺少参数");
        }
    }

    @Data
    class VariantsObject{
        private String option1;

        private String option2;

        private Integer quantity;
    }
}
