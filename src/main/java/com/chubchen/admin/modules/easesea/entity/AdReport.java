package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-16
 */
@ToString
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AdReport对象", description="")
public class AdReport extends Model<AdReport> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String platform;

    private String accountId;

    private String date;

    private String hour;

    private Integer clickCnt;

    private BigDecimal clickCost;

    private BigDecimal ctr;

    private BigDecimal ecpm;

    private BigDecimal statCost;

    private Integer showCnt;

    private Integer showUv;

    private BigDecimal conversionCost;

    private BigDecimal conversionRate;

    private Integer convertCnt;

    private Integer dyComment;

    private Integer dyHomeVisited;

    private Integer dyLike;

    private Integer dyShare;

    private Integer skip;

    private Date statDatetime;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
