package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.modules.easesea.vo.AbandonedStatic;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IAbandonedOrdersService extends IService<AbandonedOrders> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param abandonedOrders 过滤条件
     * @return IPage<AbandonedOrders>
     */
    IPage<AbandonedOrders> findListByPage(PageVo pageVo, AbandonedOrders abandonedOrders, SearchVo searchVo);

   /**
    * 查询分页数据
    *
    * @return  List<AbandonedOrders>
    */
    List<AbandonedOrders> findListAll(AbandonedOrders abandonedOrders);

    /**
     * 添加
     *
     * @param abandonedOrders 
     * @return int
     */
    int add(AbandonedOrders abandonedOrders);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param abandonedOrders 
     * @return int
     */
    int updateData(AbandonedOrders abandonedOrders);

    /**
     * id查询数据
     *
     * @param id id
     * @return AbandonedOrders
     */
    AbandonedOrders findById(Long id);

    /**
     * 根据业务Id查询
     * @param list
     * @return
     */
    List<AbandonedOrders> findByUniqueId(List<AbandonedOrders> list);

    /**
     * 根据统计弃购信息
     * @param domainId
     * @param pageVo
     * @param searchVo
     * @return
     */
    IPage<AbandonedStatic> getStatic(Integer domainId, PageVo pageVo, SearchVo searchVo);
}
