package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-07-29
 */
public interface ISlPaypalRefundRequestService extends IService<SlPaypalRefundRequest> {

    /**
     * 添加
     *
     * @param slPaypalRefundRequest 
     * @return int
     */
    int add(SlPaypalRefundRequest slPaypalRefundRequest);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(String id);

    /**
     * 修改
     *
     * @param slPaypalRefundRequest 
     * @return int
     */
    int updateData(SlPaypalRefundRequest slPaypalRefundRequest);

    /**
     * id查询数据
     *
     * @param id id
     * @return SlPaypalRefundRequest
     */
    SlPaypalRefundRequest findById(String id);

    /**
     * 根据请求处理任务
     * @param  request
     */
    void handelRequest(SlPaypalRefundRequest request);
    /**
     * 根据请求处理任务
     * @param  request
     */
    void handelRequestRetry(SlPaypalRefundRequest request);

    /**
     * 分页查询数据
     * @param searchVo
     * @param pageVo
     * @param slPaypalRefundRequest
     * @return
     */
    IPage<SlPaypalRefundRequest> findListByPage(SearchVo searchVo, PageVo pageVo, SlPaypalRefundRequest slPaypalRefundRequest);

    /**
     * 判断是否完成
     * @param batchId
     */
    void updateRequestStatus(String batchId);
}
