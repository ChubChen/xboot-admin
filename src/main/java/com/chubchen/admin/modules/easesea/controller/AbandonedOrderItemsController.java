package com.chubchen.admin.modules.easesea.controller;

import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAbandonedOrderItemsService;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrderItems;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/abandoned-order-items")
public class AbandonedOrderItemsController {

    @Resource
    private IAbandonedOrderItemsService abandonedOrderItemsService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AbandonedOrderItems abandonedOrderItems){
        Integer count = abandonedOrderItemsService.add(abandonedOrderItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = abandonedOrderItemsService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AbandonedOrderItems abandonedOrderItems){
        Integer count = abandonedOrderItemsService.updateData(abandonedOrderItems);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AbandonedOrderItems>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AbandonedOrderItems abandonedOrderItems){
            IPage<AbandonedOrderItems> page = abandonedOrderItemsService.findListByPage(pageVo, abandonedOrderItems);
         return new ResultUtil<IPage<AbandonedOrderItems>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AbandonedOrderItems> findById(@PathVariable Long id){
        AbandonedOrderItems abandonedOrderItems=  abandonedOrderItemsService.findById(id);
        return new ResultUtil<AbandonedOrderItems>().setData(abandonedOrderItems);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AbandonedOrderItems>> findById(@ModelAttribute AbandonedOrderItems abandonedOrderItems){
        List<AbandonedOrderItems> abandonedOrderItemsList =  abandonedOrderItemsService.findListAll(abandonedOrderItems);
        return new ResultUtil<List<AbandonedOrderItems>>().setData(abandonedOrderItemsList);
    }

}
