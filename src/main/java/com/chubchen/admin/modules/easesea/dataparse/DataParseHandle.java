package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.modules.easesea.entity.*;

import java.util.List;

/**
 * 数据解析处理器类型
 * @author chubchen
 */
public interface DataParseHandle {

    /**
     * 处理器
     * @param dataParse
     */
    void handle(JSONObject dataParse);


}
