package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestService;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-07-30
 */
@Api(tags = {""})
@RestController
@RequestMapping("/sl-paypal-refund-request")
public class SlPaypalRefundRequestController {

    @Resource
    private ISlPaypalRefundRequestService slPaypalRefundRequestService;

    @Resource
    private ISlPaypalRefundRequestDetailService paypalRefundRequestDetailService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute SlPaypalRefundRequest slPaypalRefundRequest){
        Integer count = slPaypalRefundRequestService.add(slPaypalRefundRequest);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") String id){
        Integer count = slPaypalRefundRequestService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute SlPaypalRefundRequest slPaypalRefundRequest){
        Integer count = slPaypalRefundRequestService.updateData(slPaypalRefundRequest);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }


    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<SlPaypalRefundRequest> findById(@PathVariable String id){
        SlPaypalRefundRequest slPaypalRefundRequest=  slPaypalRefundRequestService.findById(id);
        return new ResultUtil<SlPaypalRefundRequest>().setData(slPaypalRefundRequest);
    }

}
