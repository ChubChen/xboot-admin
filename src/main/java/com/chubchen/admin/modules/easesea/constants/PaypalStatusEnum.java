package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * domain 域名状态枚举类
 * @author chubchen
 */
@Getter
public enum PaypalStatusEnum {

    DISABLED(-1, "停用"),
    NEW(0, "新建"),
    USED(1, "使用中");

    private int code;

    private String desc;

    PaypalStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> getMap(){
        return Arrays.stream(PaypalStatusEnum.values()).
                collect(Collectors.toMap(PaypalStatusEnum::getCode, PaypalStatusEnum::getDesc));
    }
}
