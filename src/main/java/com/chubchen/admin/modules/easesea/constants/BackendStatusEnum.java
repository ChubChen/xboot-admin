package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 后台账户表枚举类
 * @author chubchen
 */
@Getter
public enum BackendStatusEnum {


    ACTIVE_CLOSE(-2, "主动关闭"),
    DISABLE(-1, "被封"),
    NEW(0, "新建"),
    OPEN(1, "使用中"),
    OTHER(2, "其他"),
    PAUSED(3, "暂停"),
    SEND(6, "送人了");

    private int code;

    private String desc;

    BackendStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> getMap(){
        return Arrays.stream(BackendStatusEnum.values()).
                collect(Collectors.toMap(BackendStatusEnum::getCode, BackendStatusEnum::getDesc));
    }
}
