package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.constants.PaypalStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAccountPaypalService;
import com.chubchen.admin.modules.easesea.entity.AccountPaypal;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-24
 */
@RestController
@RequestMapping("xboot/easesea/account-paypal")
public class AccountPaypalController {

    @Resource
    private IAccountPaypalService accountPaypalService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AccountPaypal accountPaypal){
        Result result = validateParam(accountPaypal);
        if(result.isSuccess()){
            AccountPaypal dbPaypal = accountPaypalService.getOne(new QueryWrapper<AccountPaypal>()
                    .eq("email", accountPaypal.getEmail()));
            if(dbPaypal != null && dbPaypal.getPaypalId() >0){
                return new ResultUtil<Integer>().setErrorMsg("账户邮箱已经存在不能重复添加");
            }
            accountPaypal.setCreatedAt(new Date());
            accountPaypal.setUpdatedAt(new Date());
            Integer count = accountPaypalService.add(accountPaypal);
            if(count > 0){
                return new ResultUtil<Integer>().setData(count);
            }else{
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    private Result validateParam(AccountPaypal accountPaypal){
        if(StringUtils.isBlank(accountPaypal.getName())){
            return new ResultUtil<Integer>().setErrorMsg("名称不能为空");
        }
        if(StringUtils.isBlank(accountPaypal.getEmail())){
            return new ResultUtil<Integer>().setErrorMsg("Email不能为空");
        }
        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountPaypalService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AccountPaypal accountPaypal){
        Result result = validateParam(accountPaypal);
        if(result.isSuccess()) {
            accountPaypal.setUpdatedAt(new Date());
            Integer count = accountPaypalService.updateData(accountPaypal);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AccountPaypal>> findListByPage(@ModelAttribute PageVo pageVo,
                                                       @ModelAttribute AccountPaypal accountPaypal,
                                                       @ModelAttribute SearchVo searchVo){
            IPage<AccountPaypal> page = accountPaypalService.findListByPage(pageVo,searchVo, accountPaypal);
         return new ResultUtil<IPage<AccountPaypal>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AccountPaypal> findById(@PathVariable Long id){
        AccountPaypal accountPaypal=  accountPaypalService.findById(id);
        return new ResultUtil<AccountPaypal>().setData(accountPaypal);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountPaypal>> findById(@ModelAttribute AccountPaypal accountPaypal,
                                                @ModelAttribute SearchVo searchVo){
        List<AccountPaypal> accountPaypalList =  accountPaypalService.findListAll(accountPaypal,searchVo);
        return new ResultUtil<List<AccountPaypal>>().setData(accountPaypalList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getExceptionType(){
        Map<Integer, String> map = Arrays.stream(PaypalStatusEnum.values()).collect(
                Collectors.toMap(PaypalStatusEnum::getCode, PaypalStatusEnum::getDesc));
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "启用或者关闭状态", type = LogType.OPERATION)
    @ApiOperation(value = "开启或者关闭单个站点")
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!PaypalStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        boolean count = accountPaypalService.update(new UpdateWrapper<AccountPaypal>().set("status",status)
                .in("paypal_id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
