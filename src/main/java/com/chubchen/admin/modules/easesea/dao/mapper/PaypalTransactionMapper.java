package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.PaypalTransactionMapper;
import com.chubchen.admin.modules.easesea.entity.PaypalTransaction;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-21
*/
@DS("easesea")
public interface PaypalTransactionMapper extends BaseMapper<PaypalTransaction> {

    /**
     * 根据条件分页查询
     * @param page
     * @param paypalTransaction
     * @return IPage<PaypalTransaction>
    */
    IPage<PaypalTransaction> selectListByPage(IPage<PaypalTransaction> page, @Param("paypalTransaction") PaypalTransaction paypalTransaction);

    /**
     * 根据条件查询全部
     * @param paypalTransaction
     * @return List<PaypalTransaction>
    */
    List<PaypalTransaction> selectAll(@Param("paypalTransaction") PaypalTransaction paypalTransaction);

    /**
     * 根据唯一索引查询数据
     * @param list
     * @return
     */
    List<PaypalTransaction> findByUniqueId(@Param("list") List<PaypalTransaction> list);

    /**
     * 批量插入
     * @param list
     */
    void insertBatch(@Param("list") List<PaypalTransaction> list);
}
