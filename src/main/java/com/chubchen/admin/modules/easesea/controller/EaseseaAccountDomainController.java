package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.constants.BackendStatusEnum;
import com.chubchen.admin.modules.easesea.constants.DomainStatusEnum;
import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import com.chubchen.admin.modules.easesea.service.IAccountBackendService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IEaseseaAccountDomainService;
import com.chubchen.admin.modules.easesea.entity.AccountDomain;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.*;
import java.util.stream.Collectors;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/account-domain")
@CacheConfig(cacheNames = "domainAccount")
public class EaseseaAccountDomainController {

    @Resource
    private IEaseseaAccountDomainService accountDomainService;

    @Resource
    private IAccountBackendService backendService;

    @Autowired
    private SecurityUtil securityUtil;

    @Autowired
    private RedisTemplate redisTemplate;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AccountDomain accountDomain) {
        Result result = validateParam(accountDomain);
        if (result.isSuccess()) {
            AccountDomain exists = accountDomainService.getOne(new QueryWrapper<AccountDomain>().eq("domain_url", accountDomain.getDomainUrl()));
            if (exists == null || exists.getDomainId() != null) {
              if(accountDomain.getBackendId() != null && accountDomain.getBackendId() >0){
                    Result result2 = validateExists(accountDomain);
                    if(result2.isSuccess()){
                        Integer count = accountDomainService.add(accountDomain);
                        if (count > 0) {
                            redisTemplate.delete("AccountDomainController:getUserDomainId:*");
                            return new ResultUtil<Integer>().setData(count);
                        } else {
                            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                        }
                    }else{
                        return result2;
                    }
                }else{
                  Integer count = accountDomainService.add(accountDomain);
                  if (count > 0) {
                      redisTemplate.delete("AccountDomainController:getUserDomainId:*");
                      return new ResultUtil<Integer>().setData(count);
                  } else {
                      return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                  }
                }
            } else {
                return new ResultUtil<Integer>().setErrorMsg("该域名已经存在不能重复添加");
            }
        } else {
            return result;
        }
    }
    private Result validateParam(AccountDomain accountDomain){
        if(StringUtils.isBlank(accountDomain.getDomainUrl())){
            return new ResultUtil<Integer>().setErrorMsg("admiUrl参数必填");
        }

        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    private Result validateExists(AccountDomain accountDomain){
        int backendIdCount = accountDomainService.count(new QueryWrapper<AccountDomain>()
                .eq("backend_id", accountDomain.getBackendId())
                .ne("domain_url", accountDomain.getDomainUrl()));
        if(backendIdCount <= 0){
            //校验状态并且更新后台状态为使用中。
            AccountBackend backend = backendService.getById(accountDomain.getBackendId());
            if(backend == null || !backend.getStoreStatus().equals(BackendStatusEnum.NEW.getCode())){
                return new ResultUtil<Integer>().setErrorMsg("后台站点状态并非新建状态不可关联");
            }else{
                return new ResultUtil().setSuccessMsg("校验通过");
            }
        }else{
            return new ResultUtil<Integer>().setErrorMsg("后台站点已经在使用中不能绑定到该域名下面");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountDomainService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AccountDomain accountDomain) {
        Result result = validateParam(accountDomain);
        if (result.isSuccess()) {
            AccountDomain db = accountDomainService.getById(accountDomain.getDomainId());
            if(accountDomain.getBackendId() != null && accountDomain.getBackendId() >0 && !db.getBackendId().equals(db.getBackendId())){
                Result result2 = validateExists(accountDomain);
                if(result2.isSuccess()){
                    redisTemplate.delete("AccountDomainController:getUserDomainId:*");
                    Integer count = accountDomainService.updateData(accountDomain);
                    if (count > 0) {
                        return new ResultUtil<Integer>().setData(count);
                    } else {
                        return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                    }
                }else{
                    return result2;
                }
            }else{
                Integer count = accountDomainService.updateData(accountDomain);
                if (count > 0) {
                    redisTemplate.delete("AccountDomainController:getUserDomainId:*");
                    return new ResultUtil<Integer>().setData(count);
                } else {
                    return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                }
            }
        } else {
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AccountDomain>> findListByPage(@ModelAttribute PageVo pageVo,
                                                       @ModelAttribute SearchVo searchVo,
                                                       @ModelAttribute AccountDomain accountDomain){
        IPage<AccountDomain> page = accountDomainService.findListByPage(pageVo, accountDomain, searchVo);
        return new ResultUtil<IPage<AccountDomain>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AccountDomain> findById(@PathVariable Long id){
        AccountDomain accountDomain=  accountDomainService.findById(id);
        return new ResultUtil<AccountDomain>().setData(accountDomain);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountDomain>> findById(@ModelAttribute AccountDomain accountDomain,
                                                @ModelAttribute SearchVo searchVo){
        List<AccountDomain> accountDomainList =  accountDomainService.findListAll(accountDomain, searchVo);
        return new ResultUtil<List<AccountDomain>>().setData(accountDomainList);
    }


    @ApiOperation(value = "新增导入")
    @SystemLog(description = "批量导入到站点", type = LogType.OPERATION)
    @PostMapping("/import")
    public Result<Integer> importAdd(@RequestBody List<AccountDomain> accountBackendList){
        int count = 0;
        for (AccountDomain item: accountBackendList) {
            Result result = validateParam(item);
            if(result.isSuccess()){
                AccountDomain exists = accountDomainService.getOne(new QueryWrapper<AccountDomain>().eq("domain_url", item.getDomainUrl()));
                if(exists == null || exists.getBackendId() != null) {
                    item.setCreatedAt(new Date());
                    item.setUpdatedAt(new Date());
                    count += accountDomainService.add(item);
                }
            }
        }
        return new ResultUtil<Integer>().setSuccessMsg("成功导入"+ count+ "条数据");
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getExceptionType(){
        Map<Integer, String> map = Arrays.stream(DomainStatusEnum.values()).collect(
                Collectors.toMap(DomainStatusEnum::getCode, DomainStatusEnum::getDesc));
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "启用或者关闭状态", type = LogType.OPERATION)
    @ApiOperation(value = "开启或者关闭单个站点")
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!BackendStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        boolean count = accountDomainService.update(new UpdateWrapper<AccountDomain>().set("domain_status", status)
                .set("updated_at", new Date())
                .in("domain_id", ids));
        if(count){
            redisTemplate.delete("AccountDomainController:getUserDomainId:*");
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }

    @RequestMapping(value = "/getUserDomain", method = RequestMethod.GET)
    @ApiOperation(value = "获取当前用户可见的域名以及后台数据")
    @Cacheable(keyGenerator = "myKeyGenerator")
    public Result<List<AccountDomain>> getUserDomainId(){
        List<Integer> domainIds = securityUtil.getUserDomainId();
        //全部权限返回所有
        if(domainIds == null){
            List<AccountDomain> list = accountDomainService.getAll();
            return new ResultUtil<List<AccountDomain>>().setData(list);
        }else if(domainIds.size() == 0){
            return new ResultUtil<List<AccountDomain>>().setData(new ArrayList<>());
        }else if(domainIds.size() > 0){
            List<AccountDomain> list = accountDomainService.getAll();
            HashSet<Integer> set = new HashSet<Integer>(domainIds);
            List<AccountDomain> filterList = list.stream().filter(x->set.contains(x.getDomainId())).collect(Collectors.toList());
            return new ResultUtil<List<AccountDomain>>().setData(filterList);
        }else{
            return new ResultUtil<List<AccountDomain>>().setData(new ArrayList<>());
        }
    }

}

