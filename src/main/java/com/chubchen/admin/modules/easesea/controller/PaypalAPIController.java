package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.utils.SnowFlakeUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequest;
import com.chubchen.admin.modules.easesea.entity.SlPaypalRefundRequestDetail;
import com.chubchen.admin.modules.easesea.service.ISlPaypalAccountService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestDetailService;
import com.chubchen.admin.modules.easesea.service.ISlPaypalRefundRequestService;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@RestController
@RequestMapping("/xboot/easesea/paypalApi")
public class PaypalAPIController {


    @Autowired
    private ISlPaypalRefundRequestDetailService paypalRefundRequestDetailService;

    @Autowired
    private ISlPaypalRefundRequestService paypalRefundRequestService;
    @Autowired
    private ISlPaypalAccountService paypalAccountService;

    @PostMapping("/batchRefund")
    @DS("easesea")
    @Transactional(rollbackFor = Exception.class)
    public Result<Object> batchRefund(@RequestBody List<SlPaypalRefundRequestDetail> list){
        //step 1 进行校验
        if(list != null && list.size() >0){
            //查询所有账户信息
            List<SlPaypalAccount> paypalAccountList = paypalAccountService.
                    list(new QueryWrapper<SlPaypalAccount>().eq("is_del", 0)
                    .eq("is_sandbox", 0));
            Map<String,SlPaypalAccount> accountMap = paypalAccountList.stream().
                    collect(Collectors.toMap(SlPaypalAccount::getAccount, item->item));

            //校验参数
            checkParams(list, accountMap);

            SlPaypalRefundRequest createRequest = createRequest(list.size());
            //构造数据
            int errorSize = buildDetail(list, createRequest);
            if(errorSize == list.size()){
                createRequest.setStatus(SlPaypalRefundRequest.SlPaypalRefundStatusEnum.FINISH.getCode());
            }
            paypalRefundRequestService.save(createRequest);
            paypalRefundRequestDetailService.saveBatch(list);

            return new ResultUtil<Object>().setSuccessMsg("任务创建成功任务Id,请等待任务处理结果");
        }else{
            return new ResultUtil<Object>().setErrorMsg("数据为空");
        }
    }

    private void checkParams(List<SlPaypalRefundRequestDetail> list,Map<String, SlPaypalAccount> accountMap){
        list.forEach(item->{
            if(StringUtils.isEmpty(item.getCaptureId())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，交易号为空");
            }
            if(item.getAmount() == null){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，金额为空");
            }
            if(StringUtils.isEmpty(item.getCurrencyCode())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，币种为空");
            }
            if(StringUtils.isEmpty(item.getPlatform())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，支付平台必填");
            }else{
                item.setPlatform(item.getPlatform().toUpperCase());
                if(!SlPaypalRefundRequestDetail.PayPlatform.getMap().containsKey(item.getPlatform())){
                    item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                    item.setMessage("参数校验失败，暂时不支持该平台"+ item.getPlatform());
                }
            }
            if(StringUtils.isEmpty(item.getAccount())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，账户邮箱为空");
            }
            if(!accountMap.containsKey(item.getAccount())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.ERROR.getCode());
                item.setMessage("参数校验失败，该邮箱没有申请clientId以及接口权限");
            }

        });
    }

    private SlPaypalRefundRequest createRequest(int count){
        SlPaypalRefundRequest slPaypalRefundRequest = new SlPaypalRefundRequest();
        slPaypalRefundRequest.setBatchId(SnowFlakeUtil.getFlowIdInstance().nextIdStr());
        slPaypalRefundRequest.setCreatedTime(new Date());
        slPaypalRefundRequest.setFinishedTime(new Date());
        slPaypalRefundRequest.setCount(count);
        slPaypalRefundRequest.setStatus(SlPaypalRefundRequest.SlPaypalRefundStatusEnum.INIT.getCode());
        return slPaypalRefundRequest;
    }

    private int buildDetail(List<SlPaypalRefundRequestDetail> list,
                                                          SlPaypalRefundRequest refundRequest){
        final ArrayList<String> errorList = new ArrayList<>();
        list.forEach(item->{
            item.setBatchId(refundRequest.getBatchId());
            item.setCreatedTime(new Date());
            if(StringUtils.isEmpty(item.getStatus())){
                item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.INIT.getCode());
            }else{
                errorList.add(item.getStatus());
            }
            item.setRefundTime(new Date());
            item.setId(SnowFlakeUtil.getFlowIdInstance().nextIdStr());
        });
        return errorList.size();
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping("/listPage")
    public Result<IPage<SlPaypalRefundRequest>> findListByPage(@ModelAttribute PageVo pageVo,
                                                               @ModelAttribute SlPaypalRefundRequest slPaypalRefundRequest,
                                                               @ModelAttribute SearchVo searchVo){
        IPage<SlPaypalRefundRequest> page = paypalRefundRequestService.findListByPage(searchVo, pageVo, slPaypalRefundRequest);
        return new ResultUtil<IPage<SlPaypalRefundRequest>>().setData(page);
    }

    @GetMapping("/downLoadDetail")
    public Result<List<SlPaypalRefundRequestDetail>> downLoadDetail(@RequestParam("batchId") String batchId){
        List<SlPaypalRefundRequestDetail> list = paypalRefundRequestDetailService.list(
                new QueryWrapper<SlPaypalRefundRequestDetail>().eq("batch_id", batchId));
        list.forEach(item->{
            item.setStatus(SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.getMap().get(item.getStatus()));
        });
        return new ResultUtil<List<SlPaypalRefundRequestDetail>>().setData(list);
    }

    @GetMapping("/getRefundRequestStatus")
    public Result<Map<Integer,String>> getRefundRequstStatus(){
        Map<Integer,String> map = SlPaypalRefundRequest.SlPaypalRefundStatusEnum.getMap();
        return new ResultUtil<Map<Integer,String>>().setData(map);
    }

    @GetMapping("/cancel")
    @DS("easesea")
    @Transactional(rollbackFor = Exception.class)
    public Result<Object> cancelRequest(@RequestParam("batchId") String batchId){
        boolean result = paypalRefundRequestService.update(new UpdateWrapper<SlPaypalRefundRequest>().set("status",
                SlPaypalRefundRequest.SlPaypalRefundStatusEnum.CANCEL.getCode())
                .eq("status", SlPaypalRefundRequest.SlPaypalRefundStatusEnum.INIT.getCode())
                .eq("batch_id", batchId));
        if(result){
           boolean resultDetail = paypalRefundRequestDetailService.update(new UpdateWrapper<SlPaypalRefundRequestDetail>()
                   .set("status", SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.CANCEL.getCode())
                   .eq("batch_id", batchId)
                   .eq("status", SlPaypalRefundRequestDetail.SlPaypalRefundDetailStatusEnum.CANCEL.getCode()));
            return new ResultUtil<Object>().setData(resultDetail);
        }else{
            return new ResultUtil<Object>().setErrorMsg("状态已经不能进行取消操作");
        }
    }
}
