package com.chubchen.admin.modules.easesea.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author chubchen
 */
@Data
public class AbandonedStatic {

    private String staticDate;

    private Integer orderCount;

    private Integer abandonCount;

    private Integer sendCount;

    private Integer recoverCount;
}
