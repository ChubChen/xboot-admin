package com.chubchen.admin.modules.easesea.controller;

import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.vo.AbandonedStatic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAbandonedOrdersService;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrders;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import java.util.List;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/abandoned-orders")
public class AbandonedOrdersController {

    @Resource
    private IAbandonedOrdersService abandonedOrdersService;

    @Autowired
    private SecurityUtil securityUtil;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AbandonedOrders abandonedOrders){
        Integer count = abandonedOrdersService.add(abandonedOrders);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = abandonedOrdersService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AbandonedOrders abandonedOrders){
        Integer count = abandonedOrdersService.updateData(abandonedOrders);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询分页数据")
    @RequestMapping(value = "/getByCondition",method = RequestMethod.GET)
    public Result<IPage<AbandonedOrders>> findListByPage(@ModelAttribute PageVo pageVo,
                                                         @ModelAttribute AbandonedOrders abandonedOrders,
                                                         @ModelAttribute SearchVo searchVo){
         IPage<AbandonedOrders> page = abandonedOrdersService.findListByPage(pageVo, abandonedOrders, searchVo);
         return new ResultUtil<IPage<AbandonedOrders>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AbandonedOrders> findById(@PathVariable Long id){
        AbandonedOrders abandonedOrders=  abandonedOrdersService.findById(id);
        return new ResultUtil<AbandonedOrders>().setData(abandonedOrders);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AbandonedOrders>> findById(@ModelAttribute AbandonedOrders abandonedOrders){
        List<AbandonedOrders> abandonedOrdersList =  abandonedOrdersService.findListAll(abandonedOrders);
        return new ResultUtil<List<AbandonedOrders>>().setData(abandonedOrdersList);
    }

    @RequestMapping(value = "/getStatic", method = RequestMethod.GET)
    public Result<IPage<AbandonedStatic>> getStatic(@RequestParam(required = false)Integer domainId,
                                                    @ModelAttribute PageVo pageVo,
                                                    @ModelAttribute SearchVo searchVo){
        securityUtil.setSearchVoPermissionDomain(domainId, searchVo);
        IPage<AbandonedStatic> page = abandonedOrdersService.getStatic(domainId, pageVo, searchVo);
        return new ResultUtil<IPage<AbandonedStatic>>().setData(page);
    }

}
