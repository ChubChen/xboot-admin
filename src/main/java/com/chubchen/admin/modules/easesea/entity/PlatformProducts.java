package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.math.BigDecimal;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PlatformProducts对象", description="")
public class PlatformProducts extends Model<PlatformProducts> {

    @Tolerate
    public PlatformProducts(){

    }

    private static final long serialVersionUID = 1L;

    private String id;

    @ApiModelProperty(value = "产品id")
    private String pProductId;

    private Integer backendId;

    private String spu;

    @ApiModelProperty(value = "handle")
    private String handle;

    @ApiModelProperty(value = "产品名称")
    private String title;

    @ApiModelProperty(value = "图片路径")
    private String imageSrc;

    @ApiModelProperty(value = "tags")
    private String tags;

    @ApiModelProperty(value = "创建时间")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    private Date updatedAt;

    @TableField(exist = false)
    private Integer quantity;

    @TableField(exist = false)
    private BigDecimal price;

    @TableField(exist = false)
    private Integer storeCount;

    @TableField(exist = false)
    private String noTags;

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
