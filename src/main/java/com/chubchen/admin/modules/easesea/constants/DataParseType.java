package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@Getter
public enum DataParseType {

    Orders("orders"),
    Products("products"),
    Checkouts("checkouts");

    /**
     * code
     */
    private String code;

    DataParseType(String code){
        this.code = code;
    }

    public static Set<String> toSet(){
        return Arrays.stream(DataParseType.values()).map(DataParseType::getCode).collect(Collectors.toSet());
    }
}
