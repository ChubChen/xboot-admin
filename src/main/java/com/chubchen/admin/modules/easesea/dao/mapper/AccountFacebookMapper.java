package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.AccountFacebookMapper;
import com.chubchen.admin.modules.easesea.entity.AccountFacebook;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-08
*/
@DS("easesea")
public interface AccountFacebookMapper extends BaseMapper<AccountFacebook> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountFacebook
     * @return IPage<AccountFacebook>
    */
    IPage<AccountFacebook> selectListByPage(IPage<AccountFacebook> page, @Param("accountFacebook") AccountFacebook accountFacebook);

    /**
     * 根据条件查询全部
     * @param accountFacebook
     * @return List<AccountFacebook>
    */
    List<AccountFacebook> selectAll(@Param("accountFacebook") AccountFacebook accountFacebook);

}
