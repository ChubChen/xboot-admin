package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.AccountFacebook;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
public interface IAccountFacebookService extends IService<AccountFacebook> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param accountFacebook 过滤条件
     * @return IPage<AccountFacebook>
     */
    IPage<AccountFacebook> findListByPage(PageVo pageVo, AccountFacebook accountFacebook);

   /**
    * 查询分页数据
    *
    * @return  List<AccountFacebook>
    */
    List<AccountFacebook> findListAll(AccountFacebook accountFacebook);

    /**
     * 添加
     *
     * @param accountFacebook 
     * @return int
     */
    int add(AccountFacebook accountFacebook);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param accountFacebook 
     * @return int
     */
    int updateData(AccountFacebook accountFacebook);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountFacebook
     */
    AccountFacebook findById(Long id);
}
