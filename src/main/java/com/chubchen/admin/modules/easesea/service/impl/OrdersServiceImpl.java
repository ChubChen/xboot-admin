package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.dao.mapper.AbandonedOrdersMapper;
import com.chubchen.admin.modules.easesea.dao.mapper.PlatformProductsMapper;
import com.chubchen.admin.modules.easesea.entity.Orders;
import com.chubchen.admin.modules.easesea.dao.mapper.OrdersMapper;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import com.chubchen.admin.modules.easesea.service.IOrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class OrdersServiceImpl extends ServiceImpl<OrdersMapper, Orders> implements IOrdersService {

    @Autowired
    private AbandonedOrdersMapper abandonedOrdersMapper;

    @Autowired
    private PlatformProductsMapper platformProductsMapper;

    @Override
    public  IPage<Orders> findListByPage(PageVo pageVo, Orders orders, SearchVo search){
        IPage<Orders> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, orders, search);
    }

    @Override
    public List<Orders> findListAll(Orders orders,SearchVo searchVo){
        return baseMapper.selectAll(orders, searchVo);
    }

    @Override
    public int add(Orders orders){
        return baseMapper.insert(orders);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(Orders orders){
        return baseMapper.updateById(orders);
    }

    @Override
    public Orders findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public int batchSaveOnDuplicateKey(List<Orders> orders) {
        if(orders!= null && !orders.isEmpty()){
            return baseMapper.batchSaveOnDuplicateKey(orders);
        }else{
            return 0;
        }
    }

    @Override
    public List<Orders> findByUniqueId(List<Orders> ordersList) {
        if(ordersList != null && !ordersList.isEmpty()){
            return baseMapper.findByUniqueId(ordersList);
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public List<ShopifyOrderStatistic> getSaleOrderStatistic(String timeZone, Boolean flag,
                                                             List<Integer> domainIdList, PageVo pageVo,
                                                             SearchVo searchVo) {
        List<ShopifyOrderStatistic> abandonedStatistic = abandonedOrdersMapper.getAbandonedOrderStatistic(
                timeZone, flag, domainIdList, searchVo);

        List<ShopifyOrderStatistic> orderStatistic = baseMapper.getSaleOrderStatistic(
                timeZone, flag, domainIdList, searchVo);

        List<ShopifyOrderStatistic> productStatistic = platformProductsMapper.getSaleProductStatistic(
                timeZone, flag, domainIdList, searchVo);

        List<ShopifyOrderStatistic> adStatistic = baseMapper.getAdStatistic(
                timeZone, flag, domainIdList, searchVo);

        Set<String> keySet = new HashSet<>();
        List<ShopifyOrderStatistic> list = new ArrayList<>();

        Map<String, ShopifyOrderStatistic> abandoneMap = abandonedStatistic.stream()
                .map(item->{keySet.add(this.getKey(flag, item)); return item;})
                .collect(Collectors.toMap(item->this.getKey(flag, item), item->item));
        Map<String, ShopifyOrderStatistic> orderMap = orderStatistic.stream()
                .map(item->{keySet.add(this.getKey(flag, item)); return item;})
                .collect(Collectors.toMap(item->this.getKey(flag, item), item->item));
        Map<String, ShopifyOrderStatistic> productMap = productStatistic.stream()
                .map(item->{keySet.add(this.getKey(flag, item)); return item;})
                .collect(Collectors.toMap(item->this.getKey(flag, item), item->item));
        Map<String, ShopifyOrderStatistic> adMap = adStatistic.stream()
                .map(item->{keySet.add(this.getKey(flag, item)); return item;})
                .collect(Collectors.toMap(item->this.getKey(flag, item), item->item));

        keySet.forEach(item->{
            ShopifyOrderStatistic orders = new ShopifyOrderStatistic();
            if(flag){
                orders.setCDate(item.split("\\|")[0]);
                orders.setDomainId(Integer.valueOf(item.split("\\|")[1]));
            }else{
                orders.setCDate(item.split("\\|")[0]);
            }
            copyProperties(orders, abandoneMap.get(item), orderMap.get(item), productMap.get(item), adMap.get(item));
            list.add(orders);
        });
        return list;
    }

    private String getKey(boolean flag, ShopifyOrderStatistic item){
        if(flag){
            return item.getCDate() + "|" + item.getDomainId();
        }else{
            return item.getCDate();
        }
    }

    private void copyProperties(ShopifyOrderStatistic orders,
                                ShopifyOrderStatistic abandoned,
                                ShopifyOrderStatistic item,
                                ShopifyOrderStatistic product,
                                ShopifyOrderStatistic ad){
        if( abandoned != null){
            orders.setAbandonedOrders(abandoned.getAbandonedOrders());
            orders.setSendEmailOrders(abandoned.getSendEmailOrders());
            orders.setRecoverOrders(abandoned.getRecoverOrders());
        }
        if( item != null){
            orders.setValidOrders(item.getValidOrders());
            orders.setNetTotalAmount(item.getNetTotalAmount());
            orders.setTotalOrders(item.getTotalOrders());
            orders.setTotalAmount(item.getTotalAmount());
            orders.setFulfilled5Days(item.getFulfilled5Days());
            orders.setFulfilled10Days(item.getFulfilled10Days());
            orders.setFulfilledOrders(item.getFulfilledOrders());
            orders.setRefundAmount(item.getRefundAmount());
            orders.setReturning(item.getReturning());
        }
        if( product != null){
            orders.setProductsCount(product.getProductsCount());
            orders.setSpuCount(product.getSpuCount());
            orders.setQuantitySum(product.getQuantitySum());
        }
        if( ad != null){
            orders.setSpend(ad.getSpend());
//            orders.setPurchase(ad.getPurchase());
//            orders.setPurchaseValue(ad.getPurchaseValue());
            if(orders.getSpend() != null && orders.getSpend().compareTo(BigDecimal.ZERO) != 0 && orders.getNetTotalAmount() != null){
                orders.setRoas(orders.getNetTotalAmount().divide(orders.getSpend(), 2, BigDecimal.ROUND_HALF_UP));
            }
        }
    }
}
