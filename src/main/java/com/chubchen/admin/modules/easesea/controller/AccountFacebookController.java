package com.chubchen.admin.modules.easesea.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.easesea.constants.BackendStatusEnum;
import com.chubchen.admin.modules.easesea.constants.FBAccountStatusEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.easesea.service.IAccountFacebookService;
import com.chubchen.admin.modules.easesea.entity.AccountFacebook;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/easesea/account-facebook")
public class AccountFacebookController {

    @Resource
    private IAccountFacebookService accountFacebookService;


    @ApiOperation(value = "新增")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AccountFacebook accountFacebook){
        Result result = validateParam(accountFacebook);
        if (result.isSuccess()) {
            AccountFacebook exists = accountFacebookService.getOne(new QueryWrapper<AccountFacebook>().eq("account_name", accountFacebook.getAccountName()));
            if(exists == null || exists.getAccountId() != null) {
                Integer count = accountFacebookService.add(accountFacebook);
                if (count > 0) {
                    return new ResultUtil<Integer>().setData(count);
                } else {
                    return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                }
            }else{
                return new ResultUtil<Integer>().setErrorMsg("该账号已经存在,不能重复添加");
            }
        }else{
            return result;
        }
    }

    private Result validateParam(AccountFacebook accountFacebook){
        if(StringUtils.isBlank(accountFacebook.getAccountName())){
            return new ResultUtil<Integer>().setErrorMsg("账户名称不能为空");
        }

        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountFacebookService.delete(id);
        if (count > 0) {
            return new ResultUtil<Integer>().setData(count);
        } else {
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AccountFacebook accountFacebook){
        Result result = validateParam(accountFacebook);
        if (result.isSuccess()) {
            accountFacebook.setUpdatedAt(new Date());
            Integer count = accountFacebookService.updateData(accountFacebook);
            if(count > 0){
                return new ResultUtil<Integer>().setData(count);
            }else{
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AccountFacebook>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountFacebook accountFacebook){
            IPage<AccountFacebook> page = accountFacebookService.findListByPage(pageVo, accountFacebook);
         return new ResultUtil<IPage<AccountFacebook>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AccountFacebook> findById(@PathVariable Long id){
        AccountFacebook accountFacebook=  accountFacebookService.findById(id);
        return new ResultUtil<AccountFacebook>().setData(accountFacebook);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountFacebook>> findById(@ModelAttribute AccountFacebook accountFacebook){
        List<AccountFacebook> accountFacebookList =  accountFacebookService.findListAll(accountFacebook);
        return new ResultUtil<List<AccountFacebook>>().setData(accountFacebookList);
    }

    @ApiOperation(value = "新增导入")
    @SystemLog(description = "批量导入广告账户", type = LogType.OPERATION)
    @PostMapping("/import")
    public Result<Integer> importAdd(@RequestBody List<AccountFacebook> accountBackendList){
        int count = 0;
        for (AccountFacebook item: accountBackendList) {
            Result result = validateParam(item);
            if(result.isSuccess()){
                AccountFacebook exists = accountFacebookService.getOne(new QueryWrapper<AccountFacebook>().eq("account_name", item.getAccountName()));
                if(exists == null || exists.getAccountId() != null) {
                    item.setCreatedAt(new Date());
                    item.setUpdatedAt(new Date());
                    count += accountFacebookService.add(item);
                }
            }
        }
        return new ResultUtil<Integer>().setSuccessMsg("成功导入"+ count+ "条数据");
    }


    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = FBAccountStatusEnum.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "启用或者关闭状态", type = LogType.OPERATION)
    @ApiOperation(value = "开启或者关闭单个站点")
    public Result<Boolean> enable(@RequestParam List<Long> ids, @RequestParam Integer status){
        if(!BackendStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = accountFacebookService.update(new UpdateWrapper<AccountFacebook>()
                .set("account_status", status)
                .set("updated_at", new Date())
                .in("account_id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
