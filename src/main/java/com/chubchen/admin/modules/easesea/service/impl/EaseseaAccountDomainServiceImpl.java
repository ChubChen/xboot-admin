package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.constants.DomainStatusEnum;
import com.chubchen.admin.modules.easesea.entity.AccountDomain;
import com.chubchen.admin.modules.easesea.dao.mapper.EaseseaAccountDomainMapper;
import com.chubchen.admin.modules.easesea.service.IEaseseaAccountDomainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
@Service
public class EaseseaAccountDomainServiceImpl extends ServiceImpl<EaseseaAccountDomainMapper, AccountDomain> implements IEaseseaAccountDomainService {

    @Override
    public  IPage<AccountDomain> findListByPage(PageVo pageVo, AccountDomain accountDomain,SearchVo searchVo){
        IPage<AccountDomain> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountDomain, searchVo);
    }

    @Override
    public List<AccountDomain> findListAll(AccountDomain accountDomain,SearchVo searchVo){
        return baseMapper.selectAll(accountDomain, searchVo);
    }

    @Override
    public int add(AccountDomain accountDomain){
        return baseMapper.insert(accountDomain);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountDomain accountDomain){
        return baseMapper.updateById(accountDomain);
    }

    @Override
    public AccountDomain findById(Long id){
        return  baseMapper.selectById(id);
    }

    @Override
    public List<AccountDomain> getAll() {
        return baseMapper.selectList(new QueryWrapper<AccountDomain>().ne("domain_status", DomainStatusEnum.DISABLED.getCode()));
    }
}
