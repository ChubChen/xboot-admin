package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-09
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountBackend对象", description="")
public class AccountBackend extends Model<AccountBackend> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "backend_id", type = IdType.AUTO)
    private Integer backendId;

    @ApiModelProperty(value = "OMS 系统对应 id")
    private Integer omsId;

    @ApiModelProperty(value = "可读的后端店铺名称")
    private String storeName;

    @ApiModelProperty(value = "平台： - shopify - shoplazza")
    private String platform;

    @ApiModelProperty(value = "店铺在平台中的 id")
    private String platformName;

    @ApiModelProperty(value = "管理后台 url")
    private String adminUrl;

    private String apiKey;

    private String apiPassword;

    private String sharedSecret;

    private String token;

    @ApiModelProperty(value = "shopify： - develop - basic - shopify - advanced shoplazza: - 旗舰版")
    private String storePlan;

    @ApiModelProperty(value = "后端店铺状态： - 0 初始状态")
    private Integer storeStatus;

    @ApiModelProperty(value = "Paypal 收款账号")
    private String paypalAccount;

    @ApiModelProperty(value = "信用卡收单服务商")
    private String ccGateway;

    @ApiModelProperty(value = "绑定信用卡")
    private String billCc;

    @ApiModelProperty(value = "地址信息-第一行")
    private String addressLine1;

    @ApiModelProperty(value = "地址信息-第二行")
    private String addressLine2;

    @ApiModelProperty(value = "地址信息-城市")
    private String addressCity;

    @ApiModelProperty(value = "地址信息-邮编")
    private String addressZip;

    @ApiModelProperty(value = "地址信息-省份")
    private String addressProvince;

    @ApiModelProperty(value = "地址信息-国家")
    private String addressCountryRegion;

    @ApiModelProperty(value = "店铺主账号邮箱")
    private String ownerEmail;

    @ApiModelProperty(value = "店铺主账号密码")
    private String ownerPassword;

    @ApiModelProperty(value = "店铺员工邮箱")
    private String staffEmail;

    @ApiModelProperty(value = "员工账号密码")
    private String staffPassword;

    @ApiModelProperty(value = "邮箱密码")
    private String emailPassword;

    @ApiModelProperty(value = "主账号虚拟人格名")
    private String ownerFirstName;

    @ApiModelProperty(value = "主账号虚拟人格姓")
    private String ownerLastName;

    @ApiModelProperty(value = "店铺主账号注册手机")
    private String ownerPhone;

    @ApiModelProperty(value = "紫鸟浏览器 IP")
    private String superBrowserIp;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt = new Date();


    @Override
    protected Serializable pkVal() {
        return this.backendId;
    }

}
