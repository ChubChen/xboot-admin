package com.chubchen.admin.modules.easesea.dataparse;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.common.utils.SnowFlakeUtil;
import com.chubchen.admin.common.utils.SpringContextUtil;
import com.chubchen.admin.modules.easesea.constants.*;
import com.chubchen.admin.modules.easesea.entity.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.*;

/**
 * 店匠平台的数据解析类
 * @author chubchen
 */
@Component
@Slf4j
public class ShopifyDataParseHandle extends AbstractDataParseHandle{


    @Override
    public void parseCheckout(List<JSONObject> dataList, String platform) {
        List<AbandonedOrders> list = new ArrayList<>(dataList.size());
        List<AbandonedOrderItems> itemList = new ArrayList<>(dataList.size());
        dataList.forEach(jsonOrder -> {
            AbandonedOrders order = this.buildAbandonedOrder(jsonOrder, platform);
            list.add(order);
            itemList.addAll(this.buildAbandonedOrderItems(jsonOrder, order, platform));
        });

        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveAbandonedOrder(list, itemList);
    }

    @Override
    public void parseOrders(List<JSONObject> dataList, String platform) {
        List<Orders> list = new ArrayList<>(dataList.size());
        List<OrderItems> itemList = new ArrayList<>(dataList.size());
        List<OrderFulfillments> orderFulfillments = new ArrayList<>(dataList.size());
        List<OrderFulfillmentItems> orderFulfillmentItemssList = new ArrayList<>(dataList.size());
        List<ShippingAddress> shippingAddresses = new ArrayList<>(dataList.size());
        dataList.forEach(jsonOrder->
            this.thisBuildOrder(jsonOrder,list, itemList, orderFulfillments, orderFulfillmentItemssList, shippingAddresses, platform));
        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveOrders(list, itemList,
                orderFulfillments, orderFulfillmentItemssList, shippingAddresses);
    }

    @Override
    public Orders buildOrder(JSONObject jsonOrder) {
        Orders orders =  Orders.builder()
                .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                .pOrderId(String.valueOf(jsonOrder.getLong("id")))
                .domainId(jsonOrder.getInteger("domain_id"))
                .backendId(jsonOrder.getInteger("backend_id"))
                .createdAt(jsonOrder.getDate("created_at"))
                .canceledAt(jsonOrder.getDate("cancelled_at"))
                .closedAt(jsonOrder.getDate("closed_at"))
                .updatedAt(jsonOrder.getDate("updated_at"))
                .gateway(jsonOrder.getString("gateway"))
                .pAbandonedId(String.valueOf(jsonOrder.getLong("checkout_id")))
                .placedAt(jsonOrder.getDate("processed_at"))
                .number(jsonOrder.getString("order_number"))
                .subtotalPrice(jsonOrder.getBigDecimal("total_line_items_price"))
                .totalPrice(jsonOrder.getBigDecimal("total_price"))
                .totalDiscount(jsonOrder.getBigDecimal("total_discounts"))
                .totalTax(jsonOrder.getBigDecimal("total_tax"))
                .currency(jsonOrder.getString("currency"))
                .build();
        JSONArray jsonArray = jsonOrder.getJSONArray("shipping_lines");
        if(jsonArray!= null && jsonArray.size()>0){
            BigDecimal shipingAmount = BigDecimal.ZERO;
            for (int i = 0; i < jsonArray.size(); i++) {
                shipingAmount = shipingAmount.add(jsonArray.getJSONObject(i).getBigDecimal("price"));
            }
            orders.setTotalShipping(shipingAmount);
        }
        String financialStatus = jsonOrder.getString("financial_status");
        if(StringUtils.isEmpty(financialStatus)){
            financialStatus = "";
        }
        switch (financialStatus){
            case "pending" : orders.setFinancialStatus(OrdersFinancialStatusEnum.waiting.getCode()); break;
            case "authorized" : orders.setFinancialStatus(OrdersFinancialStatusEnum.paying.getCode()); break;
            case "paid" : orders.setFinancialStatus(OrdersFinancialStatusEnum.paid.getCode()); break;
            case "partially_paid" : orders.setFinancialStatus(OrdersFinancialStatusEnum.partially_paid.getCode()); break;
            case "refunded" : {
                orders.setFinancialStatus(OrdersFinancialStatusEnum.refunded.getCode());
                JSONArray refunds = jsonOrder.getJSONArray("refunds");
                this.buildRefund(refunds, orders);
            }
                break;
            case "partially_refunded" : {
                orders.setFinancialStatus(OrdersFinancialStatusEnum.partially_refunded.getCode());
                JSONArray refunds = jsonOrder.getJSONArray("refunds");
                this.buildRefund(refunds, orders);
            } break;
            case "voided" : orders.setFinancialStatus(OrdersFinancialStatusEnum.voided.getCode()); break;
            default : orders.setFinancialStatus(""); break;
        }
        String fulfillmentStatus = jsonOrder.getString("fulfillment_status");
        if(fulfillmentStatus == null){
            fulfillmentStatus = "null";
        }
        switch (fulfillmentStatus){
            case "null" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.unShipped.getCode()); break;
            case "partial" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.partial.getCode()); break;
            case "fulfilled" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.shipped.getCode()); break;
            case "restocked" : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.cancelled.getCode()); break;
            default : orders.setFulfillmentStatus(OrdersFulfillmentStatusEnum.unShipped.getCode()); break;
        }
        Date canceledAt = jsonOrder.getDate("cancelled_at");
        if(canceledAt != null){
            orders.setStatus(OrdersStatusEnum.canceled.getCode());
        }


        JSONObject customer = jsonOrder.getJSONObject("customer");
        if(customer != null){
            orders.setPCustomerId(customer.getLong("id").toString());
            orders.setOrdersCount(customer.getInteger("orders_count"));
            JSONObject defaultAddress = customer.getJSONObject("default_address");
            orders.setCountry( defaultAddress!= null ? defaultAddress.getString("country") : "");
        }
        return orders;
    }

    private void buildRefund(JSONArray refunds, Orders orders){
        if(refunds != null && refunds.size() > 0){
            BigDecimal refundAmount = BigDecimal.ZERO;
            Date firstProcessedAt = null;
            for (int i = 0; i < refunds.size(); i++) {
                JSONObject refund = refunds.getJSONObject(i);
                JSONArray transactions = refund.getJSONArray("transactions");
                if(transactions != null && transactions.size() >0){
                    for (int j = 0; j < transactions.size(); j++) {
                        JSONObject tran = transactions.getJSONObject(j);
                        if("success".equals(tran.getString("status"))){
                            refundAmount = refundAmount.add(tran.getBigDecimal("amount"));
                            Date processedAt = tran.getDate("processed_at");
                            if(firstProcessedAt == null){
                                firstProcessedAt = processedAt;
                            }
                            if(firstProcessedAt.after(processedAt)){
                                firstProcessedAt = processedAt;
                            }
                        }
                    }
                }
            }
            orders.setRefund(refundAmount);
            orders.setRefundAt(firstProcessedAt);
        }
    }

    @Override
    public List<OrderFulfillments> buildFulfillments(JSONObject jsonOrder, Orders orders, String platform) {
        JSONArray jsonArray = jsonOrder.getJSONArray("fulfillments");
        if(jsonArray != null && jsonArray.size() > 0){
            orders.setFulfilledAt(orders.getCreatedAt());
            List<OrderFulfillments> list = new ArrayList<>(jsonArray.size());
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject fulfillment = jsonArray.getJSONObject(i);
                OrderFulfillments orderFulfillments = OrderFulfillments.builder().id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                        .orderId(orders.getId())
                        .pOrderId(orders.getPOrderId())
                        .backendId(orders.getBackendId())
                        .domainId(orders.getDomainId())
                        .pFulfillmentId(fulfillment.getLong("id").toString())
                        .trackingNumber(fulfillment.getString("tracking_number"))
                        .trackingCompany(fulfillment.getString("tracking_company"))
                        .trackingUrl(fulfillment.getString("tracking_url"))
                        .updatedAt(fulfillment.getDate("updated_at"))
                        .createdAt(fulfillment.getDate("created_at"))
                        .build();
                Date createdAt = fulfillment.getDate("created_at");
                if(orders.getFulfilledAt().before(createdAt)){
                    orders.setFulfilledAt(createdAt);
                }

                orderFulfillments.setOrderFulfillmentItemsList(this.buildFulfillmentItems(fulfillment, orderFulfillments, platform));
                list.add(orderFulfillments);
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }

    @Override
    public void parseProduct(List<JSONObject> dataList, String platform) {
        List<PlatformProducts> list = new ArrayList<>(dataList.size());
        List<PlatformProductVariants> variantList = new ArrayList<>();
        dataList.forEach( jsonOrder->{
            PlatformProducts products = this.buildProduct(jsonOrder, platform);
            list.add(products);
            variantList.addAll(this.buildProductVariants(jsonOrder, products));
        });
        //批量保存数据到数据库
        SpringContextUtil.getBean(ShopifyDataParseHandle.class).saveProducts(list, variantList);
    }

    @Override
    public List<PlatformProductVariants> buildProductVariants(JSONObject jsonProduct, PlatformProducts products) {
        JSONArray jsonArray = jsonProduct.getJSONArray("variants");
        if(jsonArray != null && jsonArray.size()>0) {
            JSONArray imageArray = jsonProduct.getJSONArray("images");
            Map<String, String> map = new HashMap<>(1);
            if(imageArray != null && imageArray.size() > 0){
                map = new HashMap<>(imageArray.size());
                for (int i = 0; i < imageArray.size(); i++) {
                    JSONObject image = imageArray.getJSONObject(i);
                    map.put(String.valueOf(image.getLong("id")), image.getString("src"));
                }
            }
            List<PlatformProductVariants> list = new ArrayList<>();
            Set<String> spuSet = new HashSet<>(1);
            String spu = "";
            for (int i = 0; i < jsonArray.size(); i++) {
                JSONObject jsonVariants = jsonArray.getJSONObject(i);
                PlatformProductVariants variants = PlatformProductVariants.builder()
                        .id(SnowFlakeUtil.getFlowIdInstance().nextIdStr())
                        .productId(products.getId())
                        .backendId(products.getBackendId())
                        .pProductId(String.valueOf(jsonVariants.getLong("product_id")))
                        .pVariantId(String.valueOf(jsonVariants.getLong("id")))
                        .title(jsonVariants.getString("title"))
                        .sku(jsonVariants.getString("sku"))
                        .price(jsonVariants.getBigDecimal("price"))
                        .comparedAtPrice(jsonVariants.getBigDecimal("compared_at_price"))
                        .grams(jsonVariants.getInteger("weight"))
                        .createdAt(jsonVariants.getDate("created_at"))
                        .updatedAt(jsonVariants.getDate("updated_at"))
                        .build();
                spu = this.parseSkuToSpu(jsonVariants.getString("sku"));
                variants.setImageSrc(map.get(String.valueOf(jsonVariants.getLong("image_id"))));
                variants.setSpu(spu);
                spuSet.add(spu);
                list.add(variants);
            }
            if(spuSet.size() == 1){
                products.setSpu(spu);
            }
            return list;
        }else{
            return new ArrayList<>();
        }
    }
}
