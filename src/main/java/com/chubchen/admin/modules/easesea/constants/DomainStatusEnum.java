package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * domain 域名状态枚举类
 * @author chubchen
 */
@Getter
public enum DomainStatusEnum {

    DISABLED(-1, "已过期"),
    BACKUP(0, "备用"),
    USED(1, "使用中");

    private int code;

    private String desc;

    DomainStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> getMap(){
        return Arrays.stream(DomainStatusEnum.values()).
                collect(Collectors.toMap(DomainStatusEnum::getCode, DomainStatusEnum::getDesc));
    }
}
