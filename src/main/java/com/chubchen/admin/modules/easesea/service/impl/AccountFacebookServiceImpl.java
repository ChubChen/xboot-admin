package com.chubchen.admin.modules.easesea.service.impl;

import com.chubchen.admin.modules.easesea.entity.AccountFacebook;
import com.chubchen.admin.modules.easesea.dao.mapper.AccountFacebookMapper;
import com.chubchen.admin.modules.easesea.service.IAccountFacebookService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-08
 */
@Service
public class AccountFacebookServiceImpl extends ServiceImpl<AccountFacebookMapper, AccountFacebook> implements IAccountFacebookService {

    @Override
    public  IPage<AccountFacebook> findListByPage(PageVo pageVo, AccountFacebook accountFacebook){
        IPage<AccountFacebook> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountFacebook);
    }

    @Override
    public List<AccountFacebook> findListAll(AccountFacebook accountFacebook){
        return baseMapper.selectAll(accountFacebook);
    }

    @Override
    public int add(AccountFacebook accountFacebook){
        return baseMapper.insert(accountFacebook);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountFacebook accountFacebook){
        return baseMapper.updateById(accountFacebook);
    }

    @Override
    public AccountFacebook findById(Long id){
        return  baseMapper.selectById(id);
    }
}
