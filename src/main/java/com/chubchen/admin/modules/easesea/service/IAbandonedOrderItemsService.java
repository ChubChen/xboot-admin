package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.AbandonedOrderItems;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
public interface IAbandonedOrderItemsService extends IService<AbandonedOrderItems> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param abandonedOrderItems 过滤条件
     * @return IPage<AbandonedOrderItems>
     */
    IPage<AbandonedOrderItems> findListByPage(PageVo pageVo, AbandonedOrderItems abandonedOrderItems);

   /**
    * 查询分页数据
    *
    * @return  List<AbandonedOrderItems>
    */
    List<AbandonedOrderItems> findListAll(AbandonedOrderItems abandonedOrderItems);

    /**
     * 添加
     *
     * @param abandonedOrderItems 
     * @return int
     */
    int add(AbandonedOrderItems abandonedOrderItems);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param abandonedOrderItems 
     * @return int
     */
    int updateData(AbandonedOrderItems abandonedOrderItems);

    /**
     * id查询数据
     *
     * @param id id
     * @return AbandonedOrderItems
     */
    AbandonedOrderItems findById(Long id);
}
