package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
public enum AdyenEventCode {

    CANCEL_OR_REFUND("CANCEL_OR_REFUND","取消或者退款"),
    REFUND("REFUND", "已退款"),
    REFUNDED_REVERSED("REFUNDED_REVERSED", "退款冲销"),
    REFUND_FAILED("REFUND_FAILED", "退款失败");

    private String code;
    private String messgage;

    AdyenEventCode(String code, String message) {
        this.code = code;
        this.messgage = message;
    }

    public static Map<String,String> getMap(){
        return Arrays.stream(AdyenEventCode.values()).collect(Collectors.toMap(AdyenEventCode::getCode,
                AdyenEventCode::getMessgage));
    }
}
