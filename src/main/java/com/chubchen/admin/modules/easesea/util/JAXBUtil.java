package com.chubchen.admin.modules.easesea.util;

import com.chubchen.admin.modules.easesea.worldpay.PaymentService;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.transform.sax.SAXSource;
import java.io.StringWriter;

public class JAXBUtil {

    public static String getXml(Object obj)throws Exception{
        JAXBContext context = JAXBContext.newInstance(obj.getClass());
        StringWriter sw = new StringWriter();
        Marshaller marshaller = context.createMarshaller();
        marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<!DOCTYPE paymentService PUBLIC \"-//WorldPay//DTD WorldPay PaymentService v1//EN\"\n" +
                "\"http://dtd.worldpay.com/paymentService_v1.dtd\">");
        marshaller.marshal(obj,sw);
        String xmlRequest = sw.toString();
        return xmlRequest;
    }

    public static <T> T parseXMl(String xml, Class<T> tClass) throws Exception{
        if(StringUtils.isNotEmpty(xml) && xml.indexOf("<paymentService") > 0){
            xml = "<paymentService" + xml.split("<paymentService")[1];
            JAXBContext jc = JAXBContext.newInstance(tClass);
            Unmarshaller u = jc.createUnmarshaller();
            u.setSchema(null);
            T obj = (T)u.unmarshal(IOUtils.toInputStream(xml));
            return obj;
        }else{
            return parseXMlNoValidate(xml, tClass);
        }
    }

    public static <T> T parseXMlNoValidate(String xml, Class<T> tClass) throws Exception{
        JAXBContext j2c = JAXBContext.newInstance(tClass);
        Unmarshaller u2 = j2c.createUnmarshaller();
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setValidating(false);
        XMLReader xmlReader = spf.newSAXParser().getXMLReader();
        InputSource inputSource = new InputSource(IOUtils.toInputStream(xml));
        SAXSource source = new SAXSource(xmlReader, inputSource);
        T obj = (T)u2.unmarshal(source);
        return obj;
    }
}
