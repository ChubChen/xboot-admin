package com.chubchen.admin.modules.easesea.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.OrderItems;
import com.chubchen.admin.modules.easesea.dao.mapper.OrderItemsMapper;
import com.chubchen.admin.modules.easesea.service.IOrderItemsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-11
 */
@Service
@DS("easesea")
public class OrderItemsServiceImpl extends ServiceImpl<OrderItemsMapper, OrderItems> implements IOrderItemsService {

    @Override
    public  IPage<OrderItems> findListByPage(PageVo pageVo, OrderItems orderItems){
        IPage<OrderItems> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, orderItems);
    }

    @Override
    public List<OrderItems> findListAll(OrderItems orderItems){
        return baseMapper.selectAll(orderItems);
    }

    @Override
    public int add(OrderItems orderItems){
        return baseMapper.insert(orderItems);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(OrderItems orderItems){
        return baseMapper.updateById(orderItems);
    }

    @Override
    public OrderItems findById(Long id){
        return  baseMapper.selectById(id);
    }
}
