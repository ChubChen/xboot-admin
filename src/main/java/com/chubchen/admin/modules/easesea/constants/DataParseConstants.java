package com.chubchen.admin.modules.easesea.constants;

/**
 * @author chubchen
 */
public interface DataParseConstants {

    /**
     * 未处理
     */
    int PARSE_STATUS_INIT = 1;

    /**
     * 处理中
     */
    int PARSE_STATUS_ING = 2;

    /**
     * 解析完成
     */
    int PARSE_STATUS_FINISH = 3;

    /**
     * 解析失败
     */
    int PARSE_STATUS_ERROR = 4;


    /**
     * 店匠订单状态
     */
    String SHOPLAZZA_STATUS_OPENED = "opened";
    /**
     * 店匠订单状态
     */
    String SHOPLAZZA_STATUS_PLACED = "placed";
    /**
     * 店匠订单状态
     */
    String SHOPLAZZA_STATUS_FINISHED = "finished";
    /**
     * 店匠订单状态
     */
    String SHOPLAZZA_STATUS_CANCEL = "cancelled";
}
