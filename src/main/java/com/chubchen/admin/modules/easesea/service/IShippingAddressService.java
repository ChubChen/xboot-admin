package com.chubchen.admin.modules.easesea.service;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.entity.ShippingAddress;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-12
 */
public interface IShippingAddressService extends IService<ShippingAddress> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param shippingAddress 过滤条件
     * @return IPage<ShippingAddress>
     */
    IPage<ShippingAddress> findListByPage(PageVo pageVo, ShippingAddress shippingAddress);

   /**
    * 查询分页数据
    *
    * @return  List<ShippingAddress>
    */
    List<ShippingAddress> findListAll(ShippingAddress shippingAddress);

    /**
     * 添加
     *
     * @param shippingAddress 
     * @return int
     */
    int add(ShippingAddress shippingAddress);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param shippingAddress 
     * @return int
     */
    int updateData(ShippingAddress shippingAddress);

    /**
     * id查询数据
     *
     * @param id id
     * @return ShippingAddress
     */
    ShippingAddress findById(Long id);
}
