package com.chubchen.admin.modules.easesea.constants;

import lombok.Getter;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * domain 域名状态枚举类
 * @author chubchen
 */
@Getter
public enum FBAccountStatusEnum {

    DISABLED(-1, "被封"),
    CLOSE(-2, "停用"),
    PATOZON(-3, "帕拓逊协议"),
    ZERO(-4, "限额清零"),
    DOMAIN_DISABLE(-5, "域名被标记"),
    PAGE_SCORE(-6, "主页评分过低"),
    BM_DISABLE(-7, "BM被封"),
    NEW(0, "新建"),
    USED(1, "使用中"),
    PAUSED(2, "暂停中");

    private int code;

    private String desc;

    FBAccountStatusEnum(int code, String desc){
        this.code = code;
        this.desc = desc;
    }

    public static Map<Integer, String> getMap(){
        return Arrays.stream(FBAccountStatusEnum.values()).
                collect(Collectors.toMap(FBAccountStatusEnum::getCode, FBAccountStatusEnum::getDesc));
    }
}
