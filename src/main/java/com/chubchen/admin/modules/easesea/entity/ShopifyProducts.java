package com.chubchen.admin.modules.easesea.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-02-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ShopifyProducts extends Model<ShopifyProducts> {

    private static final long serialVersionUID = 1L;

    @TableId("product_id")
    private String productId;

    @TableField("store_code")
    private String storeCode;

    @TableField("spu")
    private String spu;

    @TableField("handle")
    private String handle;

    @TableField("title")
    private String title;

    @TableField("image_src")
    private String imageSrc;

    @TableField("tags")
    private String tags;

    @TableField(exist = false)
    private Integer quantity;

    @TableField(exist = false)
    private BigDecimal price;

    @TableField("created_at")
    private LocalDateTime createdAt;

    @TableField("updated_at")
    private LocalDateTime updatedAt;

    @TableField(exist = false)
    private Integer storeCount;

    @TableField(exist = false)
    private String noTags;

    @TableField(exist = false)
    private BigDecimal dayToday;

    @Override
    protected Serializable pkVal() {
        return this.productId;
    }

}
