package com.chubchen.admin.modules.easesea.controller;

import cn.hutool.core.util.StrUtil;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.imagesearch.model.v20190325.SearchImageRequest;
import com.aliyuncs.imagesearch.model.v20190325.SearchImageResponse;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.PageUtil;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;
import com.chubchen.admin.modules.easesea.util.AlibabaSearchImageUtil;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@RestController
@Slf4j
@RequestMapping("/xboot/choose-product")
public class ChooseProductController {

    @Autowired
    AlibabaSearchImageUtil alibabaSearchImageUtil;

    @Autowired
    @Qualifier("chooseMongoTemplate")
    private MongoTemplate mongoTemplate;

    @RequestMapping(value = "/file",method = RequestMethod.POST)
    @ApiOperation(value = "将图片转化为Base64")
    public Result<String> upload(@RequestParam(required = false) MultipartFile file,
                                 @RequestParam(required = false) String base64,
                                 HttpServletRequest request) {

        if(StrUtil.isNotBlank(base64)){
            // base64上传
            return new ResultUtil<String>().setData(base64);
        }
        try {
            InputStream inputStream = file.getInputStream();
            String encoded = Base64.getEncoder().encodeToString(IOUtils.toByteArray(inputStream));
            return new ResultUtil<String>().setData(encoded);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResultUtil<String>().setErrorMsg("图片转换失败");
        }
    }

    @RequestMapping(value = "/searchImage",method = RequestMethod.POST)
    public Result<IPage<SearchImageResponse.Auction>> searchImage(@ModelAttribute PageVo pageVo,
                                                                  @RequestParam String base64,
                                                                  @RequestParam(required = false) Integer categoryId,
                                                                  @RequestParam(required = false) Integer crop) {
        SearchImageRequest request = new SearchImageRequest();
        try {
            IPage<SearchImageResponse.Auction> page = PageUtil.initMpPage(pageVo);
            request.setInstanceName(alibabaSearchImageUtil.getInstanceName());
            request.setType("SearchByPic");
            // 1. Type=SearchByPic时，必填
            // 2. Type=SearchByName时，无需填写。
            request.setPicContent(base64);
            if(categoryId != null){
                request.setCategoryId(categoryId);
            }
            // 选填，是否需要进行主体识别，默认为true。
            // 1.为true时，由系统进行主体识别，以识别的主体进行搜索，主体识别结果可在Response中获取。
            // 2. 为false时，则不进行主体识别，以整张图进行搜索。
            // 3.对于布料图片搜索，此参数会被忽略，系统会以整张图进行搜索。
            if(crop != null && crop > 0){
                request.setCrop(true);
            }else{
                request.setCrop(false);
            }
            // 选填，图片的主体区域，格式为 x1,x2,y1,y2, 其中 x1,y1 是左上角的点，x2，y2是右下角的点。
            // 若用户设置了Region，则不论Crop参数为何值，都将以用户输入Region进行搜索。
            // 3.对于布料图片搜索，此参数会被忽略，系统会以整张图进行搜索。
            // 选填，返回结果的数目。取值范围：1-100。默认值：10。
            request.setNum(pageVo.getPageSize());
            // 选填，返回结果的起始位置。取值范围：0-499。默认值：0。
            request.setStart((pageVo.getPageNumber()-1) * pageVo.getPageSize());
            SearchImageResponse response = alibabaSearchImageUtil.getClient().getAcsResponse(request);
            log.info("调用以图搜图接口返回的code{}", response.getCode());
            if(response.getSuccess()){
                List<String> imageIds = response.getAuctions().stream().map(item->item.getProductId()).collect(Collectors.toList());
                //查询图片真实路径
                Query query = new Query(Criteria.where("image_id").in(imageIds));
                List<JSONObject> list = mongoTemplate.find(query, JSONObject.class, "image_collector");
                Map<String, JSONArray> imageUrl = list.stream().collect(
                        Collectors.toMap(item->item.getString("image_id"), item->item.getJSONArray("shops")));
                response.getAuctions().forEach(item->{
                    if(imageUrl.containsKey(item.getProductId())){
                        JSONArray jsonArray = imageUrl.get(item.getProductId());
                        if(jsonArray != null && jsonArray.size() > 0){
                            item.setCustomContent(jsonArray.getString(0));
                        }
                    }
                });
                if(response.getHead().getDocsFound() > 500){
                    page.setTotal(500);
                }else{
                    page.setTotal(response.getHead().getDocsFound());
                }
                page.setRecords(response.getAuctions());
                return new ResultUtil<IPage<SearchImageResponse.Auction>>().setData(page);
            }else{
                return new ResultUtil<IPage<SearchImageResponse.Auction>>().setErrorMsg(response.getMsg());
            }
        } catch (ClientException e) {
            log.error("调用阿里远程接口失败",e);
            return new ResultUtil<IPage<SearchImageResponse.Auction>>().setErrorMsg("调用接口异常");
        }
    }
}
