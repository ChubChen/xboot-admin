package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.SlPaypalAccountMapper;
import com.chubchen.admin.modules.easesea.entity.SlPaypalAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-07-30
*/
@DS("easesea")
public interface SlPaypalAccountMapper extends BaseMapper<SlPaypalAccount> {

    /**
     * 根据条件分页查询
     * @param page
     * @param slPaypalAccount
     * @return IPage<SlPaypalAccount>
    */
    IPage<SlPaypalAccount> selectListByPage(IPage<SlPaypalAccount> page, @Param("slPaypalAccount") SlPaypalAccount slPaypalAccount);

    /**
     * 根据条件查询全部
     * @param slPaypalAccount
     * @return List<SlPaypalAccount>
    */
    List<SlPaypalAccount> selectAll(@Param("slPaypalAccount") SlPaypalAccount slPaypalAccount);

}
