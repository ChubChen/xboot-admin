package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.dao.mapper.AccountPaypalMapper;
import com.chubchen.admin.modules.easesea.entity.AccountPaypal;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-24
*/
@DS("easesea")
public interface AccountPaypalMapper extends BaseMapper<AccountPaypal> {

    /**
     * 根据条件分页查询
     * @param page
     * @param searchVo
     * @param accountPaypal
     * @return IPage<AccountPaypal>
    */
    IPage<AccountPaypal> selectListByPage(IPage<AccountPaypal> page,
                                          @Param("search") SearchVo searchVo,
                                          @Param("accountPaypal") AccountPaypal accountPaypal);

    /**
     * 根据条件查询全部
     * @param accountPaypal
     * @param searchVo
     * @return List<AccountPaypal>
    */
    List<AccountPaypal> selectAll(@Param("accountPaypal") AccountPaypal accountPaypal, @Param("search")SearchVo searchVo);

}
