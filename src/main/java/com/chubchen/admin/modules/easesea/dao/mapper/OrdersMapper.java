package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.easesea.entity.Orders;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.easesea.entity.ShopifyOrderStatistic;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface OrdersMapper extends BaseMapper<Orders> {

    /**
     * 根据条件分页查询
     * @param page
     * @param orders
     * @param search
     * @return IPage<Orders>
    */
    IPage<Orders> selectListByPage(IPage<Orders> page,
                                   @Param("orders") Orders orders,
                                   @Param("search") SearchVo search);

    /**
     * 根据条件查询全部
     * @param orders
     * @param search
     * @return List<Orders>
    */
    List<Orders> selectAll(@Param("orders") Orders orders,
                           @Param("search") SearchVo search);

    /**
     * 保存根据业务ID更新
     * @param orders
     * @return
     */
    int batchSaveOnDuplicateKey(List<Orders> orders);

    /**
     * 删除数据
     * @param ordersList
     * @return
     */
    List<Orders> findByUniqueId(@Param("list") List<Orders> ordersList);

    /**
     * 根据条件汇总统计
     * @param timeZone
     * @param flag
     * @param domainIdList
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getSaleOrderStatistic(@Param("timeZone") String timeZone,
                                                      @Param("flag") Boolean flag,
                                                      @Param("domainIdList") List<Integer> domainIdList,
                                                      @Param("searchVo") SearchVo searchVo);

    /**
     * 更具条件统计广告
     * @param timeZone
     * @param flag
     * @param domainIdList
     * @param searchVo
     * @return
     */
    List<ShopifyOrderStatistic> getAdStatistic(@Param("timeZone") String timeZone,
                                                @Param("flag") Boolean flag,
                                                @Param("domainIdList") List<Integer> domainIdList,
                                                @Param("searchVo") SearchVo searchVo);
}
