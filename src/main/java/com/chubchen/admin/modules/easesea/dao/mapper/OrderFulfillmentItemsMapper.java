package com.chubchen.admin.modules.easesea.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.easesea.dao.mapper.OrderFulfillmentItemsMapper;
import com.chubchen.admin.modules.easesea.entity.OrderFulfillmentItems;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-11
*/
@DS("easesea")
public interface OrderFulfillmentItemsMapper extends BaseMapper<OrderFulfillmentItems> {

    /**
     * 根据条件分页查询
     * @param page
     * @param orderFulfillmentItems
     * @return IPage<OrderFulfillmentItems>
    */
    IPage<OrderFulfillmentItems> selectListByPage(IPage<OrderFulfillmentItems> page, @Param("orderFulfillmentItems") OrderFulfillmentItems orderFulfillmentItems);

    /**
     * 根据条件查询全部
     * @param orderFulfillmentItems
     * @return List<OrderFulfillmentItems>
    */
    List<OrderFulfillmentItems> selectAll(@Param("orderFulfillmentItems") OrderFulfillmentItems orderFulfillmentItems);

}
