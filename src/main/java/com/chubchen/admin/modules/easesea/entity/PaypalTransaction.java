package com.chubchen.admin.modules.easesea.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import java.util.Date;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-07-21
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="PaypalTransaction对象", description="")
public class PaypalTransaction extends Model<PaypalTransaction> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键ID")
    private String id;

    private Integer paypalAccountId;

    @ApiModelProperty(value = "交易号")
    private String transactionId;

    @ApiModelProperty(value = "账单号对应orders表p_abandoned_id")
    private String invoiceId;

    @ApiModelProperty(value = "用户邮箱号")
    private String payerEmail;

    @ApiModelProperty(value = "用户名称")
    private String payerFullName;

    @ApiModelProperty(value = "国家code")
    private String countryCode;

    @ApiModelProperty(value = "event_code")
    private String transactionEventCode;

    @ApiModelProperty(value = "交易金额")
    private BigDecimal transactionAmount;

    @ApiModelProperty(value = "币种")
    private String currencyCode;

    @ApiModelProperty(value = "状态")
    private String transactionStatus;

    @ApiModelProperty(value = "可用余额，")
    private BigDecimal availableBlance;

    @ApiModelProperty(value = "交易时间")
    private Date transactionInitiationDate;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
