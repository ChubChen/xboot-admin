package com.chubchen.admin.modules.easesea.service;

import com.chubchen.admin.modules.easesea.entity.AccountBackend;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-05
 */
public interface IAccountBackendService extends IService<AccountBackend> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param accountBackend 过滤条件
     * @return IPage<AccountBackend>
     */
    IPage<AccountBackend> findListByPage(PageVo pageVo, AccountBackend accountBackend);

   /**
    * 查询分页数据
    *
    * @return  List<AccountBackend>
    */
    List<AccountBackend> findListAll(AccountBackend accountBackend);

    /**
     * 添加
     *
     * @param accountBackend 
     * @return int
     */
    int add(AccountBackend accountBackend);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param accountBackend 
     * @return int
     */
    int updateData(AccountBackend accountBackend);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountBackend
     */
    AccountBackend findById(Long id);

    /**
     * 批量更新状态
     * @param status
     * @param ids
     * @return
     */
    Integer updateStatusById(Integer status, Integer[] ids);

}
