package com.chubchen.admin.modules.paypal;

import com.paypal.api.payments.Error;
import com.paypal.api.payments.Links;
import lombok.Data;

import java.util.List;

/**
 *  @author chubchen
 * tracker Batch 请求返回对象
 */
@Data
public class Trackers {

    public List<Tracker> trackers;

    public List<TrackerIndentifiers> trackerIdentifiers;

    public List<Error> errors;

    public List<Links> links;
}
