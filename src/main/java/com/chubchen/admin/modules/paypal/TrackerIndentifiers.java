package com.chubchen.admin.modules.paypal;

import com.paypal.api.payments.Links;
import lombok.Data;

import java.util.List;

/**
 *@author chubchen
 * tracking 返回对象
 */
@Data
public class TrackerIndentifiers {

    private String transactionId;

    private String trackingNumber;

    private List<Links> links;
}
