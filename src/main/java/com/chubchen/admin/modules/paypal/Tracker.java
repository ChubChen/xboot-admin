package com.chubchen.admin.modules.paypal;

import lombok.Data;

/**
 * @author chubchen
 * 物流跟踪类
 */
@Data
public class Tracker {

    private String transactionId;

    private String trackingNumber;

    private TrackerTypeEnum trackingNumberType=TrackerTypeEnum.CARRIER_PROVIDED;

    private TrackerStatusEnum status;

    private String shipmentDate;

    private String carrier="OTHER";

    private String carrierNameOther;


    public enum TrackerStatusEnum{
        CANCELLED,SHIPPED,SHIPMENT_CREATED
    }

    public enum TrackerTypeEnum{
        CARRIER_PROVIDED,E2E_PARTNER_PROVIDED
    }
}
