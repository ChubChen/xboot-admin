package com.chubchen.admin.modules.starlink.service.impl;

import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountDomainMapper;
import com.chubchen.admin.modules.starlink.service.IAccountDomainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 * 域名表 服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Service
public class AccountDomainServiceImpl extends ServiceImpl<AccountDomainMapper, AccountDomain> implements IAccountDomainService {

    @Override
    public  IPage<AccountDomain> findListByPage(PageVo pageVo, AccountDomain accountDomain){
        IPage<AccountDomain> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountDomain);
    }

    @Override
    public List<AccountDomain> findListAll(AccountDomain accountDomain){
        return baseMapper.selectAll(accountDomain);
    }

    @Override
    public int add(AccountDomain accountDomain){
        return baseMapper.insert(accountDomain);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountDomain accountDomain){
        return baseMapper.updateById(accountDomain);
    }

    @Override
    public AccountDomain findById(Long id){
        return  baseMapper.selectById(id);
    }
}
