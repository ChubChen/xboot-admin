package com.chubchen.admin.modules.starlink.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountAdManagerMapper;
import com.chubchen.admin.modules.starlink.entity.AccountAdManager;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    * BM，MCC 或 BC表 服务类
    * </p>
*
* @author chubchen
* @since 2020-08-12
*/
@DS("config")
public interface AccountAdManagerMapper extends BaseMapper<AccountAdManager> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountAdManager
     * @return IPage<AccountAdManager>
    */
    IPage<AccountAdManager> selectListByPage(IPage<AccountAdManager> page, @Param("accountAdManager") AccountAdManager accountAdManager);

    /**
     * 根据条件查询全部
     * @param accountAdManager
     * @return List<AccountAdManager>
    */
    List<AccountAdManager> selectAll(@Param("accountAdManager") AccountAdManager accountAdManager);

}
