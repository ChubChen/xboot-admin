package com.chubchen.admin.modules.starlink.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.starlink.entity.AccountAd;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountAdMapper;
import com.chubchen.admin.modules.starlink.service.IAccountAdService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 * 广告账号表 服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Service
public class AccountAdServiceImpl extends ServiceImpl<AccountAdMapper, AccountAd> implements IAccountAdService {

    @Override
    public  IPage<AccountAd> findListByPage(PageVo pageVo, AccountAd accountAd){
        IPage<AccountAd> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountAd);
    }

    @Override
    public List<AccountAd> findListAll(AccountAd accountAd){
        return baseMapper.selectAll(accountAd);
    }

    @Override
    public int add(AccountAd accountAd){
        return baseMapper.insert(accountAd);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountAd accountAd){
        return baseMapper.updateById(accountAd);
    }

    @Override
    public AccountAd findById(Long id){
        return  baseMapper.selectById(id);
    }
}
