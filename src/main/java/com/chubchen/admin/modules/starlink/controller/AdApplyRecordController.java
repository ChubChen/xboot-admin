package com.chubchen.admin.modules.starlink.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.starlink.entity.AccountCompany;
import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.starlink.service.IAdApplyRecordService;
import com.chubchen.admin.modules.starlink.entity.AdApplyRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-08-19
 */
@Api(tags = {""})
@RestController
@RequestMapping("/xboot/starlink/ad-apply-record")
public class AdApplyRecordController {

    @Resource
    private IAdApplyRecordService adApplyRecordService;


    @ApiOperation(value = "新增")
    @PostMapping()
    @SystemLog(description = "新增广告申请记录", type = LogType.OPERATION)
    public Result<Integer> add(@ModelAttribute AdApplyRecord adApplyRecord){
        Result result = validateParam(adApplyRecord);
        if (result.isSuccess()) {
            Integer count = adApplyRecordService.add(adApplyRecord);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }


    private Result validateParam(AdApplyRecord adApplyRecord){
        if(adApplyRecord.getDomainId() == null){
            return new ResultUtil<Integer>().setErrorMsg("请选择为那个域名进行申请广告");
        }
        if(adApplyRecord.getCompanyId() == null){
            return new ResultUtil<Integer>().setErrorMsg("请选择使用的公司主体");
        }
        if(adApplyRecord.getAdNum() == null){
            return new ResultUtil<Integer>().setErrorMsg("请填写");
        }
        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }


    @ApiOperation(value = "删除")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = adApplyRecordService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新")
    @PutMapping()
    @SystemLog(description = "修改广告申请记录", type = LogType.OPERATION)
    public Result<Integer> update(@ModelAttribute AdApplyRecord adApplyRecord){
        Result result = validateParam(adApplyRecord);
        if (result.isSuccess()) {
            Integer count = adApplyRecordService.updateData(adApplyRecord);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询分页数据")
    @GetMapping()
    public Result<IPage<AdApplyRecord>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AdApplyRecord adApplyRecord){
            IPage<AdApplyRecord> page = adApplyRecordService.findListByPage(pageVo, adApplyRecord);
         return new ResultUtil<IPage<AdApplyRecord>>().setData(page);
    }

    @ApiOperation(value = "id查询")
    @GetMapping("{id}")
    public Result<AdApplyRecord> findById(@PathVariable Long id){
        AdApplyRecord adApplyRecord=  adApplyRecordService.findById(id);
        return new ResultUtil<AdApplyRecord>().setData(adApplyRecord);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AdApplyRecord>> findById(@ModelAttribute AdApplyRecord adApplyRecord){
        List<AdApplyRecord> adApplyRecordList =  adApplyRecordService.findListAll(adApplyRecord);
        return new ResultUtil<List<AdApplyRecord>>().setData(adApplyRecordList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AdApplyRecord.AppliedStatus.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "批量修改申请记录状态", type = LogType.OPERATION)
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!AdApplyRecord.AppliedStatus.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = adApplyRecordService.update(new UpdateWrapper<AdApplyRecord>()
                .set("status", status)
                .set("updated_at", new Date())
                .in("id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
