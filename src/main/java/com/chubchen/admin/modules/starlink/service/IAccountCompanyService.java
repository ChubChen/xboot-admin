package com.chubchen.admin.modules.starlink.service;

import com.chubchen.admin.modules.starlink.entity.AccountCompany;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 * 公司主体表 服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
public interface IAccountCompanyService extends IService<AccountCompany> {

    /**
     * 查询公司主体表分页数据
     *
     * @param pageVo      页码
     * @param accountCompany 过滤条件
     * @return IPage<AccountCompany>
     */
    IPage<AccountCompany> findListByPage(PageVo pageVo, AccountCompany accountCompany);

   /**
    * 查询公司主体表分页数据
    *
    * @return  List<AccountCompany>
    */
    List<AccountCompany> findListAll(AccountCompany accountCompany);

    /**
     * 添加公司主体表
     *
     * @param accountCompany 公司主体表
     * @return int
     */
    int add(AccountCompany accountCompany);

    /**
     * 删除公司主体表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改公司主体表
     *
     * @param accountCompany 公司主体表
     * @return int
     */
    int updateData(AccountCompany accountCompany);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountCompany
     */
    AccountCompany findById(Long id);
}
