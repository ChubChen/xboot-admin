package com.chubchen.admin.modules.starlink.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.starlink.entity.AccountAdManager;
import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.starlink.service.IAccountCompanyService;
import com.chubchen.admin.modules.starlink.entity.AccountCompany;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 公司主体表 前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Api(tags = {"公司主体表"})
@RestController
@RequestMapping("/xboot/starlink/account-company")
public class AccountCompanyController {

    @Resource
    private IAccountCompanyService accountCompanyService;


    @ApiOperation(value = "新增公司主体表")
    @PostMapping()
    public Result<Integer> add(@ModelAttribute AccountCompany accountCompany){
        Result result = validateParam(accountCompany);
        if (result.isSuccess()) {
            AccountCompany exists = accountCompanyService.getOne(new QueryWrapper<AccountCompany>()
                    .eq("name_zh", accountCompany.getNameZh()).or()
                    .eq("company_usci", accountCompany.getCompanyUsci()));
            if(exists != null && StringUtils.isNotEmpty(exists.getNameZh())){
                return new ResultUtil<Integer>().setErrorMsg("已经存在不能重复添加");
            }
            accountCompany.setCreatedAt(new Date());
            accountCompany.setUpdatedAt(new Date());
            Integer count = accountCompanyService.add(accountCompany);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else {
            return result;
        }
    }

    private Result validateParam(AccountCompany accountCompany){
        if(StringUtils.isBlank(accountCompany.getNameZh())){
            return new ResultUtil<Integer>().setErrorMsg("中文名称不能为空");
        }
        if(StringUtils.isBlank(accountCompany.getNameEn())){
            return new ResultUtil<Integer>().setErrorMsg("英文名称不能为空");
        }
        if(StringUtils.isBlank(accountCompany.getCompanyUsci())){
            return new ResultUtil<Integer>().setErrorMsg("社会信用代码不能为空");
        }
        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除公司主体表")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountCompanyService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新公司主体表")
    @PutMapping()
    public Result<Integer> update(@ModelAttribute AccountCompany accountCompany){
        accountCompany.setUpdatedAt(new Date());
        Integer count = accountCompanyService.updateData(accountCompany);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询公司主体表分页数据")
    @GetMapping()
    public Result<IPage<AccountCompany>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountCompany accountCompany){
            IPage<AccountCompany> page = accountCompanyService.findListByPage(pageVo, accountCompany);
         return new ResultUtil<IPage<AccountCompany>>().setData(page);
    }

    @ApiOperation(value = "id查询公司主体表")
    @GetMapping("{id}")
    public Result<AccountCompany> findById(@PathVariable Long id){
        AccountCompany accountCompany=  accountCompanyService.findById(id);
        return new ResultUtil<AccountCompany>().setData(accountCompany);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountCompany>> findById(@ModelAttribute AccountCompany accountCompany){
        List<AccountCompany> accountCompanyList =  accountCompanyService.findListAll(accountCompany);
        return new ResultUtil<List<AccountCompany>>().setData(accountCompanyList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AccountCompany.CompanyStatus.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "修改BM账号状态", type = LogType.OPERATION)
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!AccountCompany.CompanyStatus.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = accountCompanyService.update(new UpdateWrapper<AccountCompany>()
                .set("company_status", status)
                .set("updated_at", new Date())
                .in("id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
