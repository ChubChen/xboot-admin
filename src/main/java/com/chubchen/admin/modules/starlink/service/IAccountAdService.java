package com.chubchen.admin.modules.starlink.service;

import com.chubchen.admin.modules.starlink.entity.AccountAd;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 * 广告账号表 服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
public interface IAccountAdService extends IService<AccountAd> {

    /**
     * 查询广告账号表分页数据
     *
     * @param pageVo      页码
     * @param accountAd 过滤条件
     * @return IPage<AccountAd>
     */
    IPage<AccountAd> findListByPage(PageVo pageVo, AccountAd accountAd);

   /**
    * 查询广告账号表分页数据
    *
    * @return  List<AccountAd>
    */
    List<AccountAd> findListAll(AccountAd accountAd);

    /**
     * 添加广告账号表
     *
     * @param accountAd 广告账号表
     * @return int
     */
    int add(AccountAd accountAd);

    /**
     * 删除广告账号表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改广告账号表
     *
     * @param accountAd 广告账号表
     * @return int
     */
    int updateData(AccountAd accountAd);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountAd
     */
    AccountAd findById(Long id);
}
