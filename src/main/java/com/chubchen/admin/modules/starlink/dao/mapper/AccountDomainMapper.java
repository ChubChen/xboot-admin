package com.chubchen.admin.modules.starlink.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountDomainMapper;
import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    * 域名表 服务类
    * </p>
*
* @author chubchen
* @since 2020-08-12
*/
@DS("config")
public interface AccountDomainMapper extends BaseMapper<AccountDomain> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountDomain
     * @return IPage<AccountDomain>
    */
    IPage<AccountDomain> selectListByPage(IPage<AccountDomain> page, @Param("accountDomain") AccountDomain accountDomain);

    /**
     * 根据条件查询全部
     * @param accountDomain
     * @return List<AccountDomain>
    */
    List<AccountDomain> selectAll(@Param("accountDomain") AccountDomain accountDomain);

}
