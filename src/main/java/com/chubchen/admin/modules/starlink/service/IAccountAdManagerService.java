package com.chubchen.admin.modules.starlink.service;

import com.chubchen.admin.modules.starlink.entity.AccountAdManager;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 * BM，MCC 或 BC表 服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
public interface IAccountAdManagerService extends IService<AccountAdManager> {

    /**
     * 查询BM，MCC 或 BC表分页数据
     *
     * @param pageVo      页码
     * @param accountAdManager 过滤条件
     * @return IPage<AccountAdManager>
     */
    IPage<AccountAdManager> findListByPage(PageVo pageVo, AccountAdManager accountAdManager);

   /**
    * 查询BM，MCC 或 BC表分页数据
    *
    * @return  List<AccountAdManager>
    */
    List<AccountAdManager> findListAll(AccountAdManager accountAdManager);

    /**
     * 添加BM，MCC 或 BC表
     *
     * @param accountAdManager BM，MCC 或 BC表
     * @return int
     */
    int add(AccountAdManager accountAdManager);

    /**
     * 删除BM，MCC 或 BC表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改BM，MCC 或 BC表
     *
     * @param accountAdManager BM，MCC 或 BC表
     * @return int
     */
    int updateData(AccountAdManager accountAdManager);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountAdManager
     */
    AccountAdManager findById(Long id);
}
