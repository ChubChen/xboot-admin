package com.chubchen.admin.modules.starlink.controller;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.starlink.entity.AdApplyRecord;
import com.chubchen.admin.modules.starlink.service.IAdApplyRecordService;
import com.chubchen.admin.modules.starlink.vo.AppliedAd;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.starlink.service.IAccountAdService;
import com.chubchen.admin.modules.starlink.entity.AccountAd;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 广告账号表 前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Slf4j
@Api(tags = {"广告账号表"})
@RestController
@RequestMapping("/xboot/starlink/account-ad")
public class AccountAdController {

    @Resource
    private IAccountAdService accountAdService;

    @Resource
    private IAdApplyRecordService adApplyRecordService;


    @ApiOperation(value = "新增广告账号表")
    @PostMapping()
    @SystemLog(description = "添加广告账号", type = LogType.OPERATION)
    public Result<Integer> add(@ModelAttribute AccountAd accountAd){
        Result result = validateParam(accountAd);
        if (result.isSuccess()) {
            return addInner(accountAd);
        }else{
            return result;
        }
    }

    private Result<Integer> addInner(AccountAd accountAd){
        int count = accountAdService.count(new QueryWrapper<AccountAd>()
                .eq("account_name", accountAd.getAccountName())
                .or().eq("account_id", accountAd.getAccountId()));
        if(count <= 0){
            return new ResultUtil<Integer>().setData(accountAdService.add(accountAd));
        }else{
            return new ResultUtil<Integer>().setErrorMsg("存在重复账号,不能重复添加");
        }
    }

    private Result validateParam(AccountAd accountAd){
        if(StringUtils.isBlank(accountAd.getAccountId())){
            return new ResultUtil<Integer>().setErrorMsg("账户ID不能为空");
        }
        if(StringUtils.isBlank(accountAd.getAccountName())){
            return new ResultUtil<Integer>().setErrorMsg("账户名称不能为空");
        }

        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除广告账号表")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountAdService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新广告账号表")
    @PutMapping()
    @SystemLog(description = "更新广告账号", type = LogType.OPERATION)
    public Result<Integer> update(@ModelAttribute AccountAd accountAd){
        Result result = validateParam(accountAd);
        if(result.isSuccess()){
            Integer count = accountAdService.updateData(accountAd);
            if(count > 0){
                return new ResultUtil<Integer>().setData(count);
            }else{
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    @ApiOperation(value = "查询广告账号表分页数据")
    @GetMapping()
    public Result<IPage<AccountAd>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountAd accountAd){
            IPage<AccountAd> page = accountAdService.findListByPage(pageVo, accountAd);
         return new ResultUtil<IPage<AccountAd>>().setData(page);
    }

    @ApiOperation(value = "id查询广告账号表")
    @GetMapping("{id}")
    public Result<AccountAd> findById(@PathVariable Long id){
        AccountAd accountAd=  accountAdService.findById(id);
        return new ResultUtil<AccountAd>().setData(accountAd);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountAd>> findById(@ModelAttribute AccountAd accountAd){
        List<AccountAd> accountAdList =  accountAdService.findListAll(accountAd);
        return new ResultUtil<List<AccountAd>>().setData(accountAdList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AccountAd.AccountStatusEnum.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "批量修改广告状态", type = LogType.OPERATION)
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!AccountAd.AccountStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = accountAdService.update(new UpdateWrapper<AccountAd>()
                .set("account_status", status)
                .set("updated_at", new Date())
                .in("id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }

    @ApiOperation(value = "新增广告账号表")
    @PostMapping("/add")
    @SystemLog(description = "申请成功，存入广告表", type = LogType.OPERATION)
    @DS("config")
    @Transactional(rollbackFor = Exception.class)
    public Result<Integer> add(@RequestBody AppliedAd appliedAd){
        int count = 0;
        if(appliedAd.getAdAccountList() != null && appliedAd.getAdAccountList().size() > 0){
            for(AccountAd accountAd : appliedAd.getAdAccountList()){
                Result validate = validateParam(accountAd);
                if(validate.isSuccess()){
                    accountAd.setOpenedAt(new Date());
                    accountAd.setCreatedAt(new Date());
                    accountAd.setUpdatedAt(new Date());
                    Result<Integer> result = addInner(accountAd);
                    if(!result.isSuccess()){
                        throw new RuntimeException("添加广告账号失败,"+result.getMessage());
                    }else{
                        count++;
                    }
                }
            }
            //更新状态到申请成功
            adApplyRecordService.update(new UpdateWrapper<AdApplyRecord>()
                    .set("status", AdApplyRecord.AppliedStatus.FINISH.getCode()).eq("id",appliedAd.getId()));
        }
        return new ResultUtil<Integer>().setData(count);
    }
}
