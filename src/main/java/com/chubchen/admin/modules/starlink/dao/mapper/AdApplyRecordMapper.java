package com.chubchen.admin.modules.starlink.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.starlink.dao.mapper.AdApplyRecordMapper;
import com.chubchen.admin.modules.starlink.entity.AdApplyRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-08-19
*/
@DS("config")
public interface AdApplyRecordMapper extends BaseMapper<AdApplyRecord> {

    /**
     * 根据条件分页查询
     * @param page
     * @param adApplyRecord
     * @return IPage<AdApplyRecord>
    */
    IPage<AdApplyRecord> selectListByPage(IPage<AdApplyRecord> page, @Param("adApplyRecord") AdApplyRecord adApplyRecord);

    /**
     * 根据条件查询全部
     * @param adApplyRecord
     * @return List<AdApplyRecord>
    */
    List<AdApplyRecord> selectAll(@Param("adApplyRecord") AdApplyRecord adApplyRecord);

}
