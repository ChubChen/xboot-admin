package com.chubchen.admin.modules.starlink.service;

import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 * 域名表 服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
public interface IAccountDomainService extends IService<AccountDomain> {

    /**
     * 查询域名表分页数据
     *
     * @param pageVo      页码
     * @param accountDomain 过滤条件
     * @return IPage<AccountDomain>
     */
    IPage<AccountDomain> findListByPage(PageVo pageVo, AccountDomain accountDomain);

   /**
    * 查询域名表分页数据
    *
    * @return  List<AccountDomain>
    */
    List<AccountDomain> findListAll(AccountDomain accountDomain);

    /**
     * 添加域名表
     *
     * @param accountDomain 域名表
     * @return int
     */
    int add(AccountDomain accountDomain);

    /**
     * 删除域名表
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改域名表
     *
     * @param accountDomain 域名表
     * @return int
     */
    int updateData(AccountDomain accountDomain);

    /**
     * id查询数据
     *
     * @param id id
     * @return AccountDomain
     */
    AccountDomain findById(Long id);
}
