package com.chubchen.admin.modules.starlink.service.impl;

import com.chubchen.admin.modules.starlink.entity.AdApplyRecord;
import com.chubchen.admin.modules.starlink.dao.mapper.AdApplyRecordMapper;
import com.chubchen.admin.modules.starlink.service.IAdApplyRecordService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-19
 */
@Service
public class AdApplyRecordServiceImpl extends ServiceImpl<AdApplyRecordMapper, AdApplyRecord> implements IAdApplyRecordService {

    @Override
    public  IPage<AdApplyRecord> findListByPage(PageVo pageVo, AdApplyRecord adApplyRecord){
        IPage<AdApplyRecord> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, adApplyRecord);
    }

    @Override
    public List<AdApplyRecord> findListAll(AdApplyRecord adApplyRecord){
        return baseMapper.selectAll(adApplyRecord);
    }

    @Override
    public int add(AdApplyRecord adApplyRecord){
        return baseMapper.insert(adApplyRecord);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AdApplyRecord adApplyRecord){
        return baseMapper.updateById(adApplyRecord);
    }

    @Override
    public AdApplyRecord findById(Long id){
        return  baseMapper.selectById(id);
    }
}
