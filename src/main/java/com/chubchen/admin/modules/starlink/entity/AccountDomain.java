package com.chubchen.admin.modules.starlink.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 域名表
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountDomain对象", description="域名表")
public class AccountDomain extends Model<AccountDomain> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "域名（无http://）")
    private String domainUrl;

    @ApiModelProperty(value = "域名状态")
    private Integer domainStatus;

    @ApiModelProperty(value = "是否在 shopify 被禁用")
    private Integer shopifyDisabled;

    @ApiModelProperty(value = "绑定平台店铺 ID")
    private Integer backendId;

    @ApiModelProperty(value = "过期日期")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date expiredAt;

    @ApiModelProperty(value = "GoDaddy 账号名")
    private String godaddyAccount;

    @ApiModelProperty(value = "站点 logo 地址")
    private String logoUrl;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    @Getter
    public enum DomainStatusEnum {
        DISABLED(-1, "已过期"),
        BACKUP(0, "备用"),
        USED(1, "使用中");

        private int code;

        private String desc;

        DomainStatusEnum(int code, String desc){
            this.code = code;
            this.desc = desc;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(DomainStatusEnum.values()).
                    collect(Collectors.toMap(DomainStatusEnum::getCode, DomainStatusEnum::getDesc));
        }
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
