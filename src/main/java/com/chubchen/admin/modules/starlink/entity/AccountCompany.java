package com.chubchen.admin.modules.starlink.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 公司主体表
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountCompany对象", description="公司主体表")
public class AccountCompany extends Model<AccountCompany> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "公司主体中文名")
    private String nameZh;

    @ApiModelProperty(value = "公司主体英文名")
    private String nameEn;

    @ApiModelProperty(value = "统一社会信用代码")
    private String companyUsci;

    @ApiModelProperty(value = "中文地址")
    private String addressZh;

    @ApiModelProperty(value = "英文地址")
    private String addressEn;

    @ApiModelProperty(value = "主体状态")
    private Integer companyStatus;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    @Getter
    public enum CompanyStatus{

        DISABLE( -1, "被标记"),
        OPEN(1, "可用");

        private int code;

        private String desc;

        CompanyStatus(int code, String desc){
            this.code = code;
            this.desc = desc;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(CompanyStatus.values()).
                    collect(Collectors.toMap(CompanyStatus::getCode, CompanyStatus::getDesc));
        }

    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
