package com.chubchen.admin.modules.starlink.service;

import com.chubchen.admin.modules.starlink.entity.AdApplyRecord;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-19
 */
public interface IAdApplyRecordService extends IService<AdApplyRecord> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param adApplyRecord 过滤条件
     * @return IPage<AdApplyRecord>
     */
    IPage<AdApplyRecord> findListByPage(PageVo pageVo, AdApplyRecord adApplyRecord);

   /**
    * 查询分页数据
    *
    * @return  List<AdApplyRecord>
    */
    List<AdApplyRecord> findListAll(AdApplyRecord adApplyRecord);

    /**
     * 添加
     *
     * @param adApplyRecord 
     * @return int
     */
    int add(AdApplyRecord adApplyRecord);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param adApplyRecord 
     * @return int
     */
    int updateData(AdApplyRecord adApplyRecord);

    /**
     * id查询数据
     *
     * @param id id
     * @return AdApplyRecord
     */
    AdApplyRecord findById(Long id);
}
