package com.chubchen.admin.modules.starlink.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import java.time.LocalDate;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-08-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AdApplyRecord对象", description="")
public class AdApplyRecord extends Model<AdApplyRecord> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "申请的域名Id")
    private Integer domainId;

    @ApiModelProperty(value = "公司主体ID")
    private Integer companyId;

    @ApiModelProperty(value = "申请广告数量")
    private Integer adNum;

    @ApiModelProperty(value = "状态")
    private Integer status;

    @ApiModelProperty(value = "申请人")
    private String applyUser;

    @ApiModelProperty(value = "一级代理")
    private String agency1;

    @ApiModelProperty(value = "二级代理")
    private String agency2;

    @ApiModelProperty(value = "申请时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date appliedAt;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdTime;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedTime;

    @ApiModelProperty(value = "备注信息")
    private String note;

    @ApiModelProperty(value = "平台")
    private String platform;

    @ApiModelProperty(value = "时区")
    private String timeZone;

    @TableField(exist = false)
    private AccountDomain domain;

    @TableField(exist = false)
    private AccountCompany company;


    @Getter
    public enum AppliedStatus{

        ERROR(-1, "申请失败"),
        APPLYING(0, "申请中"),
        FINISH(1, "完成申请");

        private int code;

        private String desc;

        AppliedStatus(int code, String desc){
            this.code = code;
            this.desc = desc;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(AppliedStatus.values()).
                    collect(Collectors.toMap(AppliedStatus::getCode, AppliedStatus::getDesc));
        }
    }
    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
