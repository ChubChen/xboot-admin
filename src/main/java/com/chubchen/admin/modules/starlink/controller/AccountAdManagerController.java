package com.chubchen.admin.modules.starlink.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.easesea.constants.AdAccountStatusEnum;
import com.chubchen.admin.modules.starlink.entity.AccountAd;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.starlink.service.IAccountAdManagerService;
import com.chubchen.admin.modules.starlink.entity.AccountAdManager;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * BM，MCC 或 BC表 前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Api(tags = {"BM，MCC 或 BC表"})
@RestController
@RequestMapping("/xboot/starlink/account-ad-manager")
public class AccountAdManagerController {

    @Resource
    private IAccountAdManagerService accountAdManagerService;


    @ApiOperation(value = "新增BM，MCC 或 BC表")
    @PostMapping()
    @SystemLog(description = "添加BM账号", type = LogType.OPERATION)
    public Result<Integer> add(@ModelAttribute AccountAdManager accountAdManager){
        Result result = validateParam(accountAdManager);
        if (result.isSuccess()) {
            AccountAdManager exists = accountAdManagerService.getOne(new QueryWrapper<AccountAdManager>().
                    eq("ads_manager_id",accountAdManager.getAdsManagerId()).
                    eq("platform", accountAdManager.getPlatform()));
            if(exists != null && StringUtils.isNotEmpty(exists.getAdsManagerId())){
                return new ResultUtil<Integer>().setErrorMsg("已经存在不能重复添加");
            }
            accountAdManager.setManagerStatus(AccountAdManager.ManagerStatusEnum.OPEN.getCode());
            accountAdManager.setCreatedAt(new Date());
            accountAdManager.setUpdateAt(new Date());
            Integer count = accountAdManagerService.add(accountAdManager);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else{
            return result;
        }
    }

    private Result validateParam(AccountAdManager accountAdManager){
        if(StringUtils.isEmpty(accountAdManager.getPlatform())){
            return new ResultUtil<Integer>().setErrorMsg("广告平台不能为空");
        }
        if(StringUtils.isEmpty(accountAdManager.getAdsManagerId())){
            return new ResultUtil<Integer>().setErrorMsg("账户ID不能为空");
        }

        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除BM，MCC 或 BC表")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountAdManagerService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新BM，MCC 或 BC表")
    @PutMapping()
    @SystemLog(description = "更新BM账号", type = LogType.OPERATION)
    public Result<Integer> update(@ModelAttribute AccountAdManager accountAdManager){
        Result result = validateParam(accountAdManager);
        if (result.isSuccess()) {
            Integer count = accountAdManagerService.updateData(accountAdManager);
            if (count > 0) {
                return new ResultUtil<Integer>().setData(count);
            } else {
                return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
            }
        }else {
            return result;
        }
    }

    @ApiOperation(value = "查询BM，MCC 或 BC表分页数据")
    @GetMapping()
    public Result<IPage<AccountAdManager>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountAdManager accountAdManager){
            IPage<AccountAdManager> page = accountAdManagerService.findListByPage(pageVo, accountAdManager);
         return new ResultUtil<IPage<AccountAdManager>>().setData(page);
    }

    @ApiOperation(value = "id查询BM，MCC 或 BC表")
    @GetMapping("{id}")
    public Result<AccountAdManager> findById(@PathVariable Long id){
        AccountAdManager accountAdManager=  accountAdManagerService.findById(id);
        return new ResultUtil<AccountAdManager>().setData(accountAdManager);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountAdManager>> findById(@ModelAttribute AccountAdManager accountAdManager){
        List<AccountAdManager> accountAdManagerList =  accountAdManagerService.findListAll(accountAdManager);
        return new ResultUtil<List<AccountAdManager>>().setData(accountAdManagerList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AccountAdManager.ManagerStatusEnum.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "修改BM账号状态", type = LogType.OPERATION)
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!AccountAdManager.ManagerStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = accountAdManagerService.update(new UpdateWrapper<AccountAdManager>()
                .set("manager_status", status)
                .set("updated_at", new Date())
                .in("id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }

}
