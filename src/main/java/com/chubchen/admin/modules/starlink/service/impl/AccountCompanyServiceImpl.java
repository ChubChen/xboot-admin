package com.chubchen.admin.modules.starlink.service.impl;

import com.chubchen.admin.modules.starlink.entity.AccountCompany;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountCompanyMapper;
import com.chubchen.admin.modules.starlink.service.IAccountCompanyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 * 公司主体表 服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Service
public class AccountCompanyServiceImpl extends ServiceImpl<AccountCompanyMapper, AccountCompany> implements IAccountCompanyService {

    @Override
    public  IPage<AccountCompany> findListByPage(PageVo pageVo, AccountCompany accountCompany){
        IPage<AccountCompany> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountCompany);
    }

    @Override
    public List<AccountCompany> findListAll(AccountCompany accountCompany){
        return baseMapper.selectAll(accountCompany);
    }

    @Override
    public int add(AccountCompany accountCompany){
        return baseMapper.insert(accountCompany);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountCompany accountCompany){
        return baseMapper.updateById(accountCompany);
    }

    @Override
    public AccountCompany findById(Long id){
        return  baseMapper.selectById(id);
    }
}
