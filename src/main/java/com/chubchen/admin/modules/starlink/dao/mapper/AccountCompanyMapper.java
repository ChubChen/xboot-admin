package com.chubchen.admin.modules.starlink.dao.mapper;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountCompanyMapper;
import com.chubchen.admin.modules.starlink.entity.AccountCompany;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    * 公司主体表 服务类
    * </p>
*
* @author chubchen
* @since 2020-08-12
*/
@DS("config")
public interface AccountCompanyMapper extends BaseMapper<AccountCompany> {

    /**
     * 根据条件分页查询
     * @param page
     * @param accountCompany
     * @return IPage<AccountCompany>
    */
    IPage<AccountCompany> selectListByPage(IPage<AccountCompany> page, @Param("accountCompany") AccountCompany accountCompany);

    /**
     * 根据条件查询全部
     * @param accountCompany
     * @return List<AccountCompany>
    */
    List<AccountCompany> selectAll(@Param("accountCompany") AccountCompany accountCompany);

}
