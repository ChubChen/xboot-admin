package com.chubchen.admin.modules.starlink.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * BM，MCC 或 BC表
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountAdManager对象", description="BM，MCC 或 BC表")
public class AccountAdManager extends Model<AccountAdManager> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String adsManagerId;

    private String adsManagerName;

    private Integer managerStatus;

    @ApiModelProperty(value = "广告平台")
    private String platform;

    @ApiModelProperty(value = "FB BM 访问 token")
    private String fbBmToken;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateAt;

    private String type;

    @Getter
    public enum ManagerStatusEnum{
        DISABLE(-1, "被封"),
        NEW(0, "新建"),
        OPEN(1, "使用中");

        private int code;

        private String desc;

        ManagerStatusEnum(int code, String desc){
            this.code = code;
            this.desc = desc;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(ManagerStatusEnum.values()).
                    collect(Collectors.toMap(ManagerStatusEnum::getCode, ManagerStatusEnum::getDesc));
        }
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
