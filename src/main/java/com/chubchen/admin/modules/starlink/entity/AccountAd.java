package com.chubchen.admin.modules.starlink.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.util.Arrays;
import java.util.Date;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.util.Map;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * <p>
 * 广告账号表
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="AccountAd对象", description="广告账号表")
public class AccountAd extends Model<AccountAd> {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "广告账号的平台 ID")
    private String accountId;

    @ApiModelProperty(value = "广告账号名称")
    private String accountName;

    @ApiModelProperty(value = "广告账号状态")
    private Integer accountStatus;

    @ApiModelProperty(value = "域名 ID：account_domain.id")
    private Integer domainId;

    @ApiModelProperty(value = "FB 主页 ID")
    private String fbPageId;

    @ApiModelProperty(value = "开户主体 ID：account_company.id")
    private Integer companyId;

    @ApiModelProperty(value = "时区")
    private String timeZone;

    @ApiModelProperty(value = "一级代理")
    private String agency1;

    @ApiModelProperty(value = "二级代理")
    private String agency2;

    @ApiModelProperty(value = "广告管理平台 ID：account_ad_manager.id")
    private Integer managerId;

    @ApiModelProperty(value = "备注")
    private String note;

    @ApiModelProperty(value = "申请开户时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date appliedAt;

    @ApiModelProperty(value = "账号开通时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date openedAt;

    @ApiModelProperty(value = "创建时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createdAt;

    @ApiModelProperty(value = "更新时间")
    @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedAt;

    @TableField(exist = false)
    private AccountDomain domain;

    @TableField(exist = false)
    private AccountCompany company;

    @TableField(exist = false)
    private AccountAdManager manager;

    @Getter
    public enum AccountStatusEnum {

        BM_DISABLE(-3, "BM被封"),
        PAGE_SCORE(-2, "主页评分过低"),
        DISABLE(-1, "被封"),
        NEW(0, "新建"),
        OPEN(1, "使用中"),
        PAUSED(2, "暂停中");

        private int code;

        private String desc;

        AccountStatusEnum(int code, String desc){
            this.code = code;
            this.desc = desc;
        }

        public static Map<Integer, String> getMap(){
            return Arrays.stream(AccountStatusEnum.values()).
                    collect(Collectors.toMap(AccountStatusEnum::getCode,AccountStatusEnum::getDesc));
        }
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
