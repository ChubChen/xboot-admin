package com.chubchen.admin.modules.starlink.service.impl;

import com.chubchen.admin.modules.starlink.entity.AccountAdManager;
import com.chubchen.admin.modules.starlink.dao.mapper.AccountAdManagerMapper;
import com.chubchen.admin.modules.starlink.service.IAccountAdManagerService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 * BM，MCC 或 BC表 服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Service
public class AccountAdManagerServiceImpl extends ServiceImpl<AccountAdManagerMapper, AccountAdManager> implements IAccountAdManagerService {

    @Override
    public  IPage<AccountAdManager> findListByPage(PageVo pageVo, AccountAdManager accountAdManager){
        IPage<AccountAdManager> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, accountAdManager);
    }

    @Override
    public List<AccountAdManager> findListAll(AccountAdManager accountAdManager){
        return baseMapper.selectAll(accountAdManager);
    }

    @Override
    public int add(AccountAdManager accountAdManager){
        return baseMapper.insert(accountAdManager);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(AccountAdManager accountAdManager){
        return baseMapper.updateById(accountAdManager);
    }

    @Override
    public AccountAdManager findById(Long id){
        return  baseMapper.selectById(id);
    }
}
