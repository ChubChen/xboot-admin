package com.chubchen.admin.modules.starlink.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.annotation.SystemLog;
import com.chubchen.admin.common.enums.LogType;
import com.chubchen.admin.modules.starlink.entity.AccountAd;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;
import com.chubchen.admin.modules.starlink.service.IAccountDomainService;
import com.chubchen.admin.modules.starlink.entity.AccountDomain;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.Result;

import java.util.Date;
import java.util.List;
import java.util.Map;


import javax.annotation.Resource;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 域名表 前端控制器
 * </p>
 *
 * @author chubchen
 * @since 2020-08-12
 */
@Api(tags = {"域名表"})
@RestController
@RequestMapping("/xboot/starlink/account-domain")
public class AccountDomainController {

    @Resource
    private IAccountDomainService accountDomainService;


    @ApiOperation(value = "新增域名表")
    @PostMapping()
    @SystemLog(description = "添加域名", type = LogType.OPERATION)
    public Result<Integer> add(@ModelAttribute AccountDomain accountDomain){
        Result result = validateParam(accountDomain);
        if (result.isSuccess()) {
            AccountDomain exists = accountDomainService.getOne(new QueryWrapper<AccountDomain>()
                    .eq("domain_url", accountDomain.getDomainUrl()));
            if(exists != null && StringUtils.isNotEmpty(exists.getDomainUrl())){
                return new ResultUtil<Integer>().setErrorMsg("该域名已经存在,不能重复添加");
            }else{
                Integer count = accountDomainService.add(accountDomain);
                if (count > 0) {
                    return new ResultUtil<Integer>().setData(count);
                } else {
                    return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
                }
            }
        }else{
            return result;
        }
    }

    private Result validateParam(AccountDomain accountDomain){
        if(StringUtils.isBlank(accountDomain.getDomainUrl())){
            return new ResultUtil<Integer>().setErrorMsg("域名不能为空");
        }
        return new ResultUtil<Integer>().setSuccessMsg("校验通过");
    }

    @ApiOperation(value = "删除域名表")
    @DeleteMapping("{id}")
    public Result<Integer> delete(@PathVariable("id") Long id){
        Integer count = accountDomainService.delete(id);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "更新域名表")
    @PutMapping()
    @SystemLog(description = "更新域名", type = LogType.OPERATION)
    public Result<Integer> update(@ModelAttribute AccountDomain accountDomain){
        accountDomain.setUpdatedAt(new Date());
        Integer count = accountDomainService.updateData(accountDomain);
        if(count > 0){
            return new ResultUtil<Integer>().setData(count);
        }else{
            return new ResultUtil<Integer>().setErrorMsg("更新成功条数为0");
        }
    }

    @ApiOperation(value = "查询域名表分页数据")
    @GetMapping()
    public Result<IPage<AccountDomain>> findListByPage(@ModelAttribute PageVo pageVo,
                                            @ModelAttribute AccountDomain accountDomain){
            IPage<AccountDomain> page = accountDomainService.findListByPage(pageVo, accountDomain);
         return new ResultUtil<IPage<AccountDomain>>().setData(page);
    }

    @ApiOperation(value = "id查询域名表")
    @GetMapping("{id}")
    public Result<AccountDomain> findById(@PathVariable Long id){
        AccountDomain accountDomain=  accountDomainService.findById(id);
        return new ResultUtil<AccountDomain>().setData(accountDomain);
    }


    @ApiOperation(value = "查询全量数据")
    @GetMapping("/all")
    public Result<List<AccountDomain>> findById(@ModelAttribute AccountDomain accountDomain){
        List<AccountDomain> accountDomainList =  accountDomainService.findListAll(accountDomain);
        return new ResultUtil<List<AccountDomain>>().setData(accountDomainList);
    }

    @RequestMapping(value = "/getEnum",method = RequestMethod.GET)
    @ApiOperation(value = "获取状态枚举")
    public Result<Object> getAccountEnum(){
        Map<Integer, String> map = AccountDomain.DomainStatusEnum.getMap();
        return new ResultUtil<>().setData(map);
    }

    @RequestMapping(value = "/updateStatus",method = RequestMethod.PUT)
    @SystemLog(description = "批量修改广告状态", type = LogType.OPERATION)
    public Result<Boolean> enable(@RequestParam List<Integer> ids, @RequestParam Integer status){
        if(!AccountDomain.DomainStatusEnum.getMap().containsKey(status)){
            return new ResultUtil<Boolean>().setErrorMsg("不存在的状态定义");
        }
        Boolean count = accountDomainService.update(new UpdateWrapper<AccountDomain>()
                .set("domain_status", status)
                .set("updated_at", new Date())
                .in("id", ids));
        if(count){
            return new ResultUtil<Boolean>().setData(count);
        }else{
            return new ResultUtil<Boolean>().setErrorMsg("更新失败");

        }
    }
}
