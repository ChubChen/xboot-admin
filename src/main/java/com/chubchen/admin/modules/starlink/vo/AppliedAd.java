package com.chubchen.admin.modules.starlink.vo;

import com.chubchen.admin.modules.starlink.entity.AccountAd;
import lombok.Data;

import java.util.List;

/**
 * @author chubchen
 */
@Data
public class AppliedAd {

    private List<AccountAd> adAccountList;

    private Integer id;

}
