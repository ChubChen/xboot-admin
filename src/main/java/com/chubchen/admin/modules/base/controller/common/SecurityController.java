package com.chubchen.admin.modules.base.controller.common;

import com.alibaba.fastjson.JSON;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.vo.Result;
import cn.hutool.http.HttpUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author chubchen
 */
@Slf4j
@RestController
@Api(description = "Security相关接口")
@RequestMapping("/xboot/common")
@Transactional
public class SecurityController {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @RequestMapping(value = "/needLogin", method = RequestMethod.GET)
    @ApiOperation(value = "没有登录")
    public Result<Object> needLogin(){

        return new ResultUtil<Object>().setErrorMsg(401, "您还未登录");
    }



    @RequestMapping(value = "/swagger/login", method = RequestMethod.GET)
    @ApiOperation(value = "Swagger接口文档专用登录接口 方便测试")
    public Result<Object> swaggerLogin(@RequestParam String username, @RequestParam String password,
                                       @ApiParam("图片验证码ID") @RequestParam(required = false) String captchaId,
                                       @ApiParam("验证码") @RequestParam(required = false) String code,
                                       @ApiParam("记住密码") @RequestParam(required = false, defaultValue = "true") Boolean saveLogin,
                                       @ApiParam("可自定义登录接口地址")
                                       @RequestParam(required = false, defaultValue = "http://127.0.0.1:8888/xboot/login")
                                               String loginUrl){

        Map<String, Object> params = new HashMap<>(16);
        params.put("username", username);
        params.put("password", password);
        params.put("captchaId", captchaId);
        params.put("code", code);
        params.put("saveLogin", saveLogin);
        String result = HttpUtil.post(loginUrl, params);
        return new ResultUtil<Object>().setData(result);
    }

    @RequestMapping(value = "/login/backimage", method = RequestMethod.GET)
    @ApiOperation(value = "获取登录背景图片")
    public Result<String> swaggerLogin(){
        String url = "https://cn.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1";
        try{
            ValueOperations<String, String> stringOperations = redisTemplate.opsForValue();
            String value = stringOperations.get("loginBackImage");
            if(StringUtils.isNotBlank(value)){
                return new ResultUtil<String>().setData(value);
            }else{
                RestTemplate restTemplate = new RestTemplate();
                String result = restTemplate.getForObject(url, String.class);
                String resultUrl = JSON.parseObject(result).getJSONArray("images").getJSONObject(0).getString("url");
                if(StringUtils.isNotBlank(resultUrl)){
                    stringOperations.set("loginBackImage", resultUrl, 8, TimeUnit.HOURS);
                }
                return new ResultUtil<String>().setData(resultUrl);
            }
        }catch (Exception e){
            return new ResultUtil<String>().setErrorMsg("请求解析失败");
        }
    }
}
