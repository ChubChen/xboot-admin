package com.chubchen.admin.modules.base.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.base.entity.QuartzJob;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chubchen
 * @since 2019-11-19
 */
public interface QuartzJobMapper extends BaseMapper<QuartzJob> {

}
