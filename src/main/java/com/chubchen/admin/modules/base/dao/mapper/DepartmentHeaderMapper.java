package com.chubchen.admin.modules.base.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.base.entity.DepartmentHeader;

/**
 * @author chubchen
 */
public interface DepartmentHeaderMapper extends BaseMapper<DepartmentHeader> {

}