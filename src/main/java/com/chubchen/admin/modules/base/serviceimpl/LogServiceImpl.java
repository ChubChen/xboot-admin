package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.base.dao.mapper.LogMapper;
import com.chubchen.admin.modules.base.entity.Log;
import com.chubchen.admin.modules.base.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 日志接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class LogServiceImpl extends ServiceImpl<LogMapper, Log> implements LogService {

    @Autowired
    private LogMapper logMapper;

    @Override
    public IPage<Log> findByConfition(Integer type, String key, SearchVo searchVo, PageVo pageVo) {
        IPage<Log> page = new Page<>(pageVo.getPageNumber(), pageVo.getPageSize());
        page = logMapper.selectPage(page, new QueryWrapper<Log>()
                .eq(type != null, "log_type", type)
                .between(StringUtils.isNotBlank(searchVo.getStartDate()) && StringUtils.isNotBlank(searchVo.getEndDate()),
                        "create_time",searchVo.getStartDate(), searchVo.getEndDate())
                .and(StringUtils.isNotBlank(key), i->i.like("name", key)
                        .or().like("request_url", key)
                        .or().like("request_type", key)
                        .or().like("request_param", key)
                        .or().like("username", key)
                        .or().like("ip", key)
                        .or().like("ip_info", key)
                        .or().like("log_type", key))
        );
        return page;
    }

    @Override
    public void deleteAll() {
        logMapper.delete(new QueryWrapper<>());
    }
}
