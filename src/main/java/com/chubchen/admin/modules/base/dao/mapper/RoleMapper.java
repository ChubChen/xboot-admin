package com.chubchen.admin.modules.base.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.base.entity.Role;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chubchen
 * @since 2019-11-19
 */
public interface RoleMapper extends BaseMapper<Role> {

}
