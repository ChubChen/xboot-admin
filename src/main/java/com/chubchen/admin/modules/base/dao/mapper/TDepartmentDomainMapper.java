package com.chubchen.admin.modules.base.dao.mapper;

import com.chubchen.admin.modules.base.dao.mapper.TDepartmentDomainMapper;
import com.chubchen.admin.modules.base.entity.TDepartmentDomain;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import com.baomidou.mybatisplus.core.metadata.IPage;
import java.util.List;


/**
* <p>
    *  服务类
    * </p>
*
* @author chubchen
* @since 2020-06-09
*/
public interface TDepartmentDomainMapper extends BaseMapper<TDepartmentDomain> {

    /**
     * 根据条件分页查询
     * @param page
     * @param tDepartmentDomain
     * @return IPage<TDepartmentDomain>
    */
    IPage<TDepartmentDomain> selectListByPage(IPage<TDepartmentDomain> page, @Param("tDepartmentDomain") TDepartmentDomain tDepartmentDomain);

    /**
     * 根据条件查询全部
     * @param tDepartmentDomain
     * @return List<TDepartmentDomain>
    */
    List<TDepartmentDomain> selectAll(@Param("tDepartmentDomain") TDepartmentDomain tDepartmentDomain);

}
