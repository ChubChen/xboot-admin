package com.chubchen.admin.modules.base.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.base.entity.User;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import java.util.List;

/**
 * 用户接口
 * @author chubchen
 */
@CacheConfig(cacheNames = "user")
public interface UserService extends IService<User> {

    /**
     * 通过用户名获取用户
     * @param username
     * @return
     */
    @Cacheable(key = "#username")
    User findByUsername(String username);

    /**
     * 通过手机获取用户
     * @param mobile
     * @return
     */
    User findByMobile(String mobile);

    /**
     * 通过邮件和状态获取用户
     * @param email
     * @return
     */
    User findByEmail(String email);

    /**
     * 多条件分页获取用户
     * @param user
     * @param searchVo
     * @param pageVo
     * @return
     */
    IPage<User> findByCondition(User user, SearchVo searchVo, PageVo pageVo);

    /**
     * 通过部门id获取
     * @param departmentId
     * @return
     */
    List<User> findByDepartmentId(String departmentId);

    /**
     * 通过部门id获取User列表有缓存
     * @param departmentId
     * @return
     */
    @Cacheable(key =  "'userListDepId:' + #departmentId")
    List<User> findByDepartmentIdByRedis(String departmentId);
}
