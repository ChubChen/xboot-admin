package com.chubchen.admin.modules.base.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.base.entity.Department;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chubchen
 * @since 2019-11-19
 */
public interface DepartmentMapper extends BaseMapper<Department> {


    /**
     * 根据parentId查询所有孩子部门
     * @param parentIds
     * @return
     */
    List<String> selectByParentIds(@Param("parentList") List<String> parentIds);
}
