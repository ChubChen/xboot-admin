package com.chubchen.admin.modules.base.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.chubchen.admin.base.AdminBaseEntity;
import com.chubchen.admin.common.constant.CommonConstant;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.math.BigDecimal;

/**
 * @author chubchen
 */
@Data
@TableName("t_dict_data")
@ApiModel(value = "字典数据")
public class DictData extends AdminBaseEntity<DictData> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "数据名称")
    private String title;

    @ApiModelProperty(value = "数据值")
    private String value;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

    @ApiModelProperty(value = "是否启用 0启用 -1禁用")
    private Integer status = CommonConstant.STATUS_NORMAL;

    @ApiModelProperty(value = "备注")
    private String description;

    @ApiModelProperty(value = "所属字典")
    private String dictId;

    @TableField(exist = false)
    private String dictType;
}