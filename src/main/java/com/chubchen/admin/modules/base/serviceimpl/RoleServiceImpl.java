package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.RoleMapper;
import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.service.RoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public List<Role> findByDefaultRole(Boolean defaultRole) {
        return roleMapper.selectList(new QueryWrapper<Role>()
                .eq("default_role",defaultRole));
    }
}
