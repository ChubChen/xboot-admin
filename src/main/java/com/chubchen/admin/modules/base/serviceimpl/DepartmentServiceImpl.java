package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.modules.base.dao.mapper.DepartmentMapper;
import com.chubchen.admin.modules.base.dao.mapper.TDepartmentDomainMapper;
import com.chubchen.admin.modules.base.entity.Department;
import com.chubchen.admin.modules.base.entity.TDepartmentDomain;
import com.chubchen.admin.modules.base.service.DepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 部门接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements DepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private TDepartmentDomainMapper departmentDomainMapper;

    @Autowired
    private SecurityUtil securityUtil;


    @Override
    public List<Department> findByParentIdOrderBySortOrder(String parentId, Boolean openDataFilter) {

        // 数据权限
        List<String> depIds = securityUtil.getDeparmentIds();
        List<Department> list;
        if(depIds!=null&&depIds.size()>0&&openDataFilter){
            list =  departmentMapper.selectList(new QueryWrapper<Department>()
                    .eq("parent_id", parentId).in("id", depIds).orderByAsc("sort_order"));
        }else{
            list = departmentMapper.selectList(new QueryWrapper<Department>()
                    .eq("parent_id", parentId).orderByAsc("sort_order"));
        }
        Set<String> ids = list.stream().map(item->item.getId()).collect(Collectors.toSet());
        List<TDepartmentDomain> doMains =
                departmentDomainMapper.selectList(new QueryWrapper<TDepartmentDomain>()
                        .eq("department_id", ids));
        Map<String, List<Integer>> map = doMains.stream().collect(Collectors.toMap(TDepartmentDomain::getDepartmentId,
                e -> Arrays.asList(e.getDomainId()),
                (List<Integer> oldList, List<Integer> newList) -> {
                    oldList.addAll(newList);
                    return oldList;
                }));
        list.forEach(item->item.setDomainIds(map.get(item.getId())));
        return list;
    }

    @Override
    public List<Department> findByParentIdAndStatusOrderBySortOrder(String parentId, Integer status) {
        return departmentMapper.selectList(new QueryWrapper<Department>()
                .eq("parent_id", parentId).eq("status",status).orderByAsc("sort_order"));
    }

    @Override
    public List<Department> findByTitleLikeOrderBySortOrder(String title, Boolean openDataFilter) {

        // 数据权限
        List<String> depIds = securityUtil.getDeparmentIds();
        if(depIds!=null&&depIds.size()>0&&openDataFilter){
            return departmentMapper.selectList(new QueryWrapper<Department>()
                    .like("title", title).in("id", depIds).orderByAsc("sort_order"));
        }
        return departmentMapper.selectList(new QueryWrapper<Department>()
                .like("title", title).orderByAsc("sort_order"));
    }

}