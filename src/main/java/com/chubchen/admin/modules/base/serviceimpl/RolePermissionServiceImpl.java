package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.RolePermissionMapper;
import com.chubchen.admin.modules.base.entity.RolePermission;
import com.chubchen.admin.modules.base.service.RolePermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色权限接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

    @Autowired
    private RolePermissionMapper rolePermissionMapper;

    @Override
    public List<RolePermission> findByPermissionId(String permissionId) {

        return rolePermissionMapper.selectList(new QueryWrapper<RolePermission>()
                .eq("permission_id",permissionId));
    }

    @Override
    public List<RolePermission> findByRoleId(String roleId) {

        return rolePermissionMapper.selectList(new QueryWrapper<RolePermission>()
                .eq("role_id",roleId));
    }

    @Override
    public void deleteByRoleId(String roleId) {

        rolePermissionMapper.delete(new QueryWrapper<RolePermission>()
                .eq("role_id",roleId));
    }
}