package com.chubchen.admin.modules.base.service.impl;

import com.chubchen.admin.modules.base.entity.TDepartmentDomain;
import com.chubchen.admin.modules.base.dao.mapper.TDepartmentDomainMapper;
import com.chubchen.admin.modules.base.service.ITDepartmentDomainService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.utils.PageUtil;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-09
 */
@Service
public class TDepartmentDomainServiceImpl extends ServiceImpl<TDepartmentDomainMapper, TDepartmentDomain> implements ITDepartmentDomainService {

    @Override
    public  IPage<TDepartmentDomain> findListByPage(PageVo pageVo, TDepartmentDomain tDepartmentDomain){
        IPage<TDepartmentDomain> page = PageUtil.initMpPage(pageVo);
        return baseMapper.selectListByPage(page, tDepartmentDomain);
    }

    @Override
    public List<TDepartmentDomain> findListAll(TDepartmentDomain tDepartmentDomain){
        return baseMapper.selectAll(tDepartmentDomain);
    }

    @Override
    public int add(TDepartmentDomain tDepartmentDomain){
        return baseMapper.insert(tDepartmentDomain);
    }

    @Override
    public int delete(Long id){
        return baseMapper.deleteById(id);
    }

    @Override
    public int updateData(TDepartmentDomain tDepartmentDomain){
        return baseMapper.updateById(tDepartmentDomain);
    }

    @Override
    public TDepartmentDomain findById(Long id){
        return  baseMapper.selectById(id);
    }
}
