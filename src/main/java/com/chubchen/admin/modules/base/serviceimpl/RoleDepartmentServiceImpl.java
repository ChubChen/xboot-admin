package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.RoleDepartmentMapper;
import com.chubchen.admin.modules.base.entity.RoleDepartment;
import com.chubchen.admin.modules.base.service.RoleDepartmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 角色部门接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class RoleDepartmentServiceImpl extends ServiceImpl<RoleDepartmentMapper, RoleDepartment> implements RoleDepartmentService {

    @Autowired
    private RoleDepartmentMapper roleDepartmentMapper;

    @Override
    public List<RoleDepartment> findByRoleId(String roleId) {

        return roleDepartmentMapper.selectList(new QueryWrapper<RoleDepartment>()
                .eq("role_id", roleId));
    }

    @Override
    public void deleteByRoleId(String roleId) {

        roleDepartmentMapper.delete(new QueryWrapper<RoleDepartment>()
                .eq("role_id", roleId));
    }

    @Override
    public void deleteByDepartmentId(String departmentId) {

        roleDepartmentMapper.delete(new QueryWrapper<RoleDepartment>()
                .eq("department_id", departmentId));
    }
}