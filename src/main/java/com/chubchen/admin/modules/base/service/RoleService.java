package com.chubchen.admin.modules.base.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.Role;

import java.util.List;

/**
 * 角色接口
 * @author chubchen
 */
public interface RoleService extends IService<Role> {

    /**
     * 获取默认角色
     * @param defaultRole
     * @return
     */
    List<Role> findByDefaultRole(Boolean defaultRole);
}
