package com.chubchen.admin.modules.base.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author chubchen
 */
@Data
@AllArgsConstructor
public class RedisVo {

    private String key;

    private String value;
}
