package com.chubchen.admin.modules.base.entity;

import com.chubchen.admin.base.AdminBaseEntity;
import com.chubchen.admin.common.constant.CommonConstant;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chubchen
 */
@Data
@TableName("t_quartz_job")
@ApiModel(value = "定时任务")
public class QuartzJob extends AdminBaseEntity<QuartzJob> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "任务类名")
    private String jobClassName;

    @ApiModelProperty(value = "cron表达式")
    private String cronExpression;

    @ApiModelProperty(value = "参数")
    private String parameter;

    @ApiModelProperty(value = "备注")
    private String description;

    @ApiModelProperty(value = "状态 0正常 -1停止")
    private Integer status = CommonConstant.STATUS_NORMAL;
}
