package com.chubchen.admin.modules.base.serviceimpl.mybatis;

import com.chubchen.admin.modules.base.dao.mapper.DepartmentMapper;
import com.chubchen.admin.modules.base.dao.mapper.UserRoleMapper;
import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.entity.UserRole;
import com.chubchen.admin.modules.base.service.mybatis.IUserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author chubchen
 */
@Service
public class IUserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements IUserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Role> findByUserId(String userId) {

        return userRoleMapper.findByUserId(userId);
    }

    @Override
    public List<String> findDepIdsByUserId(String userId, String selfDepId) {
        List<String> parentDepIds =  userRoleMapper.findDepIdsByUserId(userId);
        if(StringUtils.isNotBlank(selfDepId)){
            parentDepIds.add(selfDepId);
        }

        Set<String> allDepIds = new HashSet<>();
        allDepIds.addAll(parentDepIds);
        //递归查询所有孩子节点
        findChildrenDepIds(parentDepIds, allDepIds);
        return allDepIds.parallelStream().collect(Collectors.toList());
    }

    private Set<String> findChildrenDepIds(List<String> parentIds, Set<String> allDepIds){
        List<String> list = departmentMapper.selectByParentIds(parentIds);
        if(list != null && list.size() > 0){
            allDepIds.addAll(list);
            return findChildrenDepIds(list, allDepIds);
        }else{
            return allDepIds;
        }
    }

    @Override
    public List<String> findStoreCodes(String userId, List<String> depIds) {
        if(depIds== null || depIds.isEmpty()){
            return null;
        }
        return userRoleMapper.findStoreCodes(depIds);
    }

    @Override
    public List<Integer> findDomainIdByDeparmentId(String userId, List<String> list) {
        if(list== null || list.isEmpty()){
            return null;
        }
        return userRoleMapper.findDomainIdByDeparmentId(list);
    }
}
