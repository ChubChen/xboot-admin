package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.UserRoleMapper;
import com.chubchen.admin.modules.base.entity.UserRole;
import com.chubchen.admin.modules.base.service.UserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 用户角色接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRole> implements UserRoleService {

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Override
    public List<UserRole> findByRoleId(String roleId) {

        return userRoleMapper.selectList(new QueryWrapper<UserRole>().eq("role_id", roleId));
    }

    @Override
    public void deleteByUserId(String userId) {
        userRoleMapper.delete(new QueryWrapper<UserRole>().eq("user_id", userId));;
    }

}
