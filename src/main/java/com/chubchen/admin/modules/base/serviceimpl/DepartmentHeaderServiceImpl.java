package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.DepartmentHeaderMapper;
import com.chubchen.admin.modules.base.entity.DepartmentHeader;
import com.chubchen.admin.modules.base.service.DepartmentHeaderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门负责人接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class DepartmentHeaderServiceImpl extends ServiceImpl<DepartmentHeaderMapper, DepartmentHeader> implements DepartmentHeaderService {

    @Autowired
    private DepartmentHeaderMapper departmentHeaderMapper;

    @Override
    public List<String> findHeaderByDepartmentId(String departmentId, Integer type) {

        List<String> list = new ArrayList<>();
        List<DepartmentHeader> headers = departmentHeaderMapper.selectList(new QueryWrapper<DepartmentHeader>()
                .eq("department_id",departmentId).eq("type", type));
        headers.forEach(e->{
            list.add(e.getUserId());
        });
        return list;
    }

    @Override
    public void deleteByDepartmentId(String departmentId) {
        departmentHeaderMapper.delete(new QueryWrapper<DepartmentHeader>().eq("department_id", departmentId));
    }

    @Override
    public void deleteByUserId(String userId) {
        departmentHeaderMapper.delete(new QueryWrapper<DepartmentHeader>().eq("user_id", userId));
    }
}