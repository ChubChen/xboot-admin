package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.common.constant.CommonConstant;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.modules.base.dao.mapper.DictDataMapper;
import com.chubchen.admin.modules.base.entity.DictData;
import com.chubchen.admin.modules.base.service.DictDataService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 字典数据接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class DictDataServiceImpl extends ServiceImpl<DictDataMapper, DictData> implements DictDataService {

    @Autowired
    private DictDataMapper dictDataMapper;

    @Override
    public IPage<DictData> findByCondition(DictData dictData, PageVo pageVo) {
        IPage<DictData> page = new Page<>(pageVo.getPageNumber(), pageVo.getPageSize());
        page = dictDataMapper.selectPage(page, new QueryWrapper<DictData>()
                .like(StringUtils.isNotEmpty(dictData.getTitle()),"title", dictData.getTitle())
                .eq(dictData.getStatus() != null,"status", dictData.getStatus())
                .eq(StringUtils.isNotEmpty(dictData.getDictId()),"dict_id", dictData.getDictId()));
        return page;
    }

    @Override
    public List<DictData> findByDictId(String dictId) {

        return dictDataMapper.selectList(new QueryWrapper<DictData>()
                .eq("dict_id", dictId).eq("status", CommonConstant.STATUS_NORMAL).orderByAsc("sort_order"));
    }

    @Override
    public List<DictData> findByDictListId(List<String> ids) {

        return dictDataMapper.selectList(new QueryWrapper<DictData>()
                .in("dict_id", ids).eq("status", CommonConstant.STATUS_NORMAL).orderByAsc("sort_order"));
    }


    @Override
    public void deleteByDictId(String dictId) {

        dictDataMapper.delete(new UpdateWrapper<DictData>().eq("dict_id",dictId));
    }
}