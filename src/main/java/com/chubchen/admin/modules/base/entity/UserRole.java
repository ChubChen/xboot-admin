package com.chubchen.admin.modules.base.entity;

import com.chubchen.admin.base.AdminBaseEntity;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chubchen
 */
@Data
@TableName("t_user_role")
@ApiModel(value = "用户角色")
public class UserRole extends AdminBaseEntity<UserRole> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户唯一id")
    private String userId;

    @ApiModelProperty(value = "角色唯一id")
    private String roleId;

    @TableField(exist=false)
    @ApiModelProperty(value = "角色名")
    private String roleName;
}
