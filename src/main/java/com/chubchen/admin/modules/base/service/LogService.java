package com.chubchen.admin.modules.base.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.base.entity.Log;

/**
 * 日志接口
 * @author chubchen
 */
public interface LogService extends IService<Log> {

    /**
     * 分页搜索获取日志
     * @param type
     * @param key
     * @param searchVo
     * @param pageVo
     * @return
     */
    IPage<Log> findByConfition(Integer type, String key, SearchVo searchVo, PageVo pageVo);
    /**
     * 删除所有
     */
    void deleteAll();
}
