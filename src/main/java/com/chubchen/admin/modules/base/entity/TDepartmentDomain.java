package com.chubchen.admin.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import lombok.experimental.Tolerate;

/**
 * <p>
 * 
 * </p>
 *
 * @author chubchen
 * @since 2020-06-09
 */
@Builder
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="TDepartmentDomain对象", description="")
public class TDepartmentDomain extends Model<TDepartmentDomain> {

    private static final long serialVersionUID = 1L;

    @Tolerate
    public TDepartmentDomain(){

    }

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "部门Id")
    private String departmentId;

    @ApiModelProperty(value = "域名ID")
    private Integer domainId;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

}
