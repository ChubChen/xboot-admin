package com.chubchen.admin.modules.base.controller.manage;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.chubchen.admin.common.constant.CommonConstant;
import com.chubchen.admin.common.exception.XbootException;
import com.chubchen.admin.common.utils.CommonUtil;
import com.chubchen.admin.common.utils.ResultUtil;
import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.Result;
import com.chubchen.admin.modules.base.entity.Department;
import com.chubchen.admin.modules.base.entity.DepartmentHeader;
import com.chubchen.admin.modules.base.entity.TDepartmentDomain;
import com.chubchen.admin.modules.base.entity.User;
import com.chubchen.admin.modules.base.service.*;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * @author Exrick
 */
@Slf4j
@RestController
@Api(description = "部门管理接口")
@RequestMapping("/xboot/department")
@CacheConfig(cacheNames = "department")
@Transactional
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoleDepartmentService roleDepartmentService;

    @Autowired
    private DepartmentHeaderService departmentHeaderService;

    @Autowired
    private ITDepartmentDomainService departmentDomainService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private SecurityUtil securityUtil;

    @RequestMapping(value = "/getByParentId/{parentId}",method = RequestMethod.GET)
    @ApiOperation(value = "通过id获取")
    public Result<List<Department>> getByParentId(@PathVariable String parentId,
                                                  @ApiParam("是否开始数据权限过滤") @RequestParam(required = false, defaultValue = "true") Boolean openDataFilter){

        List<Department> list ;
        User u = securityUtil.getCurrUser();
        String key = "department::"+parentId+":"+u.getId()+"_"+openDataFilter;
        String v = redisTemplate.opsForValue().get(key);
        if(StrUtil.isNotBlank(v)){
            list = new Gson().fromJson(v, new TypeToken<List<Department>>(){}.getType());
            return new ResultUtil<List<Department>>().setData(list);
        }
        list = departmentService.findByParentIdOrderBySortOrder(parentId, openDataFilter);
        list = setInfo(list);
        redisTemplate.opsForValue().set(key, new GsonBuilder().create().toJson(list));
        return new ResultUtil<List<Department>>().setData(list);
    }

    @RequestMapping(value = "/add",method = RequestMethod.POST)
    @ApiOperation(value = "添加")
    public Result<Object> add(@ModelAttribute Department department){

        departmentService.save(department);
        //保存domainids
        bachSaveDomainIds(department.getDomainIds(), department.getId());

        // 同步该节点缓存
        Set<String> keys = redisTemplate.keys("department::"+department.getParentId()+":*");
        if(keys != null && !keys.isEmpty() ){
            redisTemplate.delete(keys);
        }
        // 如果不是添加的一级 判断设置上级为父节点标识
        if(!CommonConstant.PARENT_ID.equals(department.getParentId())){
            Department parent = departmentService.getById(department.getParentId());
            if(parent.getIsParent()==null||!parent.getIsParent()){
                parent.setIsParent(true);
                departmentService.updateById(parent);
                // 更新上级节点的缓存
                Set<String> keysParent = redisTemplate.keys("department::"+parent.getParentId()+":*");
                if (keysParent != null && !keysParent.isEmpty()) {
                    redisTemplate.delete(keysParent);
                }
            }
        }
        return new ResultUtil<>().setSuccessMsg("添加成功");
    }

    @RequestMapping(value = "/edit",method = RequestMethod.POST)
    @ApiOperation(value = "编辑")
    public Result<Object> edit(@ModelAttribute Department department,
                               @RequestParam(required = false) String[] mainHeader,
                               @RequestParam(required = false) String[] viceHeader){

        departmentService.updateById(department);
        // 先删除原数据
        departmentHeaderService.deleteByDepartmentId(department.getId());
        // 先删除原数据
        departmentDomainService.remove(new QueryWrapper<TDepartmentDomain>().eq("department_id", department.getId()));

        if(mainHeader != null && mainHeader.length > 0){
            for(String id:mainHeader){
                DepartmentHeader dh = new DepartmentHeader();
                dh.setUserId(id);
                dh.setDepartmentId(department.getId());
                dh.setType(CommonConstant.HEADER_TYPE_MAIN);
                departmentHeaderService.save(dh);
            }
        }
        if(viceHeader != null && viceHeader.length > 0){
            for(String id:viceHeader){
                DepartmentHeader dh = new DepartmentHeader();
                dh.setUserId(id);
                dh.setDepartmentId(department.getId());
                dh.setType(CommonConstant.HEADER_TYPE_VICE);
                departmentHeaderService.save(dh);
            }
        }
        bachSaveDomainIds(department.getDomainIds(), department.getId());
        // 手动删除所有部门缓存
        Set<String> keys = redisTemplate.keys("department:" + "*");
        redisTemplate.delete(keys);
        // 删除所有用户缓存
        Set<String> keysUser = redisTemplate.keys("user:" + "*");
        redisTemplate.delete(keysUser);
        redisTemplate.delete("userRole::domainIds:*");
        return new ResultUtil<>().setSuccessMsg("编辑成功");
    }

    private void bachSaveDomainIds(List<Integer> domainIds, String depId){
        if(domainIds != null && !domainIds.isEmpty()){
            ArrayList<TDepartmentDomain> domainArrayList = new ArrayList<>(domainIds.size());
            domainIds.forEach((item)->{
                domainArrayList.add(TDepartmentDomain.builder().build().setDepartmentId(depId).setDomainId(item));
            });
            departmentDomainService.saveBatch(domainArrayList);
        }
    }

    @RequestMapping(value = "/getDomainIdByDepId/{id}",method = RequestMethod.GET)
    @ApiOperation(value = "通过部门Id获取domain")
    public Result<List<TDepartmentDomain>> getDomainIdByDepId(@PathVariable String id){
        TDepartmentDomain domain = new TDepartmentDomain();
        domain.setDepartmentId(id);
        List<TDepartmentDomain> list = departmentDomainService.findListAll(domain);
        return new ResultUtil<List<TDepartmentDomain>>().setData(list);
    }

    @RequestMapping(value = "/delByIds/{ids}",method = RequestMethod.DELETE)
    @ApiOperation(value = "批量通过id删除")
    public Result<Object> delByIds(@PathVariable String[] ids){

        for(String id : ids){
            deleteRecursion(id, ids);
        }
        // 手动删除所有部门缓存
        Set<String> keys = redisTemplate.keys("department:" + "*");
        redisTemplate.delete(keys);
        // 删除数据权限缓存
        Set<String> keysUserRoleData = redisTemplate.keys("userRole::depIds:" + "*");
        redisTemplate.delete(keysUserRoleData);
        redisTemplate.delete("userRole::domainIds:*");
        return new ResultUtil<>().setSuccessMsg("批量通过id删除数据成功");
    }

    public void deleteRecursion(String id, String[] ids){

        List<User> list = userService.findByDepartmentId(id);
        if(list!=null&&list.size()>0){
            throw new XbootException("删除失败，包含正被用户使用关联的部门");
        }
        // 获得其父节点
        Department dep = departmentService.getById(id);
        Department parent = null;
        if(dep!=null&&StrUtil.isNotBlank(dep.getParentId())){
            parent = departmentService.getById(dep.getParentId());
        }
        departmentService.removeById(id);
        // 删除关联数据权限
        roleDepartmentService.deleteByDepartmentId(id);
        // 删除关联部门负责人
        departmentHeaderService.deleteByDepartmentId(id);
        // 判断父节点是否还有子节点
        if(parent!=null){
            List<Department> childrenDeps = departmentService.findByParentIdOrderBySortOrder(parent.getId(), false);
            if(childrenDeps==null||childrenDeps.size()==0){
                parent.setIsParent(false);
                departmentService.updateById(parent);
            }
        }
        // 递归删除
        List<Department> departments = departmentService.findByParentIdOrderBySortOrder(id, false);
        for(Department d : departments){
            if(!CommonUtil.judgeIds(d.getId(), ids)){
                deleteRecursion(d.getId(), ids);
            }
        }
    }

    @RequestMapping(value = "/search",method = RequestMethod.GET)
    @ApiOperation(value = "部门名模糊搜索")
    public Result<List<Department>> searchByTitle(@RequestParam String title,
                                                  @ApiParam("是否开始数据权限过滤") @RequestParam(required = false, defaultValue = "true") Boolean openDataFilter){

        List<Department> list = departmentService.findByTitleLikeOrderBySortOrder("%"+title+"%", openDataFilter);
        list = setInfo(list);
        return new ResultUtil<List<Department>>().setData(list);
    }

    public List<Department> setInfo(List<Department> list){

        // lambda表达式
        list.forEach(item -> {
            if(!CommonConstant.PARENT_ID.equals(item.getParentId())){
                Department parent = departmentService.getById(item.getParentId());
                item.setParentTitle(parent.getTitle());
            }else{
                item.setParentTitle("一级部门");
            }
            // 设置负责人
            item.setMainHeader(departmentHeaderService.findHeaderByDepartmentId(item.getId(), CommonConstant.HEADER_TYPE_MAIN));
            item.setViceHeader(departmentHeaderService.findHeaderByDepartmentId(item.getId(), CommonConstant.HEADER_TYPE_VICE));
        });
        return list;
    }
}

