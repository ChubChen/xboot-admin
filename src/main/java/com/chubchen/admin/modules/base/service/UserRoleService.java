package com.chubchen.admin.modules.base.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.UserRole;

import java.util.List;

/**
 * 用户角色接口
 * @author chubchen
 */
public interface UserRoleService extends IService<UserRole> {

    /**
     * 通过roleId查找
     * @param roleId
     * @return
     */
    List<UserRole> findByRoleId(String roleId);

    /**
     * 删除用户角色
     * @param userId
     */
    void deleteByUserId(String userId);
}
