package com.chubchen.admin.modules.base.utils;

import com.chubchen.admin.modules.base.entity.Permission;
import com.chubchen.admin.modules.base.vo.MenuVo;
import cn.hutool.core.bean.BeanUtil;

/**
 * @author chubchen
 */
public class VoUtil {

    public static MenuVo permissionToMenuVo(Permission p){

        MenuVo menuVo = new MenuVo();
        BeanUtil.copyProperties(p, menuVo);
        return menuVo;
    }
}
