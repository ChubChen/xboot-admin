package com.chubchen.admin.modules.base.service;

import com.chubchen.admin.modules.base.entity.TDepartmentDomain;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.chubchen.admin.common.vo.PageVo;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author chubchen
 * @since 2020-06-09
 */
public interface ITDepartmentDomainService extends IService<TDepartmentDomain> {

    /**
     * 查询分页数据
     *
     * @param pageVo      页码
     * @param tDepartmentDomain 过滤条件
     * @return IPage<TDepartmentDomain>
     */
    IPage<TDepartmentDomain> findListByPage(PageVo pageVo, TDepartmentDomain tDepartmentDomain);

   /**
    * 查询分页数据
    *
    * @return  List<TDepartmentDomain>
    */
    List<TDepartmentDomain> findListAll(TDepartmentDomain tDepartmentDomain);

    /**
     * 添加
     *
     * @param tDepartmentDomain 
     * @return int
     */
    int add(TDepartmentDomain tDepartmentDomain);

    /**
     * 删除
     *
     * @param id 主键
     * @return int
     */
    int delete(Long id);

    /**
     * 修改
     *
     * @param tDepartmentDomain 
     * @return int
     */
    int updateData(TDepartmentDomain tDepartmentDomain);

    /**
     * id查询数据
     *
     * @param id id
     * @return TDepartmentDomain
     */
    TDepartmentDomain findById(Long id);
}
