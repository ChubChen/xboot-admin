package com.chubchen.admin.modules.base.service.mybatis;

import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;
import java.util.Set;

/**
 * @author chubchen
 */
@CacheConfig(cacheNames = "userRole")
public interface IUserRoleService extends IService<UserRole> {

    /**
     * 通过用户id获取
     * @param userId
     * @return
     */
    @Cacheable(key = "#userId")
    List<Role> findByUserId(@Param("userId") String userId);

    /**
     * 通过用户id获取用户角色关联的部门数据
     * @param userId
     * @param selfDepId
     * @return
     */
    @Cacheable(key = "'depIds:'+#userId")
    List<String> findDepIdsByUserId(String userId, String selfDepId);

    /**
     * 通过用户id获取用户关联的站点数据
     * @param userId
     * @param allDepIds
     * @return
     */
    @Cacheable(key = "'storeCodes:'+#userId")
    List<String> findStoreCodes(String userId, List<String> allDepIds);

    /**
     * 根据部门id获取domainId权限
     * @param id
     * @param list
     * @return
     */
    @Cacheable(key = "'domainIds:'+#userId")
    List<Integer> findDomainIdByDeparmentId(String userId, List<String> list);
}
