package com.chubchen.admin.modules.base.dao.mapper;

import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.entity.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Set;

/**
 * @author chubchen
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {

    /**
     * 通过用户id获取
     *
     * @param userId
     * @return
     */
    List<Role> findByUserId(@Param("userId") String userId);

    /**
     * 通过用户id获取用户角色关联的部门数据
     *
     * @param userId
     * @return
     */
    List<String> findDepIdsByUserId(@Param("userId") String userId);


    /**
     * 通过用户id获取
     *
     * @param depIds
     * @return
     */
    List<String> findStoreCodes(@Param("depIds") List<String> depIds);

    /**
     * 通过部门id获取域名权限
     * @param list
     * @return
     */
    List<Integer> findDomainIdByDeparmentId(@Param("list") List<String> list);
}
