package com.chubchen.admin.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.Dict;
import com.chubchen.admin.modules.base.entity.DictData;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

/**
 * 字典接口
 * @author chubchen
 */
public interface DictService extends IService<Dict> {

    /**
     * 排序获取全部
     * @return
     */
    List<Dict> findAllOrderBySortOrder();

    /**
     * 通过type获取
     * @param type
     * @return
     */
    Dict findByType(String type);

    /**
     * 通过type获取
     * @param type
     * @return List<Dict>
     */
    List<Dict> findByType(List<String> type);

    /**
     * 模糊搜索
     * @param key
     * @return
     */
    List<Dict> findByTitleOrTypeLike(String key);

    /**
     * 根据type获取字典数据。
     * @param type
     * @return
     */
    List<DictData> getDictByType(List<String> type);
}