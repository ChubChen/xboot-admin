package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.common.utils.SecurityUtil;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.common.vo.SearchVo;
import com.chubchen.admin.modules.base.dao.mapper.DepartmentMapper;
import com.chubchen.admin.modules.base.dao.mapper.PermissionMapper;
import com.chubchen.admin.modules.base.dao.mapper.UserMapper;
import com.chubchen.admin.modules.base.dao.mapper.UserRoleMapper;
import com.chubchen.admin.modules.base.entity.Department;
import com.chubchen.admin.modules.base.entity.Permission;
import com.chubchen.admin.modules.base.entity.Role;
import com.chubchen.admin.modules.base.entity.User;
import com.chubchen.admin.modules.base.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Set;

/**
 * 用户接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private UserRoleMapper userRoleMapper;

    @Autowired
    private PermissionMapper permissionMapper;

    @Autowired
    private DepartmentMapper departmentMapper;

    @Autowired
    private SecurityUtil securityUtil;

    @Override
    public User findByUsername(String username) {

        User user = userMapper.selectOne(new QueryWrapper<User>().eq("username", username));
        if(user==null){
            return null;
        }
        // 关联部门
        if(StringUtils.isNotBlank(user.getDepartmentId())){
            Department department = departmentMapper.selectById(user.getDepartmentId());
            if(department!=null){
                user.setDepartmentTitle(department.getTitle());
            }
        }
        // 关联角色
        List<Role> roleList = userRoleMapper.findByUserId(user.getId());
        user.setRoles(roleList);
        // 关联权限菜单
        List<Permission> permissionList = permissionMapper.findByUserId(user.getId());
        user.setPermissions(permissionList);
        return user;
    }

    @Override
    public User findByMobile(String mobile) {

        return userMapper.selectOne(new QueryWrapper<User>().eq("mobile", mobile));
    }

    @Override
    public User findByEmail(String email) {

        return userMapper.selectOne(new QueryWrapper<User>().eq("email", email));
    }

    @Override
    public IPage<User> findByCondition(User user, SearchVo searchVo, PageVo pageVo) {
        IPage<User> page = new Page<User>(pageVo.getPageNumber(), pageVo.getPageSize());
        List<String> depIds = securityUtil.getDeparmentIds();
        page = userMapper.selectPage(page, new QueryWrapper<User>()
                .eq(StringUtils.isNotBlank(user.getDepartmentId()), "department_id", user.getDepartmentId())
                .eq(StringUtils.isNotBlank(user.getSex()), "sex", user.getSex())
                .in(depIds!=null&&depIds.size()>0 , "department_id", depIds)
                .eq(user.getType()!=null, "type", user.getType())
                .eq(user.getStatus()!=null, "status", user.getStatus())
                .between(StringUtils.isNotBlank(searchVo.getStartDate())&&StringUtils.isNotBlank(searchVo.getEndDate()),
                        "create_time", searchVo.getStartDate(), searchVo.getEndDate())
                .and(i->i.like("username",user.getUsername())
                        .like("mobile", user.getMobile())
                        .like("email", user.getEmail()))
        );
        return page;
    }

    @Override
    public List<User> findByDepartmentId(String departmentId) {

        return userMapper.selectList(new QueryWrapper<User>()
                .eq("department_id", departmentId));
    }

    @Override
    public List<User> findByDepartmentIdByRedis(String departmentId) {

        return userMapper.selectList(new QueryWrapper<User>()
                .eq("department_id", departmentId));
    }
}
