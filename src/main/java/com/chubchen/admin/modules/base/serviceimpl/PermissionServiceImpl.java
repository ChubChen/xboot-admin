package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.PermissionMapper;
import com.chubchen.admin.modules.base.entity.Permission;
import com.chubchen.admin.modules.base.service.PermissionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 权限接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService {

    @Autowired
    private PermissionMapper permissionMapper;

    @Override
    public List<Permission> findByLevelOrderBySortOrder(Integer level) {
        return permissionMapper.selectList(new QueryWrapper<Permission>()
                .eq("level", level).orderByAsc("sort_order"));
    }

    @Override
    public List<Permission> findByParentIdOrderBySortOrder(String parentId) {

        return permissionMapper.selectList(new QueryWrapper<Permission>()
                .eq("parent_id", parentId).orderByAsc("sort_order"));
    }

    @Override
    public List<Permission> findByTypeAndStatusOrderBySortOrder(Integer type, Integer status) {

        return permissionMapper.selectList(new QueryWrapper<Permission>()
                .eq("type", type)
                .eq("status", status)
                .orderByAsc("sort_order"));
    }

    @Override
    public List<Permission> findByTitle(String title) {

        return permissionMapper.selectList(new QueryWrapper<Permission>()
                .eq("title", title));
    }

    @Override
    public List<Permission> findByTitleLikeOrderBySortOrder(String title) {

        return permissionMapper.selectList(new QueryWrapper<Permission>()
                .like("title", title).orderByAsc("sort_order"));
    }
}