package com.chubchen.admin.modules.base.entity;

import com.chubchen.admin.base.AdminBaseEntity;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author chubchen
 */
@Data
@TableName("t_role_department")
@ApiModel(value = "角色部门")
public class RoleDepartment extends AdminBaseEntity <RoleDepartment>{

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "角色id")
    private String roleId;

    @ApiModelProperty(value = "部门id")
    private String departmentId;
}