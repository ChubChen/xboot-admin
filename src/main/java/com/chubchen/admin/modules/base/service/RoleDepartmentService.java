package com.chubchen.admin.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.RoleDepartment;

import java.util.List;

/**
 * 角色部门接口
 * @author chubchen
 */
public interface RoleDepartmentService extends IService<RoleDepartment> {

    /**
     * 通过roleId获取
     * @param roleId
     * @return
     */
    List<RoleDepartment> findByRoleId(String roleId);

    /**
     * 通过角色id删除
     * @param roleId
     */
    void deleteByRoleId(String roleId);

    /**
     * 通过角色id删除
     * @param departmentId
     */
    void deleteByDepartmentId(String departmentId);
}