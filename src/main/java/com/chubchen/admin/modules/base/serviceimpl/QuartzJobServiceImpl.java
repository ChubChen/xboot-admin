package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.QuartzJobMapper;
import com.chubchen.admin.modules.base.entity.QuartzJob;
import com.chubchen.admin.modules.base.service.QuartzJobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 定时任务接口实现
 * @author chubchen
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class QuartzJobServiceImpl extends ServiceImpl<QuartzJobMapper, QuartzJob> implements QuartzJobService {

    @Autowired
    private QuartzJobMapper quartzJobMapper;

    @Override
    public List<QuartzJob> findByJobClassName(String jobClassName) {

        return quartzJobMapper.selectList(new QueryWrapper<QuartzJob>()
                .eq("job_class_name", jobClassName));
    }
}