package com.chubchen.admin.modules.base.serviceimpl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.chubchen.admin.modules.base.dao.mapper.DictMapper;
import com.chubchen.admin.modules.base.entity.Dict;
import com.chubchen.admin.modules.base.entity.DictData;
import com.chubchen.admin.modules.base.service.DictDataService;
import com.chubchen.admin.modules.base.service.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 字典接口实现
 * @author chubchen
 */
@Slf4j
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements DictService {

    @Autowired
    private DictMapper dictMapper;

    @Autowired
    private DictDataService dictDataService;

    @Override
    public List<Dict> findAllOrderBySortOrder() {

        return dictMapper.selectList(new QueryWrapper<Dict>().orderByAsc("sort_order"));
    }

    @Override
    public Dict findByType(String type) {

        return dictMapper.selectOne(new QueryWrapper<Dict>().eq("type", type));
    }

    @Override
    public List<Dict> findByType(List<String> type) {

        return dictMapper.selectList(new QueryWrapper<Dict>().in("type", type));
    }

    @Override
    public List<Dict> findByTitleOrTypeLike(String key) {

        return dictMapper.selectList(new QueryWrapper<Dict>()
                .like("title", key).or().like("type",key));
    }

    @Override
    public List<DictData> getDictByType(List<String> type) {
        List<Dict> dict = this.findByType(type);
        if (dict == null && !dict.isEmpty()) {
            return null;
        }
        List<String> idList = dict.stream().map(Dict::getId).collect(Collectors.toList());
        Map<String,String> typeMap= dict.stream().collect(Collectors.toMap(Dict::getId, Dict::getType));
        //查询
        List<DictData> list = dictDataService.findByDictListId(idList);
        //组装数据
        list.forEach(item->item.setDictType(typeMap.get(item.getDictId())));
        return list;
    }
}