package com.chubchen.admin.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.RolePermission;

import java.util.List;

/**
 * 角色权限接口
 * @author chubchen
 */
public interface RolePermissionService extends IService<RolePermission> {

    /**
     * 通过permissionId获取
     * @param permissionId
     * @return
     */
    List<RolePermission> findByPermissionId(String permissionId);

    /**
     * 通过roleId获取
     * @param roleId
     */
    List<RolePermission> findByRoleId(String roleId);

    /**
     * 通过roleId删除
     * @param roleId
     */
    void deleteByRoleId(String roleId);
}