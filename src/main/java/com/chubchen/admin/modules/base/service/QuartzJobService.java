package com.chubchen.admin.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.modules.base.entity.QuartzJob;

import java.util.List;

/**
 * 定时任务接口
 * @author chubchen
 */
public interface QuartzJobService extends IService<QuartzJob> {

    /**
     * 通过类名获取
     * @param jobClassName
     * @return
     */
    List<QuartzJob> findByJobClassName(String jobClassName);
}