package com.chubchen.admin.modules.base.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.chubchen.admin.modules.base.entity.Dict;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author chubchen
 * @since 2019-11-19
 */
public interface DictMapper extends BaseMapper<Dict> {

}
