package com.chubchen.admin.modules.base.entity;

import com.chubchen.admin.base.AdminBaseEntity;
import com.chubchen.admin.common.constant.CommonConstant;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author chubchen
 */
@Data
@TableName("t_department")
@ApiModel(value = "部门")
public class Department extends AdminBaseEntity<Department> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "部门名称")
    private String title;

    @ApiModelProperty(value = "站点")
    @Deprecated
    private String origin;

    @ApiModelProperty(value = "父id")
    private String parentId;

    @ApiModelProperty(value = "是否为父节点(含子节点) 默认false")
    private Boolean isParent = false;

    @ApiModelProperty(value = "排序值")
    private BigDecimal sortOrder;

    @ApiModelProperty(value = "是否启用 0启用 -1禁用")
    private Integer status = CommonConstant.STATUS_NORMAL;

    @TableField(exist=false)
    @ApiModelProperty(value = "父节点名称")
    private String parentTitle;

    @TableField(exist=false)
    @ApiModelProperty(value = "主负责人")
    private List<String> mainHeader;

    @TableField(exist=false)
    @ApiModelProperty(value = "副负责人")
    private List<String> viceHeader;

    @TableField(exist=false)
    @ApiModelProperty(value = "部门管理的域名权限")
    private List<Integer> domainIds;
}