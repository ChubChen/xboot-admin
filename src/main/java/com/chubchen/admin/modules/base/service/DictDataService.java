package com.chubchen.admin.modules.base.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.chubchen.admin.common.vo.PageVo;
import com.chubchen.admin.modules.base.entity.DictData;

import java.util.List;

/**
 * 字典数据接口
 * @author chubchen
 */
public interface DictDataService extends IService<DictData> {

    /**
     * 多条件获取
     * @param dictData
     * @param pageVo
     * @return
     */
    IPage<DictData> findByCondition(DictData dictData, PageVo pageVo);

    /**
     * 通过dictId获取启用字典 已排序
     * @param dictId
     * @return
     */
    List<DictData> findByDictId(String dictId);


    /**
     * 通过dictId获取启用字典 已排序
     * @param dictId
     * @return
     */
    List<DictData> findByDictListId(List<String> dictId);

    /**
     * 通过dictId删除
     * @param dictId
     */
    void deleteByDictId(String dictId);
}