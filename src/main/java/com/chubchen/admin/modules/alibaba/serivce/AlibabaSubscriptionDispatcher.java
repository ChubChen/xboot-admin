package com.chubchen.admin.modules.alibaba.serivce;

import com.alibaba.fastjson.JSONObject;
import com.chubchen.admin.modules.alibaba.constants.AlibabaTypeEnum;
import com.chubchen.admin.modules.alibaba.handle.AlibabaEventHandle;
import com.chubchen.admin.modules.alibaba.handle.SendGoodsHandle;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author chubchen
 */
@Component
@Slf4j
public class AlibabaSubscriptionDispatcher {

    private Map<String, AlibabaEventHandle> handlers = new ConcurrentHashMap<>();

    @Autowired
    private SendGoodsHandle sendGoodsHandle;

    @PostConstruct
    public void init() {
        this.handlers.put(AlibabaTypeEnum.ORDER_SENDGOODS.getType(), sendGoodsHandle);
    }

    /**
     * 将事件注册到派发器
     *
     * @param type
     * @param eventHandle
     */
    public void bind(String type, AlibabaEventHandle eventHandle) {
        this.handlers.put(type, (context) -> {
            try {
                return eventHandle.handle(context);
            } catch (Exception e) {
                //记录错误日志
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        });
    }

    public void dispatch(String type, JSONObject data) {
        if(this.handlers.containsKey(type)){
            this.handlers.get(type).handle(data);
        }else{
            log.error("暂时不能接收该类型的消息，消息type:{}", type);
        }
    }
}
