package com.chubchen.admin.modules.alibaba.entity;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

import java.io.Serializable;

/**
 * 订阅消息基础类
 * @author chubchen
 */
@Data
public class SubscriptionBase implements Serializable {

    private long msgId;

    private long gmtBorn;

    private JSONObject data;

    private String userInfo;

    private String type;

    private String extraInfo;
}
