package com.chubchen.admin.modules.alibaba.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.parser.ParserConfig;
import com.chubchen.admin.modules.alibaba.entity.SubscriptionBase;
import com.chubchen.admin.modules.alibaba.entity.SubscriptionParent;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaSubscriptionDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

///**
// * @author chubchen
// *  1688推送接口订阅类
// */
//@RestController
//@RequestMapping("/api")
//@Slf4j
//public class Subscription1688 {
//
//
//    @Autowired
//    private AlibabaSubscriptionDispatcher alibabaSubscriptionDispatcher;
//
//
//    @ResponseBody
//    @RequestMapping("/subscription1688")
//    public boolean subscription1688Gateway(@ModelAttribute SubscriptionParent subParent){
//        log.info("收到1688推送消息，消息:{}", subParent.getMessage());
//        try{
//            //step1 保存消息
//            //step2 消费消息
//            SubscriptionBase subData = JSON.parseObject(subParent.getMessage(), SubscriptionBase.class);
//            alibabaSubscriptionDispatcher.dispatch(subData.getType(), subData.getData());
//            //step3 更新状态
//
//        }catch (Exception e){
//            log.error("处理推销消息失败", e);
//        }
//        return true;
//    }
//
//}
