package com.chubchen.admin.modules.alibaba.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.logistics.param.*;
import com.alibaba.ocean.rawsdk.ApiExecutor;
import com.alibaba.ocean.rawsdk.common.SDKResult;
import com.alibaba.trade.param.AlibabaOpenplatformTradeModelTradeInfo;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListParam;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListResult;
import com.chubchen.admin.modules.alibaba.config.AlibabaConfig;
import com.chubchen.admin.modules.alibaba.constants.AlibabaOrderStatusEnum;
import com.chubchen.admin.modules.alibaba.constants.CollectionNames;
import com.chubchen.admin.modules.alibaba.entity.AlibabaLogistics;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaQueryService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

/**
 * 阿里 1688 service
 *
 * @author chubchen
 */
@Slf4j
@Service
public class AlibabaQueryServiceImpl implements AlibabaQueryService {

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private AlibabaConfig alibabaConfig;



    @Override
    public AlibabaTradeGetBuyerOrderListResult queryAlibbaOrdersByModifyTime(Date startDate, Date endDate, int page) {
        AlibabaTradeGetBuyerOrderListParam param = new AlibabaTradeGetBuyerOrderListParam();
        param.setModifyStartTime(startDate);
        param.setModifyEndTime(DateUtils.setSeconds(endDate, 0));
        param.setPage(page);
        param.setPageSize(20);
        ApiExecutor apiExecutor = new ApiExecutor(alibabaConfig.getAppKey(), alibabaConfig.getSecKey());

        SDKResult<AlibabaTradeGetBuyerOrderListResult> result = apiExecutor.execute(param, alibabaConfig.getAccessToken());
        if (StringUtils.isEmpty(result.getErrorCode())) {
            return result.getResult();
        } else {
            log.error("根据更新时间查询1688订单列表发生错误错误code:{},错误原因:{}", result.getErrorCode(), result.getErrorMessage());

        }
        return null;
    }

    @Override
    public AlibabaOpenplatformTradeModelTradeInfo getLastUpdateTimeOrders() {
        Query query = Query.query(Criteria.where("baseInfo.modifyTime").gte(DateUtils.addDays(new Date(), -1)))
                .with(Sort.by(Sort.Direction.DESC, "baseInfo.modifyTime")).limit(1);
        AlibabaOpenplatformTradeModelTradeInfo tradeInfo = mongoTemplate.findOne(query,
                AlibabaOpenplatformTradeModelTradeInfo.class, CollectionNames.ORDER_COLLECTON_NAME);
        return tradeInfo;
    }

    @Override
    public void batchSaveAlibabaOrders(AlibabaOpenplatformTradeModelTradeInfo[] infos) {
        List<String> list = Arrays.stream(infos).map(item -> item.getBaseInfo().getIdOfStr()).collect(Collectors.toList());
        mongoTemplate.remove(Query.query(Criteria.where("baseInfo.idOfStr").in(list)),
                AlibabaOpenplatformTradeModelTradeInfo.class, CollectionNames.ORDER_COLLECTON_NAME);
        mongoTemplate.insert(Arrays.stream(infos).collect(Collectors.toList()), CollectionNames.ORDER_COLLECTON_NAME);
    }

    @Override
    public void batchSaveLogisticsInfos(AlibabaLogisticsOpenPlatformLogisticsOrder[] logisticsOrders,String orderId) {
        if (logisticsOrders != null && logisticsOrders.length >0) {
            Arrays.stream(logisticsOrders).forEach(logistics -> {
                Document doc = new Document();
                mongoTemplate.getConverter().write(logistics, doc);
                doc.append("idOfStr", orderId);
                mongoTemplate.upsert(
                        Query.query(Criteria.where("logisticsId").is(logistics.getLogisticsId())),
                        Update.fromDocument(doc),
                        AlibabaLogistics.class,
                        CollectionNames.LOGISTICS_COLLECTON_NAME
                );
            });
        }
    }

    @Override
    public AlibabaTradeGetLogisticsInfosBuyerViewResult queryTrackInfoByOrderId(Long orderId) {
        log.info("调用1688平台接口查询物流跟踪号，订单号：{}", orderId);
        AlibabaTradeGetLogisticsInfosBuyerViewParam param = new AlibabaTradeGetLogisticsInfosBuyerViewParam();
        param.setOrderId(orderId);
        param.setWebSite("1688");
        param.setFields("company,name,sender,receiver,sendgood");

        ApiExecutor apiExecutor = new ApiExecutor(alibabaConfig.getAppKey(), alibabaConfig.getSecKey());
        SDKResult<AlibabaTradeGetLogisticsInfosBuyerViewResult> result = apiExecutor.execute(param, alibabaConfig.getAccessToken());
        if (StringUtils.isEmpty(result.getErrorCode()) && result.getResult() != null &&
                StringUtils.isEmpty(result.getResult().getErrorCode())) {
            log.info("订单号：{},查询返回结果{}.", orderId, JSON.toJSONString(result.getResult()));
            return result.getResult();
        } else {
            return null;
        }
    }

    @Override
    public AlibabaTradeGetLogisticsTraceInfoBuyerViewResult queryLogisticsTraceInfo(Long orderId) {
        log.info("调用1688平台接口查询物流跟踪号，订单号：{}", orderId);
        AlibabaTradeGetLogisticsTraceInfoBuyerViewParam param = new AlibabaTradeGetLogisticsTraceInfoBuyerViewParam();
        param.setOrderId(orderId);
        param.setWebSite("1688");

        ApiExecutor apiExecutor = new ApiExecutor(alibabaConfig.getAppKey(), alibabaConfig.getSecKey());
        SDKResult<AlibabaTradeGetLogisticsTraceInfoBuyerViewResult> result = apiExecutor.execute(param, alibabaConfig.getAccessToken());
        if (StringUtils.isEmpty(result.getErrorCode()) && result.getResult() != null &&
                StringUtils.isEmpty(result.getResult().getErrorCode())) {
            log.info("订单号：{},查询返回结果{}.", orderId, JSON.toJSONString(result.getResult()));
            return result.getResult();
        } else {
            return null;
        }
    }

    @Override
    public void batchSaveLogisticsTraceInfos(AlibabaLogisticsOpenPlatformLogisticsTrace[] logisticsTrace, String orderIdStr) {
        if (logisticsTrace != null && logisticsTrace.length >0) {
            Arrays.stream(logisticsTrace).forEach(logistics -> {
                Document doc = new Document();
                mongoTemplate.getConverter().write(logistics, doc);
                doc.append("idOfStr", orderIdStr);
                mongoTemplate.upsert(
                        Query.query(Criteria.where("logisticsId").is(logistics.getLogisticsId())),
                        Update.fromDocument(doc),
                        AlibabaLogistics.class,
                        CollectionNames.LOGISTICS_TRACE_COLLECTON_NAME
                );
            });
        }
    }
}
