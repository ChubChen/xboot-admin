package com.chubchen.admin.modules.alibaba.constants;

import lombok.Getter;

/**
 * @author chubchen
 */
@Getter
public enum AlibabaTypeEnum {
    ORDER_SENDGOODS("ORDER_BUYER_VIEW_ANNOUNCE_SENDGOODS","1688发货通知");

    private String type;
    private String message;

    AlibabaTypeEnum(String type, String message){
        this.type = type;
        this.message = message;
    }
}
