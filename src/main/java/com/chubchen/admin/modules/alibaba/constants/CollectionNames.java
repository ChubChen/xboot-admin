package com.chubchen.admin.modules.alibaba.constants;

public interface CollectionNames {

    /**
     * 1688订单表
     */
    String ORDER_COLLECTON_NAME = "orders_1688";

    /**
     * 1688物流信息表
     */
    String LOGISTICS_COLLECTON_NAME = "logistics_1688";

    /**
     * 1688物流跟踪明细信息
     */
    String LOGISTICS_TRACE_COLLECTON_NAME = "logistics_trace_1688";


}
