package com.chubchen.admin.modules.alibaba.config;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author chubchen
 */
@Slf4j
@Component
@Data
public class AlibabaConfig {

    @Value("${alibaba.appkey}")
    private String appKey;

    @Value("${alibaba.seckey}")
    private String secKey;

    @Value("${alibaba.accessToken}")
    private String accessToken;
}
