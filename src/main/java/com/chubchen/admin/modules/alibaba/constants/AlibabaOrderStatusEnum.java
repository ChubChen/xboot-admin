package com.chubchen.admin.modules.alibaba.constants;

import lombok.Getter;

import java.util.HashSet;
import java.util.Set;

/**
 * @author chubchen
 *  1688订单状态
 */
@Getter
public enum AlibabaOrderStatusEnum {

    /**
     * 等待买家付款
     */
    waitbuyerpay("waitbuyerpay", "等待买家付款"),
    /**
     * 等待卖家发货
     */
    waitsellersend("waitsellersend", "等待卖家发货"),
    /**
     * 等待物流公司揽件
     */
    waitlogisticstakein("waitlogisticstakein", "等待物流公司揽件"),
    /**
     * 等待买家收货
     */
    waitbuyerreceive("waitbuyerreceive", "等待买家收货"),

    /**
     * 等待买家签收
     */
    waitbuyersign("waitbuyersign", "等待买家签收"),
    /**
     * 买家已签收
     */
    signinsuccess("signinsuccess", "买家已签收"),
    /**
     * 已收货
     */
    confirm_goods("confirm_goods", "已收货"),
    /**
     *  确认收货
     */
    confirm_goods_and_has_subsidy("confirm_goods_and_has_subsidy", "确认收货"),
    /**
     * 交易成功
     */
    success("success", "交易成功"),
    /**
     * 交易取消
     */
    cancel("cancel", "交易取消"),
    /**
     * 交易终止
     */
    terminated("terminated", "交易终止");

    private String code;

    private String message;

    AlibabaOrderStatusEnum(String code, String message){
        this.code = code;
        this.message = message;
    }

    public static Set<String> gteLogisticsStatus(){
        Set<String> set = new HashSet<>();
        set.add(waitlogisticstakein.getCode());
        set.add(waitbuyerreceive.getCode());
        set.add(waitbuyersign.getCode());
        set.add(signinsuccess.getCode());
        set.add(confirm_goods.getCode());
        return set;
    }
}
