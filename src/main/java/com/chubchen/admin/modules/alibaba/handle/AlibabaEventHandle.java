package com.chubchen.admin.modules.alibaba.handle;

import com.alibaba.fastjson.JSONObject;

/**
 * alibaba 事件处理接口
 * @author chubchen
 */
public interface AlibabaEventHandle {


    /**
     * 事件处理方法
     * @param data
     * @return
     */
    boolean handle(JSONObject data);
}
