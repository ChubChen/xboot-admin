package com.chubchen.admin.modules.alibaba.entity;

import com.alibaba.logistics.param.AlibabaLogisticsOpenPlatformLogisticsOrder;
import lombok.Data;

/**
 * @author chubchen
 * 在原有alibaba物流信息上扩展 订单号
 */
@Data
public class AlibabaLogistics extends AlibabaLogisticsOpenPlatformLogisticsOrder {

    private String idOfStr;

}

