package com.chubchen.admin.modules.alibaba.entity;

import lombok.Data;

import java.io.Serializable;

/**
 *@author chubchen
 * 1688推送参数
 */
@Data
public class SubscriptionParent implements Serializable {

    private String message;

    private String _aop_signature;
}
