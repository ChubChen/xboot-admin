package com.chubchen.admin.modules.alibaba.serivce;

import com.alibaba.logistics.param.AlibabaLogisticsOpenPlatformLogisticsOrder;
import com.alibaba.logistics.param.AlibabaLogisticsOpenPlatformLogisticsTrace;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewResult;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsTraceInfoBuyerViewResult;
import com.alibaba.trade.param.AlibabaOpenplatformTradeModelTradeInfo;
import com.alibaba.trade.param.AlibabaTradeGetBuyerOrderListResult;

import java.util.Date;
import java.util.Map;

/**
 * @author chubchen
 * alibaba 1688 业务类
 */
public interface AlibabaQueryService {


    /**
     * 查询订单同步数据最新的更新时间
     * @return
     */
    AlibabaOpenplatformTradeModelTradeInfo getLastUpdateTimeOrders();


    /**
     * 批量保存和更新订单信息
     * @param infos
     */
    void batchSaveAlibabaOrders(AlibabaOpenplatformTradeModelTradeInfo[] infos);

    /**
     * 保存查询到的物流信息
     * @param logisticsOrders
     * @param orderId
     */
    void batchSaveLogisticsInfos(AlibabaLogisticsOpenPlatformLogisticsOrder[] logisticsOrders, String orderId);

    /**
     * 更具订单号查询运单号
     * @param orderId
     * @return
     */
    AlibabaTradeGetLogisticsInfosBuyerViewResult queryTrackInfoByOrderId(Long orderId);

    /**
     * 更具订单号查询运单号
     * @param orderId
     * @return
     */
    AlibabaTradeGetLogisticsTraceInfoBuyerViewResult queryLogisticsTraceInfo(Long orderId);

    /**
     * 根据更新时间开始和更新时间结束查询订单列表
     * @param startDate
     * @param endDate
     * @param page
     * @return
     */
    AlibabaTradeGetBuyerOrderListResult queryAlibbaOrdersByModifyTime(Date startDate, Date endDate, int page);

    /**
     * 保存物流跟踪信息到mongodb
     * @param logisticsTrace
     * @param orderIdStr
     */
    void batchSaveLogisticsTraceInfos(AlibabaLogisticsOpenPlatformLogisticsTrace[] logisticsTrace, String orderIdStr);
}
