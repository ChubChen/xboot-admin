package com.chubchen.admin.modules.alibaba.handle;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewResult;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsTraceInfoBuyerViewResult;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.chubchen.admin.common.constant.PurchaseConstant;
import com.chubchen.admin.modules.alibaba.constants.AlibabaOrderStatusEnum;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaQueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *@author chubchen
 */
@Slf4j
@Component
public class ComfirmReciveGoodsHandle implements AlibabaEventHandle {
    @Autowired
    private AlibabaQueryService alibabaQueryService;

//    @Autowired
//    private IPurchaseOrdersService purchaseOrdersService;
    @Override
    public boolean handle(JSONObject data) {
        log.info("开始处理{}", data);
        Long orderId = data.getLong("orderId");
        String currentStatus = data.getString("currentStatus");
        if(AlibabaOrderStatusEnum.confirm_goods_and_has_subsidy.getCode().equals(currentStatus)){
            //获得订单Id后查询物流信息
            String orderIdStr = String.valueOf(orderId);
            AlibabaTradeGetLogisticsTraceInfoBuyerViewResult result = alibabaQueryService.queryLogisticsTraceInfo(orderId);
            if (result.getSuccess() && result.getLogisticsTrace().length >0 ) {
                log.info("开始根据查询到的物流信息进行保存与更新订单号");
                alibabaQueryService.batchSaveLogisticsTraceInfos(result.getLogisticsTrace(), orderIdStr);
//                purchaseOrdersService.update(new UpdateWrapper<PurchaseOrders>().
//                        set("order_status", PurchaseConstant.PURCHASE_ORDER_STATUS_FINISH)
//                        .eq("order_number", orderIdStr));
            }
        }else{
            log.info("接收到发货通知，但是状态不是waitbuyerreceive, 数据:{}", data);
        }
        return true;
    }
}
