package com.chubchen.admin.modules.alibaba.handle;

import com.alibaba.fastjson.JSONObject;
import com.alibaba.logistics.param.AlibabaTradeGetLogisticsInfosBuyerViewResult;
import com.chubchen.admin.common.constant.PurchaseConstant;
import com.chubchen.admin.modules.alibaba.constants.AlibabaOrderStatusEnum;
import com.chubchen.admin.modules.alibaba.serivce.AlibabaQueryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author chubchen
 */
@Component
@Slf4j
public class SendGoodsHandle implements AlibabaEventHandle {

    @Autowired
    private AlibabaQueryService alibabaQueryService;

    @Override
    public boolean handle(JSONObject data) {
        log.info("开始处理{}", data);
        Long orderId = data.getLong("orderId");
        String currentStatus = data.getString("currentStatus");
        if(AlibabaOrderStatusEnum.waitbuyerreceive.getCode().equals(currentStatus)){
            //获得订单Id后查询物流信息
            String orderIdStr = String.valueOf(orderId);
            AlibabaTradeGetLogisticsInfosBuyerViewResult result = alibabaQueryService.queryTrackInfoByOrderId(orderId);
            if (result.getSuccess() && result.getResult().length >0 ) {
                log.info("开始根据查询到的物流信息进行保存与更新订单号");
                alibabaQueryService.batchSaveLogisticsInfos(result.getResult(), orderIdStr);
                String logisticsTrackNumber = result.getResult()[0].getLogisticsBillNo();
                //purchaseOrdersService.updateTrackNumberAndStatusByOrderNumber(logisticsTrackNumber,
                //        PurchaseConstant.PURCHASE_ORDER_STATUS_SEND, orderIdStr);
            }
        }else{
            log.info("接收到发货通知，但是状态不是waitbuyerreceive, 数据:{}", data);
        }
        return true;
    }
}
